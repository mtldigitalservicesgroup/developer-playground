/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
         dom: [
         {
            id:'gradatedBackground',
            type:'image',
            rect:[-182,666,2048,1536],
            fill:["rgba(0,0,0,0)",im+"gradatedBackground.png"]
         },
         {
            id:'backgroundTexture',
            type:'image',
            rect:[372,386,1024,736],
            fill:["rgba(0,0,0,0)",im+"backgroundTexture.svg"],
            transform:[[140,14],[],[],[1.999,2.084]]
         },
         {
            id:'dataholder',
            type:'rect',
            rect:[-13,1140,235,52],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"],
            transform:[[-244,14]]
         },
         {
            id:'tweeningHolder',
            type:'rect',
            rect:[-13,1140,235,52],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"],
            transform:[[-244,14]]
         },
         {
            id:'teen12',
            type:'image',
            rect:[530,146,403,314],
            fill:["rgba(0,0,0,0)",im+"teen1.png"],
            transform:[[-15,105],[],[],[0.52,0.52]]
         },
         {
            id:'teen22',
            type:'image',
            rect:[194,133,530,509],
            fill:["rgba(0,0,0,0)",im+"teen2.png"],
            transform:[[405,-265],[40],[],[0.62,0.62]]
         },
         {
            id:'teen32',
            type:'image',
            rect:[156,171,638,497],
            fill:["rgba(0,0,0,0)",im+"teen3.png"],
            transform:[[420,253],[],[],[0.41,0.41]]
         },
         {
            id:'teen42',
            type:'image',
            rect:[316,240,452,362],
            fill:["rgba(0,0,0,0)",im+"teen4.png"],
            transform:[[309,-31],[-22],[],[0.7,0.7]]
         },
         {
            id:'teen5-1',
            type:'image',
            rect:[171,212,644,606],
            fill:["rgba(0,0,0,0)",im+"teen5-1.png"],
            transform:[[-102,-417],[],[],[0.5,0.5]]
         },
         {
            id:'teen62',
            type:'image',
            rect:[192,348,572,476],
            fill:["rgba(0,0,0,0)",im+"teen6.png"],
            transform:[[-68,77],[],[],[0.5,0.5]]
         },
         {
            id:'teen7-1',
            type:'image',
            rect:[89,361,543,447],
            fill:["rgba(0,0,0,0)",im+"teen7-1.png"],
            transform:[[-263,-35],[],[],[0.5,0.5]]
         },
         {
            id:'teen82',
            type:'image',
            rect:[186,314,460,397],
            fill:["rgba(0,0,0,0)",im+"teen8.png"],
            transform:[[-334,-258],[],[],[0.51,0.51]]
         },
         {
            id:'tweeter3a',
            type:'rect',
            rect:[229,1139,0,0],
            transform:[[-243,-662],[],[],[0.5,0.5]]
         },
         {
            id:'tweeter1a',
            type:'rect',
            rect:[22,592,0,0],
            transform:[[-167,-426],[],[],[0.5,0.5]]
         },
         {
            id:'tweeter2a',
            type:'rect',
            rect:[858,-85,0,0],
            transform:[[-634,-125],[],[],[0.5,0.5]]
         },
         {
            id:'tweeter4a',
            type:'rect',
            rect:[1291,849,0,0],
            transform:[[-765,-519],[],[],[0.5,0.5]]
         },
         {
            id:'tweeter5a',
            type:'rect',
            rect:[690,988,0,0],
            transform:[[-646,-952]]
         },
         {
            id:'tweeter6a',
            type:'rect',
            rect:[1482,519,0,0],
            transform:[[-867,-350],[],[],[0.5,0.5]]
         },
         {
            id:'tweeter7a',
            type:'rect',
            rect:[529,456,0,0],
            transform:[[-349,-100]]
         },
         {
            id:'tweeter8a',
            type:'rect',
            rect:[145,212,0,0],
            transform:[[-196,-269],[],[],[0.5,0.5]]
         },
         {
            id:'tweeterTargets',
            type:'rect',
            rect:[-79,-102,0,0],
            transform:[[-516,-380],[],[],[0.5,0.5]]
         },
         {
            id:'greycontinueButton',
            type:'image',
            rect:[-173,251,215,65],
            fill:["rgba(0,0,0,0)",im+"greycontinueButton.png"],
            transform:[[1962,1101]]
         }],
         symbolInstances: [
         {
            id:'tweeter6a',
            symbolName:'tweeter6a'
         },
         {
            id:'tweeter8a',
            symbolName:'tweeter8a'
         },
         {
            id:'tweeterTargets',
            symbolName:'tweeterTargets'
         },
         {
            id:'tweeter4a',
            symbolName:'tweeter4a'
         },
         {
            id:'tweeter1a',
            symbolName:'Tweeter1a'
         },
         {
            id:'tweeter5a',
            symbolName:'tweeter5a'
         },
         {
            id:'tweeter3a',
            symbolName:'tweeter3a'
         },
         {
            id:'tweeter2a',
            symbolName:'tweeter2a'
         },
         {
            id:'tweeter7a',
            symbolName:'tweeter7a'
         }
         ]
      },
   states: {
      "Base State": {
         "${_tweeter2a}": [
            ["transform", "translateX", '-634.53px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-125.98px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_teen5-1}": [
            ["transform", "scaleX", '0.5'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateX", '-102px'],
            ["transform", "translateY", '-417.19px']
         ],
         "${_tweeterTargets}": [
            ["transform", "translateX", '-516.02px'],
            ["style", "display", 'block'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateY", '-380.12px'],
            ["transform", "scaleX", '0.5']
         ],
         "${_gradatedBackground}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '-665.99px'],
            ["transform", "translateX", '181.99px']
         ],
         "body": [
            ["color", "background-color", 'rgba(0,0,0,0)']
         ],
         "${_teen22}": [
            ["transform", "translateX", '405.06px'],
            ["transform", "rotateZ", '40deg'],
            ["transform", "scaleX", '0.62'],
            ["transform", "translateY", '-265.96px'],
            ["transform", "scaleY", '0.62']
         ],
         "${_teen7-1}": [
            ["transform", "scaleX", '0.5'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateX", '-263.54px'],
            ["transform", "translateY", '-35.53px']
         ],
         "${_tweeter6a}": [
            ["transform", "translateX", '-867.22px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-350.45px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_dataholder}": [
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateX", '-244.65px'],
            ["transform", "translateY", '14.32px']
         ],
         "${_tweeter3a}": [
            ["transform", "translateX", '-243.66px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-662.54px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_tweeningHolder}": [
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateX", '-244.65px'],
            ["transform", "translateY", '14.32px']
         ],
         "${_teen42}": [
            ["transform", "translateX", '309.61px'],
            ["transform", "rotateZ", '-22deg'],
            ["transform", "scaleX", '0.7'],
            ["transform", "translateY", '-31.54px'],
            ["transform", "scaleY", '0.7']
         ],
         "${_Stage}": [
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden'],
            ["style", "height", '768px'],
            ["style", "width", '1024px']
         ],
         "${_teen12}": [
            ["transform", "scaleX", '0.52'],
            ["transform", "translateY", '105.02px'],
            ["transform", "translateX", '-15.54px'],
            ["transform", "scaleY", '0.52']
         ],
         "${_tweeter7a}": [
            ["transform", "translateX", '-349.74px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-100.82px'],
            ["transform", "scaleY", '1']
         ],
         "${_teen62}": [
            ["transform", "scaleX", '0.5'],
            ["transform", "translateY", '77.48px'],
            ["transform", "translateX", '-68.56px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_tweeter5a}": [
            ["transform", "translateX", '-646.12px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '1'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-952.99px'],
            ["transform", "scaleY", '1']
         ],
         "${_teen32}": [
            ["transform", "scaleX", '0.41'],
            ["transform", "scaleY", '0.41'],
            ["transform", "translateX", '420.62px'],
            ["transform", "translateY", '253.31px']
         ],
         "${_teen82}": [
            ["transform", "scaleX", '0.51'],
            ["transform", "scaleY", '0.51'],
            ["transform", "translateX", '-334.91px'],
            ["transform", "translateY", '-258.68px']
         ],
         "${_tweeter8a}": [
            ["transform", "translateX", '-196.81px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-269.79px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_tweeter1a}": [
            ["transform", "translateX", '-167.86px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-426.35px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_tweeter4a}": [
            ["transform", "translateX", '-765.57px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-519.48px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_backgroundTexture}": [
            ["transform", "translateX", '140.65px'],
            ["style", "display", 'block'],
            ["transform", "scaleX", '1.99938'],
            ["transform", "translateY", '14.96px'],
            ["transform", "scaleY", '2.08475']
         ],
         "${_greycontinueButton}": [
            ["transform", "translateX", '1962.99px'],
            ["transform", "translateY", '1101.3px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid23395", tween: [ "style", "${_backgroundTexture}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23384", tween: [ "style", "${_dataholder}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23396", tween: [ "style", "${_gradatedBackground}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23385", tween: [ "style", "${_tweeterTargets}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23383", tween: [ "style", "${_tweeningHolder}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" }         ]
      }
   }
},
"tweeterTargets": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[58,-61],[-9],{},[0.691]],
      type: 'rect',
      rect: [125,370,860,409],
      id: 'tweeter8target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[-89,404],[14],{},[0.691]],
      type: 'rect',
      rect: [125,370,754,350],
      id: 'tweeter1Target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[434,323],[-20],{},[0.691]],
      type: 'rect',
      rect: [125,370,864,412],
      id: 'tweeter7target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[97,888],[-14],{},[0.691]],
      type: 'rect',
      rect: [125,370,692,330],
      id: 'tweeter3target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[578,858],[19],{},[0.691]],
      type: 'rect',
      rect: [125,370,971,489],
      id: 'tweeter5target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[753,-245],[16],{},[0.691]],
      type: 'rect',
      rect: [125,370,906,444],
      id: 'tweeter2target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[1352,280],[1],{},[0.691]],
      type: 'rect',
      rect: [125,370,682,324],
      id: 'tweeter6target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[1222,636],[-13],{},[0.691]],
      type: 'rect',
      rect: [125,370,526,272],
      id: 'tweeter4target',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      transform: [[-4,-35],{},{},[0.6,0.6]],
      rect: [1719,541,325,175],
      type: 'rect',
      id: 'movieCloseButton',
      stroke: [0,'rgba(0,0,0,1)','none'],
      opacity: 0,
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_tweeter1Target}": [
            ["transform", "translateX", '-89.02px'],
            ["transform", "rotateZ", '14deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '754px'],
            ["style", "height", '350px'],
            ["transform", "translateY", '404.64px'],
            ["style", "display", 'block']
         ],
         "${_tweeter4target}": [
            ["transform", "translateX", '1222.2px'],
            ["transform", "rotateZ", '-13deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '526px'],
            ["style", "height", '272px'],
            ["transform", "translateY", '636.25px'],
            ["style", "display", 'block']
         ],
         "${_tweeter2target}": [
            ["transform", "translateX", '753.09px'],
            ["transform", "rotateZ", '16deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '906px'],
            ["style", "height", '444px'],
            ["transform", "translateY", '-245.25px'],
            ["style", "display", 'block']
         ],
         "${_tweeter6target}": [
            ["transform", "translateX", '1352.4px'],
            ["transform", "rotateZ", '1deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '682px'],
            ["style", "height", '324px'],
            ["transform", "translateY", '280.08px'],
            ["style", "display", 'block']
         ],
         "${_tweeter8target}": [
            ["transform", "translateX", '58.22px'],
            ["transform", "rotateZ", '-9deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '860px'],
            ["style", "height", '409px'],
            ["transform", "translateY", '-61.08px'],
            ["style", "display", 'block']
         ],
         "${_tweeter5target}": [
            ["transform", "translateX", '578.73px'],
            ["transform", "rotateZ", '19deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '971px'],
            ["style", "height", '489px'],
            ["transform", "translateY", '858.17px'],
            ["style", "display", 'block']
         ],
         "${_tweeter3target}": [
            ["transform", "translateX", '97.81px'],
            ["transform", "rotateZ", '-14deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '692px'],
            ["style", "height", '330px'],
            ["transform", "translateY", '888.97px'],
            ["style", "display", 'block']
         ],
         "${symbolSelector}": [
            ["style", "height", '1709.725758px'],
            ["style", "width", '2219.644897px']
         ],
         "${_tweeter7target}": [
            ["transform", "translateX", '434.32px'],
            ["transform", "rotateZ", '-20deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '864px'],
            ["style", "height", '412px'],
            ["transform", "translateY", '323.15px'],
            ["style", "display", 'block']
         ],
         "${_movieCloseButton}": [
            ["transform", "scaleY", '0.6'],
            ["style", "height", '175px'],
            ["transform", "translateX", '-4px'],
            ["style", "display", 'block'],
            ["transform", "scaleX", '0.6'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-35.64px'],
            ["style", "width", '325px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
            { id: "eid23388", tween: [ "style", "${_tweeter7target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23391", tween: [ "style", "${_tweeter2target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23387", tween: [ "style", "${_tweeter1Target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23393", tween: [ "style", "${_tweeter4target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23389", tween: [ "style", "${_tweeter3target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23398", tween: [ "style", "${_movieCloseButton}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23386", tween: [ "style", "${_tweeter8target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23392", tween: [ "style", "${_tweeter6target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid23390", tween: [ "style", "${_tweeter5target}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 }         ]
      }
   }
},
"FirstPhrase": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'image',
      id: 'forTeens3',
      rect: [-226,-140,1350,108],
      transform: [[213,162],{},{},[0.925,0.925]],
      fill: ['rgba(0,0,0,0)','images/forTeens.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '160px'],
            ["style", "width", '1315.63999px']
         ],
         "${_forTeens3}": [
            ["transform", "scaleX", '0.92512'],
            ["transform", "translateY", '162.94px'],
            ["transform", "translateX", '213.3px'],
            ["transform", "scaleY", '0.92512']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: false,
         timeline: [
         ]
      }
   }
},
"pageCurl": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'image',
      id: 'purpleWedge',
      rect: [462,1212,2036,1028],
      transform: [[522,-725],{},{},[2,2]],
      fill: ['rgba(0,0,0,0)','images/purpleWedge.svg']
   },
   {
      type: 'image',
      id: 'pageCurl',
      rect: [25,1218,834,966],
      transform: [[-281,121],{},{},[0.255,0.255]],
      fill: ['rgba(0,0,0,0)','images/pageCurl.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_purpleWedge}": [
            ["transform", "scaleX", '2'],
            ["transform", "translateX", '522.99px'],
            ["transform", "scaleY", '2'],
            ["transform", "translateY", '-725px']
         ],
         "${_pageCurl}": [
            ["transform", "scaleX", '0.25552'],
            ["transform", "translateX", '-281.4px'],
            ["transform", "scaleY", '0.25552'],
            ["transform", "translateY", '121.42px']
         ],
         "${symbolSelector}": [
            ["style", "height", '2320px'],
            ["style", "overflow", 'hidden'],
            ["style", "width", '4000px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1250,
         autoPlay: false,
         labels: {
            "curl": 300,
            "complete": 1082
         },
         timeline: [
            { id: "eid23300", tween: [ "transform", "${_pageCurl}", "translateX", '834.72px', { fromValue: '-281.4px'}], position: 327, duration: 726 },
            { id: "eid23298", tween: [ "transform", "${_pageCurl}", "scaleX", '3.2', { fromValue: '0.25552'}], position: 327, duration: 726 },
            { id: "eid23299", tween: [ "transform", "${_pageCurl}", "scaleY", '3.2', { fromValue: '0.25552'}], position: 327, duration: 726 },
            { id: "eid23301", tween: [ "transform", "${_pageCurl}", "translateY", '-1174.49px', { fromValue: '121.42px'}], position: 327, duration: 726 }         ]
      }
   }
},
"Tweeter1a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1237,-131],[14],{},[0.65,0.65]],
      type: 'image',
      id: 'mainTweetPicShadow2',
      display: 'none',
      rect: [1133,82,861,700],
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      type: 'image',
      id: 'largeTweeter1',
      rect: [-103,-244,836,601],
      transform: [[-5,202],[14],{},[0.65,0.65]],
      fill: ['rgba(0,0,0,0)','images/largeTweeter1.png']
   },
   {
      transform: [[-135,-327]],
      type: 'image',
      id: 'purpleTweet1',
      display: 'none',
      rect: [896,83,1200,677],
      fill: ['rgba(0,0,0,0)','images/purpleTweet1.png']
   },
   {
      type: 'rect',
      borderRadius: [10,10,10,10],
      id: 'tweeter1Close',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      rect: [920,562,111,124],
      display: 'none',
      transform: [[633,-506]],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_largeTweeter1}": [
            ["transform", "translateX", '-5.66px'],
            ["transform", "rotateZ", '14deg'],
            ["transform", "scaleX", '0.65'],
            ["transform", "translateY", '202.67px'],
            ["transform", "scaleY", '0.65']
         ],
         "${symbolSelector}": [
            ["style", "height", '509.875364px'],
            ["style", "width", '621.607912px']
         ],
         "${_purpleTweet1}": [
            ["transform", "translateX", '-643.99px'],
            ["style", "display", 'none'],
            ["transform", "scaleX", '0.35'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-327.8px'],
            ["transform", "scaleY", '0.35']
         ],
         "${_mainTweetPicShadow2}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '-1237.26px'],
            ["transform", "rotateZ", '14deg'],
            ["transform", "scaleX", '0.65'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-131.97px'],
            ["transform", "scaleY", '0.65']
         ],
         "${_tweeter1Close}": [
            ["transform", "translateX", '810.5px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-710.19px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 10500,
         autoPlay: true,
         labels: {
            "intro": 0
         },
         timeline: [
            { id: "eid22638", tween: [ "transform", "${_largeTweeter1}", "rotateZ", '-15deg', { fromValue: '14deg'}], position: 51, duration: 319, easing: "swing" },
            { id: "eid22639", tween: [ "transform", "${_largeTweeter1}", "rotateZ", '14deg', { fromValue: '-15deg'}], position: 655, duration: 381, easing: "easeOutQuad" },
            { id: "eid23271", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23304", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 51, duration: 0 },
            { id: "eid23268", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'block'}], position: 10500, duration: 0, easing: "swing" },
            { id: "eid23112", tween: [ "style", "${_purpleTweet1}", "opacity", '1', { fromValue: '0'}], position: 209, duration: 160, easing: "swing" },
            { id: "eid22683", tween: [ "style", "${_purpleTweet1}", "opacity", '0', { fromValue: '1'}], position: 661, duration: 230, easing: "easeOutQuad" },
            { id: "eid22636", tween: [ "transform", "${_largeTweeter1}", "scaleY", '1.01', { fromValue: '0.65'}], position: 51, duration: 319, easing: "swing" },
            { id: "eid22637", tween: [ "transform", "${_largeTweeter1}", "scaleY", '0.65', { fromValue: '1.01'}], position: 655, duration: 381, easing: "easeOutQuad" },
            { id: "eid23120", tween: [ "style", "${_tweeter1Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid23302", tween: [ "style", "${_tweeter1Close}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
            { id: "eid23305", tween: [ "style", "${_tweeter1Close}", "display", 'none', { fromValue: 'block'}], position: 655, duration: 0 },
            { id: "eid22650", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-971.56px', { fromValue: '-1237.26px'}], position: 51, duration: 324, easing: "swing" },
            { id: "eid22651", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1237.26px', { fromValue: '-971.56px'}], position: 659, duration: 389, easing: "easeOutQuad" },
            { id: "eid22634", tween: [ "transform", "${_largeTweeter1}", "scaleX", '1.01', { fromValue: '0.65'}], position: 51, duration: 319, easing: "swing" },
            { id: "eid22635", tween: [ "transform", "${_largeTweeter1}", "scaleX", '0.65', { fromValue: '1.01'}], position: 655, duration: 381, easing: "easeOutQuad" },
            { id: "eid22646", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1.01', { fromValue: '0.65'}], position: 51, duration: 324, easing: "swing" },
            { id: "eid22647", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.65', { fromValue: '1.01'}], position: 659, duration: 389, easing: "easeOutQuad" },
            { id: "eid22642", tween: [ "transform", "${_largeTweeter1}", "translateY", '113.91px', { fromValue: '202.67px'}], position: 51, duration: 319, easing: "swing" },
            { id: "eid22643", tween: [ "transform", "${_largeTweeter1}", "translateY", '202.67px', { fromValue: '113.91px'}], position: 655, duration: 381, easing: "easeOutQuad" },
            { id: "eid22640", tween: [ "transform", "${_largeTweeter1}", "translateX", '238.37px', { fromValue: '-5.66px'}], position: 51, duration: 319, easing: "swing" },
            { id: "eid22641", tween: [ "transform", "${_largeTweeter1}", "translateX", '-5.66px', { fromValue: '238.37px'}], position: 655, duration: 381, easing: "easeOutQuad" },
            { id: "eid22648", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-15deg', { fromValue: '14deg'}], position: 51, duration: 324, easing: "swing" },
            { id: "eid22649", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '14deg', { fromValue: '-15deg'}], position: 659, duration: 389, easing: "easeOutQuad" },
            { id: "eid22678", tween: [ "transform", "${_purpleTweet1}", "translateY", '-326.49px', { fromValue: '-327.8px'}], position: 209, duration: 160, easing: "swing" },
            { id: "eid22679", tween: [ "transform", "${_purpleTweet1}", "translateY", '-327.8px', { fromValue: '-326.49px'}], position: 661, duration: 230, easing: "easeOutQuad" },
            { id: "eid22644", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1.01', { fromValue: '0.65'}], position: 51, duration: 324, easing: "swing" },
            { id: "eid22645", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.65', { fromValue: '1.01'}], position: 659, duration: 389, easing: "easeOutQuad" },
            { id: "eid22676", tween: [ "transform", "${_purpleTweet1}", "translateX", '-114.09px', { fromValue: '-643.99px'}], position: 209, duration: 160, easing: "swing" },
            { id: "eid22677", tween: [ "transform", "${_purpleTweet1}", "translateX", '-643.99px', { fromValue: '-114.09px'}], position: 661, duration: 230, easing: "easeOutQuad" },
            { id: "eid23270", tween: [ "style", "${_purpleTweet1}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23303", tween: [ "style", "${_purpleTweet1}", "display", 'block', { fromValue: 'none'}], position: 209, duration: 0 },
            { id: "eid23269", tween: [ "style", "${_purpleTweet1}", "display", 'none', { fromValue: 'block'}], position: 10500, duration: 0, easing: "swing" },
            { id: "eid22674", tween: [ "transform", "${_purpleTweet1}", "scaleY", '1.03', { fromValue: '0.35'}], position: 209, duration: 160, easing: "swing" },
            { id: "eid22675", tween: [ "transform", "${_purpleTweet1}", "scaleY", '0.35', { fromValue: '1.03'}], position: 661, duration: 230, easing: "easeOutQuad" },
            { id: "eid22672", tween: [ "transform", "${_purpleTweet1}", "scaleX", '1.03', { fromValue: '0.35'}], position: 209, duration: 160, easing: "swing" },
            { id: "eid22673", tween: [ "transform", "${_purpleTweet1}", "scaleX", '0.35', { fromValue: '1.03'}], position: 661, duration: 230, easing: "easeOutQuad" },
            { id: "eid22693", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1041, duration: 144, easing: "easeOutQuad" },
            { id: "eid22652", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-232.34px', { fromValue: '-131.97px'}], position: 51, duration: 324, easing: "swing" },
            { id: "eid22653", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-131.97px', { fromValue: '-232.34px'}], position: 659, duration: 389, easing: "easeOutQuad" }         ]
      }
   }
},
"tweeter8a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'image',
      transform: [[-1217,-131],[-10],{},[0.74,0.74]],
      display: 'none',
      rect: [1133,82,861,700],
      id: 'mainTweetPicShadow2',
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      transform: [[-79,57],[-15]],
      id: 'largeTweeter8',
      type: 'image',
      rect: [79,177,866,625],
      fill: ['rgba(0,0,0,0)','images/largeTweeter8.png']
   },
   {
      rect: [-284,647,1100,700],
      transform: [[928,-519]],
      id: 'blueTweet1',
      opacity: 1,
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/blueTweet1.png']
   },
   {
      rect: [922,111,115,125],
      type: 'rect',
      display: 'none',
      id: 'tweeter8Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_tweeter8Close}": [
            ["transform", "translateX", '664.86px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '117.53px']
         ],
         "${symbolSelector}": [
            ["style", "height", '521.919028px'],
            ["style", "width", '673.625682px']
         ],
         "${_blueTweet1}": [
            ["transform", "scaleX", '0.28'],
            ["transform", "translateX", '415.82px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-704.85px'],
            ["transform", "scaleY", '0.28']
         ],
         "${_mainTweetPicShadow2}": [
            ["transform", "scaleY", '0.74'],
            ["transform", "translateX", '-1217.65px'],
            ["transform", "rotateZ", '-10deg'],
            ["transform", "scaleX", '0.74'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-131.97px'],
            ["style", "display", 'none']
         ],
         "${_largeTweeter8}": [
            ["transform", "scaleY", '0.74'],
            ["transform", "rotateZ", '-7deg'],
            ["transform", "scaleX", '0.74'],
            ["transform", "translateY", '-236px'],
            ["transform", "translateX", '-193px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 14500,
         autoPlay: true,
         labels: {
            "intro": 37,
            "open": 103
         },
         timeline: [
            { id: "eid23320", tween: [ "style", "${_tweeter8Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeOutQuad" },
            { id: "eid23145", tween: [ "style", "${_tweeter8Close}", "display", 'block', { fromValue: 'none'}], position: 407, duration: 0 },
            { id: "eid23319", tween: [ "style", "${_tweeter8Close}", "display", 'none', { fromValue: 'block'}], position: 495, duration: 0, easing: "easeOutQuad" },
            { id: "eid21310", tween: [ "transform", "${_blueTweet1}", "translateX", '928.3px', { fromValue: '415.82px'}], position: 295, duration: 117, easing: "swing" },
            { id: "eid21322", tween: [ "transform", "${_blueTweet1}", "translateX", '416px', { fromValue: '928.3px'}], position: 495, duration: 183, easing: "easeOutQuad" },
            { id: "eid23286", tween: [ "style", "${_blueTweet1}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23357", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23287", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 134, duration: 0, easing: "swing" },
            { id: "eid21213", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid21290", tween: [ "transform", "${_largeTweeter8}", "scaleY", '1', { fromValue: '0.74'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21291", tween: [ "transform", "${_largeTweeter8}", "scaleY", '0.74', { fromValue: '1'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21265", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '143.36px', { fromValue: '-131.97px'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21266", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-131.97px', { fromValue: '143.36px'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21334", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-165px', { fromValue: '-131.97px'}], position: 851, duration: 117, easing: "easeOutQuad" },
            { id: "eid21257", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1', { fromValue: '0.74'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21258", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.74', { fromValue: '1'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21288", tween: [ "transform", "${_largeTweeter8}", "scaleX", '1', { fromValue: '0.74'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21289", tween: [ "transform", "${_largeTweeter8}", "scaleX", '0.74', { fromValue: '1'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21259", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1', { fromValue: '0.74'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21260", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.74', { fromValue: '1'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21311", tween: [ "transform", "${_blueTweet1}", "translateY", '-519.79px', { fromValue: '-704.85px'}], position: 295, duration: 117, easing: "swing" },
            { id: "eid21323", tween: [ "transform", "${_blueTweet1}", "translateY", '-705px', { fromValue: '-519.79px'}], position: 495, duration: 183, easing: "easeOutQuad" },
            { id: "eid21261", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-15deg', { fromValue: '-10deg'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21262", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-10deg', { fromValue: '-15deg'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21284", tween: [ "transform", "${_largeTweeter8}", "translateY", '51.43px', { fromValue: '-236px'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21295", tween: [ "transform", "${_largeTweeter8}", "translateY", '-236.2px', { fromValue: '51.43px'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21335", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 851, duration: 117, easing: "easeOutQuad" },
            { id: "eid21293", tween: [ "transform", "${_largeTweeter8}", "rotateZ", '-15deg', { fromValue: '-7deg'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21294", tween: [ "transform", "${_largeTweeter8}", "rotateZ", '-7deg', { fromValue: '-15deg'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21263", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1094.03px', { fromValue: '-1217.65px'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21264", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1217.65px', { fromValue: '-1094.03px'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21333", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1248px', { fromValue: '-1217.65px'}], position: 851, duration: 117, easing: "easeOutQuad" },
            { id: "eid21282", tween: [ "transform", "${_largeTweeter8}", "translateX", '-93.32px', { fromValue: '-193px'}], position: 134, duration: 273, easing: "swing" },
            { id: "eid21296", tween: [ "transform", "${_largeTweeter8}", "translateX", '-193px', { fromValue: '-93.32px'}], position: 495, duration: 355, easing: "easeOutQuad" },
            { id: "eid21314", tween: [ "transform", "${_blueTweet1}", "scaleY", '1', { fromValue: '0.28'}], position: 295, duration: 117, easing: "swing" },
            { id: "eid21324", tween: [ "transform", "${_blueTweet1}", "scaleY", '0.28', { fromValue: '1'}], position: 495, duration: 183, easing: "easeOutQuad" },
            { id: "eid21315", tween: [ "style", "${_blueTweet1}", "opacity", '1', { fromValue: '0'}], position: 295, duration: 117, easing: "swing" },
            { id: "eid21325", tween: [ "style", "${_blueTweet1}", "opacity", '0', { fromValue: '1'}], position: 495, duration: 183, easing: "easeOutQuad" },
            { id: "eid21313", tween: [ "transform", "${_blueTweet1}", "scaleX", '1', { fromValue: '0.28'}], position: 295, duration: 117, easing: "swing" },
            { id: "eid21326", tween: [ "transform", "${_blueTweet1}", "scaleX", '0.28', { fromValue: '1'}], position: 495, duration: 183, easing: "easeOutQuad" }         ]
      }
   }
},
"tweeter7a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1237,-131]],
      type: 'image',
      id: 'mainTweetPicShadow2',
      display: 'none',
      rect: [1133,82,861,700],
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      transform: [[519,-20],[-21],{},[0.77,0.77]],
      id: 'largeTweeter7',
      type: 'image',
      rect: [-555,54,839,603],
      fill: ['rgba(0,0,0,0)','images/largeTweeter7.png']
   },
   {
      transform: [[550,-767]],
      type: 'image',
      display: 'none',
      id: 'blueTweet2',
      opacity: 1,
      rect: [-284,647,1100,700],
      fill: ['rgba(0,0,0,0)','images/blueTweet2.png']
   },
   {
      type: 'rect',
      rect: [922,111,115,125],
      display: 'none',
      id: 'tweeter7Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_blueTweet2}": [
            ["transform", "scaleY", '0.1'],
            ["transform", "translateX", '153.08px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.1'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-877.93px'],
            ["style", "display", 'none']
         ],
         "${_largeTweeter7}": [
            ["transform", "scaleY", '0.45'],
            ["transform", "rotateZ", '15deg'],
            ["transform", "scaleX", '0.45'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-20.72px'],
            ["transform", "translateX", '519.32px']
         ],
         "${symbolSelector}": [
            ["style", "height", '663.992148px'],
            ["style", "width", '768.519852px']
         ],
         "${_mainTweetPicShadow2}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '-1170.69px'],
            ["transform", "rotateZ", '15deg'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-72.98px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_tweeter7Close}": [
            ["transform", "translateX", '273.25px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-118.25px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1591,
         autoPlay: true,
         labels: {
            "intro": 29,
            "open": 128
         },
         timeline: [
            { id: "eid21606", tween: [ "transform", "${_blueTweet2}", "translateX", '220.68px', { fromValue: '153.08px'}], position: 360, duration: 176, easing: "swing" },
            { id: "eid23437", tween: [ "transform", "${_blueTweet2}", "translateX", '153px', { fromValue: '220.68px'}], position: 619, duration: 166, easing: "swing" },
            { id: "eid21602", tween: [ "transform", "${_blueTweet2}", "scaleX", '0.5', { fromValue: '0.1'}], position: 360, duration: 176, easing: "swing" },
            { id: "eid23441", tween: [ "transform", "${_blueTweet2}", "scaleX", '0.1', { fromValue: '0.5'}], position: 619, duration: 166, easing: "swing" },
            { id: "eid21452", tween: [ "transform", "${_largeTweeter7}", "translateY", '-325.84px', { fromValue: '-20.72px'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23444", tween: [ "transform", "${_largeTweeter7}", "translateY", '-21px', { fromValue: '-325.84px'}], position: 619, duration: 381, easing: "swing" },
            { id: "eid23358", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23284", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0, easing: "easeInQuad" },
            { id: "eid23453", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0, easing: "swing" },
            { id: "eid21603", tween: [ "transform", "${_blueTweet2}", "scaleY", '0.5', { fromValue: '0.1'}], position: 360, duration: 176, easing: "swing" },
            { id: "eid23442", tween: [ "transform", "${_blueTweet2}", "scaleY", '0.1', { fromValue: '0.5'}], position: 619, duration: 166, easing: "swing" },
            { id: "eid21458", tween: [ "transform", "${_largeTweeter7}", "scaleY", '0.5', { fromValue: '0.45'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23446", tween: [ "transform", "${_largeTweeter7}", "scaleY", '0.45', { fromValue: '0.5'}], position: 619, duration: 381, easing: "swing" },
            { id: "eid21551", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-377.98px', { fromValue: '-72.98px'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23449", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-73px', { fromValue: '-377.98px'}], position: 619, duration: 221, easing: "swing" },
            { id: "eid23455", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-81px', { fromValue: '-73px'}], position: 841, duration: 69, easing: "swing" },
            { id: "eid21547", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1450.16px', { fromValue: '-1170.69px'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23448", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1171px', { fromValue: '-1450.16px'}], position: 619, duration: 221, easing: "swing" },
            { id: "eid23454", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1186px', { fromValue: '-1171px'}], position: 841, duration: 69, easing: "swing" },
            { id: "eid21456", tween: [ "transform", "${_largeTweeter7}", "scaleX", '0.5', { fromValue: '0.45'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23445", tween: [ "transform", "${_largeTweeter7}", "scaleX", '0.45', { fromValue: '0.5'}], position: 619, duration: 381, easing: "swing" },
            { id: "eid23318", tween: [ "style", "${_tweeter7Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeOutQuad" },
            { id: "eid23140", tween: [ "style", "${_tweeter7Close}", "display", 'block', { fromValue: 'none'}], position: 448, duration: 0 },
            { id: "eid23317", tween: [ "style", "${_tweeter7Close}", "display", 'none', { fromValue: 'block'}], position: 728, duration: 0, easing: "easeOutQuad" },
            { id: "eid21549", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-15deg', { fromValue: '15deg'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23450", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '15deg', { fromValue: '-15deg'}], position: 619, duration: 221, easing: "swing" },
            { id: "eid21184", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid21450", tween: [ "transform", "${_largeTweeter7}", "translateX", '227.45px', { fromValue: '519.32px'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23443", tween: [ "transform", "${_largeTweeter7}", "translateX", '519px', { fromValue: '227.45px'}], position: 619, duration: 381, easing: "swing" },
            { id: "eid21454", tween: [ "transform", "${_largeTweeter7}", "rotateZ", '-13deg', { fromValue: '15deg'}], position: 250, duration: 286, easing: "swing" },
            { id: "eid23447", tween: [ "transform", "${_largeTweeter7}", "rotateZ", '15deg', { fromValue: '-13deg'}], position: 619, duration: 381, easing: "swing" },
            { id: "eid21607", tween: [ "transform", "${_blueTweet2}", "translateY", '-1000.66px', { fromValue: '-877.93px'}], position: 360, duration: 176, easing: "swing" },
            { id: "eid23439", tween: [ "transform", "${_blueTweet2}", "translateY", '-878px', { fromValue: '-1000.66px'}], position: 619, duration: 166, easing: "swing" },
            { id: "eid23458", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 911, duration: 53, easing: "swing" },
            { id: "eid23283", tween: [ "style", "${_blueTweet2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "easeInQuad" },
            { id: "eid23422", tween: [ "style", "${_blueTweet2}", "display", 'block', { fromValue: 'none'}], position: 360, duration: 0, easing: "swing" },
            { id: "eid23459", tween: [ "style", "${_blueTweet2}", "display", 'none', { fromValue: 'block'}], position: 811, duration: 0, easing: "swing" }         ]
      }
   }
},
"tweeter6a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1237,-131]],
      type: 'image',
      id: 'mainTweetPicShadow2',
      display: 'none',
      rect: [1133,82,861,700],
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      type: 'image',
      id: 'largeTweeter6',
      rect: [-1528,142,833,595],
      transform: [[1369,-254],{},{},[0.62,0.62]],
      fill: ['rgba(0,0,0,0)','images/largeTweeter6.png']
   },
   {
      transform: [[-405,-828]],
      type: 'image',
      display: 'none',
      id: 'blueTweet3',
      opacity: 1,
      rect: [-284,647,1100,700],
      fill: ['rgba(0,0,0,0)','images/blueTweet3.png']
   },
   {
      type: 'rect',
      rect: [922,111,115,125],
      display: 'none',
      id: 'tweeter6Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${symbolSelector}": [
            ["style", "height", '368.28px'],
            ["style", "width", '515.84px']
         ],
         "${_tweeter6Close}": [
            ["transform", "translateX", '-672.18px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-181.27px']
         ],
         "${_mainTweetPicShadow2}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '-1280.39px'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '0.62'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-221.99px'],
            ["transform", "scaleY", '0.62']
         ],
         "${_blueTweet3}": [
            ["transform", "scaleX", '0.3'],
            ["transform", "translateX", '-236.75px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-812.48px'],
            ["transform", "scaleY", '0.3']
         ],
         "${_largeTweeter6}": [
            ["transform", "scaleY", '0.62'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '0.62'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-254.1px'],
            ["transform", "translateX", '1369.29px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 14500,
         autoPlay: true,
         labels: {
            "intro": 24,
            "open": 133
         },
         timeline: [
            { id: "eid21751", tween: [ "transform", "${_blueTweet3}", "translateX", '-405.48px', { fromValue: '-236.75px'}], position: 378, duration: 241, easing: "swing" },
            { id: "eid21759", tween: [ "transform", "${_blueTweet3}", "translateX", '-237px', { fromValue: '-405.48px'}], position: 1000, duration: 241, easing: "easeOutQuad" },
            { id: "eid21750", tween: [ "transform", "${_blueTweet3}", "scaleY", '1', { fromValue: '0.3'}], position: 378, duration: 241, easing: "swing" },
            { id: "eid21762", tween: [ "transform", "${_blueTweet3}", "scaleY", '0.3', { fromValue: '1'}], position: 1000, duration: 241, easing: "easeOutQuad" },
            { id: "eid23355", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23281", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 169, duration: 0, easing: "swing" },
            { id: "eid21183", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid21732", tween: [ "transform", "${_largeTweeter6}", "scaleY", '1', { fromValue: '0.62'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21733", tween: [ "transform", "${_largeTweeter6}", "scaleY", '0.62', { fromValue: '1'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21742", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-159.69px', { fromValue: '-221.99px'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21743", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-221.99px', { fromValue: '-159.69px'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21771", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-245px', { fromValue: '-221.99px'}], position: 1469, duration: 160, easing: "easeOutQuad" },
            { id: "eid21734", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1', { fromValue: '0.62'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21735", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.62', { fromValue: '1'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21728", tween: [ "transform", "${_largeTweeter6}", "rotateZ", '-14deg', { fromValue: '0deg'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21729", tween: [ "transform", "${_largeTweeter6}", "rotateZ", '0deg', { fromValue: '-14deg'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21754", tween: [ "style", "${_blueTweet3}", "opacity", '1', { fromValue: '0'}], position: 378, duration: 241, easing: "swing" },
            { id: "eid21764", tween: [ "style", "${_blueTweet3}", "opacity", '0', { fromValue: '1'}], position: 1000, duration: 241, easing: "easeOutQuad" },
            { id: "eid21726", tween: [ "transform", "${_largeTweeter6}", "translateY", '-198.74px', { fromValue: '-254.1px'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21727", tween: [ "transform", "${_largeTweeter6}", "translateY", '-254.1px', { fromValue: '-198.74px'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21736", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1', { fromValue: '0.62'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21737", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.62', { fromValue: '1'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21773", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1469, duration: 160, easing: "easeOutQuad" },
            { id: "eid21752", tween: [ "transform", "${_blueTweet3}", "translateY", '-828.98px', { fromValue: '-812.48px'}], position: 378, duration: 241, easing: "swing" },
            { id: "eid21760", tween: [ "transform", "${_blueTweet3}", "translateY", '-813px', { fromValue: '-828.98px'}], position: 1000, duration: 241, easing: "easeOutQuad" },
            { id: "eid21738", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-14deg', { fromValue: '0deg'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21739", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '0deg', { fromValue: '-14deg'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21730", tween: [ "transform", "${_largeTweeter6}", "scaleX", '1', { fromValue: '0.62'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21731", tween: [ "transform", "${_largeTweeter6}", "scaleX", '0.62', { fromValue: '1'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21740", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-2440.28px', { fromValue: '-1280.39px'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21741", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1280.39px', { fromValue: '-2440.28px'}], position: 1000, duration: 469, easing: "easeOutQuad" },
            { id: "eid21770", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1308px', { fromValue: '-1280.39px'}], position: 1469, duration: 160, easing: "easeOutQuad" },
            { id: "eid23139", tween: [ "transform", "${_tweeter6Close}", "translateY", '-181.27px', { fromValue: '-181.27px'}], position: 836, duration: 0 },
            { id: "eid23135", tween: [ "style", "${_tweeter6Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid23314", tween: [ "style", "${_tweeter6Close}", "display", 'block', { fromValue: 'none'}], position: 836, duration: 0, easing: "easeOutQuad" },
            { id: "eid23316", tween: [ "style", "${_tweeter6Close}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0, easing: "easeOutQuad" },
            { id: "eid23356", tween: [ "style", "${_blueTweet3}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23282", tween: [ "style", "${_blueTweet3}", "display", 'block', { fromValue: 'none'}], position: 378, duration: 0, easing: "swing" },
            { id: "eid23138", tween: [ "transform", "${_tweeter6Close}", "translateX", '-672.18px', { fromValue: '-672.18px'}], position: 836, duration: 0 },
            { id: "eid21749", tween: [ "transform", "${_blueTweet3}", "scaleX", '1', { fromValue: '0.3'}], position: 378, duration: 241, easing: "swing" },
            { id: "eid21761", tween: [ "transform", "${_blueTweet3}", "scaleX", '0.3', { fromValue: '1'}], position: 1000, duration: 241, easing: "easeOutQuad" },
            { id: "eid21724", tween: [ "transform", "${_largeTweeter6}", "translateX", '193.29px', { fromValue: '1369.29px'}], position: 169, duration: 450, easing: "swing" },
            { id: "eid21725", tween: [ "transform", "${_largeTweeter6}", "translateX", '1369.29px', { fromValue: '193.29px'}], position: 1000, duration: 469, easing: "easeOutQuad" }         ]
      }
   }
},
"tweeter5a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1237,-131],{},{},[0.38,0.38]],
      rect: [1133,82,861,700],
      display: 'none',
      id: 'mainTweetPicShadow2',
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      rect: [-706,-415,832,595],
      id: 'largeTweeter5',
      transform: [[700,474],[21],{},[0.83,0.83]],
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/largeTweeter5.png']
   },
   {
      transform: [[390,-1295]],
      rect: [-284,647,1100,700],
      display: 'none',
      id: 'purpleTweet2',
      opacity: 1,
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/purpleTweet2.png']
   },
   {
      type: 'rect',
      rect: [922,111,115,125],
      display: 'none',
      id: 'tweeter5Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_largeTweeter5}": [
            ["transform", "translateX", '705.48px'],
            ["transform", "rotateZ", '-21deg'],
            ["transform", "scaleX", '0.4'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '481.91px'],
            ["transform", "scaleY", '0.4']
         ],
         "${symbolSelector}": [
            ["style", "height", '707.748208px'],
            ["style", "width", '821.375834px']
         ],
         "${_purpleTweet2}": [
            ["transform", "scaleY", '0.1'],
            ["transform", "translateX", '253px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.1'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-736px'],
            ["style", "display", 'none']
         ],
         "${_mainTweetPicShadow2}": [
            ["transform", "scaleY", '0.38'],
            ["transform", "translateX", '-1123.18px'],
            ["transform", "rotateZ", '-21deg'],
            ["transform", "scaleX", '0.38'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-54.94px'],
            ["style", "display", 'none']
         ],
         "${_tweeter5Close}": [
            ["transform", "translateX", '97.91px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-638.05px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 14500,
         autoPlay: true,
         labels: {
            "intro": 22,
            "open": 80
         },
         timeline: [
            { id: "eid22025", tween: [ "transform", "${_purpleTweet2}", "translateX", '352.83px', { fromValue: '253px'}], position: 339, duration: 233, easing: "swing" },
            { id: "eid23493", tween: [ "transform", "${_purpleTweet2}", "translateX", '253px', { fromValue: '352.83px'}], position: 1128, duration: 274, easing: "easeOutQuad" },
            { id: "eid23359", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23280", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 166, duration: 0, easing: "swing" },
            { id: "eid23478", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'block'}], position: 1128, duration: 0, easing: "easeOutQuad" },
            { id: "eid21154", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid21984", tween: [ "transform", "${_largeTweeter5}", "scaleY", '0.5', { fromValue: '0.4'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23499", tween: [ "transform", "${_largeTweeter5}", "scaleY", '0.4', { fromValue: '0.5'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid21974", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-75.85px', { fromValue: '-54.94px'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23503", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-55px', { fromValue: '-75.85px'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid23509", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-63px', { fromValue: '-55px'}], position: 1574, duration: 103, easing: "easeOutQuad" },
            { id: "eid21972", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1310.71px', { fromValue: '-1123.18px'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23502", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1123px', { fromValue: '-1310.71px'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid23508", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1139px', { fromValue: '-1123px'}], position: 1574, duration: 103, easing: "easeOutQuad" },
            { id: "eid21982", tween: [ "transform", "${_largeTweeter5}", "scaleX", '0.5', { fromValue: '0.4'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23498", tween: [ "transform", "${_largeTweeter5}", "scaleX", '0.4', { fromValue: '0.5'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid21978", tween: [ "transform", "${_largeTweeter5}", "translateY", '464.12px', { fromValue: '481.91px'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23501", tween: [ "transform", "${_largeTweeter5}", "translateY", '482px', { fromValue: '464.12px'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid22016", tween: [ "transform", "${_purpleTweet2}", "translateY", '-680.44px', { fromValue: '-736px'}], position: 339, duration: 233, easing: "swing" },
            { id: "eid23494", tween: [ "transform", "${_purpleTweet2}", "translateY", '-736px', { fromValue: '-680.44px'}], position: 1128, duration: 274, easing: "easeOutQuad" },
            { id: "eid21892", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '1', { fromValue: '1'}], position: 166, duration: 0 },
            { id: "eid23510", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1574, duration: 103, easing: "easeOutQuad" },
            { id: "eid21980", tween: [ "transform", "${_largeTweeter5}", "rotateZ", '-14deg', { fromValue: '-21deg'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23497", tween: [ "transform", "${_largeTweeter5}", "rotateZ", '-21deg', { fromValue: '-14deg'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid22023", tween: [ "transform", "${_purpleTweet2}", "scaleX", '0.5', { fromValue: '0.1'}], position: 339, duration: 233, easing: "swing" },
            { id: "eid23495", tween: [ "transform", "${_purpleTweet2}", "scaleX", '0.1', { fromValue: '0.5'}], position: 1128, duration: 274, easing: "easeOutQuad" },
            { id: "eid21986", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-14deg', { fromValue: '-21deg'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23504", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-21deg', { fromValue: '-14deg'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid23132", tween: [ "style", "${_tweeter5Close}", "display", 'block', { fromValue: 'none'}], position: 750, duration: 0 },
            { id: "eid23313", tween: [ "style", "${_tweeter5Close}", "display", 'none', { fromValue: 'block'}], position: 1051, duration: 0 },
            { id: "eid22024", tween: [ "transform", "${_purpleTweet2}", "scaleY", '0.5', { fromValue: '0.1'}], position: 339, duration: 233, easing: "swing" },
            { id: "eid23496", tween: [ "transform", "${_purpleTweet2}", "scaleY", '0.1', { fromValue: '0.5'}], position: 1128, duration: 274, easing: "easeOutQuad" },
            { id: "eid21976", tween: [ "transform", "${_largeTweeter5}", "translateX", '521.88px', { fromValue: '705.48px'}], position: 166, duration: 409, easing: "swing" },
            { id: "eid23500", tween: [ "transform", "${_largeTweeter5}", "translateX", '705px', { fromValue: '521.88px'}], position: 1128, duration: 446, easing: "easeOutQuad" },
            { id: "eid23279", tween: [ "style", "${_purpleTweet2}", "display", 'block', { fromValue: 'none'}], position: 293, duration: 0, easing: "swing" },
            { id: "eid23492", tween: [ "style", "${_purpleTweet2}", "display", 'none', { fromValue: 'block'}], position: 1443, duration: 0, easing: "easeOutQuad" }         ]
      }
   }
},
"tweeter4a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1237,-131]],
      type: 'image',
      id: 'mainTweetPicShadow2',
      display: 'none',
      rect: [1133,82,861,700],
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      type: 'image',
      id: 'largeTweeter4',
      rect: [-1187,107,835,597],
      transform: [[1005,-212],[-14],{},[0.49,0.49]],
      fill: ['rgba(0,0,0,0)','images/largeTweeter4.png']
   },
   {
      transform: [[-222,-1146]],
      rect: [-284,647,1100,700],
      id: 'purpleTweet3',
      opacity: 1,
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/purpleTweet3.png']
   },
   {
      type: 'rect',
      rect: [922,111,115,125],
      display: 'none',
      id: 'tweeter4Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_purpleTweet3}": [
            ["transform", "scaleX", '0.35'],
            ["transform", "translateX", '-127px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "display", 'block'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-1070px'],
            ["transform", "scaleY", '0.35']
         ],
         "${_largeTweeter4}": [
            ["transform", "translateX", '1005.95px'],
            ["transform", "rotateZ", '-14deg'],
            ["transform", "scaleX", '0.49'],
            ["transform", "translateY", '-212.72px'],
            ["transform", "scaleY", '0.49']
         ],
         "${symbolSelector}": [
            ["style", "height", '382.229248px'],
            ["style", "width", '467.172162px']
         ],
         "${_mainTweetPicShadow2}": [
            ["transform", "scaleX", '0.49'],
            ["transform", "translateX", '-1310px'],
            ["transform", "rotateZ", '-14deg'],
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-215px'],
            ["transform", "scaleY", '0.49']
         ],
         "${_tweeter4Close}": [
            ["transform", "translateX", '-503.72px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-513.08px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 14500,
         autoPlay: true,
         labels: {
            "intro": 11,
            "open": 173
         },
         timeline: [
            { id: "eid23275", tween: [ "style", "${_purpleTweet3}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23115", tween: [ "style", "${_purpleTweet3}", "display", 'none', { fromValue: 'block'}], position: 1279, duration: 0 },
            { id: "eid22204", tween: [ "transform", "${_purpleTweet3}", "translateX", '-224.37px', { fromValue: '-127px'}], position: 364, duration: 244, easing: "swing" },
            { id: "eid22205", tween: [ "transform", "${_purpleTweet3}", "translateX", '-127px', { fromValue: '-224.37px'}], position: 945, duration: 242, easing: "easeOutQuad" },
            { id: "eid23361", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23276", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 173, duration: 0, easing: "swing" },
            { id: "eid23274", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'block'}], position: 10500, duration: 0, easing: "swing" },
            { id: "eid22196", tween: [ "style", "${_purpleTweet3}", "opacity", '1', { fromValue: '0'}], position: 364, duration: 244, easing: "swing" },
            { id: "eid22197", tween: [ "style", "${_purpleTweet3}", "opacity", '0', { fromValue: '1'}], position: 945, duration: 242, easing: "easeOutQuad" },
            { id: "eid21153", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid22156", tween: [ "transform", "${_largeTweeter4}", "scaleY", '1', { fromValue: '0.49'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22157", tween: [ "transform", "${_largeTweeter4}", "scaleY", '0.49', { fromValue: '1'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22168", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-497px', { fromValue: '-215px'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22169", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-215px', { fromValue: '-497px'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22214", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-238px', { fromValue: '-215px'}], position: 1360, duration: 117, easing: "easeOutQuad" },
            { id: "eid22162", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1', { fromValue: '0.49'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22163", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.49', { fromValue: '1'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid23150", tween: [ "transform", "${_tweeter4Close}", "translateX", '-503.72px', { fromValue: '-503.72px'}], position: 815, duration: 0 },
            { id: "eid23129", tween: [ "style", "${_tweeter4Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid23311", tween: [ "style", "${_tweeter4Close}", "display", 'block', { fromValue: 'none'}], position: 815, duration: 0 },
            { id: "eid23312", tween: [ "style", "${_tweeter4Close}", "display", 'none', { fromValue: 'block'}], position: 1000, duration: 0 },
            { id: "eid23277", tween: [ "style", "${_tweeter4Close}", "display", 'none', { fromValue: 'none'}], position: 10500, duration: 0, easing: "swing" },
            { id: "eid22164", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1', { fromValue: '0.49'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22165", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.49', { fromValue: '1'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22206", tween: [ "transform", "${_purpleTweet3}", "translateY", '-1142.24px', { fromValue: '-1070px'}], position: 364, duration: 244, easing: "swing" },
            { id: "eid22207", tween: [ "transform", "${_purpleTweet3}", "translateY", '-1070px', { fromValue: '-1142.24px'}], position: 945, duration: 242, easing: "easeOutQuad" },
            { id: "eid22160", tween: [ "transform", "${_largeTweeter4}", "translateY", '-497px', { fromValue: '-212.72px'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22161", tween: [ "transform", "${_largeTweeter4}", "translateY", '-212.72px', { fromValue: '-497px'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22158", tween: [ "transform", "${_largeTweeter4}", "translateX", '50px', { fromValue: '1005.95px'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22159", tween: [ "transform", "${_largeTweeter4}", "translateX", '1005.95px', { fromValue: '50px'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22200", tween: [ "transform", "${_purpleTweet3}", "scaleX", '1', { fromValue: '0.35'}], position: 364, duration: 244, easing: "swing" },
            { id: "eid22201", tween: [ "transform", "${_purpleTweet3}", "scaleX", '0.35', { fromValue: '1'}], position: 945, duration: 242, easing: "easeOutQuad" },
            { id: "eid22166", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-2240px', { fromValue: '-1310px'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22167", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1310px', { fromValue: '-2240px'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22213", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1325px', { fromValue: '-1310px'}], position: 1360, duration: 117, easing: "easeOutQuad" },
            { id: "eid22202", tween: [ "transform", "${_purpleTweet3}", "scaleY", '1', { fromValue: '0.35'}], position: 364, duration: 244, easing: "swing" },
            { id: "eid22203", tween: [ "transform", "${_purpleTweet3}", "scaleY", '0.35', { fromValue: '1'}], position: 945, duration: 242, easing: "easeOutQuad" },
            { id: "eid22154", tween: [ "transform", "${_largeTweeter4}", "scaleX", '1', { fromValue: '0.49'}], position: 173, duration: 442, easing: "swing" },
            { id: "eid22155", tween: [ "transform", "${_largeTweeter4}", "scaleX", '0.49', { fromValue: '1'}], position: 935, duration: 425, easing: "easeOutQuad" },
            { id: "eid22218", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1360, duration: 117, easing: "easeOutQuad" },
            { id: "eid23151", tween: [ "transform", "${_tweeter4Close}", "translateY", '-513.08px', { fromValue: '-513.08px'}], position: 815, duration: 0 }         ]
      }
   }
},
"tweeter3a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1267,-218],{},{},[0.61,0.61]],
      type: 'image',
      id: 'mainTweetPicShadow2',
      display: 'none',
      rect: [1133,82,861,700],
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      type: 'image',
      id: 'largeTweeter3',
      rect: [-174,-926,836,602],
      transform: [[13,811],{},{},[0.61,0.61]],
      fill: ['rgba(0,0,0,0)','images/largeTweeter3.png']
   },
   {
      transform: [[853,-1446]],
      type: 'image',
      display: 'none',
      id: 'blueTweet4',
      opacity: 1,
      rect: [-284,647,1100,700],
      fill: ['rgba(0,0,0,0)','images/blueTweet4.png']
   },
   {
      type: 'rect',
      rect: [922,111,115,125],
      display: 'none',
      id: 'tweeter3Close',
      stroke: [0,'rgba(0,0,0,1)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_blueTweet4}": [
            ["transform", "scaleX", '0.35'],
            ["transform", "translateX", '495.75px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-1306.41px'],
            ["transform", "scaleY", '0.35']
         ],
         "${symbolSelector}": [
            ["style", "height", '367.22px'],
            ["style", "width", '509.96px']
         ],
         "${_largeTweeter3}": [
            ["transform", "translateX", '13.6px'],
            ["transform", "rotateZ", '-14deg'],
            ["transform", "scaleX", '0.61'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '811.85px'],
            ["transform", "scaleY", '0.61']
         ],
         "${_mainTweetPicShadow2}": [
            ["transform", "scaleX", '0.61'],
            ["transform", "scaleY", '0.61'],
            ["transform", "rotateZ", '-14deg'],
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-218.5px'],
            ["transform", "translateX", '-1267.69px']
         ],
         "${_tweeter3Close}": [
            ["transform", "translateX", '578.35px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-805.42px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 14500,
         autoPlay: true,
         labels: {
            "intro": 24,
            "open": 92
         },
         timeline: [
            { id: "eid23327", tween: [ "style", "${_blueTweet4}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23326", tween: [ "style", "${_blueTweet4}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0, easing: "swing" },
            { id: "eid22263", tween: [ "style", "${_largeTweeter3}", "opacity", '1', { fromValue: '1'}], position: 115, duration: 0 },
            { id: "eid22275", tween: [ "style", "${_largeTweeter3}", "opacity", '1', { fromValue: '1'}], position: 1099, duration: 0 },
            { id: "eid22289", tween: [ "transform", "${_largeTweeter3}", "translateY", '253.24px', { fromValue: '811.85px'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22290", tween: [ "transform", "${_largeTweeter3}", "translateY", '811.85px', { fromValue: '253.24px'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23272", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23360", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 115, duration: 0, easing: "swing" },
            { id: "eid21124", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid21123", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 14500, duration: 0 },
            { id: "eid22299", tween: [ "transform", "${_largeTweeter3}", "scaleY", '1', { fromValue: '0.61'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22300", tween: [ "transform", "${_largeTweeter3}", "scaleY", '0.61', { fromValue: '1'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23334", tween: [ "transform", "${_blueTweet4}", "scaleY", '1', { fromValue: '0.35'}], position: 250, duration: 222, easing: "swing" },
            { id: "eid23349", tween: [ "transform", "${_blueTweet4}", "scaleY", '0.35', { fromValue: '1'}], position: 727, duration: 158, easing: "swing" },
            { id: "eid22293", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-777.09px', { fromValue: '-218.5px'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22294", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-218.5px', { fromValue: '-777.09px'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23244", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-245px', { fromValue: '-218.5px'}], position: 1099, duration: 117, easing: "easeOutQuad" },
            { id: "eid22291", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1188.21px', { fromValue: '-1267.69px'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22292", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1267.69px', { fromValue: '-1188.21px'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23243", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1292px', { fromValue: '-1267.69px'}], position: 1099, duration: 117, easing: "easeOutQuad" },
            { id: "eid23126", tween: [ "style", "${_tweeter3Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid23337", tween: [ "style", "${_tweeter3Close}", "display", 'block', { fromValue: 'none'}], position: 472, duration: 0, easing: "swing" },
            { id: "eid23310", tween: [ "style", "${_tweeter3Close}", "display", 'none', { fromValue: 'block'}], position: 727, duration: 0 },
            { id: "eid22297", tween: [ "transform", "${_largeTweeter3}", "scaleX", '1', { fromValue: '0.61'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22298", tween: [ "transform", "${_largeTweeter3}", "scaleX", '0.61', { fromValue: '1'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid22303", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1', { fromValue: '0.61'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22304", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.61', { fromValue: '1'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23336", tween: [ "transform", "${_blueTweet4}", "translateY", '-1461.2px', { fromValue: '-1306.41px'}], position: 250, duration: 222, easing: "swing" },
            { id: "eid23353", tween: [ "transform", "${_blueTweet4}", "translateY", '-1306px', { fromValue: '-1461.2px'}], position: 727, duration: 158, easing: "swing" },
            { id: "eid22301", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1', { fromValue: '0.61'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22302", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.61', { fromValue: '1'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23245", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1099, duration: 117, easing: "easeOutQuad" },
            { id: "eid23338", tween: [ "style", "${_blueTweet4}", "opacity", '1', { fromValue: '0'}], position: 250, duration: 222, easing: "swing" },
            { id: "eid23345", tween: [ "style", "${_blueTweet4}", "opacity", '0', { fromValue: '1'}], position: 727, duration: 158, easing: "swing" },
            { id: "eid23333", tween: [ "transform", "${_blueTweet4}", "scaleX", '1', { fromValue: '0.35'}], position: 250, duration: 222, easing: "swing" },
            { id: "eid23348", tween: [ "transform", "${_blueTweet4}", "scaleX", '0.35', { fromValue: '1'}], position: 727, duration: 158, easing: "swing" },
            { id: "eid22287", tween: [ "transform", "${_largeTweeter3}", "translateX", '93.07px', { fromValue: '13.6px'}], position: 115, duration: 357, easing: "swing" },
            { id: "eid22288", tween: [ "transform", "${_largeTweeter3}", "translateX", '13.6px', { fromValue: '93.07px'}], position: 727, duration: 371, easing: "easeOutQuad" },
            { id: "eid23335", tween: [ "transform", "${_blueTweet4}", "translateX", '824.43px', { fromValue: '495.75px'}], position: 250, duration: 222, easing: "swing" },
            { id: "eid23352", tween: [ "transform", "${_blueTweet4}", "translateX", '496px', { fromValue: '824.43px'}], position: 727, duration: 158, easing: "swing" }         ]
      }
   }
},
"tweeter2a": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-1130,-77],[17],{},[0.81,0.81]],
      rect: [1133,82,861,700],
      display: 'none',
      id: 'mainTweetPicShadow2',
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/mainTweetPicShadow.png']
   },
   {
      rect: [-807,386,845,603],
      id: 'largeTweeter2',
      transform: [[785,-352],[17],{},[0.81,0.81]],
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/largeTweeter2.png']
   },
   {
      transform: [[221,-226]],
      rect: [-284,647,1100,700],
      id: 'purpleTweet4',
      opacity: 1,
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/purpleTweet4.png']
   },
   {
      rect: [920,562,111,124],
      type: 'rect',
      borderRadius: [10,10,10,10],
      id: 'tweeter2Close',
      display: 'none',
      stroke: [0,'rgb(0, 0, 0)','none'],
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_purpleTweet4}": [
            ["transform", "scaleY", '0.35'],
            ["transform", "translateX", '143px'],
            ["transform", "rotateZ", '0deg'],
            ["transform", "scaleX", '0.35'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-409px'],
            ["style", "display", 'block']
         ],
         "${symbolSelector}": [
            ["style", "height", '666.190338px'],
            ["style", "width", '796.33455px']
         ],
         "${_tweeter2Close}": [
            ["transform", "translateX", '-53.87px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateY", '-9.96px']
         ],
         "${_mainTweetPicShadow2}": [
            ["transform", "scaleY", '0.81'],
            ["transform", "translateX", '-1130.03px'],
            ["transform", "rotateZ", '17deg'],
            ["transform", "scaleX", '0.81'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-77.05px'],
            ["style", "display", 'none']
         ],
         "${_largeTweeter2}": [
            ["transform", "translateX", '785.83px'],
            ["transform", "rotateZ", '17deg'],
            ["transform", "scaleX", '0.81'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-352.28px'],
            ["transform", "scaleY", '0.81']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1295,
         autoPlay: true,
         labels: {
            "intro": 14,
            "open": 91
         },
         timeline: [
            { id: "eid22530", tween: [ "style", "${_purpleTweet4}", "opacity", '1', { fromValue: '0'}], position: 278, duration: 172 },
            { id: "eid22540", tween: [ "style", "${_purpleTweet4}", "opacity", '0', { fromValue: '1'}], position: 667, duration: 159 },
            { id: "eid22529", tween: [ "transform", "${_purpleTweet4}", "translateX", '210px', { fromValue: '143px'}], position: 278, duration: 172 },
            { id: "eid22536", tween: [ "transform", "${_purpleTweet4}", "translateX", '143px', { fromValue: '210px'}], position: 667, duration: 159 },
            { id: "eid23354", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23267", tween: [ "style", "${_mainTweetPicShadow2}", "display", 'block', { fromValue: 'none'}], position: 91, duration: 0 },
            { id: "eid21088", tween: [ "style", "${_tweetBalloonBlue}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0 },
            { id: "eid22471", tween: [ "transform", "${_largeTweeter2}", "scaleY", '1', { fromValue: '0.81'}], position: 112, duration: 341 },
            { id: "eid22472", tween: [ "transform", "${_largeTweeter2}", "scaleY", '0.81', { fromValue: '1'}], position: 661, duration: 380 },
            { id: "eid22481", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '433px', { fromValue: '-77.05px'}], position: 112, duration: 341 },
            { id: "eid22482", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-77.05px', { fromValue: '433px'}], position: 661, duration: 380 },
            { id: "eid22544", tween: [ "transform", "${_mainTweetPicShadow2}", "translateY", '-87px', { fromValue: '-77.05px'}], position: 1042, duration: 163 },
            { id: "eid22475", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '1', { fromValue: '0.81'}], position: 112, duration: 341 },
            { id: "eid22476", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleX", '0.81', { fromValue: '1'}], position: 661, duration: 380 },
            { id: "eid22469", tween: [ "transform", "${_largeTweeter2}", "scaleX", '1', { fromValue: '0.81'}], position: 112, duration: 341 },
            { id: "eid22470", tween: [ "transform", "${_largeTweeter2}", "scaleX", '0.81', { fromValue: '1'}], position: 662, duration: 380 },
            { id: "eid22477", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '1', { fromValue: '0.81'}], position: 112, duration: 341 },
            { id: "eid22478", tween: [ "transform", "${_mainTweetPicShadow2}", "scaleY", '0.81', { fromValue: '1'}], position: 661, duration: 380 },
            { id: "eid22467", tween: [ "transform", "${_largeTweeter2}", "translateY", '165.18px', { fromValue: '-352.28px'}], position: 112, duration: 341 },
            { id: "eid22468", tween: [ "transform", "${_largeTweeter2}", "translateY", '-352.28px', { fromValue: '165.18px'}], position: 662, duration: 380 },
            { id: "eid22422", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '1', { fromValue: '1'}], position: 112, duration: 0 },
            { id: "eid22546", tween: [ "style", "${_mainTweetPicShadow2}", "opacity", '0', { fromValue: '1'}], position: 1042, duration: 163 },
            { id: "eid22479", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '-13deg', { fromValue: '17deg'}], position: 112, duration: 341 },
            { id: "eid22480", tween: [ "transform", "${_mainTweetPicShadow2}", "rotateZ", '17deg', { fromValue: '-13deg'}], position: 661, duration: 380 },
            { id: "eid22526", tween: [ "transform", "${_purpleTweet4}", "scaleX", '1', { fromValue: '0.35'}], position: 278, duration: 172 },
            { id: "eid22538", tween: [ "transform", "${_purpleTweet4}", "scaleX", '0.35', { fromValue: '1'}], position: 667, duration: 159 },
            { id: "eid23266", tween: [ "style", "${_purpleTweet4}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23123", tween: [ "style", "${_tweeter2Close}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid23306", tween: [ "style", "${_tweeter2Close}", "display", 'block', { fromValue: 'none'}], position: 450, duration: 0 },
            { id: "eid23307", tween: [ "style", "${_tweeter2Close}", "display", 'none', { fromValue: 'block'}], position: 661, duration: 0 },
            { id: "eid22483", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1812px', { fromValue: '-1130.03px'}], position: 112, duration: 341 },
            { id: "eid22484", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1130.03px', { fromValue: '-1812px'}], position: 661, duration: 380 },
            { id: "eid22545", tween: [ "transform", "${_mainTweetPicShadow2}", "translateX", '-1147px', { fromValue: '-1130.03px'}], position: 1042, duration: 163 },
            { id: "eid22527", tween: [ "transform", "${_purpleTweet4}", "scaleY", '1', { fromValue: '0.35'}], position: 278, duration: 172 },
            { id: "eid22539", tween: [ "transform", "${_purpleTweet4}", "scaleY", '0.35', { fromValue: '1'}], position: 667, duration: 159 },
            { id: "eid22528", tween: [ "transform", "${_purpleTweet4}", "translateY", '-211px', { fromValue: '-409px'}], position: 278, duration: 172 },
            { id: "eid22537", tween: [ "transform", "${_purpleTweet4}", "translateY", '-409px', { fromValue: '-211px'}], position: 667, duration: 159 },
            { id: "eid22465", tween: [ "transform", "${_largeTweeter2}", "translateX", '82.33px', { fromValue: '785.83px'}], position: 112, duration: 341 },
            { id: "eid22466", tween: [ "transform", "${_largeTweeter2}", "translateX", '785.83px', { fromValue: '82.33px'}], position: 662, duration: 380 },
            { id: "eid22473", tween: [ "transform", "${_largeTweeter2}", "rotateZ", '-13deg', { fromValue: '17deg'}], position: 112, duration: 341 },
            { id: "eid22474", tween: [ "transform", "${_largeTweeter2}", "rotateZ", '17deg', { fromValue: '-13deg'}], position: 661, duration: 380 }         ]
      }
   }
},
"SafeMovie": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0015',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0015.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0014',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0014.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0013',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0013.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0012',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0012.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0011',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0011.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0010',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0010.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0009',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0009.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0008',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0008.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0007',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0007.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0006',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0006.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0005',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0005.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0004',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0004.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0003',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0003.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0002',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0002.png']
   },
   {
      rect: [-310,334,300,115],
      transform: [[309,-328]],
      id: 'safeAni0001',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0001.png']
   },
   {
      rect: [114,72,300,115],
      transform: [[-113,-66]],
      id: 'safe',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safe.png']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_safeAni0011}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0002}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0015}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0005}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${symbolSelector}": [
            ["style", "height", '114px'],
            ["style", "width", '300px']
         ],
         "${_safeAni0006}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0004}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0012}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0013}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safe}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-66.99px'],
            ["transform", "translateX", '-113.58px']
         ],
         "${_safeAni0008}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0003}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0009}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0010}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0007}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0014}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ],
         "${_safeAni0001}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-328.75px'],
            ["transform", "translateX", '309.99px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 580.45894075276,
         autoPlay: true,
         timeline: [
            { id: "eid23182", tween: [ "style", "${_safeAni0005}", "display", 'block', { fromValue: 'none'}], position: 354, duration: 0, easing: "swing" },
            { id: "eid23208", tween: [ "style", "${_safeAni0005}", "display", 'none', { fromValue: 'block'}], position: 382, duration: 0, easing: "swing" },
            { id: "eid23179", tween: [ "style", "${_safeAni0002}", "display", 'block', { fromValue: 'none'}], position: 453, duration: 0, easing: "swing" },
            { id: "eid23205", tween: [ "style", "${_safeAni0002}", "display", 'none', { fromValue: 'block'}], position: 500, duration: 0, easing: "swing" },
            { id: "eid23191", tween: [ "style", "${_safeAni0014}", "display", 'block', { fromValue: 'none'}], position: 61, duration: 0, easing: "swing" },
            { id: "eid23217", tween: [ "style", "${_safeAni0014}", "display", 'none', { fromValue: 'block'}], position: 121, duration: 0, easing: "swing" },
            { id: "eid23185", tween: [ "style", "${_safeAni0008}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0, easing: "swing" },
            { id: "eid23211", tween: [ "style", "${_safeAni0008}", "display", 'none', { fromValue: 'block'}], position: 290, duration: 0, easing: "swing" },
            { id: "eid23204", tween: [ "style", "${_safe}", "display", 'block', { fromValue: 'none'}], position: 551, duration: 0, easing: "swing" },
            { id: "eid23178", tween: [ "style", "${_safeAni0001}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0, easing: "swing" },
            { id: "eid23219", tween: [ "style", "${_safeAni0001}", "display", 'none', { fromValue: 'block'}], position: 551, duration: 0, easing: "swing" },
            { id: "eid23188", tween: [ "style", "${_safeAni0011}", "display", 'block', { fromValue: 'none'}], position: 154, duration: 0, easing: "swing" },
            { id: "eid23214", tween: [ "style", "${_safeAni0011}", "display", 'none', { fromValue: 'block'}], position: 185, duration: 0, easing: "swing" },
            { id: "eid23184", tween: [ "style", "${_safeAni0007}", "display", 'block', { fromValue: 'none'}], position: 290, duration: 0, easing: "swing" },
            { id: "eid23210", tween: [ "style", "${_safeAni0007}", "display", 'none', { fromValue: 'block'}], position: 325, duration: 0, easing: "swing" },
            { id: "eid23183", tween: [ "style", "${_safeAni0006}", "display", 'block', { fromValue: 'none'}], position: 325, duration: 0, easing: "swing" },
            { id: "eid23209", tween: [ "style", "${_safeAni0006}", "display", 'none', { fromValue: 'block'}], position: 354, duration: 0, easing: "swing" },
            { id: "eid23203", tween: [ "style", "${_safeAni0015}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0, easing: "swing" },
            { id: "eid23192", tween: [ "style", "${_safeAni0015}", "display", 'block', { fromValue: 'none'}], position: 31, duration: 0, easing: "swing" },
            { id: "eid23218", tween: [ "style", "${_safeAni0015}", "display", 'none', { fromValue: 'block'}], position: 90, duration: 0, easing: "swing" },
            { id: "eid23190", tween: [ "style", "${_safeAni0013}", "display", 'block', { fromValue: 'none'}], position: 90, duration: 0, easing: "swing" },
            { id: "eid23216", tween: [ "style", "${_safeAni0013}", "display", 'none', { fromValue: 'block'}], position: 154, duration: 0, easing: "swing" },
            { id: "eid23180", tween: [ "style", "${_safeAni0003}", "display", 'block', { fromValue: 'none'}], position: 417, duration: 0, easing: "swing" },
            { id: "eid23206", tween: [ "style", "${_safeAni0003}", "display", 'none', { fromValue: 'block'}], position: 453, duration: 0, easing: "swing" },
            { id: "eid23181", tween: [ "style", "${_safeAni0004}", "display", 'block', { fromValue: 'none'}], position: 382, duration: 0, easing: "swing" },
            { id: "eid23207", tween: [ "style", "${_safeAni0004}", "display", 'none', { fromValue: 'block'}], position: 417, duration: 0, easing: "swing" },
            { id: "eid23187", tween: [ "style", "${_safeAni0010}", "display", 'block', { fromValue: 'none'}], position: 185, duration: 0, easing: "swing" },
            { id: "eid23213", tween: [ "style", "${_safeAni0010}", "display", 'none', { fromValue: 'block'}], position: 217, duration: 0, easing: "swing" },
            { id: "eid23189", tween: [ "style", "${_safeAni0012}", "display", 'block', { fromValue: 'none'}], position: 121, duration: 0, easing: "swing" },
            { id: "eid23215", tween: [ "style", "${_safeAni0012}", "display", 'none', { fromValue: 'block'}], position: 185, duration: 0, easing: "swing" },
            { id: "eid23186", tween: [ "style", "${_safeAni0009}", "display", 'block', { fromValue: 'none'}], position: 217, duration: 0, easing: "swing" },
            { id: "eid23212", tween: [ "style", "${_safeAni0009}", "display", 'none', { fromValue: 'block'}], position: 250, duration: 0, easing: "swing" }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-70165390");
