/***********************
* Adobe Edge Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   // temp
   
   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2250, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_continueButtonHit}", "click", function(sym, e) {
         sym.play();
         sym.getComposition().getStage().getSymbol("intro").play();
         sym.getComposition().getStage().getSymbol("intro").getSymbol("questionBalloon").play();
         
         // insert code for mouse click here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3296, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 5112, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answerA}", "click", function(sym, e) {
         // insert code for mouse click here
         sym.play('incorrect');

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answerB}", "click", function(sym, e) {
         // insert code for mouse click here
         sym.play('incorrect');

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answerC}", "click", function(sym, e) {
         // insert code for mouse click here
         sym.play('incorrect');

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_answerD}", "click", function(sym, e) {
         // insert code for mouse click here
         sym.play('correct');
         

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 3683, function(sym, e) {
         // insert code here
         sym.getComposition().getStage().getSymbol("CorrectAnimation").play();
         sym.getComposition().getStage().getSymbol("CorrectAnimation").getSymbol("logoROllOut").play();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 6075, function(sym, e) {
         // insert code here
         sym.getComposition().getStage().getSymbol("incorrwectAnimate").play('start');
         
         sym.getComposition().getStage().getSymbol("incorrwectAnimate").getSymbol("logoROllOut2").play();
         

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

//=========================================================
   //Edge symbol: 'QUESTIONANSWERBALLOON'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1617, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1331, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("QUESTIONANSWERBALLOON");
   //Edge symbol end:'QUESTIONANSWERBALLOON'

//=========================================================
   //Edge symbol: 'INTROBUILD'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1899, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 2295, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("INTROBUILD");
   //Edge symbol end:'INTROBUILD'

//=========================================================
   //Edge symbol: 'logoROllOut'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("logoROllOut");
   //Edge symbol end:'logoROllOut'

//=========================================================
   //Edge symbol: 'CorrectAnimation'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 21, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("CorrectAnimation");
   //Edge symbol end:'CorrectAnimation'

//=========================================================
   //Edge symbol: 'incorrwectAnimate'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("incorrwectAnimate");
   //Edge symbol end:'incorrwectAnimate'

})(jQuery, AdobeEdge, "EDGE-29364989");