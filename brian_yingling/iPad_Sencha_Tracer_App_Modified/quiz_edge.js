/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
         dom: [
         {
            id:'greyFill',
            type:'rect',
            rect:[-333,-57,1026,734],
            fill:["rgba(138,140,142,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[331,55]]
         },
         {
            id:'blueFill',
            display:'none',
            type:'rect',
            rect:[-333,-57,1026,767],
            fill:["rgba(28,63,148,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[331,55]]
         },
         {
            id:'intro',
            type:'rect',
            rect:[479,99,0,0]
         },
         {
            id:'continueButtonHit',
            display:'none',
            type:'rect',
            rect:[565,326,179,62],
            cursor:['pointer'],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"]
         },
         {
            id:'answers',
            display:'none',
            type:'image',
            rect:[167,100,322,432],
            fill:["rgba(0,0,0,0)",im+"answers.svg"],
            transform:[[-42,1]]
         },
         {
            id:'CorrectAnimation',
            display:'none',
            type:'rect',
            rect:[213,228,0,0]
         },
         {
            id:'answerA',
            display:'none',
            type:'rect',
            rect:[139,112,286,95],
            cursor:['pointer'],
            fill:["rgba(28,63,148,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[8]]
         },
         {
            id:'answerB',
            display:'none',
            type:'rect',
            rect:[139,112,286,95],
            cursor:['pointer'],
            fill:["rgba(28,63,148,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[9,113]]
         },
         {
            id:'answerC',
            display:'none',
            type:'rect',
            rect:[139,112,286,95],
            cursor:['pointer'],
            fill:["rgba(28,63,148,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[9,221]]
         },
         {
            id:'answerD',
            display:'none',
            type:'rect',
            rect:[139,112,286,95],
            cursor:['pointer'],
            fill:["rgba(28,63,148,1)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[9,329]]
         },
         {
            id:'commonAnswer',
            display:'none',
            type:'image',
            rect:[298,539,782,120],
            fill:["rgba(0,0,0,0)",im+"commonAnswer.svg"],
            transform:[[-153,-106]]
         },
         {
            id:'incorrwectAnimate',
            display:'none',
            type:'rect',
            rect:[121,245,0,0]
         },
         {
            id:'continueBlue',
            display:'none',
            type:'image',
            rect:[756,647,271,91],
            fill:["rgba(0,0,0,0)",im+"continueBlue.svg"],
            transform:[[42,-25],[],[],[0.44,0.44]]
         },
         {
            id:'Rectangle',
            type:'rect',
            rect:[-17,714,1026,63],
            fill:["rgba(192,192,192,1)"],
            stroke:[0,"rgba(0,0,0,1)","none"],
            transform:[[15,-5]]
         }],
         symbolInstances: [
         {
            id:'CorrectAnimation',
            symbolName:'CorrectAnimation'
         },
         {
            id:'intro',
            symbolName:'INTROBUILD'
         },
         {
            id:'incorrwectAnimate',
            symbolName:'incorrwectAnimate'
         }
         ]
      },
   states: {
      "Base State": {
         "${_answerC}": [
            ["transform", "translateX", '9.76px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '221.12px'],
            ["style", "cursor", 'pointer']
         ],
         "${_incorrwectAnimate}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '0']
         ],
         "${_continueButtonHit}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer']
         ],
         "${_intro}": [
            ["transform", "translateX", '0'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '1'],
            ["transform", "scaleY", '1'],
            ["transform", "translateY", '-18.92px'],
            ["style", "display", 'block']
         ],
         "${_CorrectAnimation}": [
            ["style", "display", 'none']
         ],
         "${_blueFill}": [
            ["color", "background-color", 'rgba(28,63,148,1.00)'],
            ["transform", "translateX", '331.77px'],
            ["style", "height", '767px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '55.29px'],
            ["style", "width", '1026px']
         ],
         "${_continueBlue}": [
            ["transform", "translateX", '42.19px'],
            ["transform", "scaleY", '0.44'],
            ["transform", "scaleX", '0.44'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-25.96px'],
            ["style", "display", 'none']
         ],
         "${_greyFill}": [
            ["color", "background-color", 'rgba(138,140,142,1.00)'],
            ["transform", "translateX", '331.77px'],
            ["style", "height", '734px'],
            ["style", "display", 'block'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '55.29px'],
            ["style", "width", '1026px']
         ],
         "${_answers}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '1.22px'],
            ["transform", "translateX", '-42.02px']
         ],
         "${_commonAnswer}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-106.58px'],
            ["transform", "translateX", '-153.23px']
         ],
         "${_answerA}": [
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateX", '8.4px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Stage}": [
            ["style", "height", '769px'],
            ["style", "width", '1026px'],
            ["color", "background-color", 'rgba(28,63,148,1.00)'],
            ["style", "overflow", 'hidden']
         ],
         "${_answerB}": [
            ["transform", "translateX", '9.76px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '113.12px'],
            ["style", "cursor", 'pointer']
         ],
         "${_answerD}": [
            ["transform", "translateX", '9.76px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '329.11px'],
            ["style", "cursor", 'pointer']
         ],
         "${_Rectangle}": [
            ["style", "height", '63px'],
            ["transform", "translateY", '-5.8px'],
            ["transform", "translateX", '15.84px'],
            ["style", "width", '1026px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 8116,
         autoPlay: true,
         labels: {
            "correct": 3525,
            "incorrect": 5876
         },
         timeline: [
            { id: "eid243", tween: [ "style", "${_answers}", "opacity", '1', { fromValue: '0'}], position: 3035, duration: 227, easing: "swing" },
            { id: "eid164", tween: [ "transform", "${_intro}", "rotateZ", '-42deg', { fromValue: '0deg'}], position: 1750, duration: 500, easing: "swing" },
            { id: "eid193", tween: [ "transform", "${_intro}", "rotateZ", '-132deg', { fromValue: '-42deg'}], position: 2681, duration: 466, easing: "swing" },
            { id: "eid165", tween: [ "transform", "${_intro}", "translateX", '-103.3px', { fromValue: '0px'}], position: 1750, duration: 500, easing: "swing" },
            { id: "eid196", tween: [ "transform", "${_intro}", "translateX", '-376.97px', { fromValue: '-103.3px'}], position: 2681, duration: 466, easing: "swing" },
            { id: "eid250", tween: [ "style", "${_blueFill}", "opacity", '1', { fromValue: '0'}], position: 3654, duration: 67, easing: "swing" },
            { id: "eid375", tween: [ "style", "${_blueFill}", "opacity", '1', { fromValue: '1'}], position: 5112, duration: 0 },
            { id: "eid341", tween: [ "style", "${_blueFill}", "opacity", '0', { fromValue: '1'}], position: 5599, duration: 332, easing: "swing" },
            { id: "eid342", tween: [ "style", "${_blueFill}", "opacity", '1', { fromValue: '0'}], position: 5932, duration: 67, easing: "swing" },
            { id: "eid321", tween: [ "style", "${_continueBlue}", "opacity", '1', { fromValue: '0'}], position: 4862, duration: 250, easing: "swing" },
            { id: "eid359", tween: [ "style", "${_continueBlue}", "opacity", '0', { fromValue: '0'}], position: 5300, duration: 0, easing: "swing" },
            { id: "eid362", tween: [ "style", "${_continueBlue}", "opacity", '1', { fromValue: '0'}], position: 7805, duration: 311, easing: "swing" },
            { id: "eid302", tween: [ "style", "${_answerD}", "display", 'block', { fromValue: 'none'}], position: 3237, duration: 0, easing: "swing" },
            { id: "eid309", tween: [ "style", "${_answerD}", "display", 'none', { fromValue: 'block'}], position: 3325, duration: 0, easing: "swing" },
            { id: "eid240", tween: [ "style", "${_answers}", "display", 'block', { fromValue: 'none'}], position: 3035, duration: 0, easing: "swing" },
            { id: "eid251", tween: [ "style", "${_answers}", "display", 'none', { fromValue: 'block'}], position: 3525, duration: 0, easing: "swing" },
            { id: "eid300", tween: [ "style", "${_CorrectAnimation}", "display", 'block', { fromValue: 'none'}], position: 3741, duration: 0, easing: "swing" },
            { id: "eid325", tween: [ "style", "${_CorrectAnimation}", "display", 'none', { fromValue: 'block'}], position: 5300, duration: 0, easing: "swing" },
            { id: "eid244", tween: [ "style", "${_greyFill}", "display", 'block', { fromValue: 'block'}], position: 3654, duration: 0, easing: "swing" },
            { id: "eid304", tween: [ "style", "${_answerB}", "display", 'block', { fromValue: 'none'}], position: 3237, duration: 0, easing: "swing" },
            { id: "eid307", tween: [ "style", "${_answerB}", "display", 'none', { fromValue: 'block'}], position: 3325, duration: 0, easing: "swing" },
            { id: "eid245", tween: [ "style", "${_blueFill}", "display", 'block', { fromValue: 'none'}], position: 3654, duration: 0, easing: "swing" },
            { id: "eid324", tween: [ "style", "${_blueFill}", "display", 'none', { fromValue: 'block'}], position: 5250, duration: 0, easing: "swing" },
            { id: "eid326", tween: [ "style", "${_blueFill}", "display", 'block', { fromValue: 'none'}], position: 5932, duration: 0, easing: "swing" },
            { id: "eid246", tween: [ "style", "${_greyFill}", "opacity", '1', { fromValue: '1'}], position: 3654, duration: 0, easing: "swing" },
            { id: "eid168", tween: [ "transform", "${_intro}", "translateY", '-155.19px', { fromValue: '-18.92px'}], position: 1750, duration: 500, easing: "swing" },
            { id: "eid197", tween: [ "transform", "${_intro}", "translateY", '-65.86px', { fromValue: '-155.19px'}], position: 2681, duration: 466, easing: "swing" },
            { id: "eid374", tween: [ "transform", "${_incorrwectAnimate}", "translateX", '-425px', { fromValue: '0px'}], position: 6130, duration: 1213 },
            { id: "eid303", tween: [ "style", "${_answerC}", "display", 'block', { fromValue: 'none'}], position: 3237, duration: 0, easing: "swing" },
            { id: "eid308", tween: [ "style", "${_answerC}", "display", 'none', { fromValue: 'block'}], position: 3325, duration: 0, easing: "swing" },
            { id: "eid253", tween: [ "style", "${_intro}", "display", 'none', { fromValue: 'block'}], position: 3525, duration: 0, easing: "swing" },
            { id: "eid305", tween: [ "style", "${_answerA}", "display", 'block', { fromValue: 'none'}], position: 3237, duration: 0, easing: "swing" },
            { id: "eid306", tween: [ "style", "${_answerA}", "display", 'none', { fromValue: 'block'}], position: 3325, duration: 0, easing: "swing" },
            { id: "eid167", tween: [ "transform", "${_intro}", "scaleY", '1.45', { fromValue: '1'}], position: 1750, duration: 500, easing: "swing" },
            { id: "eid195", tween: [ "transform", "${_intro}", "scaleY", '2.18', { fromValue: '1.45'}], position: 2681, duration: 466, easing: "swing" },
            { id: "eid378", tween: [ "style", "${_commonAnswer}", "opacity", '1', { fromValue: '0'}], position: 4432, duration: 318, easing: "swing" },
            { id: "eid376", tween: [ "style", "${_commonAnswer}", "opacity", '0.46145400404930115', { fromValue: '1'}], position: 5112, duration: 188, easing: "swing" },
            { id: "eid348", tween: [ "style", "${_commonAnswer}", "opacity", '0', { fromValue: '0.46145400404930115'}], position: 5300, duration: 1167, easing: "swing" },
            { id: "eid347", tween: [ "style", "${_commonAnswer}", "opacity", '1', { fromValue: '0'}], position: 7344, duration: 281, easing: "swing" },
            { id: "eid312", tween: [ "style", "${_commonAnswer}", "display", 'block', { fromValue: 'none'}], position: 4432, duration: 0, easing: "swing" },
            { id: "eid323", tween: [ "style", "${_commonAnswer}", "display", 'none', { fromValue: 'block'}], position: 5343, duration: 0, easing: "swing" },
            { id: "eid343", tween: [ "style", "${_commonAnswer}", "display", 'block', { fromValue: 'none'}], position: 6468, duration: 0, easing: "swing" },
            { id: "eid371", tween: [ "style", "${_incorrwectAnimate}", "display", 'block', { fromValue: 'none'}], position: 5932, duration: 0, easing: "swing" },
            { id: "eid184", tween: [ "style", "${_continueButtonHit}", "display", 'block', { fromValue: 'none'}], position: 2215, duration: 0, easing: "swing" },
            { id: "eid185", tween: [ "style", "${_continueButtonHit}", "display", 'none', { fromValue: 'block'}], position: 2306, duration: 0, easing: "swing" },
            { id: "eid252", tween: [ "style", "${_continueButtonHit}", "display", 'none', { fromValue: 'none'}], position: 3525, duration: 0, easing: "swing" },
            { id: "eid166", tween: [ "transform", "${_intro}", "scaleX", '1.45', { fromValue: '1'}], position: 1750, duration: 500, easing: "swing" },
            { id: "eid194", tween: [ "transform", "${_intro}", "scaleX", '2.18', { fromValue: '1.45'}], position: 2681, duration: 466, easing: "swing" },
            { id: "eid317", tween: [ "style", "${_continueBlue}", "display", 'block', { fromValue: 'none'}], position: 4862, duration: 0, easing: "swing" },
            { id: "eid322", tween: [ "style", "${_continueBlue}", "display", 'none', { fromValue: 'block'}], position: 5300, duration: 0, easing: "swing" },
            { id: "eid356", tween: [ "style", "${_continueBlue}", "display", 'block', { fromValue: 'none'}], position: 7750, duration: 0, easing: "swing" }         ]
      }
   }
},
"QUESTIONANSWERBALLOON": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-36,-518],[42],{},[0.33,0.33]],
      rect: [-380,-264,1131,1344],
      display: 'none',
      id: 'whiteFilledBalloon2',
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/whiteFilledBalloon.svg']
   },
   {
      transform: [[-40,-101],[42],{},[0.63,0.63]],
      rect: [-15,-157,446,249],
      display: 'none',
      id: 'question4',
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/question.svg']
   },
   {
      transform: [[719,121],[42],{},[0.41,0.41]],
      rect: [-767,-220,279,104],
      display: 'none',
      id: 'continueGrey',
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/continueGrey.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_whiteFilledBalloon2}": [
            ["transform", "scaleY", '0.02'],
            ["transform", "translateX", '-167px'],
            ["transform", "rotateZ", '42deg'],
            ["transform", "scaleX", '0.02'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-389px'],
            ["style", "display", 'none']
         ],
         "${_question4}": [
            ["transform", "scaleY", '0.63'],
            ["transform", "translateX", '-40.85px'],
            ["transform", "rotateZ", '42deg'],
            ["transform", "scaleX", '0.63'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-101.79px'],
            ["style", "display", 'none']
         ],
         "${_continueGrey}": [
            ["transform", "scaleY", '0.41'],
            ["transform", "translateX", '719.68px'],
            ["transform", "rotateZ", '42deg'],
            ["transform", "scaleX", '0.41'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '121.95px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '35.112045px'],
            ["style", "width", '34.796805px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1617.9715708315,
         autoPlay: true,
         labels: {
            "INTRO": 17
         },
         timeline: [
            { id: "eid126", tween: [ "style", "${_question4}", "opacity", '1', { fromValue: '0'}], position: 721, duration: 125 },
            { id: "eid212", tween: [ "style", "${_question4}", "opacity", '0', { fromValue: '1'}], position: 1389, duration: 149, easing: "swing" },
            { id: "eid148", tween: [ "style", "${_whiteFilledBalloon2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid149", tween: [ "style", "${_whiteFilledBalloon2}", "display", 'block', { fromValue: 'none'}], position: 33, duration: 0 },
            { id: "eid42", tween: [ "style", "${_whiteFilledBalloon2}", "opacity", '1', { fromValue: '1'}], position: 100, duration: 0 },
            { id: "eid31", tween: [ "style", "${_whiteFilledBalloon2}", "opacity", '1', { fromValue: '1'}], position: 738, duration: 0 },
            { id: "eid169", tween: [ "style", "${_continueGrey}", "display", 'block', { fromValue: 'none'}], position: 1048, duration: 0, easing: "swing" },
            { id: "eid90", tween: [ "transform", "${_whiteFilledBalloon2}", "translateY", '-518.33px', { fromValue: '-389px'}], position: 100, duration: 638 },
            { id: "eid81", tween: [ "transform", "${_whiteFilledBalloon2}", "scaleY", '0.33', { fromValue: '0.02'}], position: 100, duration: 638 },
            { id: "eid80", tween: [ "transform", "${_whiteFilledBalloon2}", "scaleX", '0.33', { fromValue: '0.02'}], position: 100, duration: 638 },
            { id: "eid91", tween: [ "transform", "${_whiteFilledBalloon2}", "translateX", '-36.43px', { fromValue: '-167px'}], position: 100, duration: 638 },
            { id: "eid124", tween: [ "style", "${_question4}", "display", 'block', { fromValue: 'none'}], position: 721, duration: 0 },
            { id: "eid43", tween: [ "transform", "${_whiteFilledBalloon2}", "rotateZ", '42deg', { fromValue: '42deg'}], position: 100, duration: 0 },
            { id: "eid13", tween: [ "transform", "${_whiteFilledBalloon2}", "rotateZ", '42deg', { fromValue: '42deg'}], position: 738, duration: 0 },
            { id: "eid173", tween: [ "style", "${_continueGrey}", "opacity", '1', { fromValue: '0'}], position: 1048, duration: 251, easing: "swing" },
            { id: "eid209", tween: [ "style", "${_continueGrey}", "opacity", '0', { fromValue: '1'}], position: 1389, duration: 149, easing: "swing" }         ]
      }
   }
},
"INTROBUILD": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      rect: [60,480,530,501],
      id: 'balloonLowerleft',
      transform: [[-242,-236],{},{},[0.42,0.42]],
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/balloon.svg']
   },
   {
      transform: [[-69,-203],[90],{},[0.246,0.246]],
      type: 'image',
      id: 'balloonlowerRight',
      display: 'none',
      rect: [60,480,530,501],
      fill: ['rgba(0,0,0,0)','images/balloon.svg']
   },
   {
      transform: [[-147,-181],{},{},[0.67,0.67]],
      type: 'image',
      id: 'startTalking2',
      display: 'none',
      rect: [-221,88,605,605],
      fill: ['rgba(0,0,0,0)','images/startTalking.svg']
   },
   {
      transform: [[256,-160],[270],{},[0.69,0.69]],
      type: 'image',
      id: 'balloon2',
      display: 'none',
      rect: [-565,136,530,501],
      fill: ['rgba(0,0,0,0)','images/balloon.svg']
   },
   {
      display: 'none',
      type: 'rect',
      id: 'questionBalloon',
      rect: [138,400,0,0]
   },
   {
      id: 'questionBalloonCopy',
      type: 'rect',
      rect: [138,400,0,0]
   },
   {
      transform: [[330,61],[42],{},[0.3,0.3]],
      type: 'image',
      id: 'talkingLogo2',
      display: 'none',
      rect: [-436,311,434,208],
      fill: ['rgba(0,0,0,0)','images/talkingLogo.svg']
   },
   {
      transform: [[-130,-725],{},{},[0.42,0.42]],
      type: 'image',
      id: 'balloonUpperRight',
      display: 'none',
      rect: [60,480,530,501],
      fill: ['rgba(0,0,0,0)','images/balloon.svg']
   }],
   symbolInstances: [
   {
      id: 'questionBalloonCopy',
      symbolName: 'QUESTIONANSWERBALLOON'
   },
   {
      id: 'questionBalloon',
      symbolName: 'QUESTIONANSWERBALLOON'
   }   ]
   },
   states: {
      "Base State": {
         "${_balloon2}": [
            ["transform", "scaleY", '0.69'],
            ["transform", "translateX", '256.23px'],
            ["transform", "rotateZ", '270deg'],
            ["transform", "scaleX", '0.69'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-160.38px'],
            ["style", "display", 'none']
         ],
         "${_balloonUpperRight}": [
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.02'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '0.02'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-636.27px'],
            ["transform", "translateX", '-264.58px']
         ],
         "${_balloonlowerRight}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '-130.92px'],
            ["transform", "rotateZ", '90deg'],
            ["transform", "scaleX", '0.02'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-251.55px'],
            ["transform", "scaleY", '0.02']
         ],
         "${_talkingLogo2}": [
            ["transform", "scaleY", '0.3'],
            ["transform", "translateX", '330.48px'],
            ["transform", "rotateZ", '42deg'],
            ["transform", "scaleX", '0.3'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '61.33px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '602.459297px'],
            ["style", "width", '574.13685px']
         ],
         "${_startTalking2}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '-6.66px'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '0.03'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-9.66px'],
            ["transform", "scaleY", '0.03']
         ],
         "${_questionBalloonCopy}": [
            ["style", "display", 'block'],
            ["style", "opacity", '1']
         ],
         "${_balloonLowerleft}": [
            ["transform", "scaleY", '0.04'],
            ["transform", "rotateZ", '0'],
            ["transform", "scaleX", '0.04'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-138.47px'],
            ["transform", "translateX", '-308.84px']
         ],
         "${_questionBalloon}": [
            ["style", "display", 'none'],
            ["style", "opacity", '1']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 2295.1741484581,
         autoPlay: true,
         timeline: [
            { id: "eid142", tween: [ "transform", "${_balloonUpperRight}", "scaleX", '0.42', { fromValue: '0.02'}], position: 1314, duration: 414, easing: "swing" },
            { id: "eid70", tween: [ "transform", "${_balloonLowerleft}", "translateX", '-246.5px', { fromValue: '-308.84px'}], position: 0, duration: 431, easing: "swing" },
            { id: "eid76", tween: [ "transform", "${_startTalking2}", "scaleX", '0.57999', { fromValue: '0.03'}], position: 475, duration: 700, easing: "swing" },
            { id: "eid239", tween: [ "style", "${_balloon2}", "opacity", '1', { fromValue: '0'}], position: 1899, duration: 118, easing: "swing" },
            { id: "eid144", tween: [ "style", "${_questionBalloon}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid147", tween: [ "style", "${_questionBalloon}", "display", 'block', { fromValue: 'none'}], position: 693, duration: 0 },
            { id: "eid143", tween: [ "transform", "${_balloonUpperRight}", "scaleY", '0.42', { fromValue: '0.02'}], position: 1314, duration: 414, easing: "swing" },
            { id: "eid61", tween: [ "transform", "${_balloonLowerleft}", "rotateZ", '0', { fromValue: '0deg'}], position: 0, duration: 0 },
            { id: "eid16", tween: [ "transform", "${_balloonLowerleft}", "rotateZ", '0', { fromValue: '0deg'}], position: 431, duration: 0 },
            { id: "eid235", tween: [ "style", "${_balloon2}", "display", 'block', { fromValue: 'none'}], position: 1899, duration: 0, easing: "swing" },
            { id: "eid198", tween: [ "style", "${_talkingLogo2}", "display", 'block', { fromValue: 'none'}], position: 1696, duration: 0, easing: "swing" },
            { id: "eid77", tween: [ "transform", "${_startTalking2}", "scaleY", '0.57999', { fromValue: '0.03'}], position: 475, duration: 700, easing: "swing" },
            { id: "eid49", tween: [ "transform", "${_startTalking2}", "rotateZ", '0', { fromValue: '0deg'}], position: 475, duration: 0 },
            { id: "eid14", tween: [ "transform", "${_startTalking2}", "rotateZ", '0', { fromValue: '0deg'}], position: 1176, duration: 0 },
            { id: "eid153", tween: [ "style", "${_balloonUpperRight}", "display", 'block', { fromValue: 'none'}], position: 1314, duration: 0 },
            { id: "eid73", tween: [ "transform", "${_balloonlowerRight}", "scaleY", '0.24611', { fromValue: '0.02'}], position: 431, duration: 500, easing: "swing" },
            { id: "eid78", tween: [ "transform", "${_startTalking2}", "translateX", '-122.55px', { fromValue: '-6.66px'}], position: 475, duration: 700, easing: "swing" },
            { id: "eid74", tween: [ "transform", "${_balloonlowerRight}", "translateX", '-69.34px', { fromValue: '-130.92px'}], position: 431, duration: 500, easing: "swing" },
            { id: "eid48", tween: [ "style", "${_startTalking2}", "opacity", '1', { fromValue: '1'}], position: 475, duration: 0 },
            { id: "eid32", tween: [ "style", "${_startTalking2}", "opacity", '1', { fromValue: '1'}], position: 1176, duration: 0 },
            { id: "eid68", tween: [ "transform", "${_balloonLowerleft}", "scaleX", '0.42', { fromValue: '0.04'}], position: 0, duration: 431, easing: "swing" },
            { id: "eid137", tween: [ "transform", "${_balloonUpperRight}", "translateY", '-732.01px', { fromValue: '-636.27px'}], position: 1314, duration: 414, easing: "swing" },
            { id: "eid136", tween: [ "transform", "${_balloonUpperRight}", "translateX", '-158.94px', { fromValue: '-264.58px'}], position: 1314, duration: 414, easing: "swing" },
            { id: "eid79", tween: [ "transform", "${_startTalking2}", "translateY", '-157.05px', { fromValue: '-9.66px'}], position: 475, duration: 700, easing: "swing" },
            { id: "eid54", tween: [ "style", "${_balloonlowerRight}", "opacity", '1', { fromValue: '1'}], position: 431, duration: 0 },
            { id: "eid33", tween: [ "style", "${_balloonlowerRight}", "opacity", '1', { fromValue: '1'}], position: 931, duration: 0 },
            { id: "eid55", tween: [ "transform", "${_balloonlowerRight}", "rotateZ", '90deg', { fromValue: '90deg'}], position: 431, duration: 0 },
            { id: "eid15", tween: [ "transform", "${_balloonlowerRight}", "rotateZ", '90deg', { fromValue: '90deg'}], position: 931, duration: 0 },
            { id: "eid69", tween: [ "transform", "${_balloonLowerleft}", "scaleY", '0.42', { fromValue: '0.04'}], position: 0, duration: 431, easing: "swing" },
            { id: "eid72", tween: [ "transform", "${_balloonlowerRight}", "scaleX", '0.24611', { fromValue: '0.02'}], position: 431, duration: 500, easing: "swing" },
            { id: "eid151", tween: [ "style", "${_balloonlowerRight}", "display", 'block', { fromValue: 'none'}], position: 431, duration: 0 },
            { id: "eid75", tween: [ "transform", "${_balloonlowerRight}", "translateY", '-203.71px', { fromValue: '-251.55px'}], position: 431, duration: 500, easing: "swing" },
            { id: "eid145", tween: [ "style", "${_startTalking2}", "display", 'none', { fromValue: 'none'}], position: 0, duration: 0 },
            { id: "eid146", tween: [ "style", "${_startTalking2}", "display", 'block', { fromValue: 'none'}], position: 431, duration: 0 },
            { id: "eid201", tween: [ "style", "${_talkingLogo2}", "opacity", '1', { fromValue: '0'}], position: 1696, duration: 184, easing: "swing" },
            { id: "eid71", tween: [ "transform", "${_balloonLowerleft}", "translateY", '-231.51px', { fromValue: '-138.47px'}], position: 0, duration: 431, easing: "swing" },
            { id: "eid60", tween: [ "style", "${_balloonLowerleft}", "opacity", '1', { fromValue: '1'}], position: 0, duration: 0 },
            { id: "eid34", tween: [ "style", "${_balloonLowerleft}", "opacity", '1', { fromValue: '1'}], position: 431, duration: 0 },
            { id: "eid152", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_questionBalloon}', ['INTRO'] ], ""], position: 678.09685071131 }         ]
      }
   }
},
"logoROllOut": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      id: 'logoTalkBlueHead',
      type: 'image',
      rect: [4,69,65,71],
      fill: ['rgba(0,0,0,0)','images/logoTalkBlueHead.svg']
   },
   {
      transform: [[-4,-69]],
      id: 'logoTalkWhiteHead',
      type: 'image',
      rect: [4,69,70,77],
      fill: ['rgba(0,0,0,0)','images/logoTalkWhiteHead.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_logoTalkWhiteHead}": [
            ["transform", "translateX", '-2.07px'],
            ["transform", "rotateZ", '90deg'],
            ["transform", "scaleX", '0.05'],
            ["transform", "translateY", '-40px'],
            ["transform", "scaleY", '0.05']
         ],
         "${_logoTalkBlueHead}": [
            ["transform", "translateX", '1.27px'],
            ["transform", "rotateZ", '-90deg'],
            ["transform", "scaleX", '0.05'],
            ["transform", "translateY", '-26px'],
            ["transform", "scaleY", '0.05']
         ],
         "${symbolSelector}": [
            ["style", "height", '140.18px'],
            ["style", "width", '70px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 646.11648257603,
         autoPlay: true,
         labels: {
            "rollOut": 11
         },
         timeline: [
            { id: "eid275", tween: [ "transform", "${_logoTalkWhiteHead}", "scaleY", '1', { fromValue: '0.05'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid276", tween: [ "transform", "${_logoTalkBlueHead}", "scaleX", '1', { fromValue: '0.05'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid280", tween: [ "transform", "${_logoTalkBlueHead}", "translateY", '0px', { fromValue: '-26px'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid277", tween: [ "transform", "${_logoTalkBlueHead}", "scaleY", '1', { fromValue: '0.05'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid291", tween: [ "transform", "${_logoTalkWhiteHead}", "rotateZ", '0deg', { fromValue: '90deg'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid279", tween: [ "transform", "${_logoTalkWhiteHead}", "translateY", '-69.18px', { fromValue: '-40px'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid278", tween: [ "transform", "${_logoTalkWhiteHead}", "translateX", '-4px', { fromValue: '-2.07px'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid288", tween: [ "transform", "${_logoTalkBlueHead}", "rotateZ", '0deg', { fromValue: '-90deg'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid281", tween: [ "transform", "${_logoTalkBlueHead}", "translateX", '0px', { fromValue: '1.27px'}], position: 22, duration: 623, easing: "swing" },
            { id: "eid274", tween: [ "transform", "${_logoTalkWhiteHead}", "scaleX", '1', { fromValue: '0.05'}], position: 22, duration: 623, easing: "swing" }         ]
      }
   }
},
"CorrectAnimation": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[43,-17]],
      type: 'image',
      id: 'correct',
      display: 'none',
      rect: [184,52,229,79],
      fill: ['rgba(0,0,0,0)','images/correct.svg']
   },
   {
      transform: [[-23,19]],
      rect: [25,16,217,74],
      id: 'blueCorrectMask',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(28,63,148,1)']
   },
   {
      transform: [[89,13]],
      display: 'none',
      type: 'rect',
      id: 'logoROllOut',
      rect: [84,-9,0,0]
   }],
   symbolInstances: [
   {
      id: 'logoROllOut',
      symbolName: 'logoROllOut'
   }   ]
   },
   states: {
      "Base State": {
         "${_correct}": [
            ["style", "display", 'none'],
            ["transform", "translateY", '-17.2px'],
            ["transform", "translateX", '-182px']
         ],
         "${_logoROllOut}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '89.36px'],
            ["transform", "translateY", '13.12px']
         ],
         "${symbolSelector}": [
            ["style", "height", '141.179999px'],
            ["style", "width", '241.359995px']
         ],
         "${_blueCorrectMask}": [
            ["style", "height", '74px'],
            ["transform", "translateY", '19.2px'],
            ["transform", "translateX", '-23.13px'],
            ["style", "width", '217px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1367.7973462291,
         autoPlay: true,
         labels: {
            "answer": 38
         },
         timeline: [
            { id: "eid298", tween: [ "transform", "${_correct}", "translateX", '43.81px', { fromValue: '-182px'}], position: 381, duration: 986, easing: "swing" },
            { id: "eid311", tween: [ "style", "${_logoROllOut}", "display", 'block', { fromValue: 'none'}], position: 21, duration: 0, easing: "swing" },
            { id: "eid293", tween: [ "style", "${_correct}", "display", 'block', { fromValue: 'none'}], position: 381, duration: 0, easing: "swing" }         ]
      }
   }
},
"incorrwectAnimate": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'image',
      id: 'incorrectAnswer',
      rect: [278,88,825,51],
      transform: [[-722,-37]],
      fill: ['rgba(0,0,0,0)','images/incorrectAnswer.svg']
   },
   {
      transform: [[22,7]],
      rect: [-538,28,931,61],
      id: 'Rectangle5',
      stroke: [0,'rgb(0, 0, 0)','none'],
      type: 'rect',
      fill: ['rgba(28,63,148,1)']
   },
   {
      id: 'logoROllOut2',
      type: 'rect',
      rect: [375,45,0,0],
      transform: [[3,-41]]
   }],
   symbolInstances: [
   {
      id: 'logoROllOut2',
      symbolName: 'logoROllOut'
   }   ]
   },
   states: {
      "Base State": {
         "${_Rectangle5}": [
            ["transform", "translateX", '22.55px'],
            ["transform", "translateY", '7.19px']
         ],
         "${_logoROllOut2}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '-41.88px'],
            ["transform", "translateX", '3.64px']
         ],
         "${_incorrectAnswer}": [
            ["style", "display", 'block'],
            ["transform", "translateY", '-37.84px'],
            ["transform", "translateX", '-722.92px']
         ],
         "${symbolSelector}": [
            ["style", "height", '141.179999px'],
            ["style", "width", '823px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1637,
         autoPlay: true,
         labels: {
            "start": 112
         },
         timeline: [
            { id: "eid370", tween: [ "transform", "${_incorrectAnswer}", "translateX", '159px', { fromValue: '-722.92px'}], position: 250, duration: 1387, easing: "swing" },
            { id: "eid363", tween: [ "style", "${_logoROllOut2}", "display", 'block', { fromValue: 'block'}], position: 250, duration: 0, easing: "swing" },
            { id: "eid364", tween: [ "style", "${_incorrectAnswer}", "display", 'block', { fromValue: 'block'}], position: 0, duration: 0, easing: "swing" }         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-29364989");
