/***********************
* Adobe Edge Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
   })("stage");
   //Edge symbol end:'stage'

//=========================================================
   //Edge symbol: 'tweeterTargets'
   (function(symbolName) {

      Symbol.bindElementAction(compId, symbolName, "${_tweeter8target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter8a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter8a").play("open");


      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter1Target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  


sym.getComposition().getStage().$("tweeter1a").css('z-index',myDepth-1);

sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter1a").play("open");






//var myCurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
//var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];

//alert("myCurrentTweet " + myCurrentTweet + " mynextTweet " + mynextTweet);

/*
if(myCurrentTweet != undefined){
	mynextTweet = "Tweeter1";
} else {
	myCurrentTweet = "Tweeter1";
	mynextTweet = undefined;
}	
*/






//sym.getComposition().getStage().getSymbol("Tweeter1").play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter5target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter5a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter5a").play("open");



      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter7target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter7a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter7a").play("open");



      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter3target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter3a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter3a").play("open");

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter2target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter2a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter2a").play("open");


/*
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 1;

var myCurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];

alert("myCurrentTweet " + myCurrentTweet + " mynextTweet " + mynextTweet);

if(myCurrentTweet != undefined){
	mynextTweet = "Tweeter2";
} else {
	myCurrentTweet = "Tweeter2";
	mynextTweet = undefined;
}
	
var infoObject = {
	nextHighest:myDepth,
	currentTweet:myCurrentTweet,
	nextTweet:mynextTweet
};
 
sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
 
sym.getComposition().getStage().$(myCurrentTweet).css('z-index',myDepth);
sym.getComposition().getStage().getSymbol(myCurrentTweet).play();

*/

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter6target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter6a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter6a").play("open");



      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter4target}", "click", function(sym, e) {
var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
myDepth = myDepth + 2;

 
var infoObject = {
	nextHighest:myDepth,
	//currentTweet:myCurrentTweet,
	//nextTweet:mynextTweet
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

sym.getComposition().getStage().$("tweeter4a").css('z-index',myDepth-1);
sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);

sym.getComposition().getStage().getSymbol("tweeter4a").play("open");




      });
      //Edge binding end

   })("tweeterTargets");
   //Edge symbol end:'tweeterTargets'

//=========================================================
   //Edge symbol: 'tweeterTargetsNew'
   (function(symbolName) {

   })("tweeterTargetsNew");
   //Edge symbol end:'tweeterTargetsNew'

//=========================================================
   //Edge symbol: 'effectiveAnimation'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1000, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("effectiveAnimation");
   //Edge symbol end:'effectiveAnimation'

//=========================================================
   //Edge symbol: 'Safeanimation'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1156, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("Safeanimation");
   //Edge symbol end:'Safeanimation'

//=========================================================
   //Edge symbol: 'pageCurl'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1110, function(sym, e) {
         sym.stop('complete');

      });
      //Edge binding end

   })("pageCurl");
   //Edge symbol end:'pageCurl'

//=========================================================
   //Edge symbol: 'pageCurlNewAnimation'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1500, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

   })("pageCurlNewAnimation");
   //Edge symbol end:'pageCurlNewAnimation'

//=========================================================
   //Edge symbol: 'darkerBG'
   (function(symbolName) {

   })("darkerBG");
   //Edge symbol end:'darkerBG'

})(jQuery, AdobeEdge, "EDGE-108138811");