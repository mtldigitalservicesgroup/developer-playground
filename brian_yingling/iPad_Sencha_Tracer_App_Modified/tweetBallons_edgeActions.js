/***********************
* Adobe Edge Composition Actions
*
* Edit this file with caution, being careful to preserve 
* function signatures and comments starting with 'Edge' to maintain the 
* ability to interact with these actions from within Adobe Edge
*
***********************/
(function($, Edge, compId){
var Composition = Edge.Composition, Symbol = Edge.Symbol; // aliases for commonly used Edge classes

   //Edge symbol: 'stage'
   (function(symbolName) {
      
      
      

      

      Symbol.bindElementAction(compId, symbolName, "document", "compositionReady", function(sym, e) {
var infoObject = {
	nextHighest:5000,
	currentTweet:'',
	nextTweet:''
};

sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);

      });
      //Edge binding end

      

      

      

      

      

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         			var infoObject = {
         					nextHighest:500,
         					currentTweet:'',
         					nextTweet:''
         				};
         				 
         				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  

      });
      //Edge binding end

   })("stage");
   //Edge symbol end:'stage'

//=========================================================
   //Edge symbol: 'tweeterTargets'
   (function(symbolName) {

      Symbol.bindElementAction(compId, symbolName, "${_tweeter8target}", "click", function(sym, e) {
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter8a") {
				return;	  
			}
		  		sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
		  		var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
				if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter8a";
				mynextTweet = '';
				
					
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
		

      });
      //Edge binding end

Symbol.bindElementAction(compId, symbolName, "${_tweeter1Target}", "click", function(sym, e) {
					
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter1a") {
				return;	  
			}
	sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
				var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
				if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter1a";
				mynextTweet = '';
					
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter5target}", "click", function(sym, e) {
				console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter7a") {
				return;	  
			}
		  sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
		  			var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
				if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter7a";
				mynextTweet = '';
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter7target}", "click", function(sym, e) {
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter5a") {
				return;	  
			}
		  sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
			var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
			 	if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter5a";
				mynextTweet = '';
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();    

				});
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter3target}", "click", function(sym, e) {
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter3a") {
				return;	  
			}
		  sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
			var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
			var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
			var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
			myDepth = myDepth + 2;
			
			if(mycurrentTweet != ''){
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
			} 
			
			mycurrentTweet = "tweeter3a";
			mynextTweet = '';
				
			var infoObject = {
				nextHighest:myDepth,
				currentTweet:mycurrentTweet,
				nextTweet:mynextTweet
			};
			 
			sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
			sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
			sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
			sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
			

		});
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter2target}", "click", function(sym, e) {
					
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter2a") {
				return;	  
			}
			
			
				sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
				var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
				if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter2a";
				mynextTweet = '';
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				 console.log("clicked " + mycurrentTweet + "  "  + sym.getComposition().getStage().$("tweeterTargets") );

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter6target}", "click", function(sym, e) {
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter6a") {
				return;	  
			}
				sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
				var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
				if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter6a";
				mynextTweet = '';
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter4target}", "click", function(sym, e) {
					console.log("clicked");
					if(sym.getComposition().getStage().$("tweeningHolder").data("isTweening") == "true" || sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"] == "tweeter4a") {
				return;	  
			}
sym.getComposition().getStage().$("tweeningHolder").data("isTweening","true");
				var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
				var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
				var mynextTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextTweet"];
				myDepth = myDepth + 2;
				
			if(mycurrentTweet != ''){
					sym.getComposition().getStage().getSymbol(mycurrentTweet).play();
				} 
				
				mycurrentTweet = "tweeter4a";
				mynextTweet = '';
				
				var infoObject = {
					nextHighest:myDepth,
					currentTweet:mycurrentTweet,
					nextTweet:mynextTweet
				};
				 
				sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);  
				sym.getComposition().getStage().$(mycurrentTweet).css('z-index',myDepth);
				sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth);
				sym.getComposition().getStage().getSymbol(mycurrentTweet).play();

      });
      //Edge binding end

      

      Symbol.bindElementAction(compId, symbolName, "${_movieCloseButton}", "click", function(sym, e) {
         // insert code for mouse click here
         
         var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
         var mycurrentTweet = sym.getComposition().getStage().$("dataholder").data("infoObject")["currentTweet"];
         
         myDepth = myDepth + 2;
         			
         sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         
         		
         var infoObject = {
         	nextHighest:myDepth,
         	currentTweet:'',
         	nextTweet:''
         };
         		 
         sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);			
         sym.getComposition().getStage().getSymbol(mycurrentTweet).play();

      });
      //Edge binding end

   })("tweeterTargets");
   //Edge symbol end:'tweeterTargets'

//=========================================================
   //Edge symbol: 'FirstPhrase'
   (function(symbolName) {

   })("FirstPhrase");
   //Edge symbol end:'FirstPhrase'

//=========================================================
   //Edge symbol: 'pageCurl'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 0, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1250, function(sym, e) {
         sym.stop('complete');

      });
      //Edge binding end

   })("pageCurl");
   //Edge symbol end:'pageCurl'

//=========================================================
   //Edge symbol: 'Tweeter1a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 27, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 631, function(sym, e) {
         sym.stop();
         // insert code here
	
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");


      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1185, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter1Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                 myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         
         sym.getComposition().getStage().getSymbol("tweeter1a").play();
         

      });
      //Edge binding end

   })("Tweeter1a");
   //Edge symbol end:'Tweeter1a'

//=========================================================
   //Edge symbol: 'tweeter8a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 76, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1031, function(sym, e) {
         sym.play("intro");

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 471, function(sym, e) {
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");

         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter8Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                         myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter8a").play();

      });
      //Edge binding end

   })("tweeter8a");
   //Edge symbol end:'tweeter8a'

//=========================================================
   //Edge symbol: 'tweeter7a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 92, function(sym, e) {
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 606, function(sym, e) {
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");

         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1591, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter7Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                        myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter7a").play();

      });
      //Edge binding end

   })("tweeter7a");
   //Edge symbol end:'tweeter7a'

//=========================================================
   //Edge symbol: 'tweeter6a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 89, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1709, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 965, function(sym, e) {
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");

         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter6Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                        myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter6a").play();

      });
      //Edge binding end

   })("tweeter6a");
   //Edge symbol end:'tweeter6a'

//=========================================================
   //Edge symbol: 'tweeter5a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 48, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 980, function(sym, e) {
         sym.stop();
         // insert code here
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");


      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1750, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter5Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                        myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter5a").play();

      });
      //Edge binding end

   })("tweeter5a");
   //Edge symbol end:'tweeter5a'

//=========================================================
   //Edge symbol: 'tweeter4a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 44, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 895, function(sym, e) {
         sym.stop();
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");

         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1528, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter4Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                         myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter4a").play();

      });
      //Edge binding end

   })("tweeter4a");
   //Edge symbol end:'tweeter4a'

//=========================================================
   //Edge symbol: 'tweeter3a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 58, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 690, function(sym, e) {
         sym.stop();
         // insert code here
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");


      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1162, function(sym, e) {
         sym.play('intro')
         
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter3Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                        myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         		
         sym.getComposition().getStage().getSymbol("tweeter3a").play();

      });
      //Edge binding end

   })("tweeter3a");
   //Edge symbol end:'tweeter3a'

//=========================================================
   //Edge symbol: 'tweeter2a'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 30, function(sym, e) {
         sym.stop();
         // insert code here

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 573, function(sym, e) {
         sym.stop();
         // insert code here
         sym.getComposition().getStage().$("tweeningHolder").data("isTweening","false");


      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 1295, function(sym, e) {
         sym.play("intro");
         // insert code here

      });
      //Edge binding end

      Symbol.bindElementAction(compId, symbolName, "${_tweeter2Close}", "click", function(sym, e) {
         // insert code for mouse click here
         	var myDepth = sym.getComposition().getStage().$("dataholder").data("infoObject")["nextHighest"];
                        myDepth = myDepth + 2;
         			
         		var infoObject = {
         			nextHighest:myDepth,
         			currentTweet:'',
         			nextTweet:''
         		};
         		 
         		sym.getComposition().getStage().$("dataholder").data("infoObject",infoObject);		 
         		sym.getComposition().getStage().$("tweeterTargets").css('z-index',myDepth-1);
         		sym.getComposition().getStage().$("greycontinueButton").css('z-index',myDepth);
         
         sym.getComposition().getStage().getSymbol("tweeter2a").play();
         

      });
      //Edge binding end

   })("tweeter2a");
   //Edge symbol end:'tweeter2a'

//=========================================================
   //Edge symbol: 'SafeMovie'
   (function(symbolName) {

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 580, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

      Symbol.bindTriggerAction(compId, symbolName, "Default Timeline", 22, function(sym, e) {
         // insert code here
         sym.stop();

      });
      //Edge binding end

   })("SafeMovie");
   //Edge symbol end:'SafeMovie'

})(jQuery, AdobeEdge, "EDGE-70165390");