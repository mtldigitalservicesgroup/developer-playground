/**
 * Adobe Edge: symbol definitions
 */
(function($, Edge, compId){
//images folder
var im='images/';

var fonts = {};


var resources = [
];
var symbols = {
"stage": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
         dom: [
         {
            id:'backgroundTexture',
            type:'image',
            rect:[-85,191,1024,736],
            fill:["rgba(0,0,0,0)",im+"backgroundTexture.svg"],
            transform:[[108,-175],[0,0],[0],[1.044,1.044]]
         },
         {
            id:'forTeens',
            type:'image',
            rect:[303,206,1350,108],
            fill:["rgba(0,0,0,0)",im+"forTeens.svg"],
            transform:[[-438,107],[],[],[0.5,0.5]]
         },
         {
            id:'purpleSkin',
            display:'none',
            type:'image',
            rect:[688,93,290,125],
            fill:["rgba(0,0,0,0)",im+"purpleSkin.png"],
            transform:[[52,211],[],[],[0.5,0.5]]
         },
         {
            id:'purpleTheir',
            display:'none',
            type:'image',
            rect:[498,81,300,125],
            fill:["rgba(0,0,0,0)",im+"purpleTheir.png"],
            transform:[[78,223],[],[],[0.5,0.5]]
         },
         {
            id:'purpleOn',
            display:'none',
            type:'image',
            rect:[206,192,199,117],
            fill:["rgba(0,0,0,0)",im+"purpleOn.png"],
            transform:[[294,120],[],[],[0.5,0.5]]
         },
         {
            id:'purpleFelt',
            display:'none',
            type:'image',
            rect:[434,193,240,125],
            fill:["rgba(0,0,0,0)",im+"purpleFelt.png"],
            transform:[[-64,111],[],[],[0.5,0.5]]
         },
         {
            id:'purplejust',
            display:'none',
            type:'image',
            rect:[584,228,254,120],
            fill:["rgba(0,0,0,0)",im+"purplejust.png"],
            transform:[[-354,83],[],[],[0.5,0.5]]
         },
         {
            id:'purpleArent',
            display:'none',
            type:'image',
            rect:[198,229,380,120],
            fill:["rgba(0,0,0,0)",im+"purpleArent.png"],
            transform:[[-195,82],[],[],[0.5,0.5]]
         },
         {
            id:'teen2',
            display:'none',
            type:'image',
            rect:[-37,264,530,509],
            fill:["rgba(0,0,0,0)",im+"teen2.png"],
            transform:[[645,-379],[44],[],[0.579,0.579]]
         },
         {
            id:'teen5',
            display:'none',
            type:'image',
            rect:[-37,264,644,606],
            fill:["rgba(0,0,0,0)",im+"teen5.png"],
            transform:[[16,-458],[],[],[0.53,0.53]]
         },
         {
            id:'teen8',
            display:'none',
            type:'image',
            rect:[-37,264,460,397],
            fill:["rgba(0,0,0,0)",im+"teen8.png"],
            transform:[[-115,-222],[],[],[0.46,0.46]]
         },
         {
            id:'teen4',
            display:'none',
            type:'image',
            rect:[-37,264,452,362],
            fill:["rgba(0,0,0,0)",im+"teen4.png"],
            transform:[[653,-29],[-24],[],[0.5,0.5]]
         },
         {
            id:'teen1',
            display:'none',
            type:'image',
            rect:[-37,264,403,314],
            fill:["rgba(0,0,0,0)",im+"teen1.png"],
            transform:[[536,45],[],[],[0.5,0.5]]
         },
         {
            id:'teen3',
            display:'none',
            type:'image',
            rect:[-37,264,638,497],
            fill:["rgba(0,0,0,0)",im+"teen3.png"],
            transform:[[627,126],[],[],[0.34,0.34]]
         },
         {
            id:'teen7',
            display:'none',
            type:'image',
            rect:[-37,264,543,447],
            fill:["rgba(0,0,0,0)",im+"teen7.png"],
            transform:[[-153,-28],[],[],[0.405,0.405]]
         },
         {
            id:'teen6',
            display:'none',
            type:'image',
            rect:[-37,264,572,476],
            fill:["rgba(0,0,0,0)",im+"teen6.png"],
            transform:[[6,123],[],[],[0.462,0.462]]
         },
         {
            id:'acneCanLeaveA',
            display:'none',
            type:'image',
            rect:[327,160,843,106],
            fill:["rgba(0,0,0,0)",im+"acneCanLeaveA.svg"],
            transform:[[-235,61],[],[],[0.5,0.5]]
         },
         {
            id:'visibleMark',
            display:'none',
            type:'image',
            rect:[408,423,1135,180],
            fill:["rgba(0,0,0,0)",im+"visibleMark.png"],
            transform:[[-471,-134],[],[],[0.5,0.5]]
         },
         {
            id:'mark2',
            type:'image',
            rect:[363,328,40,40],
            fill:["rgba(0,0,0,0)",im+"mark2.png"],
            transform:[[-4,-1],[],[],[0.5,0.5]]
         },
         {
            id:'mark1',
            type:'image',
            rect:[274,323,40,40],
            fill:["rgba(0,0,0,0)",im+"mark1.png"],
            transform:[[-1,4],[],[],[0.5,0.5]]
         },
         {
            id:'onTheirLives',
            type:'image',
            rect:[283,478,610,87],
            fill:["rgba(0,0,0,0)",im+"onTheirLives.svg"],
            transform:[[-66,-32],[],[],[0.5,0.5]]
         },
         {
            id:'largeTweeter1',
            display:'none',
            type:'image',
            rect:[-304,227,836,601],
            fill:["rgba(0,0,0,0)",im+"largeTweeter1.png"],
            transform:[[78,-135],[14],[],[0.32,0.32]]
         },
         {
            id:'largeTweeter2',
            display:'none',
            type:'image',
            rect:[-304,227,845,603],
            fill:["rgba(0,0,0,0)",im+"largeTweeter2.png"],
            transform:[[507,-428],[15],[],[0.37,0.37]]
         },
         {
            id:'largeTweeter3',
            display:'none',
            type:'image',
            rect:[-304,227,836,602],
            fill:["rgba(0,0,0,0)",im+"largeTweeter3.png"],
            transform:[[148,56],[-9],[],[0.27,0.27]]
         },
         {
            id:'largeTweeter4',
            display:'none',
            type:'image',
            rect:[-304,227,835,597],
            fill:["rgba(0,0,0,0)",im+"largeTweeter4.png"],
            transform:[[659,-45],[-10],[],[0.22,0.22]]
         },
         {
            id:'largeTweeter5',
            display:'none',
            type:'image',
            rect:[-304,227,832,595],
            fill:["rgba(0,0,0,0)",im+"largeTweeter5.png"],
            transform:[[356,-155],[-20],[],[0.37,0.37]]
         },
         {
            id:'largeTweeter6',
            display:'none',
            type:'image',
            rect:[-304,227,833,595],
            fill:["rgba(0,0,0,0)",im+"largeTweeter6.png"],
            transform:[[756,-195],[],[],[0.3,0.3]]
         },
         {
            id:'largeTweeter7',
            display:'none',
            type:'image',
            rect:[-304,227,839,603],
            fill:["rgba(0,0,0,0)",im+"largeTweeter7.png"],
            transform:[[451,96],[21],[],[0.37,0.37]]
         },
         {
            id:'largeTweeter8',
            display:'none',
            type:'image',
            rect:[-304,227,866,625],
            fill:["rgba(0,0,0,0)",im+"largeTweeter8.png"],
            transform:[[125,-330],[-7],[],[0.34,0.34]]
         },
         {
            id:'tweeterTargetsNew',
            type:'rect',
            rect:[-393,-402,0,0],
            transform:[[-64,-86]]
         },
         {
            id:'patientsNeed',
            display:'none',
            type:'image',
            rect:[82,219,1290,115],
            fill:["rgba(0,0,0,0)",im+"patientsNeed.png"],
            transform:[[-196,8],[],[],[0.5,0.5]]
         },
         {
            id:'effectiveAnimation',
            display:'none',
            type:'rect',
            rect:[149,336,0,0]
         },
         {
            id:'Safeanimation',
            display:'none',
            type:'rect',
            rect:[488,334,0,0]
         },
         {
            id:'simple',
            display:'none',
            type:'image',
            rect:[-153,433,525,115],
            fill:["rgba(0,0,0,0)",im+"simple.png"],
            transform:[[657,-129],[],[],[0.48,0.48]]
         },
         {
            id:'Rectangle',
            display:'none',
            type:'rect',
            rect:[-12,-3,1043,775],
            fill:["rgba(126,101,155,1.00)"],
            stroke:[0,"rgb(0, 0, 0)","none"],
            transform:[[1053,0],[0,0],[0],[1,1]]
         },
         {
            id:'backgroundTextureDarker2',
            display:'none',
            type:'image',
            rect:[45,148,1024,736],
            fill:["rgba(0,0,0,0)",im+"backgroundTextureDarker.svg"],
            transform:[[-45,-130],[],[],[1,1.044]]
         },
         {
            id:'onceDailySubhead',
            display:'none',
            type:'image',
            rect:[904,1027,1200,100],
            fill:["rgba(0,0,0,0)",im+"onceDailySubhead.png"],
            transform:[[-844,-622],[],[],[0.5,0.5]]
         },
         {
            id:'FindTheRightChemistry',
            display:'none',
            type:'image',
            rect:[995,511,1040,300],
            fill:["rgba(0,0,0,0)",im+"FindTheRightChemistry.png"],
            transform:[[-900,-308],[],[],[0.5,0.5]]
         },
         {
            id:'purpleBlend2Copy',
            display:'none',
            type:'image',
            rect:[301,758,1525,300],
            fill:["rgba(0,0,0,0)",im+"purpleBlend.png"],
            transform:[[-557,-555],[],[],[0.5,0.975]]
         },
         {
            id:'pump',
            display:'none',
            type:'image',
            rect:[-495,-81,735,1040],
            fill:["rgba(0,0,0,0)",im+"pump.png"],
            transform:[[311,-41],[],[],[0.5,0.5]]
         },
         {
            id:'purpleBlend2',
            display:'none',
            type:'image',
            rect:[301,758,1525,300],
            fill:["rgba(0,0,0,0)",im+"purpleBlend.png"],
            transform:[[-852,-426],[-90]]
         },
         {
            id:'skipQuizButton',
            type:'image',
            rect:[7,54,106,68],
            fill:["rgba(0,0,0,0)",im+"skipQuizButton.png"],
            transform:[[2,-39],[],[],[0.5,0.5]]
         }],
         symbolInstances: [
         {
            id:'Safeanimation',
            symbolName:'Safeanimation'
         },
         {
            id:'effectiveAnimation',
            symbolName:'effectiveAnimation'
         },
         {
            id:'tweeterTargetsNew',
            symbolName:'tweeterTargetsNew'
         }
         ]
      },
   states: {
      "Base State": {
         "${_largeTweeter1}": [
            ["transform", "scaleY", '0.32'],
            ["transform", "rotateZ", '29deg'],
            ["transform", "scaleX", '0.32'],
            ["style", "display", 'none'],
            ["transform", "translateY", '309.63px'],
            ["transform", "translateX", '-234.54px']
         ],
         "${_purpleBlend2}": [
            ["transform", "translateX", '-852.03px'],
            ["transform", "rotateZ", '-90deg'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '1'],
            ["transform", "translateY", '-218px'],
            ["transform", "scaleX", '1']
         ],
         "${_largeTweeter8}": [
            ["transform", "scaleY", '0.34'],
            ["transform", "rotateZ", '30deg'],
            ["transform", "scaleX", '0.34'],
            ["style", "display", 'none'],
            ["transform", "translateY", '-731.91px'],
            ["transform", "translateX", '50.53px']
         ],
         "body": [
            ["color", "background-color", 'rgba(0,0,0,0)']
         ],
         "${_largeTweeter5}": [
            ["transform", "scaleY", '0.37'],
            ["transform", "rotateZ", '41deg'],
            ["transform", "scaleX", '0.37'],
            ["style", "display", 'none'],
            ["transform", "translateY", '436.52px'],
            ["transform", "translateX", '333.3px']
         ],
         "${_teen7}": [
            ["transform", "translateX", '0.41px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.40561'],
            ["transform", "translateY", '495.45px'],
            ["transform", "scaleX", '0.40561']
         ],
         "${_skipQuizButton}": [
            ["transform", "scaleX", '0.5'],
            ["transform", "translateX", '2.54px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateY", '-39.25px']
         ],
         "${_onTheirLives}": [
            ["transform", "translateX", '-66.4px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-32.7px'],
            ["style", "display", 'block']
         ],
         "${_largeTweeter7}": [
            ["transform", "scaleY", '0.37'],
            ["transform", "rotateZ", '-45deg'],
            ["transform", "scaleX", '0.37'],
            ["style", "display", 'none'],
            ["transform", "translateY", '430.48px'],
            ["transform", "translateX", '657.37px']
         ],
         "${_purpleBlend2Copy}": [
            ["transform", "translateX", '-530.68px'],
            ["transform", "rotateZ", '0deg'],
            ["style", "display", 'none'],
            ["transform", "scaleX", '1'],
            ["transform", "translateY", '-555.84px'],
            ["transform", "scaleY", '1']
         ],
         "${_effectiveAnimation}": [
            ["style", "display", 'none']
         ],
         "${_teen1}": [
            ["transform", "translateX", '365.71px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateY", '465.49px'],
            ["transform", "scaleX", '0.5']
         ],
         "${_teen4}": [
            ["transform", "translateX", '998.04px'],
            ["transform", "rotateZ", '-24deg'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.5'],
            ["transform", "translateY", '7.51px'],
            ["transform", "scaleX", '0.5']
         ],
         "${_mark2}": [
            ["transform", "translateX", '-4px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-1px'],
            ["style", "display", 'block']
         ],
         "${_backgroundTexture}": [
            ["transform", "scaleX", '1.04489'],
            ["transform", "scaleY", '1.04489'],
            ["transform", "translateX", '108.02px'],
            ["transform", "translateY", '-175.84px']
         ],
         "${_visibleMark}": [
            ["transform", "translateX", '-471.95px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-134.4px'],
            ["style", "display", 'none']
         ],
         "${_largeTweeter2}": [
            ["transform", "scaleY", '0.37'],
            ["transform", "rotateZ", '-24deg'],
            ["transform", "scaleX", '0.37'],
            ["style", "display", 'none'],
            ["transform", "translateY", '-668.73px'],
            ["transform", "translateX", '-158.31px']
         ],
         "${_largeTweeter4}": [
            ["transform", "scaleY", '0.22'],
            ["transform", "rotateZ", '29deg'],
            ["transform", "scaleX", '0.22'],
            ["style", "display", 'none'],
            ["transform", "translateY", '291px'],
            ["transform", "translateX", '999.58px']
         ],
         "${_teen5}": [
            ["transform", "translateX", '-624.63px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.53'],
            ["transform", "translateY", '-189.4px'],
            ["transform", "scaleX", '0.53']
         ],
         "${_purpleArent}": [
            ["transform", "translateX", '-195.51px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '82.31px'],
            ["style", "display", 'none']
         ],
         "${_FindTheRightChemistry}": [
            ["transform", "translateX", '-900.67px'],
            ["transform", "scaleY", '0.5'],
            ["style", "display", 'none'],
            ["style", "opacity", '1'],
            ["transform", "translateY", '-308.72px'],
            ["transform", "scaleX", '0.5']
         ],
         "${_backgroundTextureDarker2}": [
            ["transform", "translateX", '-45px'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-130.16px'],
            ["transform", "scaleY", '1.04489']
         ],
         "${_teen2}": [
            ["transform", "translateX", '175.89px'],
            ["transform", "rotateZ", '44deg'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.57999'],
            ["transform", "translateY", '383.93px'],
            ["transform", "scaleX", '0.57999']
         ],
         "${_purpleTheir}": [
            ["transform", "translateX", '78.98px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '223.72px'],
            ["style", "display", 'none']
         ],
         "${_simple}": [
            ["transform", "translateX", '657.41px'],
            ["transform", "scaleY", '0.48'],
            ["transform", "scaleX", '0.48'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-129.66px'],
            ["style", "display", 'none']
         ],
         "${_mark1}": [
            ["transform", "translateX", '-1px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '4px'],
            ["style", "display", 'block']
         ],
         "${_Rectangle}": [
            ["style", "display", 'none'],
            ["transform", "translateX", '1053.16px'],
            ["color", "background-color", 'rgba(126,101,155,1.00)']
         ],
         "${_patientsNeed}": [
            ["transform", "translateX", '-196.97px'],
            ["style", "display", 'none'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '8.76px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_pump}": [
            ["transform", "translateX", '311.24px'],
            ["transform", "scaleX", '0.5'],
            ["style", "display", 'none'],
            ["transform", "translateY", '-41.16px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_teen3}": [
            ["transform", "translateX", '765.01px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.34'],
            ["transform", "translateY", '380.42px'],
            ["transform", "scaleX", '0.34']
         ],
         "${_purpleOn}": [
            ["transform", "translateX", '294.29px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '120.21px'],
            ["style", "display", 'none']
         ],
         "${_teen6}": [
            ["transform", "translateX", '385.22px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.46234'],
            ["transform", "translateY", '513.98px'],
            ["transform", "scaleX", '0.46234']
         ],
         "${_largeTweeter3}": [
            ["transform", "scaleY", '0.27'],
            ["transform", "rotateZ", '-40deg'],
            ["transform", "scaleX", '0.27'],
            ["style", "display", 'none'],
            ["transform", "translateY", '405.92px'],
            ["transform", "translateX", '339.05px']
         ],
         "${_teen8}": [
            ["transform", "translateX", '68.2px'],
            ["transform", "rotateZ", '0'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.46'],
            ["transform", "translateY", '479.16px'],
            ["transform", "scaleX", '0.46']
         ],
         "${_purplejust}": [
            ["transform", "translateX", '-354.69px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '83.3px'],
            ["style", "display", 'none']
         ],
         "${_acneCanLeaveA}": [
            ["transform", "translateX", '-235.97px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '61.39px'],
            ["style", "display", 'none']
         ],
         "${_Safeanimation}": [
            ["style", "display", 'none']
         ],
         "${_purpleFelt}": [
            ["transform", "translateX", '-64.2px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '111.79px'],
            ["style", "display", 'none']
         ],
         "${_forTeens}": [
            ["transform", "translateX", '-438.9px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '107.79px'],
            ["style", "display", 'block']
         ],
         "${_largeTweeter6}": [
            ["transform", "scaleY", '0.3'],
            ["transform", "rotateZ", '42deg'],
            ["transform", "scaleX", '0.3'],
            ["style", "display", 'none'],
            ["transform", "translateY", '-27.31px'],
            ["transform", "translateX", '1127.38px']
         ],
         "${_Stage}": [
            ["style", "height", '769px'],
            ["style", "width", '1025px'],
            ["color", "background-color", 'rgba(255,255,255,1)'],
            ["style", "overflow", 'hidden']
         ],
         "${_tweeterTargetsNew}": [
            ["transform", "translateX", '-64.87px'],
            ["transform", "scaleX", '0.48'],
            ["style", "display", 'block'],
            ["transform", "translateY", '-86.64px'],
            ["transform", "scaleY", '0.5']
         ],
         "${_onceDailySubhead}": [
            ["transform", "translateX", '-844.84px'],
            ["transform", "scaleY", '0.5'],
            ["style", "display", 'none'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '-622.33px'],
            ["transform", "scaleX", '0.5']
         ],
         "${_purpleSkin}": [
            ["transform", "translateX", '52.29px'],
            ["transform", "scaleY", '0.5'],
            ["transform", "scaleX", '0.5'],
            ["style", "opacity", '0'],
            ["transform", "translateY", '211.7px'],
            ["style", "display", 'none']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 27000,
         autoPlay: true,
         timeline: [
            { id: "eid14", tween: [ "style", "${_purplejust}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid161", tween: [ "style", "${_purplejust}", "display", 'none', { fromValue: 'block'}], position: 2965, duration: 0 },
            { id: "eid179", tween: [ "style", "${_teen4}", "display", 'block', { fromValue: 'none'}], position: 3745, duration: 0 },
            { id: "eid627", tween: [ "style", "${_teen4}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid88", tween: [ "transform", "${_purpleOn}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid358", tween: [ "transform", "${_largeTweeter2}", "rotateZ", '15deg', { fromValue: '-24deg'}], position: 5954, duration: 410, easing: "swing" },
            { id: "eid364", tween: [ "transform", "${_largeTweeter8}", "rotateZ", '-7deg', { fromValue: '30deg'}], position: 8216, duration: 410, easing: "swing" },
            { id: "eid138", tween: [ "style", "${_mark2}", "opacity", '1', { fromValue: '0'}], position: 3050, duration: 130 },
            { id: "eid480", tween: [ "style", "${_mark2}", "opacity", '0', { fromValue: '1'}], position: 6410, duration: 1000 },
            { id: "eid362", tween: [ "transform", "${_largeTweeter6}", "rotateZ", '0deg', { fromValue: '42deg'}], position: 7499, duration: 410, easing: "swing" },
            { id: "eid219", tween: [ "transform", "${_teen1}", "rotateZ", '0', { fromValue: '0deg'}], position: 4009, duration: 0 },
            { id: "eid201", tween: [ "transform", "${_teen1}", "rotateZ", '0', { fromValue: '0deg'}], position: 4789, duration: 0 },
            { id: "eid172", tween: [ "style", "${_onTheirLives}", "opacity", '1', { fromValue: '0'}], position: 4250, duration: 322 },
            { id: "eid478", tween: [ "style", "${_onTheirLives}", "opacity", '0', { fromValue: '1'}], position: 6410, duration: 1000 },
            { id: "eid282", tween: [ "transform", "${_tweeterTargetsNew}", "scaleY", '0.5', { fromValue: '0.5'}], position: 5891, duration: 0 },
            { id: "eid152", tween: [ "style", "${_visibleMark}", "display", 'block', { fromValue: 'none'}], position: 3465, duration: 0 },
            { id: "eid621", tween: [ "style", "${_visibleMark}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid156", tween: [ "style", "${_visibleMark}", "opacity", '1', { fromValue: '0'}], position: 3465, duration: 566 },
            { id: "eid481", tween: [ "style", "${_visibleMark}", "opacity", '0', { fromValue: '1'}], position: 6410, duration: 1000 },
            { id: "eid240", tween: [ "transform", "${_teen7}", "translateY", '-28.15px', { fromValue: '495.45px'}], position: 4596, duration: 408, easing: "swing" },
            { id: "eid105", tween: [ "transform", "${_purplejust}", "translateX", '-419.21px', { fromValue: '-354.69px'}], position: 2573, duration: 392 },
            { id: "eid12", tween: [ "style", "${_purpleOn}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid157", tween: [ "style", "${_purpleOn}", "display", 'none', { fromValue: 'block'}], position: 3145, duration: 0 },
            { id: "eid107", tween: [ "transform", "${_purpleFelt}", "translateX", '-259.6px', { fromValue: '-64.2px'}], position: 2573, duration: 392 },
            { id: "eid117", tween: [ "style", "${_mark2}", "display", 'block', { fromValue: 'block'}], position: 3050, duration: 0 },
            { id: "eid620", tween: [ "style", "${_mark2}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid225", tween: [ "transform", "${_teen3}", "rotateZ", '0', { fromValue: '0deg'}], position: 4301, duration: 0 },
            { id: "eid203", tween: [ "transform", "${_teen3}", "rotateZ", '0', { fromValue: '0deg'}], position: 5140, duration: 0 },
            { id: "eid176", tween: [ "style", "${_teen6}", "display", 'block', { fromValue: 'none'}], position: 4887, duration: 0 },
            { id: "eid623", tween: [ "style", "${_teen6}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid13", tween: [ "style", "${_purpleFelt}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid162", tween: [ "style", "${_purpleFelt}", "display", 'none', { fromValue: 'block'}], position: 2965, duration: 0 },
            { id: "eid87", tween: [ "transform", "${_purpleOn}", "scaleX", '0.08', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid95", tween: [ "transform", "${_purpleArent}", "scaleX", '0.03157', { fromValue: '0.5'}], position: 2573, duration: 326 },
            { id: "eid239", tween: [ "transform", "${_teen7}", "translateX", '-153.13px', { fromValue: '0.41px'}], position: 4596, duration: 408, easing: "swing" },
            { id: "eid173", tween: [ "style", "${_teen2}", "display", 'block', { fromValue: 'none'}], position: 2884, duration: 0 },
            { id: "eid630", tween: [ "style", "${_teen2}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid378", tween: [ "style", "${_Safeanimation}", "display", 'block', { fromValue: 'none'}], position: 11136, duration: 0 },
            { id: "eid344", tween: [ "transform", "${_largeTweeter2}", "translateY", '-428.49px', { fromValue: '-668.73px'}], position: 5954, duration: 410, easing: "swing" },
            { id: "eid174", tween: [ "style", "${_teen8}", "display", 'block', { fromValue: 'none'}], position: 3385, duration: 0 },
            { id: "eid628", tween: [ "style", "${_teen8}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid552", tween: [ "style", "${_onceDailySubhead}", "display", 'block', { fromValue: 'none'}], position: 16500, duration: 0, easing: "swing" },
            { id: "eid97", tween: [ "transform", "${_purpleTheir}", "scaleX", '0.05331', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid37", tween: [ "style", "${_purpleArent}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid178", tween: [ "style", "${_teen1}", "display", 'block', { fromValue: 'none'}], position: 4009, duration: 0 },
            { id: "eid626", tween: [ "style", "${_teen1}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid34", tween: [ "style", "${_purpleSkin}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid38", tween: [ "style", "${_purpleTheir}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid35", tween: [ "style", "${_purpleFelt}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid235", tween: [ "transform", "${_teen2}", "translateX", '645.09px', { fromValue: '175.89px'}], position: 2871, duration: 408, easing: "swing" },
            { id: "eid3", tween: [ "style", "${_forTeens}", "opacity", '1', { fromValue: '0'}], position: 422, duration: 327 },
            { id: "eid6", tween: [ "style", "${_forTeens}", "opacity", '0', { fromValue: '1'}], position: 1250, duration: 250 },
            { id: "eid286", tween: [ "style", "${_largeTweeter4}", "display", 'block', { fromValue: 'none'}], position: 6732, duration: 0 },
            { id: "eid611", tween: [ "style", "${_largeTweeter4}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid363", tween: [ "transform", "${_largeTweeter7}", "rotateZ", '21deg', { fromValue: '-45deg'}], position: 7878, duration: 410, easing: "swing" },
            { id: "eid283", tween: [ "style", "${_largeTweeter1}", "display", 'block', { fromValue: 'none'}], position: 5544, duration: 0 },
            { id: "eid608", tween: [ "style", "${_largeTweeter1}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid232", tween: [ "transform", "${_teen1}", "translateY", '45.15px', { fromValue: '465.49px'}], position: 4009, duration: 408, easing: "swing" },
            { id: "eid242", tween: [ "transform", "${_teen5}", "translateY", '-458.21px', { fromValue: '-189.4px'}], position: 3139, duration: 408, easing: "swing" },
            { id: "eid108", tween: [ "transform", "${_purpleFelt}", "translateY", '92.06px', { fromValue: '111.79px'}], position: 2573, duration: 392 },
            { id: "eid230", tween: [ "transform", "${_teen3}", "translateY", '126.48px', { fromValue: '380.42px'}], position: 4301, duration: 408, easing: "swing" },
            { id: "eid36", tween: [ "style", "${_purplejust}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid554", tween: [ "style", "${_pump}", "display", 'block', { fromValue: 'none'}], position: 14564, duration: 0, easing: "swing" },
            { id: "eid228", tween: [ "transform", "${_teen2}", "rotateZ", '44deg', { fromValue: '44deg'}], position: 2871, duration: 0 },
            { id: "eid204", tween: [ "transform", "${_teen2}", "rotateZ", '44deg', { fromValue: '44deg'}], position: 3423, duration: 0 },
            { id: "eid370", tween: [ "style", "${_patientsNeed}", "display", 'block', { fromValue: 'none'}], position: 9574, duration: 0, easing: "swing" },
            { id: "eid361", tween: [ "transform", "${_largeTweeter5}", "rotateZ", '-20deg', { fromValue: '41deg'}], position: 7129, duration: 410, easing: "swing" },
            { id: "eid355", tween: [ "transform", "${_largeTweeter7}", "translateX", '451.64px', { fromValue: '657.37px'}], position: 7878, duration: 410, easing: "swing" },
            { id: "eid104", tween: [ "transform", "${_purpleArent}", "translateY", '58.38px', { fromValue: '82.31px'}], position: 2573, duration: 326 },
            { id: "eid285", tween: [ "style", "${_largeTweeter3}", "display", 'block', { fromValue: 'none'}], position: 6358, duration: 0 },
            { id: "eid610", tween: [ "style", "${_largeTweeter3}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid238", tween: [ "transform", "${_teen6}", "translateY", '123.98px', { fromValue: '513.98px'}], position: 4887, duration: 408, easing: "swing" },
            { id: "eid216", tween: [ "transform", "${_teen5}", "rotateZ", '0', { fromValue: '0deg'}], position: 3139, duration: 0 },
            { id: "eid200", tween: [ "transform", "${_teen5}", "rotateZ", '0', { fromValue: '0deg'}], position: 3745, duration: 0 },
            { id: "eid234", tween: [ "transform", "${_teen4}", "translateY", '-29.88px', { fromValue: '7.51px'}], position: 3717, duration: 408, easing: "swing" },
            { id: "eid92", tween: [ "transform", "${_purpleFelt}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2573, duration: 392 },
            { id: "eid96", tween: [ "transform", "${_purpleArent}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2573, duration: 326 },
            { id: "eid163", tween: [ "style", "${_acneCanLeaveA}", "display", 'block', { fromValue: 'none'}], position: 3300, duration: 0 },
            { id: "eid622", tween: [ "style", "${_acneCanLeaveA}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid90", tween: [ "transform", "${_purpleSkin}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid109", tween: [ "style", "${_forTeens}", "display", 'none', { fromValue: 'block'}], position: 1580, duration: 0 },
            { id: "eid167", tween: [ "style", "${_acneCanLeaveA}", "opacity", '1', { fromValue: '0'}], position: 3300, duration: 333 },
            { id: "eid484", tween: [ "style", "${_acneCanLeaveA}", "opacity", '0', { fromValue: '1'}], position: 6410, duration: 1000 },
            { id: "eid357", tween: [ "transform", "${_largeTweeter1}", "rotateZ", '14deg', { fromValue: '29deg'}], position: 5544, duration: 410, easing: "swing" },
            { id: "eid584", tween: [ "style", "${_backgroundTextureDarker2}", "display", 'block', { fromValue: 'none'}], position: 17613, duration: 0, easing: "swing" },
            { id: "eid553", tween: [ "style", "${_FindTheRightChemistry}", "display", 'block', { fromValue: 'none'}], position: 15559, duration: 0, easing: "swing" },
            { id: "eid210", tween: [ "transform", "${_teen7}", "rotateZ", '0', { fromValue: '0deg'}], position: 4596, duration: 0 },
            { id: "eid198", tween: [ "transform", "${_teen7}", "rotateZ", '0', { fromValue: '0deg'}], position: 5494, duration: 0 },
            { id: "eid281", tween: [ "transform", "${_tweeterTargetsNew}", "scaleX", '0.48', { fromValue: '0.48'}], position: 5891, duration: 0 },
            { id: "eid339", tween: [ "transform", "${_largeTweeter1}", "translateX", '78.01px', { fromValue: '-234.54px'}], position: 5544, duration: 410, easing: "swing" },
            { id: "eid359", tween: [ "transform", "${_largeTweeter3}", "rotateZ", '-9deg', { fromValue: '-40deg'}], position: 6358, duration: 410, easing: "swing" },
            { id: "eid94", tween: [ "transform", "${_purplejust}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2573, duration: 392 },
            { id: "eid616", tween: [ "style", "${_tweeterTargetsNew}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid594", tween: [ "transform", "${_purpleBlend2Copy}", "scaleY", '1', { fromValue: '1'}], position: 15500, duration: 0, easing: "swing" },
            { id: "eid180", tween: [ "style", "${_teen3}", "display", 'block', { fromValue: 'none'}], position: 4301, duration: 0 },
            { id: "eid625", tween: [ "style", "${_teen3}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid237", tween: [ "transform", "${_teen6}", "translateX", '6.53px', { fromValue: '385.22px'}], position: 4887, duration: 408, easing: "swing" },
            { id: "eid345", tween: [ "transform", "${_largeTweeter8}", "translateX", '125.94px', { fromValue: '50.53px'}], position: 8216, duration: 410, easing: "swing" },
            { id: "eid289", tween: [ "style", "${_largeTweeter7}", "display", 'block', { fromValue: 'none'}], position: 7878, duration: 0 },
            { id: "eid614", tween: [ "style", "${_largeTweeter7}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid592", tween: [ "transform", "${_purpleBlend2Copy}", "translateX", '552px', { fromValue: '-530.68px'}], position: 15500, duration: 1370, easing: "swing" },
            { id: "eid144", tween: [ "transform", "${_mark2}", "scaleX", '0.69', { fromValue: '0.5'}], position: 3050, duration: 130 },
            { id: "eid145", tween: [ "transform", "${_mark2}", "scaleX", '0.5', { fromValue: '0.69'}], position: 3181, duration: 119 },
            { id: "eid374", tween: [ "style", "${_patientsNeed}", "opacity", '0.96', { fromValue: '0'}], position: 9574, duration: 268, easing: "swing" },
            { id: "eid110", tween: [ "transform", "${_purpleOn}", "translateX", '69.13px', { fromValue: '294.29px'}], position: 2760, duration: 392 },
            { id: "eid290", tween: [ "style", "${_largeTweeter8}", "display", 'block', { fromValue: 'none'}], position: 8216, duration: 0 },
            { id: "eid615", tween: [ "style", "${_largeTweeter8}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid113", tween: [ "transform", "${_purpleTheir}", "translateY", '203.65px', { fromValue: '223.72px'}], position: 2760, duration: 392 },
            { id: "eid561", tween: [ "transform", "${_purpleBlend2}", "translateY", '-1729px', { fromValue: '-218px'}], position: 14500, duration: 1500, easing: "swing" },
            { id: "eid347", tween: [ "transform", "${_largeTweeter6}", "translateX", '756.25px', { fromValue: '1127.38px'}], position: 7499, duration: 410, easing: "swing" },
            { id: "eid169", tween: [ "style", "${_onTheirLives}", "display", 'block', { fromValue: 'block'}], position: 4250, duration: 0 },
            { id: "eid618", tween: [ "style", "${_onTheirLives}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid150", tween: [ "transform", "${_mark1}", "scaleY", '0.72', { fromValue: '0.5'}], position: 2800, duration: 130 },
            { id: "eid151", tween: [ "transform", "${_mark1}", "scaleY", '0.5', { fromValue: '0.72'}], position: 2931, duration: 119 },
            { id: "eid236", tween: [ "transform", "${_teen2}", "translateY", '-379.66px', { fromValue: '383.93px'}], position: 2871, duration: 408, easing: "swing" },
            { id: "eid351", tween: [ "transform", "${_largeTweeter4}", "translateX", '659.26px', { fromValue: '999.58px'}], position: 6732, duration: 410, easing: "swing" },
            { id: "eid229", tween: [ "transform", "${_teen3}", "translateX", '627.86px', { fromValue: '765.01px'}], position: 4301, duration: 408, easing: "swing" },
            { id: "eid233", tween: [ "transform", "${_teen4}", "translateX", '653.55px', { fromValue: '998.04px'}], position: 3717, duration: 408, easing: "swing" },
            { id: "eid89", tween: [ "transform", "${_purpleSkin}", "scaleX", '0.05424', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid98", tween: [ "transform", "${_purpleTheir}", "scaleY", '0.08', { fromValue: '0.5'}], position: 2760, duration: 392 },
            { id: "eid10", tween: [ "style", "${_purpleTheir}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid158", tween: [ "style", "${_purpleTheir}", "display", 'none', { fromValue: 'block'}], position: 3145, duration: 0 },
            { id: "eid284", tween: [ "style", "${_largeTweeter2}", "display", 'block', { fromValue: 'none'}], position: 5954, duration: 0 },
            { id: "eid609", tween: [ "style", "${_largeTweeter2}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid11", tween: [ "style", "${_purpleSkin}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid159", tween: [ "style", "${_purpleSkin}", "display", 'none', { fromValue: 'block'}], position: 3145, duration: 0 },
            { id: "eid551", tween: [ "style", "${_purpleBlend2}", "display", 'block', { fromValue: 'none'}], position: 14500, duration: 0, easing: "swing" },
            { id: "eid636", tween: [ "style", "${_purpleBlend2}", "display", 'none', { fromValue: 'block'}], position: 16000, duration: 0 },
            { id: "eid350", tween: [ "transform", "${_largeTweeter3}", "translateY", '56.92px', { fromValue: '405.92px'}], position: 6358, duration: 410, easing: "swing" },
            { id: "eid288", tween: [ "style", "${_largeTweeter6}", "display", 'block', { fromValue: 'none'}], position: 7499, duration: 0 },
            { id: "eid613", tween: [ "style", "${_largeTweeter6}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid346", tween: [ "transform", "${_largeTweeter8}", "translateY", '-330.23px', { fromValue: '-731.91px'}], position: 8216, duration: 410, easing: "swing" },
            { id: "eid111", tween: [ "transform", "${_purpleOn}", "translateY", '95.92px', { fromValue: '120.21px'}], position: 2760, duration: 392 },
            { id: "eid241", tween: [ "transform", "${_teen5}", "translateX", '16.44px', { fromValue: '-624.63px'}], position: 3139, duration: 408, easing: "swing" },
            { id: "eid379", tween: [ "style", "${_simple}", "display", 'block', { fromValue: 'none'}], position: 12070, duration: 0 },
            { id: "eid177", tween: [ "style", "${_teen5}", "display", 'block', { fromValue: 'none'}], position: 3139, duration: 0 },
            { id: "eid629", tween: [ "style", "${_teen5}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid570", tween: [ "style", "${_onceDailySubhead}", "opacity", '1', { fromValue: '0'}], position: 16500, duration: 863, easing: "swing" },
            { id: "eid651", tween: [ "style", "${_Rectangle}", "display", 'block', { fromValue: 'none'}], position: 13500, duration: 0, easing: "swing" },
            { id: "eid587", tween: [ "style", "${_backgroundTextureDarker2}", "opacity", '1', { fromValue: '0'}], position: 17613, duration: 1250, easing: "swing" },
            { id: "eid353", tween: [ "transform", "${_largeTweeter5}", "translateX", '356.22px', { fromValue: '333.3px'}], position: 7129, duration: 410, easing: "swing" },
            { id: "eid244", tween: [ "transform", "${_teen8}", "translateY", '-222.49px', { fromValue: '479.16px'}], position: 3385, duration: 408, easing: "swing" },
            { id: "eid243", tween: [ "transform", "${_teen8}", "translateX", '-115.81px', { fromValue: '68.2px'}], position: 3385, duration: 408, easing: "swing" },
            { id: "eid556", tween: [ "style", "${_purpleBlend2Copy}", "display", 'block', { fromValue: 'none'}], position: 15500, duration: 0, easing: "swing" },
            { id: "eid637", tween: [ "style", "${_purpleBlend2Copy}", "display", 'none', { fromValue: 'block'}], position: 16870, duration: 0 },
            { id: "eid555", tween: [ "style", "${_purpleBlend2Copy}", "display", 'block', { fromValue: 'none'}], position: 27000, duration: 0, easing: "swing" },
            { id: "eid93", tween: [ "transform", "${_purplejust}", "scaleX", '0.06067', { fromValue: '0.5'}], position: 2573, duration: 392 },
            { id: "eid207", tween: [ "transform", "${_teen8}", "rotateZ", '0', { fromValue: '0deg'}], position: 3385, duration: 0 },
            { id: "eid197", tween: [ "transform", "${_teen8}", "rotateZ", '0', { fromValue: '0deg'}], position: 4040, duration: 0 },
            { id: "eid213", tween: [ "transform", "${_teen6}", "rotateZ", '0', { fromValue: '0deg'}], position: 4887, duration: 0 },
            { id: "eid199", tween: [ "transform", "${_teen6}", "rotateZ", '0', { fromValue: '0deg'}], position: 5843, duration: 0 },
            { id: "eid377", tween: [ "style", "${_effectiveAnimation}", "display", 'block', { fromValue: 'none'}], position: 10250, duration: 0 },
            { id: "eid115", tween: [ "transform", "${_purpleSkin}", "translateY", '191.65px', { fromValue: '211.7px'}], position: 2760, duration: 392 },
            { id: "eid116", tween: [ "style", "${_mark1}", "display", 'block', { fromValue: 'block'}], position: 2800, duration: 0 },
            { id: "eid619", tween: [ "style", "${_mark1}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid146", tween: [ "transform", "${_mark2}", "scaleY", '0.69', { fromValue: '0.5'}], position: 3050, duration: 130 },
            { id: "eid147", tween: [ "transform", "${_mark2}", "scaleY", '0.5', { fromValue: '0.69'}], position: 3181, duration: 119 },
            { id: "eid222", tween: [ "transform", "${_teen4}", "rotateZ", '-24deg', { fromValue: '-24deg'}], position: 3717, duration: 0 },
            { id: "eid202", tween: [ "transform", "${_teen4}", "rotateZ", '-24deg', { fromValue: '-24deg'}], position: 4439, duration: 0 },
            { id: "eid340", tween: [ "transform", "${_largeTweeter1}", "translateY", '-135.49px', { fromValue: '309.63px'}], position: 5544, duration: 410, easing: "swing" },
            { id: "eid349", tween: [ "transform", "${_largeTweeter3}", "translateX", '148.18px', { fromValue: '339.05px'}], position: 6358, duration: 410, easing: "swing" },
            { id: "eid9", tween: [ "style", "${_purpleArent}", "display", 'block', { fromValue: 'none'}], position: 1623, duration: 0 },
            { id: "eid160", tween: [ "style", "${_purpleArent}", "display", 'none', { fromValue: 'block'}], position: 2899, duration: 0 },
            { id: "eid593", tween: [ "transform", "${_purpleBlend2Copy}", "scaleX", '1', { fromValue: '1'}], position: 15500, duration: 0, easing: "swing" },
            { id: "eid33", tween: [ "style", "${_purpleOn}", "opacity", '1', { fromValue: '0'}], position: 1623, duration: 379 },
            { id: "eid103", tween: [ "transform", "${_purpleArent}", "translateX", '-94.61px', { fromValue: '-195.51px'}], position: 2573, duration: 326 },
            { id: "eid112", tween: [ "transform", "${_purpleTheir}", "translateX", '-273.82px', { fromValue: '78.98px'}], position: 2760, duration: 392 },
            { id: "eid352", tween: [ "transform", "${_largeTweeter4}", "translateY", '-45.15px', { fromValue: '291px'}], position: 6732, duration: 410, easing: "swing" },
            { id: "eid360", tween: [ "transform", "${_largeTweeter4}", "rotateZ", '-10deg', { fromValue: '29deg'}], position: 6732, duration: 410, easing: "swing" },
            { id: "eid114", tween: [ "transform", "${_purpleSkin}", "translateX", '-458.36px', { fromValue: '52.29px'}], position: 2760, duration: 392 },
            { id: "eid356", tween: [ "transform", "${_largeTweeter7}", "translateY", '96.53px', { fromValue: '430.48px'}], position: 7878, duration: 410, easing: "swing" },
            { id: "eid343", tween: [ "transform", "${_largeTweeter2}", "translateX", '507.22px', { fromValue: '-158.31px'}], position: 5954, duration: 410, easing: "swing" },
            { id: "eid140", tween: [ "style", "${_mark1}", "opacity", '1', { fromValue: '0'}], position: 2800, duration: 130 },
            { id: "eid479", tween: [ "style", "${_mark1}", "opacity", '0', { fromValue: '1'}], position: 6410, duration: 1000 },
            { id: "eid175", tween: [ "style", "${_teen7}", "display", 'block', { fromValue: 'none'}], position: 4572, duration: 0 },
            { id: "eid624", tween: [ "style", "${_teen7}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid91", tween: [ "transform", "${_purpleFelt}", "scaleX", '0.08', { fromValue: '0.5'}], position: 2573, duration: 392 },
            { id: "eid106", tween: [ "transform", "${_purplejust}", "translateY", '58.27px', { fromValue: '83.3px'}], position: 2573, duration: 392 },
            { id: "eid287", tween: [ "style", "${_largeTweeter5}", "display", 'block', { fromValue: 'none'}], position: 7129, duration: 0 },
            { id: "eid612", tween: [ "style", "${_largeTweeter5}", "display", 'none', { fromValue: 'block'}], position: 9316, duration: 0, easing: "swing" },
            { id: "eid348", tween: [ "transform", "${_largeTweeter6}", "translateY", '-195.03px', { fromValue: '-27.31px'}], position: 7499, duration: 410, easing: "swing" },
            { id: "eid654", tween: [ "transform", "${_Rectangle}", "translateX", '4px', { fromValue: '1053.16px'}], position: 13500, duration: 750, easing: "easeOutQuad" },
            { id: "eid573", tween: [ "style", "${_simple}", "opacity", '1', { fromValue: '0'}], position: 12070, duration: 930, easing: "swing" },
            { id: "eid354", tween: [ "transform", "${_largeTweeter5}", "translateY", '-155.09px', { fromValue: '436.52px'}], position: 7129, duration: 410, easing: "swing" },
            { id: "eid148", tween: [ "transform", "${_mark1}", "scaleX", '0.72', { fromValue: '0.5'}], position: 2800, duration: 130 },
            { id: "eid149", tween: [ "transform", "${_mark1}", "scaleX", '0.5', { fromValue: '0.72'}], position: 2931, duration: 119 },
            { id: "eid231", tween: [ "transform", "${_teen1}", "translateX", '536.63px', { fromValue: '365.71px'}], position: 4009, duration: 408, easing: "swing" },
            { id: "eid518", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_effectiveAnimation}', ['start'] ], ""], position: 10250.615462151 },
            { id: "eid523", trigger: [ function executeSymbolFunction(e, data) { this._executeSymbolAction(e, data); }, ['play', '${_Safeanimation}', ['start'] ], ""], position: 11136.90658764 }         ]
      }
   }
},
"tweeterTargets": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'rect',
      rect: [125,370,848,396],
      transform: [[-20,-3],[-9],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter8target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,754,350],
      transform: [[-89,404],[14],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter1Target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,864,412],
      transform: [[434,323],[-20],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter7target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,692,330],
      transform: [[97,888],[-1],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter3target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,954,444],
      transform: [[589,845],[21],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter5target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,906,444],
      transform: [[753,-245],[16],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter2target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,682,324],
      transform: [[1352,280],[1],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter6target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [125,370,526,272],
      transform: [[1222,636],[-13],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter4target',
      opacity: 0.40752933951965,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_tweeter1Target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '-89.02px'],
            ["transform", "rotateZ", '14deg'],
            ["style", "height", '350px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '404.64px'],
            ["style", "width", '754px']
         ],
         "${_tweeter6target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '1352.4px'],
            ["transform", "rotateZ", '1deg'],
            ["style", "height", '324px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '280.08px'],
            ["style", "width", '682px']
         ],
         "${_tweeter2target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '753.09px'],
            ["transform", "rotateZ", '16deg'],
            ["style", "height", '444px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '-245.25px'],
            ["style", "width", '906px']
         ],
         "${_tweeter7target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '434.32px'],
            ["transform", "rotateZ", '-20deg'],
            ["style", "height", '412px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '323.15px'],
            ["style", "width", '864px']
         ],
         "${_tweeter5target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '589.82px'],
            ["transform", "rotateZ", '21deg'],
            ["style", "height", '444px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '845.2px'],
            ["style", "width", '954px']
         ],
         "${_tweeter8target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '-20.78px'],
            ["transform", "rotateZ", '-9deg'],
            ["style", "height", '396px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '-3.39px'],
            ["style", "width", '848px']
         ],
         "${_tweeter3target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '97.81px'],
            ["transform", "rotateZ", '-1deg'],
            ["style", "height", '330px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '888.97px'],
            ["style", "width", '692px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1709.725758px'],
            ["style", "width", '2219.644897px']
         ],
         "${_tweeter4target}": [
            ["transform", "scaleX", '0.69148'],
            ["style", "cursor", 'pointer'],
            ["transform", "translateX", '1222.2px'],
            ["transform", "rotateZ", '-13deg'],
            ["style", "height", '272px'],
            ["style", "opacity", '0.40752933951965'],
            ["transform", "translateY", '636.25px'],
            ["style", "width", '526px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
},
"tweeterTargetsNew": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      type: 'rect',
      rect: [518,772,848,396],
      transform: [[-529,-452],[-9],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter8target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,754,350],
      transform: [[-597,-44],[14],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter1Target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,864,412],
      transform: [[-74,-125],[-20],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter7target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,692,330],
      transform: [[-410,439],[-1],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter3target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,954,444],
      transform: [[81,396],[21],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter5target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,906,444],
      transform: [[244,-694],[16],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter2target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,682,324],
      transform: [[843,-168],[1],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter6target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   },
   {
      type: 'rect',
      rect: [518,772,526,272],
      transform: [[713,187],[-13],{},[0.691]],
      stroke: [0,'rgba(0,0,0,1)','none'],
      id: 'tweeter4target',
      opacity: 0,
      cursor: ['pointer'],
      fill: ['rgba(192,192,192,1)']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_tweeter1Target}": [
            ["transform", "translateX", '-597.46px'],
            ["transform", "rotateZ", '14deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '754px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '350px'],
            ["transform", "translateY", '-44.36px']
         ],
         "${_tweeter6target}": [
            ["transform", "translateX", '843.95px'],
            ["transform", "rotateZ", '1deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '682px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '324px'],
            ["transform", "translateY", '-168.92px']
         ],
         "${_tweeter2target}": [
            ["transform", "translateX", '244.64px'],
            ["transform", "rotateZ", '16deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '906px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '444px'],
            ["transform", "translateY", '-694.25px']
         ],
         "${_tweeter7target}": [
            ["transform", "translateX", '-74.12px'],
            ["transform", "rotateZ", '-20deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '864px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '412px'],
            ["transform", "translateY", '-125.85px']
         ],
         "${_tweeter3target}": [
            ["transform", "translateX", '-410.63px'],
            ["transform", "rotateZ", '-1deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '692px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '330px'],
            ["transform", "translateY", '439.96px']
         ],
         "${symbolSelector}": [
            ["style", "height", '1715.695496px'],
            ["style", "width", '1939.539171px']
         ],
         "${_tweeter4target}": [
            ["transform", "translateX", '713.75px'],
            ["transform", "rotateZ", '-13deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '526px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '272px'],
            ["transform", "translateY", '187.24px']
         ],
         "${_tweeter5target}": [
            ["transform", "translateX", '81.37px'],
            ["transform", "rotateZ", '21deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '954px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '444px'],
            ["transform", "translateY", '396.19px']
         ],
         "${_tweeter8target}": [
            ["transform", "translateX", '-529.22px'],
            ["transform", "rotateZ", '-9deg'],
            ["transform", "scaleX", '0.69148'],
            ["style", "opacity", '0'],
            ["style", "cursor", 'pointer'],
            ["style", "width", '848px'],
            ["transform", "scaleY", '1'],
            ["style", "height", '396px'],
            ["transform", "translateY", '-452.39px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 5891.107421875,
         autoPlay: false,
         timeline: [
            { id: "eid249", tween: [ "transform", "${_tweeter2target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid247", tween: [ "transform", "${_tweeter6target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid260", tween: [ "transform", "${_tweeter8target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid258", tween: [ "transform", "${_tweeter1Target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid245", tween: [ "transform", "${_tweeter4target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid246", tween: [ "transform", "${_tweeter4target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid248", tween: [ "transform", "${_tweeter6target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid250", tween: [ "transform", "${_tweeter2target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid251", tween: [ "transform", "${_tweeter5target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid252", tween: [ "transform", "${_tweeter5target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid257", tween: [ "transform", "${_tweeter1Target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid254", tween: [ "transform", "${_tweeter3target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid255", tween: [ "transform", "${_tweeter7target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid256", tween: [ "transform", "${_tweeter7target}", "scaleY", '1', { fromValue: '1'}], position: 5891, duration: 0 },
            { id: "eid259", tween: [ "transform", "${_tweeter8target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 },
            { id: "eid253", tween: [ "transform", "${_tweeter3target}", "scaleX", '0.69148', { fromValue: '0.69148'}], position: 5891, duration: 0 }         ]
      }
   }
},
"effectiveAnimation": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      rect: [-308,-29,30,44],
      transform: [[610,53],{},{},[0.48,0.48]],
      id: 'comma',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/comma.png']
   },
   {
      rect: [-739,-237,85,105],
      transform: [[989,212],{},{},[0.48,0.48]],
      id: 'eeCopy2',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/ee.png']
   },
   {
      rect: [-764,-145,88,104],
      transform: [[977,120],{},{},[0.48,0.48]],
      id: 'vee',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/vee.png']
   },
   {
      rect: [-720,-19,60,105],
      transform: [[917,-5],{},{},[0.48,0.48]],
      id: 'i',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/i.png']
   },
   {
      rect: [-294,-437,85,105],
      transform: [[449,410],{},{},[0.48,0.48]],
      id: 'tee',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/tee.png']
   },
   {
      rect: [-752,-251,85,105],
      transform: [[870,226],{},{},[0.48,0.48]],
      id: 'cee',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/cee.png']
   },
   {
      rect: [-739,-237,85,105],
      transform: [[821,212],{},{},[0.48,0.48]],
      id: 'eeCopy',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/ee.png']
   },
   {
      rect: [-646,-198,85,105],
      transform: [[694,172],{},{},[0.48,0.48]],
      id: 'eff',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/eff.png']
   },
   {
      rect: [-646,-198,85,105],
      transform: [[659,172],{},{},[0.48,0.48]],
      id: 'effCopy',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/eff.png']
   },
   {
      rect: [-739,-237,85,105],
      transform: [[717,212],{},{},[0.48,0.48]],
      id: 'ee',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/ee.png']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_effCopy}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '659.23px'],
            ["transform", "translateY", '172.83px'],
            ["style", "display", 'none']
         ],
         "${_eff}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '694.44px'],
            ["transform", "translateY", '172.83px'],
            ["style", "display", 'none']
         ],
         "${_tee}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '449.16px'],
            ["transform", "translateY", '410.82px'],
            ["style", "display", 'none']
         ],
         "${_ee}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '717.15px'],
            ["transform", "translateY", '212.83px'],
            ["style", "display", 'none']
         ],
         "${_eeCopy}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '821.17px'],
            ["transform", "translateY", '212.83px'],
            ["style", "display", 'none']
         ],
         "${_cee}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '870.15px'],
            ["transform", "translateY", '226.82px'],
            ["style", "display", 'none']
         ],
         "${_eeCopy2}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '989.09px'],
            ["transform", "translateY", '212.82px'],
            ["style", "display", 'none']
         ],
         "${symbolSelector}": [
            ["style", "height", '54.51px'],
            ["style", "width", '323.889973px']
         ],
         "${_vee}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '977.17px'],
            ["transform", "translateY", '120.82px'],
            ["style", "display", 'none']
         ],
         "${_i}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '917.11px'],
            ["transform", "translateY", '-5.15px'],
            ["style", "display", 'none']
         ],
         "${_comma}": [
            ["transform", "scaleY", '0.68'],
            ["transform", "scaleX", '0.68'],
            ["transform", "translateX", '610.16px'],
            ["transform", "translateY", '53.25px'],
            ["style", "display", 'none']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1000,
         autoPlay: true,
         labels: {
            "start": 101
         },
         timeline: [
            { id: "eid462", tween: [ "transform", "${_comma}", "scaleX", '0.48', { fromValue: '0.68'}], position: 748, duration: 133, easing: "swing" },
            { id: "eid397", tween: [ "style", "${_vee}", "display", 'block', { fromValue: 'none'}], position: 615, duration: 0, easing: "swing" },
            { id: "eid396", tween: [ "style", "${_i}", "display", 'block', { fromValue: 'none'}], position: 561, duration: 0, easing: "swing" },
            { id: "eid464", tween: [ "transform", "${_i}", "scaleX", '0.48', { fromValue: '0.68'}], position: 561, duration: 133, easing: "swing" },
            { id: "eid456", tween: [ "transform", "${_ee}", "scaleX", '0.48', { fromValue: '0.68'}], position: 161, duration: 133, easing: "swing" },
            { id: "eid455", tween: [ "transform", "${_tee}", "scaleY", '0.48', { fromValue: '0.68'}], position: 482, duration: 133, easing: "swing" },
            { id: "eid395", tween: [ "style", "${_tee}", "display", 'block', { fromValue: 'none'}], position: 482, duration: 0, easing: "swing" },
            { id: "eid466", tween: [ "transform", "${_eeCopy2}", "scaleX", '0.48', { fromValue: '0.68'}], position: 694, duration: 133, easing: "swing" },
            { id: "eid457", tween: [ "transform", "${_ee}", "scaleY", '0.48', { fromValue: '0.68'}], position: 161, duration: 133, easing: "swing" },
            { id: "eid452", tween: [ "transform", "${_eeCopy}", "scaleX", '0.48', { fromValue: '0.68'}], position: 348, duration: 133, easing: "swing" },
            { id: "eid399", tween: [ "style", "${_comma}", "display", 'block', { fromValue: 'none'}], position: 748, duration: 0, easing: "swing" },
            { id: "eid465", tween: [ "transform", "${_i}", "scaleY", '0.48', { fromValue: '0.68'}], position: 561, duration: 133, easing: "swing" },
            { id: "eid390", tween: [ "style", "${_ee}", "display", 'block', { fromValue: 'none'}], position: 161, duration: 0, easing: "swing" },
            { id: "eid398", tween: [ "style", "${_eeCopy2}", "display", 'block', { fromValue: 'none'}], position: 694, duration: 0, easing: "swing" },
            { id: "eid460", tween: [ "transform", "${_eff}", "scaleX", '0.48', { fromValue: '0.68'}], position: 294, duration: 133, easing: "swing" },
            { id: "eid458", tween: [ "transform", "${_effCopy}", "scaleX", '0.48', { fromValue: '0.68'}], position: 215, duration: 133, easing: "swing" },
            { id: "eid459", tween: [ "transform", "${_effCopy}", "scaleY", '0.48', { fromValue: '0.68'}], position: 215, duration: 133, easing: "swing" },
            { id: "eid453", tween: [ "transform", "${_eeCopy}", "scaleY", '0.48', { fromValue: '0.68'}], position: 348, duration: 133, easing: "swing" },
            { id: "eid391", tween: [ "style", "${_effCopy}", "display", 'block', { fromValue: 'none'}], position: 215, duration: 0, easing: "swing" },
            { id: "eid451", tween: [ "transform", "${_cee}", "scaleY", '0.48', { fromValue: '0.68'}], position: 428, duration: 133, easing: "swing" },
            { id: "eid392", tween: [ "style", "${_eff}", "display", 'block', { fromValue: 'none'}], position: 294, duration: 0, easing: "swing" },
            { id: "eid463", tween: [ "transform", "${_comma}", "scaleY", '0.48', { fromValue: '0.68'}], position: 748, duration: 133, easing: "swing" },
            { id: "eid393", tween: [ "style", "${_eeCopy}", "display", 'block', { fromValue: 'none'}], position: 348, duration: 0, easing: "swing" },
            { id: "eid454", tween: [ "transform", "${_tee}", "scaleX", '0.48', { fromValue: '0.68'}], position: 482, duration: 133, easing: "swing" },
            { id: "eid468", tween: [ "transform", "${_vee}", "scaleX", '0.48', { fromValue: '0.68'}], position: 615, duration: 133, easing: "swing" },
            { id: "eid469", tween: [ "transform", "${_vee}", "scaleY", '0.48', { fromValue: '0.68'}], position: 615, duration: 133, easing: "swing" },
            { id: "eid450", tween: [ "transform", "${_cee}", "scaleX", '0.48', { fromValue: '0.68'}], position: 428, duration: 133, easing: "swing" },
            { id: "eid461", tween: [ "transform", "${_eff}", "scaleY", '0.48', { fromValue: '0.68'}], position: 294, duration: 133, easing: "swing" },
            { id: "eid467", tween: [ "transform", "${_eeCopy2}", "scaleY", '0.48', { fromValue: '0.68'}], position: 694, duration: 133, easing: "swing" },
            { id: "eid394", tween: [ "style", "${_cee}", "display", 'block', { fromValue: 'none'}], position: 428, duration: 0, easing: "swing" }         ]
      }
   }
},
"Safeanimation": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0015',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0015.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0014',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0014.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0013',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0013.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0012',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0012.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0011',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0011.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0010',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0010.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0009',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0009.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0008',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0008.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0007',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0007.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0006',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0006.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0005',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0005.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0004',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0004.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0003',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0003.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0002',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0002.png']
   },
   {
      rect: [-606,-113,300,115],
      transform: [[527,82],{},{},[0.48,0.48]],
      id: 'safeAni0001',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safeAni0001.png']
   },
   {
      rect: [-640,-86,300,115],
      transform: [[561,55],{},{},[0.48,0.48]],
      id: 'safe',
      type: 'image',
      display: 'none',
      fill: ['rgba(0,0,0,0)','images/safe.png']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_safeAni0011}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0002}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0015}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0005}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${symbolSelector}": [
            ["style", "height", '54.72px'],
            ["style", "width", '143.52px']
         ],
         "${_safeAni0006}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0004}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0012}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0013}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safe}": [
            ["transform", "scaleY", '0.48'],
            ["style", "display", 'none'],
            ["transform", "translateX", '561.38px'],
            ["transform", "translateY", '55.84px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0008}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0003}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0009}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0010}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0007}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0014}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ],
         "${_safeAni0001}": [
            ["transform", "translateX", '527.38px'],
            ["style", "display", 'none'],
            ["transform", "scaleY", '0.48'],
            ["transform", "translateY", '82.47px'],
            ["transform", "scaleX", '0.48']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1156.0631390784,
         autoPlay: true,
         labels: {
            "start": 81
         },
         timeline: [
            { id: "eid489", tween: [ "style", "${_safeAni0005}", "display", 'block', { fromValue: 'none'}], position: 651, duration: 0 },
            { id: "eid507", tween: [ "style", "${_safeAni0005}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid493", tween: [ "style", "${_safeAni0009}", "display", 'block', { fromValue: 'none'}], position: 406, duration: 0 },
            { id: "eid511", tween: [ "style", "${_safeAni0009}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid498", tween: [ "style", "${_safeAni0014}", "display", 'block', { fromValue: 'none'}], position: 147, duration: 0 },
            { id: "eid516", tween: [ "style", "${_safeAni0014}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid492", tween: [ "style", "${_safeAni0008}", "display", 'block', { fromValue: 'none'}], position: 500, duration: 0 },
            { id: "eid510", tween: [ "style", "${_safeAni0008}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid501", tween: [ "style", "${_safe}", "display", 'block', { fromValue: 'none'}], position: 1000, duration: 0 },
            { id: "eid485", tween: [ "style", "${_safeAni0001}", "display", 'block', { fromValue: 'none'}], position: 861, duration: 0 },
            { id: "eid503", tween: [ "style", "${_safeAni0001}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid499", tween: [ "style", "${_safeAni0015}", "display", 'block', { fromValue: 'none'}], position: 147, duration: 0 },
            { id: "eid517", tween: [ "style", "${_safeAni0015}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid494", tween: [ "style", "${_safeAni0010}", "display", 'block', { fromValue: 'none'}], position: 355, duration: 0 },
            { id: "eid512", tween: [ "style", "${_safeAni0010}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid496", tween: [ "style", "${_safeAni0012}", "display", 'block', { fromValue: 'none'}], position: 250, duration: 0 },
            { id: "eid514", tween: [ "style", "${_safeAni0012}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid487", tween: [ "style", "${_safeAni0003}", "display", 'block', { fromValue: 'none'}], position: 750, duration: 0 },
            { id: "eid505", tween: [ "style", "${_safeAni0003}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid497", tween: [ "style", "${_safeAni0013}", "display", 'block', { fromValue: 'none'}], position: 198, duration: 0 },
            { id: "eid515", tween: [ "style", "${_safeAni0013}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid490", tween: [ "style", "${_safeAni0006}", "display", 'block', { fromValue: 'none'}], position: 600, duration: 0 },
            { id: "eid508", tween: [ "style", "${_safeAni0006}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid488", tween: [ "style", "${_safeAni0004}", "display", 'block', { fromValue: 'none'}], position: 697, duration: 0 },
            { id: "eid506", tween: [ "style", "${_safeAni0004}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid491", tween: [ "style", "${_safeAni0007}", "display", 'block', { fromValue: 'none'}], position: 553, duration: 0 },
            { id: "eid509", tween: [ "style", "${_safeAni0007}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid495", tween: [ "style", "${_safeAni0011}", "display", 'block', { fromValue: 'none'}], position: 302, duration: 0 },
            { id: "eid513", tween: [ "style", "${_safeAni0011}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 },
            { id: "eid486", tween: [ "style", "${_safeAni0002}", "display", 'block', { fromValue: 'none'}], position: 799, duration: 0 },
            { id: "eid504", tween: [ "style", "${_safeAni0002}", "display", 'none', { fromValue: 'block'}], position: 1050, duration: 0 }         ]
      }
   }
},
"pageCurl": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[-351,-1324],{},{},[0.74,0.74]],
      id: 'purpleWedge',
      type: 'image',
      rect: [462,1212,2036,1028],
      fill: ['rgba(0,0,0,0)','images/purpleWedge2.svg']
   },
   {
      transform: [[-281,121],{},{},[0.255,0.255]],
      id: 'pageCurl',
      type: 'image',
      rect: [25,1218,834,966],
      fill: ['rgba(0,0,0,0)','images/pageCurl2.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_pageCurl}": [
            ["transform", "scaleX", '0.15'],
            ["transform", "translateY", '-999.85px'],
            ["transform", "translateX", '10.76px'],
            ["transform", "scaleY", '0.15']
         ],
         "${_purpleWedge}": [
            ["transform", "scaleX", '0.74'],
            ["transform", "translateY", '-1324.31px'],
            ["transform", "translateX", '-351.19px'],
            ["transform", "scaleY", '0.74']
         ],
         "${symbolSelector}": [
            ["style", "height", '2320px'],
            ["style", "width", '4000px'],
            ["style", "overflow", 'hidden']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1110,
         autoPlay: false,
         labels: {
            "curl": 300,
            "complete": 942
         },
         timeline: [
            { id: "eid23298", tween: [ "transform", "${_pageCurl}", "scaleX", '1', { fromValue: '0.15'}], position: 327, duration: 543 },
            { id: "eid23300", tween: [ "transform", "${_pageCurl}", "translateX", '265.16px', { fromValue: '10.76px'}], position: 327, duration: 543 },
            { id: "eid23299", tween: [ "transform", "${_pageCurl}", "scaleY", '1', { fromValue: '0.15'}], position: 327, duration: 543 },
            { id: "eid23301", tween: [ "transform", "${_pageCurl}", "translateY", '-1298.99px', { fromValue: '-999.85px'}], position: 327, duration: 543 }         ]
      }
   }
},
"pageCurlNewAnimation": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      transform: [[197,-411]],
      id: 'purpleWedge',
      type: 'image',
      rect: [-198,413,2200,770],
      fill: ['rgba(0,0,0,0)','images/purpleWedge.png']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_purpleWedge}": [
            ["transform", "translateX", '197.66px'],
            ["transform", "translateY", '-411.66px']
         ],
         "${symbolSelector}": [
            ["style", "height", '775px'],
            ["style", "width", '2141.9396551724px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 1500,
         autoPlay: true,
         labels: {
            "start": 150
         },
         timeline: [
         ]
      }
   }
},
"darkerBG": {
   version: "0.1.6",
   build: "0.11.0.150",
   baseState: "Base State",
   initialState: "Base State",
   gpuAccelerate: true,
   content: {
   dom: [
   {
      rect: [0,0,1021,766],
      transform: {},
      id: 'Rectangle',
      stroke: [0,'rgba(0,0,0,1)','none'],
      type: 'rect',
      fill: ['rgba(238,238,238,1.00)']
   },
   {
      rect: [-337,114,1024,736],
      id: 'backgroundTextureDarker2',
      transform: [[356,-97],{},{},[1.038,1.038]],
      type: 'image',
      fill: ['rgba(0,0,0,0)','images/backgroundTextureDarker.svg']
   }],
   symbolInstances: [
   ]
   },
   states: {
      "Base State": {
         "${_Rectangle}": [
            ["style", "height", '766px'],
            ["color", "background-color", 'rgba(238,238,238,1.00)']
         ],
         "${symbolSelector}": [
            ["style", "height", '735px'],
            ["style", "width", '1023px']
         ],
         "${_backgroundTextureDarker2}": [
            ["transform", "scaleX", '1.03853'],
            ["transform", "scaleY", '1.03853'],
            ["transform", "translateX", '356.37px'],
            ["transform", "translateY", '-97.5px']
         ]
      }
   },
   timelines: {
      "Default Timeline": {
         fromState: "Base State",
         toState: "",
         duration: 0,
         autoPlay: true,
         timeline: [
         ]
      }
   }
}
};


Edge.registerCompositionDefn(compId, symbols, fonts, resources);

/**
 * Adobe Edge DOM Ready Event Handler
 */
$(window).ready(function() {
     Edge.launchComposition(compId);
});
})(jQuery, AdobeEdge, "EDGE-108138811");
