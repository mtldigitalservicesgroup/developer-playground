Ext.define('epiduo_ped.view.VideoPlayer',
{
	extend:  'Ext.Container',  
    xtype:   'videoPlayer',
    height: '768px',
    width:  '1024px',
    id: 'videoPlayer',
    playthroughs: 0,
    requires:['Ext.Video', 'Ext.Panel','Ext.util.DelayedTask'],
    config:{
       //cls:"page1_0BG",
	items:[					
	
							 
		{
			xtype     : 'video',
			enableControls: false,
			left      : '0px',
			top       : '0px',
			height    : '768px',
			width     : '1024px',
			hidden    : false,
			autoResume: true,
			url       : "app/view/1-0/video/1_1intro.mp4",
			posterUrl : 'app/view/1-0/images/slide.png',
			style     : 'border-style:Solid; border-width:0px;',
			id        : 'introvideoPlayer',
			element: 'element'
		},	
					 
		{
			xtype	: 'panel',
			id      : 'tweetBallons',
			left	: '0px',
			top	: '0px',
			height	: '768px',
			width	: '1024px',
			hidden	: true,

			listeners: {
                            painted: {                        
                                fn: function(e) {
                                Ext.getCmp('introvideoPlayer').setPosterUrl('app/view/1-0/images/slide3@2x.png');
                                Ext.getCmp('introvideoPlayer').setUrl('app/view/1-0/video/1_1outro.mp4');
                        }
                    }
			},
			items:[
                    {
                            style:'background-color: #8a8c8e;',				                           
                            html: ["<iframe id=\"quiz\" src=\"tweetBalloons.html\" scrolling=\"no\" frameborder=\"0\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>"].join("")
                    },
                    ]
			
		},	
		
		
		{	
			id: 'pleaseWait',
			xtype:   'container',			
			height:  '20px',
			width:   '100px',
			bottom:     '20px',
			left:    '20px',
			html: ["Please wait..."].join(""),
			hidden: true
		},

		{	
			id: 'introContBtn',
			xtype:   'image',			
			height:  '23px',
			width:   '100px',
			top:     '715px',
			right:    '45px',
			cls: 'btn_continue',
			hidden: true
		},	
					  
					   
					   
	   {  //Tapped to play outro
			xtype:'hotspot',	
			width:125,
			height:50,
			top:700,
			right:35,
			id: 'tweetBalloonContinue',
			hidden    : true,
			listeners: {                             
                            tap: {
                                fn: function(e) {
                                     Ext.getCmp('tweetBallons').setHidden(true);   
                                     Ext.getCmp('introvideoPlayer').play();     
                                     Ext.getCmp('tweetBalloonContinue2').setHidden(false);                                                                           
                                     Ext.getCmp('tweetBalloonContinue').setHidden(true);
                                     Ext.getCmp('introContBtn').setHidden(true);
                                },
                                element: 'element'
                            }
			  }
		},	
		{	
			xtype:   'image',			
			height:  '32px',
			width:   '53px',
			top:     '25px',
			right:    '25px',
			cls: 'btn_skip'
		},			
		{  
			xtype:'hotspot',
			height:  '70px',
			width:   '78px',
			top:10,
			right:10, 
			id:'1_0skipIntro'
		},             
		{  //tapped to go to content
			xtype:'hotspot',                         
			id: 'tweetBalloonContinue2',
			hidden:true,
			width:1024,
			height:768,
			top:0,
			left:0,			
			listeners: {                             
				tap: {
					fn: function(e) {
					epiduo_ped.app.getController('Controller').showPagebyID('1-1-1',1,0);
					epiduo_ped.app.getController('Controller').showMenu();
					epiduo_ped.app.getController('Controller').getCmpHighlightNavBtn('1-1-1'); 
				 },
			element: 'element'
			}
		}
	}				
	]
},
	
	
initialize: function(){
	
	var vidEnded = function () {
			if (Ext.getCmp('introvideoPlayer').getUrl() == "app/view/1-0/video/1_1intro.mp4") {
				Ext.getCmp('pleaseWait').setHidden(true);	
				Ext.getCmp('tweetBallons').setHidden(false);			
				Ext.getCmp('tweetBalloonContinue').setHidden(false);
				Ext.getCmp('introContBtn').setHidden(false);				
				//console.log("################ vidEnded if statement executed ###################");////////////Remove
			}	
		//Ext.getCmp('introvideoPlayer').setDisabled(true);
		Ext.getCmp('introvideoPlayer').removeListener('ended', vidEnded);
		//alert("vidEnded fired");////////////Remove
	};
	
	var vidPlaying = function () {	
		var delayedPleaseWait = Ext.create('Ext.util.DelayedTask', function() {						
			Ext.getCmp('pleaseWait').setHidden(false);	
		
		});							
		delayedPleaseWait.delay(8000);
		epiduo_ped.app.getController('Controller').setTabsVisibility('1-0');
		Ext.getCmp('introvideoPlayer').removeListener('play', vidPlaying);	
                //alert("vidPlaying fired");////////////////Remove
	};

	Ext.getCmp('introvideoPlayer').addListener('ended', vidEnded);
	Ext.getCmp('introvideoPlayer').addListener('play', vidPlaying);
	
	},
	 
        // I want to be able to call this from the parent reset function
	killListeners:function(){
        Ext.getCmp('introvideoPlayer').clearListeners();
   }
});
