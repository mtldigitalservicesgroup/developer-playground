/* 
 * 
 * Abstract Base Class for all Sliding Containers (i.e: pi, is etc...)
 * 
 */
Ext.define('epiduo_ped.view.AbsSlidingContainer',
{
    extend:  'Ext.Container',  
    xtype:   'absSlidingContainer',
    left: '988px',
    
    slide_panel_type: 0,
        
    openFromleft:   988,
    openToLeft:      28,
    closeFromleft:   28,
    closeToLeft:    988,
    
    bOpened:        false,
    
    
    config:
    {
    
    },
    initialize:function(){
        
       epiduo_ped.app.on({
                slidingPanelOpened: this.onSliderOpen,
                scope:this
            });
            
       epiduo_ped.app.on({
                slidingPanelClosed: this.onSliderClosed,
                scope:this
            });
            
       this.callParent();      
    },
    /* 
     * NOTE: right now all child classes implementation
     * of these functions are the same (they either hide or show themselves)
     * if that changes then they can override these
     * */
     onSliderOpen:function(type){
             
             //alert('pi onOpenEvent type: '+ type);
             
             //if window being opened is not us then hide ourself
             var arItems = this.getInnerItems();
             if(type != this.slide_panel_type)
                 arItems[0].setHidden(true);
             else
                 this.bOpened = true;
         
     },
     //------------------------
     onSliderClosed:function(type){

         //alert('pi onClosedEvent type: '+ type);

         //if window being closed is not us then show ourself
         var arItems = this.getInnerItems();
         if(type != this.slide_panel_type)
             arItems[0].setHidden(false);
          else
             this.bOpened = false;
     }
        
    
});


