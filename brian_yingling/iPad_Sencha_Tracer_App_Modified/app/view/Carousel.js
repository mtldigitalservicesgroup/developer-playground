Ext.define('epiduo_ped.view.Carousel', {
	extend: 'Ext.carousel.Carousel',
        
        xtype:     'thecarousel',
        direction: 'horizontal',
        directionLock : true,
        indicator : false,
        zindex :0,
        config: {
		defaults: {
                styleHtmlContent: true
        }
	
    },
	initialize: function(){
		var the_carousel = this;
	        //this code block so Controller will become aware when
                //this carousel is painted on screen
                //then we will have reference in controller for this carousel
                    the_carousel.on('painted', function() {
                        the_carousel.fireEvent('viewready', the_carousel);
                    },//
                
                //Override on drag end so we can prevent user from dragging back to
                //splash and sync up carousel with nav button active states
                //Controller will handle
                    the_carousel.onDragStart = function(e) {
                        the_carousel.fireEvent('dragstart', e);
                    },
                    the_carousel.onDrag = function(e) {
                        the_carousel.fireEvent('drag', e);
                    },
                    the_carousel.onDragEnd = function(e) {
                        the_carousel.fireEvent('dragend', e);
                    },
                    the_carousel.animationListeners.animationend = function(e) {
                       the_carousel.fireEvent('animationended', e);
                    }, null, { single : true });
                
                // prevent native nidicator from appearing on page
                the_carousel.setIndicator(false);
                
                
                //add all slides from our app/store/Slides.js
                Ext.getStore('Slides').load(function(slides){
                
                var items_final = [];
                var items = [];
                var item ="";
                var carousel_item ="";
                
                
                var length = 18; // slides.length would throw an error as the sitemap is included in the store and would throw an error
                for(var i=0 ;i< length; ++i){
                       if( i +1 < slides.length && slides[i+1].get('etl_slide_type') != epiduo_ped.app.SLIDE_TYPE_SUB){                          
                           item ={
                              xtype: slides[i].get('etl_carousel_item_xtype'),  
                              navBtnId: slides[i].get('etl_btn_id') 
                           }
                          items_final.push(item);
                          continue;
                       }
                       
                       //vertical Carousel basic configuration:
                       carousel_item={
                            xtype: 'verticalCarousel',
                            direction: 'vertical',
                            directionLock: true,
                            navBtnId: slides[i].get('etl_btn_id'),
                            indicator : false
                       } 
                       
                       //vertical Carousel Carousel item(s):
                       var bMoreSlides = false;
                       do
                       {
                          item ={
                             xtype: slides[i].get('etl_carousel_item_xtype'),  
                             navBtnId: slides[i].get('etl_btn_id')
                          }
                          
                          items.push(item);
                          
                          if( i +1 < slides.length && slides[i+1].get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SUB){
                              ++i;
                              bMoreSlides = true;                               
                          }
                          else
                              bMoreSlides = false;  
                          
                       }
                       while (bMoreSlides);
                       
                       
                       carousel_item.items = items;
                       
                       
                       
                       //add listeners to this vertical carousel:
                       carousel_item.initialize= function() {
                        var the_vertical_carousel = this;

                            ////////Vertical Carousel ON DRAG START://////////////////////////////////////////////////////////////////////
                            the_vertical_carousel.onDragStart = function(e) {
                                
                                 //stay put if this slide doesn't allow swipe up or down:///////
                                var acIdx = the_vertical_carousel.getActiveIndex();
                                var dirY= e.deltaY;
                                var up = false;
                                var down = false;
                                if(dirY < 0)
                                   down = true
                                else if(dirY > 0)
                                   up = true;

                                var item = the_vertical_carousel.getAt(acIdx);
                                var bAllowUp = true;
                                var bAllowDown = true;
                                var slideId = '';
                                var slideCompare = false;
                                var bReturn = false;
                                Ext.getStore('Slides').load(function(slides){
                                     Ext.each(slides, function(slide){
                                         
                                         
                                        slideId     = slide.get('etl_btn_id');                
                                        bAllowUp    = slide.get('etl_allow_up');
                                        bAllowDown  = slide.get('etl_allow_down');

                                        slideCompare = slideId == item.navBtnId;
                                        
                                        if(slideCompare && !bAllowUp && up){
                                          bReturn = true;
                                          return;
                                        }
                                            
                                        else if(slideId == item.navBtnId && !bAllowDown && down == true)
                                            return;
                                     });
                                 });
                                 
                                 if(bReturn)
                                     return;
                                 ////////////////////////////////////////////////////////////////
                                
                                
                                  var direction = the_vertical_carousel.getDirection(),
                                    absDeltaX = e.absDeltaX,
                                    absDeltaY = e.absDeltaY,
                                    directionLock = the_vertical_carousel.getDirectionLock();

                                    // alert("direction " + direction + "absDeltaX " + absDeltaX + "absDeltaY " + absDeltaY + " directionLock " + directionLock );
                                    the_vertical_carousel.isDragging = true;

                                    if (directionLock) {
                                        if ((direction === 'horizontal' && absDeltaX > absDeltaY)
                                            || (direction === 'vertical' && absDeltaY > absDeltaX)) {
                                            e.stopPropagation();
                                        }
                                        else {
                                            the_vertical_carousel.isDragging = false;
                                            return;
                                        }
                                    }

                                    if (the_vertical_carousel.isAnimating) {
                                        the_vertical_carousel.getActiveCarouselItem().getTranslatable().stopAnimation();
                                    }

                                    the_vertical_carousel.dragStartOffset = the_vertical_carousel.offset;

                                    the_vertical_carousel.dragDirection = 0;
                                    
                            }
                            ////////Vertical Carousel: ON ANIMATION END:////////////////////////////////////////////////////////////////////////
                            the_vertical_carousel.animationListeners.animationend = function(translatable) {
                                 
                                 //the_vertical_carousel.fireEvent('animationended', e);
                                 //handle it here rather than in the controlller,
                                 //because it will get very complicated with all the instances of vertical carousels
                                 //then call the controller's functions as required to set refrrence or do image swap:
                                 
                                
                                


                                var currentActiveIndex = the_vertical_carousel.getActiveIndex(),
                                    animationDirection = the_vertical_carousel.animationDirection,
                                    axis = the_vertical_carousel.currentAxis,
                                    currentOffset = translatable[axis],
                                    itemLength = the_vertical_carousel.itemLength,
                                    offset;

                                the_vertical_carousel.isAnimating = false;

                                translatable.un(the_vertical_carousel.animationListeners);

                                if (animationDirection === -1) {
                                    offset = itemLength + currentOffset;
                                }
                                else if (animationDirection === 1) {
                                    offset = currentOffset - itemLength;
                                }
                                else {
                                    offset = currentOffset;
                                }

                                offset -= the_vertical_carousel.itemOffset;
                                the_vertical_carousel.offset = offset;
                                the_vertical_carousel.setActiveItem(currentActiveIndex - animationDirection);



                                ///////// SWAP FOR HIGH RES////////////////////////////////////////
                                var increment = 0;
                                var idx = (currentActiveIndex- animationDirection);
                                
                                item = the_vertical_carousel.getAt(idx);
                                //this.swapImage(item);
                                //////////////////////////////////////////////////////////////////////////////////
                                
                                //alert(idx);
                                 
                                //alert(item.navBtnId);
                                
                                //this.setRefImg(item.navBtnId);
                                epiduo_ped.app.getController('Controller').setRefImg(item.navBtnId);
                                
                            }
                       }
                       
                       
                       items_final.push(carousel_item);
                       
                       items = [];
                       
                       
                       
                }
                
                the_carousel.setItems(items_final);
               
            }); //end  Ext.getStore('Slides').load(function(slides)
                
            the_carousel.callParent();
	}
        
});

/* var items = m_carousel.getInnerItems();
                theparent.setTabsVisibility('1-1-1');
                theparent.setCarouselActiveItem(1);
                items[1].setActiveItem(0);
                
                */