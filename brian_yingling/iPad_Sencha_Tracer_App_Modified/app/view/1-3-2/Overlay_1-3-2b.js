var me = null;
Ext.define('epiduo_ped.view.1-3-2.Overlay_1-3-2b',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_1-3-2b',
    id:     'overlay_1-3-2b',
    
    config:
    {
        //src: 'app/view/1-2/images/overlay.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
          //style:   'background-color: #ffffff;',
        
        items: [

            {   
                cls:'overlay_1-3-2HeaderMale',
                height: '19px',
                width :'560px',
                left: '50px',
                top: '-40px',
                id:'1_3_2bOverLayHeader'

            },
            { 
                 xtype     : 'video',
                 left      : '50px',
                 top       :'0px',
                 height    : '371px',
                 width     :'654px',
                 url       : "app/view/1-3-2/video/male_Tolerability.mp4",
                 posterUrl : 'resources/images/video/genericPoster@2x.png',
                 style     : 'border-style:Solid; border-width:1px;',
                 id:'1_3_2bvideoPlayer'
             },
              {   
                cls:'overlay_1-3-2Text',
                height: '204px',
                width :'936px',
                left:'5px',
                top: '395px'
            }
            ,
              {   
                cls:'overlay_1-3-2ButtonFemaleInactive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_3_2bfemaleButton'
            }
            ,
              {   
                cls:'overlay_1-3-2ButtonMaleActive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_3_2bmaleButton'
            },
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_3_2bfemaleVidButton'
            }
            ,
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_3_2bmaleVidButton'
            }
            
            
            
        ]
    },

    initialize: function(){        
      //1_1_1_femaleVidButton.me_video = ''; 
    //  myobject = Ext.get('1_1_1_femaleVidButton');
    // alert(myobject);
    //    myobject.setVideo('app/view/1-1-1/video/male_Baseline.mp4');
    }
    
   
});
