Ext.define('epiduo_ped.view.1-3-2.Page_1-3-2',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-3-2',
    fullscreen: true,
    
    config:
    {
     cls:"page1_3_2BG"
        
    },
       items:[ 
      
      
       {  
           xtype:'hotspot',
           width:320,
           height:400,
           top:200,
           left:170,
           id: 'hotspot_page1-3-2_a'
        },
        {  
           xtype:'hotspot',
           width:320,
		   height:400,
		   top:200,
		   left:533,
           id: 'hotspot_page1-3-2_b'
        },
        {
            xtype:'pleasesee'
            
        }
       ]
    }
);
