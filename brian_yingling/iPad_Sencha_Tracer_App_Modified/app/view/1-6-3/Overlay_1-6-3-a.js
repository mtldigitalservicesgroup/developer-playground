var me = null;
Ext.define('epiduo_ped.view.1-6-3.Overlay_1-6-3-a.js',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_1-6-3-a',
    curIdx:  0,
    
    
    config:
    {
        //src: 'app/view/1-6-3/images/overlay-a.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
        
    
        items:[
        {  
           xtype:'img',
           src: 'app/view/1-6-3/images/overlay-a@2x.png',
	   	   cls:'page1_6_3-a-BG',
           height: '603px',
           width:  '936px',		   
           top:0,
           right:0,
           id:'id_img_overlay1-6-3'
        },    
        {  
           xtype:'hotspot',
           width:86,
           height:35,
           top:465,
           left:380,
           id: 'hotspot_1-6-3a',
           listeners: {
            tap: {
                fn: function(e) {
                   this.parent.swapImg('app/view/1-6-3/images/overlay-a@2x.png');
                   this.parent.curIdx = 0;
                },
                element: 'element'
            }
           }
        },
        {  
           xtype:'hotspot',
           width:95,
           height:35,
           top:465,
           left:469,
           id: 'hotspot_1-6-3b',
           listeners: {
            tap: {
                fn: function(e) {
                   //alert('test2');
                   this.parent.swapImg('app/view/1-6-3/images/overlay-b@2x.png');
                   this.parent.curIdx = 1;     
                },
                element: 'element'
            }
           }
        },
        {  
           xtype:'hotspot',
           width:575,
           height:380,
           top:60,
           left:180,
           id: 'hotspot_1-6-3c',
           listeners: {
            tap: {
                fn: function(e) {
                   //alert('test2');
                   
                   if(this.parent.curIdx == 0){
                        this.parent.swapImg('app/view/1-6-3/images/overlay-b@2x.png');
                        this.parent.curIdx = 1;
                   }
                   else{
                        this.parent.swapImg('app/view/1-6-3/images/overlay-a@2x.png');
                        this.parent.curIdx = 0;
                   }     
                },
                element: 'element'
            }
           }
        }
    
    ]
    
    },
    swapImg:function(the_src){
        
        //alert('got it');
        var items = this.getItems();
        
        var overlay_img =  items.items[0];
        
        overlay_img.setSrc(the_src);
    }
    

});


