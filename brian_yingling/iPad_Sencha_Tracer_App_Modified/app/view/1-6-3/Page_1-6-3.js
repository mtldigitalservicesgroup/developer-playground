Ext.define('epiduo_ped.view.1-6-3.Page_1-6-3',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-6-3',
    fullscreen: true,
    
    config:
    {
        cls:"page1_6_3BG",
       items:[ 
       
        {  
           xtype:'hotspot',
           width:715,
           height:263,
		   top:188,
		   left:150,
           id: 'hotspot_page1-6-3_a'
        },
        {
            xtype:'pleasesee'
            
        }
       ]
    }
    
   
});
