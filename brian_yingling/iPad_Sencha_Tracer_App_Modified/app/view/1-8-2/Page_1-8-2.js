Ext.define('epiduo_ped.view.1-8-2.Page_1-8-2',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-8-2',
    fullscreen: true,
    
    config:
    {
     cls:"page1_8_2BG",
     
       items:[ 
      
        {  
           xtype:'hotspot',
           width:150,
           height:220,
           top:320,
           left:130,
           id: 'hotspot_page1-8-2_a',
           etl_setImgbyID:0,
           etl_overlay_id:'overlay_1-8-2-a'
        },
        {  
           xtype:'hotspot',
           width:150,
           height:220,
           top:320,
           left:282,
           id: 'hotspot_page1-8-2_b',
           etl_setImgbyID:1,
           etl_overlay_id:'overlay_1-8-2-a'
        },
        {  
           xtype:'hotspot',
           width:150,
           height:220,
           top:320,
           left:440,
           id: 'hotspot_page1-8-2_c',
           etl_setImgbyID:2,
           etl_overlay_id:'overlay_1-8-2-a'
        },
        {  
           xtype:'hotspot',
           width:170,
           height:220,
           top:320,
           left:610,
           id: 'hotspot_page1-8-2_d',
           etl_setImgbyID:3,
           etl_overlay_id:'overlay_1-8-2-a'
        },
        {  
           xtype:'hotspot',
           width:150,
           height:220,
           top:320,
           left:790,
           id: 'hotspot_page1-8-2_e',
           etl_setImgbyID:4,
           etl_overlay_id:'overlay_1-8-2-a'
        },
        {
            xtype:'hotspot',
            width:100,
            height: 45,
            top: 270,
            left:850,
            id: 'hotspot_page1-8-2_a2',
            etl_setImgbyID:0,
            etl_overlay_id:'overlay_1-8-2-a'
        },
        {
            xtype:'pleasesee',
            cls:'pleasee_white'
        }
       ]
    }
    
   
});
