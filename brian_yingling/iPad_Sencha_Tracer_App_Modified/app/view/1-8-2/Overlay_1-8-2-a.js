var me = null;
Ext.define('epiduo_ped.view.1-8-2.Overlay_1-8-2-a.js',
{
    extend:  'epiduo_ped.view.AbsOverlayContent',  
    xtype:   'overlay_1-8-2-a',
    id: 'overlay_1-8-2',
   
    
    config:
    {
        //src: 'app/view/1-6-3/images/overlay-a.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
        
    
        items:[
        {  
           xtype:'img',
           //src: 'app/view/1-8-2/images/overlay-a@2x.png',
           height: '603px',
           width:  '936px',
           top:0,
           right:0,
           cls:'',
           id:'id_img_overlay1-8-2',
           hidden:false
		   
        },    
        {  
           xtype:'hotspot_pagination',
           width:100,
           height:100,
           top:341,
           left:572,
           id: 'hotspot_1-8-2a',
           etl_paginate_direction:'rev'
        },
        {  
           xtype:'hotspot_pagination',
           width:100,
           height:100,
           top:341,
           left:672,
           id: 'hotspot_1-8-2b',
           etl_paginate_direction:'fwd'
        }
    
    ]
    
    },
    initialize: function(){
    
       //set up pagination:
       var img_array = new Array();
       img_array[0]= 'page1_8_2-a-BG';//'app/view/1-8-2/images/overlay-a@2x.png';
       img_array[1]= 'page1_8_2-b-BG';//'app/view/1-8-2/images/overlay-b@2x.png';
       img_array[2]= 'page1_8_2-c-BG';//'app/view/1-8-2/images/overlay-c@2x.png';
       img_array[3]= 'page1_8_2-d-BG';//'app/view/1-8-2/images/overlay-d@2x.png';
       img_array[4]= 'page1_8_2-e-BG';//'app/view/1-8-2/images/overlay-e@2x.png';
       
       this.paginateCurIdx = 0;
       this.paginateTarget = this.getItems().items[0];
       
       this.paginateImageArray = img_array;
      
       
       //do preload:
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0]='app/view/1-8-2/images/overlay-a@2x.png';
       ar_bkgrnd_img[1]='app/view/1-8-2/images/overlay-b@2x.png';
       ar_bkgrnd_img[2]='app/view/1-8-2/images/overlay-c@2x.png';
       ar_bkgrnd_img[3]='app/view/1-8-2/images/overlay-d@2x.png';
       ar_bkgrnd_img[4]='app/view/1-8-2/images/overlay-e@2x.png';
       this.firePreloadEvent(ar_bkgrnd_img);
       //////////////////////////
       
       
       this.callParent();
      
    }
    //---------------------------------
    
    

});


