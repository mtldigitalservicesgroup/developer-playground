Ext.define('epiduo_ped.view.1-2-2.Page_1-2-2',
{
    extend:  'epiduo_ped.view.AbsPage',
    
    /*
    SAVE! WE MAY NEED THIS IN THE FUTURE
    Implements:
    This is substitute for 'implements'functionality: (i.e: Thisclass implements IPreloader )  
    
    mixins: {
        ipreloader: 'epiduo_ped.util.Preloader'
     },
    */
    xtype:   'page1-2-2',
    me_1_2_2: '',
    theTab: '',
    
   
    config:
    {
       cls:"page1_2_2BG",
       items:[ 
           
            {
               xtype:'image',
               cls:'page1_2_2-tab1-BG',
               width:766,
               height:405,
               top:171,
               left:106,
               id: 'img_1-2-2_tab'
       
           },
           {  
               xtype:'hotspot',
               width:150,
               height:40,
               top:165,
               left:105,
               id: 'hotspot1-2-2a',
               listeners: {
                   tap: {
                        fn: function(e) {
                             this.parent.fireChangeTabEvent("1");
                        },
                        element: 'element'
                    }
               }
            },
            {  
               xtype:'hotspot',
               width:150,
               height:40,
               top:165,
               left:265,
               id: 'hotspot1-2-2b',
               listeners: {
                   tap: {
                        fn: function(e) {
                            this.parent.fireChangeTabEvent("2");
                        },
                        element: 'element'
                    }
               }
            },
            // Hot spots to starting points in the iPad:
             {  
                xtype:'hotspot_sitemap',
                width: 248,
                height:361,
                top:216,
                left:110,
                id: '0_0'
            },
            {  
                xtype:'hotspot_sitemap',
                width: 248,
                height:361,
                top:216,
                left:364,
                id: '1_0'
            },
            {  
                xtype:'hotspot_sitemap',
                width: 248,
                height:361,
                top:216,
                left:616,
                //id: 'hotspot_page1-2-2_d'
                id: '2_0'
            },
            {
                xtype:'pleasesee'
            
            }
       ]
      
    },
    initialize: function(){
    
       //set up the tab
       this.theTab = this.items.get('img_1-2-2_tab');
       this.tabClsNameBegin     ="page1_2_2-tab";
       this.tabClsNameEnd       = "-BG";
       
       //preload images:
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0]='app/view/1-2-2/images/tab-1.png';
       ar_bkgrnd_img[1]='app/view/1-2-2/images/tab-2.png';
       this.firePreloadEvent(ar_bkgrnd_img);
       
    }
    
});
