var me = null;
Ext.define('epiduo_ped.view.1-2-2.Overlay_1-2-2d.js',
{
    extend: 'Ext.Container',
    xtype:  'overlay_1-2-2d',
    id:     'overlay_1-2-2d',
    
    config: 
    {
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
       items: [
            {
                cls:    'overlay_1-2-2HeaderMale',
                height: '18px',
                width :'617px',
                left: '50px',
                top: '-40px',
                id:'1_2_2dOverLayHeader'

            },
            { 
                 xtype     : 'video',
                 left      : '50px',
                 top       :'0px',
                 height    : '371px',
                 width     :'654px',
                 url       : "app/view/1-2-2/video/male_Week12.mp4",
                 posterUrl : 'resources/images/video/genericPoster@2x.png',
                 style     : 'border-style:Solid; border-width:1px;',
                 id:'1_2_2dvideoPlayer'
             },
              {   
                cls:'overlay_1-2-2Text',
                height: '204px',
                width :'936px',
                left:'5px',
                top: '395px'
            }
            ,
          /*  {
              cls:      'overlay_1-2-2ButtonToggleMale',
              height:   '23px',
              width:    '167px',
              top:      '0px',
              left:     '730px',
              id:       '1_2_2toggleButton'
            },*/
            
            
            {   
                cls:'overlay_1-2-2ButtonMaleInactive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_2_2_dmaleVidButton'
            },
            {   
                cls:'overlay_1-2-2ButtonMale12WKActive',               
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_2_2_dmale12wVidButton'
            }
            ,
            
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_2_2_dplay4Wk'
            },
            {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_2_2_dplay12Wk'
            },
            /*{
                xtype:'hotspotVideo',
                height:   '23px',
                width:    '167px',
                top:      '0px',
                left:     '730px',              
                id:     '1_2_2_dmaleToggleFemale'
            }*/
            
            
        ]
    },

    initialize: function(){        
      //1_2_2_femaleVidButton.me_video = ''; 
    //  myobject = Ext.get('1_2_2_femaleVidButton');
    // alert(myobject);
    //    myobject.setVideo('app/view/1-2-2/video/male_Baseline.mp4');
    }
    
   
});

