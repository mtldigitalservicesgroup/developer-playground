var me = null;
Ext.define('epiduo_ped.view.1-1-1.Overlay_1-1-1a.js',
{
    extend:  'epiduo_ped.view.AbsOverlayContent',  
    xtype:   'overlay_1-1-1a',
    id:     'overlay_1-1-1a',
    
    config:
    {
        //src: 'app/view/1-2/images/overlay.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
          //style:   'background-color: #ffffff;',
        
        items: [

            {   
                cls:'overlay_1-1-1HeaderFemale',
                height: '18px',
                width :'571px',
                left: '50px',
                top: '-40px',
                id:'1_1_1OverLayHeader'

            },
            { 
                 xtype     : 'video',
                 autoPause : true,
                 left      : '50px',
                 top       :'0px',
                 height    : '371px',
                 width     :'654px',
                 url       : "app/view/1-1-1/video/female_Baseline.mp4",
                 posterUrl : 'resources/images/video/genericPoster@2x.png',
                 style     : 'border-style:Solid; border-width:1px;',
                 id:'1_1_1videoPlayer'
             },
              {   
                cls:'overlay_1-1-1Text',
                height: '204px',
                width :'936px',
                left:'5px',
                top: '395px'
            }
            ,
              {   
                cls:'overlay_1-1-1ButtonFemaleActive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_1_1femaleButton'
            }
            ,
              {   
                cls:'overlay_1-1-1ButtonMaleInactive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_1_1maleButton'
            },
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_1_1_femaleVidButton'
            }
            ,
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_1_1_maleVidButton'
            }
            
            
            
        ]
    }
    
   
});

