
//var me = null;
Ext.define('epiduo_ped.view.1-1-1.Overlay_1-1-chart.js',
{
    extend:  'epiduo_ped.view.AbsOverlayContent',  
    xtype:   'overlay_1-1-chart',
    curIdx:   0,
    cls_array: '',
    
    config:
    {
        //src: 'app/view/1-2/images/overlay.png',
        //cls:'page1_2-a-BG',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '-20px',
        
        
        
        items:[
	    {
               xtype: 'img',
               height: '280px',
                //height: '603px',
               //width:  '936px',
               width: '400px',
               top:    '100px',
               left:   '250px',
               //cls: 'page1_2-a-BG',
               cls: 'overlay_1-1-1ButtonFemaleActive',
               id: 'id_img_1-2a',
               hidden: false
            },
            {  
               xtype:'hotspot_pagination',
               width:820,
               height:380,
               top:100,
               left:95,
               id: 'hotspot_1-1',
               etl_paginate_direction:'fwd'
            },
            {
                
                xtype: 'sliderextended',
                baseCls: 'etl_cls_base_slider_base_1',
                cls:     'etl_cls_slider_1',
                //id:      'etl_id_slider1',
                /*
                name: 'single_slider',
                plugins: [{
                    xclass : 'Ext.plugin.SliderFill',
                    fillCls : ['x-slider-fill4']
                }],
                */
                //label: 'Set style in slider.css!',Set style in slider.css!
                value: 0,
                minValue: 0,
                maxValue: 1,
                //top:500,
                top: 500,
                left: 400,
                increment: 1,
                width:200
                
            }
        ]
        
    },
    //------------------------------------------
    initialize:function(){
      
       //this.theimg = Ext.getCmp('id_img_1-2a'); 
       
       console.log('initialize overlay_1-1-chart');
      
       //set up pagination:
       var cls_array = new Array();
       cls_array[0] = 'overlay_1-1-1ButtonFemaleActive';
       cls_array[1] = 'overlay_1-1-1ButtonFemaleInactive';
       /*cls_array[0]= 'page1_2-a-BG';
       cls_array[1]= 'page1_2-c-BG';
       cls_array[2]= 'page1_2-d-BG';
       cls_array[3]= 'page1_2-e-BG';
       cls_array[4]= 'page1_2-f-BG';
       */
       this.paginateCurIdx = 0;
       this.paginateTarget = this.getItems().items[0];
       this.paginateImageArray = cls_array;
       //this.paginateTimer(2000,1000); //start delay, time between images
       this.paginateSlider = true;
       this.paginateSliderIncrements = new Array(0,1); //this should  directly correspond to sliderextended config (minValue,maxValue,increment)
       //////////////////////////////////////////////////
       
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0] = 'app/view/1-1-1/images/femaleButtonActive@2x.png';
       ar_bkgrnd_img[1] = 'app/view/1-1-1/images/femaleButtonInactive@2x.png'
       
       /* 
       //do preload:
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0]='app/view/1-2/images/overlay-a_1@2x.png';
       //ar_bkgrnd_img[1]='app/view/1-2/images/overlay-a_2@2x.png';
       ar_bkgrnd_img[1]='app/view/1-2/images/overlay-a_3@2x.png';
       ar_bkgrnd_img[2]='app/view/1-2/images/overlay-a_4@2x.png';
       ar_bkgrnd_img[3]='app/view/1-2/images/overlay-a_5@2x.png';
       ar_bkgrnd_img[4]='app/view/1-2/images/overlay-a_6@2x.png';
       this.firePreloadEvent(ar_bkgrnd_img);
       //////////////////////////
       */
      this.firePreloadEvent(ar_bkgrnd_img);
        
    }
   

});


