Ext.define('epiduo_ped.view.1-1-1.Page_1-1-1',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-1-1',

    config:
    {
       cls:"page1_1_1BG",
       items:[ {  
           xtype:'hotspot',
           width:640,
           height:313,
           top:200,
           left:200,
           //id: 'hotspot_page1-2_a',
           id: 'hotspot_page1-1',
           etl_overlay_id:'overlay_1-1-chart'
        }
        
       ]
      
    }
});
