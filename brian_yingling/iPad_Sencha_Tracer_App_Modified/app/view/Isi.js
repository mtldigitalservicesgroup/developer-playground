Ext.define('epiduo_ped.view.Isi', {
	extend: 'epiduo_ped.view.AbsSlidingContainer',
	
        me_isi:'',
        id:'slide_panel_isi',
        
        
        slide_panel_type: 1,
        
        openFromleft:   988,
        openToLeft:     493,
        closeFromleft:  493,
        closeToLeft:    988,
       
        
        config: {
		
                items: [
                           
                        {
                            xtype:'panel',
                            layout: 'vbox',
                            items:[
                            {
                                xtype:   'image',
                                //src:     'resources/images/tabs/tab-isi.png',
                                height:  '215px',
                                width:   '38px',
                                top:     '223px',
                                id:      'tab_isi'
                             },
                             {
                                 xtype:   'container',
                                 style:   'background-color: #7c7c7c;',
                                 height:  '707px',
                                 left:    '35px',
                                 width:   '534px'
                             },
                       
                             {  
                                 xtype:'hotspotSlidingPanel',
                                 src:     '',
                                 height:  '35px',
                                 width:   '35px',
                                 top:15,
                                 left:475,
                                 slide_back:true,
                                 id: 'hotspot_isi_closeX'
                             } ,
                             {  
                                 xtype:'hotspotSlidingPanel',
                                 width:60,
                                 height:218,
                                 top:225,
                                 right:-38,
                                 id: 'hotspot_isi'
                            },
                            {
                                xtype:   'image',
                                src:     'resources/images/tabs/tab-isi_content.png',
                                height:  '348px',
                                width:   '395px',
                                top:     '60px',
                                left:    '78px',
                                id:      'id_isi_content'  
                            }
                        ]
                     }
                    
                      
                       ]
	}
        
});


