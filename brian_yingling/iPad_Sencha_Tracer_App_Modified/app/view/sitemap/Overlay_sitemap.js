var me = null;
Ext.define('epiduo_ped.view.sitemap.Overlay_sitemap.js',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_sitemap',
    
    
    /*this is where hostspots start in the top left corner
    //all other one will build off these coordinates.
    hs_top:    60,
    hs_left:   20,
    hs_width:  100,
    hs_height: 105,*/
    
    config:
    {
       
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
        
    
        items:[
        {  
           xtype:'img',
           src: 'app/view/sitemap/images/sitemap@2x.png',
           cls:"sitemap",
           height: '490px',
           width:  '906px',
           top:0,		   
           right:'10px',
           id:'id_img_overlay_sitemap'
        }, 
        /////////////////////////////////////////////////
         
         {  
           xtype:'hotspot_sitemap',
           width:           95,
           height:          80,
           top:             60,
           left:            320,
           id: '0_0'
        },
        /*
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             160,
           left:            20,
           id: '0_1'
        },
        */
        //////////////////////////////////////////////////
      
        {  
           xtype:'hotspot_sitemap',
           width:           95,
           height:          80,
           top:             60,
           left:            430,
           id: '1_0'
        },
        
        {  
           xtype:'hotspot_sitemap',
           width:           95,
           height:          80,
           top:             145,
           left:            430,
           id: '1_1'
        },
        /*
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             266,
           left:            130,
           id: '1_2'
        },
        */
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           95,
           height:          80,
           top:             60,
           left:            535,
           id: '2_0'
        },
        /*
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             160,
           left:            240,
           id: '2_1'
        },
        /*
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             266,
           left:            240,
           id: '2_2'
        },
        
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            345,
           id: '3_0'
        },
       
       /*     
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             160,
           left:            345,
           id: '3_1'
        },
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            455,
           id: '4_0'
        },
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            600,
           id: '5_0'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             160,
           left:            600,
           id: '5_1'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             266,
           left:            600,
           id: '5_2'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             373,
           left:            600,
           id: '5_3'

        },
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            710,
           id: '6_0'
        },
        //////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            820,
           id: '7_0'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             160,
           left:            820,
           id: '7_1'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             266,
           left:            820,
           id: '7_2'
        }
        */
    
    ]
    
    },
    initialize: function(){
    
       var this_overlay = this;
       
       
      
    },
    //---------------------------------
    swapImg:function(direction){
        
        
       
    },        
    
    closeIt:function(){
        this.parent.hide();
       // this.stopVids();
    }
    

});


