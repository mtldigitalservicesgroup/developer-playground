Ext.define('epiduo_ped.view.1-6-2.Page_1-6-2',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-6-2',
    fullscreen: true,
    
    config:
    {
      cls:"page1_6_2BG",
       items:[ 
      
        {  
           xtype:'hotspot',
           width:740,
           height:300,
		   top:163,
		   left:160,
           id: 'hotspot_page1-6-2_a'
        },
        {
            xtype:'pleasesee'
            
        }
       ]
    }
    
   
});
