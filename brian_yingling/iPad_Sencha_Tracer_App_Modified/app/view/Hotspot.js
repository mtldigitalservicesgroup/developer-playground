Ext.define('epiduo_ped.view.Hotspot',
{
    extend:  'Ext.Img',  
    xtype:   'hotspot',
    
    /*These should be set in the child classes' item config*/
    etl_setImgbyID:                 -1,      // Set this in the child if the image in the overlay depends on which image was selected on its parent page 
    etl_overlay_id:                 '',     // Every child that will pop an overlay NEEDS to specify this
    etl_skip_intro:                 false,  // skip the intro animation and go to first page
    etl_go_home:                    false,  // go to  the home page
    etl_tap_from_splash:            false,  // go to first page
    skipStartTalking:               false,  // special 1
    skipStartTalking2:              false,  // special 2
    etl_shortcut:                   false,  //if true then navigate to another page in the app
    etl_shortcut_target_section:    -1,     //the main page index
    etl_shortcut_target_page:       -1,     //the sub-section page 
    
    config:
    {
        src: 'resources/images/hotspot/hotspot.png',
        height: '0px',
        width:  '0px',
        border : '2px',
        //style: 'color: red; border: solid',
        
        
        
        listeners: {
            tap: {
                fn: function(e) {
                    
                    if(!this.isCorrectClass(e))
                        return;
                    
                    this.DoEvent(this)
                   
                },
                element: 'element'
                
               
            }
        }
    },
    DoEvent:function(this_instance){
        
      this.fireEvent('hotspottap', this_instance);
      
    },
    //this is a hack to getting around the issue (sencha bug?) of tap events
    //being called twice!
    //first time through appeears to be a Browser Mouse Up event
    //why doesn't sencha intercept this 'Browser Mouse Up event'??'
    isCorrectClass:function(e){
    
         
         try{ 
          if(e.browserEvent != undefined) 
              return false;
         }
         catch(err){
             return true;
         }
         
         return true;
    },
    /*Drill down items within items to get a component by id name 
     *right now only suports 2 levels
     **/
    getCmpById:function(itemArray,hotspot_pi){
        
        for(var i  = 0 ; i < itemArray.length; ++i){
            
            //first see if this is the item we are looking for
            if(itemArray[i].id == hotspot_pi)
                return itemArray[i];
            //else maybe its inside another array in here
            else if(itemArray[i].items){
                
                for(var x  = 0 ; x < itemArray[i].items.length ; ++x){
                    if(itemArray[i].items[x].id == hotspot_pi)
                        return itemArray[i].items[x];
                }
            }
        }
        return undefined;
    }
    
                             
    
 });




