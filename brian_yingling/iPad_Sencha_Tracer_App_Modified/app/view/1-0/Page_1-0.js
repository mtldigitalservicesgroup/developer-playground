Ext.define('epiduo_ped.view.1-0.Page_1-0',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-0',
    requires: ['Ext.Animator'],
    height: '768px',
    width:  '1024px',
    id: 'page1VideoIntro',
    playthroughs: 0,
    players: 0,
    autoDestroy : true,
    config:
        
    {items:[ myPanel = Ext.create('epiduo_ped.view.VideoPlayer')]},
     
    resetAnimations:function(e){
        Ext.getCmp('videoPlayer').killListeners();
        this.removeAll(true,true);
        var myPanel = Ext.create('epiduo_ped.view.VideoPlayer');
        this.add([myPanel]);
     },
     getPlaythroughs:function(){         
         return this.playthroughs;
     },
     setPlaythroughs:function(myNum){         
         this.playthroughs = myNum;
     }
     ,
     getPlayers:function(myNum){         
         return this.players;
     }

});