/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('epiduo_ped.view.AbsNavButton',
{
    extend:  'Ext.Button',  
    xtype:   'absAbstractButton',
    fullscreen: true,
    style: 'width:100px; height:52px; margin-top:10px',
    // to do - move styling to CSS
    config:
    {
     
    },
    
    initialize:function(){
       this.callParent();      
    },
    
    highlight : function(){     
       this.setIconCls( this.etl_btn_activeclass )
    },
    
    unHighlight : function(){       
        this.setIconCls( this.etl_iconClass );
    }
   
    
});


