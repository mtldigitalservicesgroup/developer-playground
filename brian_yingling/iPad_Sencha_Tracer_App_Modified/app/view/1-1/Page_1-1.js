Ext.define('epiduo_ped.view.1-1.Page_1-1',
{
    extend:  'Ext.Panel',  
    xtype:   'page1-1',
    //   
    config:
    {        
        //cls:"page1_1BG",
        items: [
                          {  
                           xtype:'hotspot_sitemap',
                           width:           55,
                           height:          40,
                           top:             30,
                           left:            35,
                           id: 'hotspot_sitemap_SkipIntro',
                           zindex:100
                        },

                        {
                            style:'background-color: #8a8c8e;',
                            id:'tweetBallons',                           
                            html: ["<iframe id=\"quiz\" src=\"tweetBalloons.html\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>"
                            ].join("")
                        },
                         { 
                             xtype     : 'video',
                             autoPause : true,
                             left      : '0px',
                             top       : '0px',
                             height    : '768px',
                             width     : '1024px',
                             url       : "app/view/1-1/video/1_1intro.mp4",
                             posterUrl : 'app/view/1-1/images/slide@2x.png',
                             //style     : 'border-style:Solid; border-width:1px;',
                             id:'introvideoPlayer',
                             listeners: {                             
                                   ended: {
                                        fn: function(e) {

                                             Ext.getCmp('introvideoPlayer').setHidden(true);
                                             //this.parent.changeTab("1");
                                        },
                                        element: 'element'
                                    }
                              }
                         }
                ], 
                    listeners: {
                    painted: {                        
                        fn: function(e) {
                           // Ext.getCmp('introanimation').setHtml(""); 
                           // Ext.getCmp('introanimation').setHtml("<iframe id=\"quiz\" src=\"introAnimation.html\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>");
                        }
                    }
                }
               
    }
});
