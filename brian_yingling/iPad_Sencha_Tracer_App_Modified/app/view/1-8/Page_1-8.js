Ext.define('epiduo_ped.view.1-8.Page_1-8',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-8',
    fullscreen: true,
    
    config:
    {
   //  cls:"page1_8BG",
                      items: [{
                        xtype	: 'container',
                        id	: 'quizanimation',
                        left	: '0px',
                        top	: '-1px',
                        height	: '768px',
                        width	: '1024px',
                        border  : '0px',
                        style:'background-color: #8a8c8e;',
                        config:
                        {
                            listeners: {
                            painted: {                        
                                fn: function(e) {
                                      alert('paint');
                                      Ext.getCmp('quizanimation').setHtml("");
                                      Ext.getCmp('quizanimation').setHtml("<iframe id=\"quiz\" src=\"quiz.html\" frameborder=\"0\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>");
                                }
                            }
                        }
				            }
						},
						{  
                           xtype:'hotspot',
                           width:           140,
                           height:          40,
                           top:             645,
                           left:            850,
                           //id: 'hotspot_sitemap_1_8_1Continue',
						   id: '1_8_skipStartTalking2',
                           zindex:100
                        },

                        // {
                            // 
                            // id:'quizanimation',
                            // html: [""
                            // ].join("")
                        // },
                     /*   {
                            
                            xtype:   'image',
                            src:     'resources/images/hotspot/skipButton.png',
                            height:  '32px',
                            width:   '53px',
                            top:     '25px',
                            right:   '25px'
                        },*/
						
												
						{	
						xtype:   'image',			
						height:  '32px',
						width:   '53px',
						top:     '25px',
						right:    '25px',
						cls: 'btn_skip'
						},
						
                        {  
                            xtype:'hotspot',
                            height:  '70px',
                            width:   '78px',
                            top:10,
                            right:10, 
                            id:'1_8_skipStartTalking'
                        } 
                ], 

       
    }
    
   
});
