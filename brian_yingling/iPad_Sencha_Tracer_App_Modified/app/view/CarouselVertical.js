Ext.define('epiduo_ped.view.CarouselVertical', {
	extend: 'Ext.carousel.Carousel',
        
        xtype:     'verticalCarousel',
        direction: 'vertical',
        directionLock: true,
        indicator : true
        
});