Ext.require([
    'Ext.Toolbar'
]);

Ext.define('epiduo_ped.view.Main', {
	extend: 'Ext.Panel',
         requires:[epiduo_ped.view.AbsNavButton],
         //style: 'color: red; border: solid;width:100px;background-color: #7a5b97;',
	config: {
                    itemId: 'main',
                    fullscreen: true,
                    layout: 'fit',
                    height: '768px',
                    width:  '1024px',
                    items: [

                               {
                                 xtype:   'container',
                                 style:   'background-color: #ffffff;',
                                 height:  '707px',
                                 left:    '0px',                                 
                                 cls:      'tabPanelBGFill',
                                 hidden : true,
                                 width:   '1024px',
                                 id:      'overlay_bgFill'
                               },
                               
                               pi = Ext.create('epiduo_ped.view.Pi'),                               
                               isi = Ext.create('epiduo_ped.view.Isi'),
                               ref = Ext.create('epiduo_ped.view.Ref'),
                               theOnlyCarousel=Ext.create('epiduo_ped.view.MainCarousel'),
                               thetoolbar=Ext.create('epiduo_ped.view.NavBar'),
                               //theOnlyCarousel=Ext.create('epiduo_ped.view.Carousel'),
                                {  
                                   xtype:'hotspot',
                                   width:125,
                                   height:105,
                                   top:25,
                                   right:10,
                                   id: 'hotspot_home',
                                   // this class item added to designate hotspot as home button
                                   etl_go_home:'etl_go_home',
                                   hidden:false
                                },
                                {  
                                   xtype:'hotspot',
                                   width:1024,
                                   height:61,
                                   bottom:0,
                                   right:0,
                                   id: 'hotspot_navblocker',
                                   hidden:true
                                }				
			  ]
	 },
         
         
         initialize:function(){
         
      
    }
        
});
