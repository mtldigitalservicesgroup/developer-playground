Ext.define('epiduo_ped.view.1-3-1.Page_1-3-1',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-3-1',
    fullscreen: true,
    
    config:
    {
     cls:"page1_3_1BG",
       items:[ 
      
        
        {  
           xtype:'hotspot',
           width:640,
           height:313,
		   top:200,
		   left:200,
           id: 'hotspot_page1-3-1_A'
        },
        {
            xtype:'pleasesee'
            
        }
       ]
    }
    
   
});
