/* 
 * 
 * Base Class for Overlays
 * 
 */
Ext.define('epiduo_ped.view.AbsOverlayContent',
{
    extend:  'epiduo_ped.view.AbsContent',  
    xtype:   'absOverlayContent',
    fullscreen: true,
    
    config:
    {
        
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px'
    }
    
});

