/* 
 * 
 * Base Class for all slides
 * 
 */
Ext.define('epiduo_ped.view.AbsPage',
{
    extend:  'epiduo_ped.view.AbsContent',  
    xtype:   'absPage',
    fullscreen: true,
    
    config:
    {
        cls: '',
        height: '768px',
        width:  '1024px'
    },
    initialize:function(){
       
       this.callParent();      
    },
    /* Called by children */
    fireShowEvent : function(currentActiveItem) {  
                console.log("############### currentActiveItem >>>>>>>>>>>>> " + currentActiveItem);

       this.fireEvent('carouselItemActivate', currentActiveItem);
    },
    fireHideEvent : function(currentActiveItem) {       
       this.fireEvent('carouselItemDeActivate', currentActiveItem);
    },
    initialize:function(){
       this.callParent();      
    }
    
});


