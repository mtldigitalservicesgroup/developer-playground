Ext.define('epiduo_ped.view.1-2-1.Page_1-2-1',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-2-1',
    me_1_2_1: '',
    theTab: '',
   
    
    config:
    {
       cls:"page1_2_1BG",
       
       items:[ 
       {
           xtype:'image',
           cls:'page1_2_1-tab1-BG',
           width:758,
           height:314,
           top:137,
           left:133,
           id: 'img_1-2-1_tab'
       
       },
       {  
           xtype:'hotspot',
           width:137,
           height:40,
           top:130,
           left:133,
           id: 'hotspot1-2-1a',
           listeners: {
               tap: {
                    fn: function(e) {
                         //alert('test');
                         this.parent.changeTab("1");
                    },
                    element: 'element'
                }
           },
                   
          
           
        },
        {  
           xtype:'hotspot',
           width:137,
           height:40,
           top:130,
           left:275,
           id: 'hotspot1-2-1b',
           listeners: {
               tap: {
                    fn: function(e) {
                         //alert('test');
                         this.parent.changeTab("2");
                    },
                    element: 'element'
                }
           }
        },
        {  
           xtype:'hotspot',
           width:137,
           height:40,
           top:130,
           left:417,
           id: 'hotspot1-2-1c',
           listeners: {
               tap: {
                    fn: function(e) {
                         //alert('test');
                         this.parent.changeTab("3");
                    },
                    element: 'element'
                }
           }
        },
        {
           xtype:'hotspot',
           width:137,
           height:40,
           top:130,
           left:559,
           id: 'hotspot1-2-1d',
           listeners: {
               tap: {
                    fn: function(e) {
                         //alert('test');
                         this.parent.changeTab("4");
                    },
                    element: 'element'
                }
           }
        },
        {  
           xtype:'hotspot',
           width:640,
           height:500,
           top:200,
           left:300,
           //id: 'hotspot_page1-2_a',
           id: 'hotspot_page1-2',
           etl_overlay_id:'overlay_1-2-1'
        },    
        {
            xtype:'pleasesee'
            
        }
      ]
      
    },
    initialize: function(){
    
       this.me121 = this;
       this.theTab= Ext.getCmp('img_1-2-1_tab');
       
       
       //pre load all the background images. so there is no flicker
       var me = this;
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0]='app/view/1-2-1/images/tab-1.png';
       ar_bkgrnd_img[1]='app/view/1-2-1/images/tab-2.png';
       ar_bkgrnd_img[2]='app/view/1-2-1/images/tab-3.png';
       ar_bkgrnd_img[3]='app/view/1-2-1/images/tab-4.png';
        
       for(var idx=0 ; idx < ar_bkgrnd_img.length ; ++ idx){
           me.image = new Image();
           me.image.src = ar_bkgrnd_img[idx];
           //alert('image loaded! '+  ar_bkgrnd_img[idx])
           me.image.onload = function() {	
           //alert('loaded');
            //nothing right now...
          }
           
       }
       //////////////////////////
       
    },
    //---------------------------------
    changeTab:function(num){
        
       var theCls = "page1_2_1-tab"+ num + "-BG";
       this.theTab.setCls(theCls);
    }
   
});
