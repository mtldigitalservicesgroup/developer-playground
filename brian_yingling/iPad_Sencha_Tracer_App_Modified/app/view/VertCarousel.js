Ext.define('epiduo_ped.view.VertCarousel', {
    extend: 'Ext.carousel.Carousel',
    xtype: 'carousel',
    direction: 'vertical',
    directionLock: true,
   // navBtnId: slides[i].get('etl_btn_id'),
    indicator : false,
    items_final:[],
    me:[],
    //the_vertical_carousel:[],
    config: {
            defaults: {
            styleHtmlContent: true
            }
    },
    
    
    //_carouselID:'',
    slideCarouselIndex:'',
    
    
    initialize: function(){
        this.me = this;
        this.setData(this.dataFile)
        this._carouselID = Number(this.carouselID);     
        this.slideCarouselIndex = Number(this.carouselID);
        this.setAnimation(this.useAnimation);
        this.setIndicator(false);
    },

    
    /* These 3 events added to let us know when carousel item has changed (or in other words, 'is in view) */
    add : function() {
        this.callParent(arguments);
        this.getActiveItem().fireShowEvent();
    },
    
     
    onActiveItemAnimationEnd : function() {
      
        var  prevActiveItem = this.me.getActiveItem();
        this.me.callParent(arguments);
        
        var  currentActiveItem = this.me.getActiveItem();
        (prevActiveItem != currentActiveItem) && currentActiveItem.fireShowEvent(currentActiveItem);
    },
    
    setOffsetAnimated: function(offset) {
        var me            = this,
            animDirection = me.animationDirection,
            prevItem      = me.getActiveItem();

        this.callParent(arguments);

        if (animDirection != 0) {
            prevItem.fireHideEvent(prevItem);
        }
    },
       
    setData: function(sectionToLoad){ 
        this.items_final = new Array();
        the_vertical_carousel = this;
        the_vertical_carousel.setDirectionLock(true);
       
        Ext.getStore(sectionToLoad).load(function(slides){
               for(var i=0;i<slides.length;i++){
                   
                   myvar =  the_vertical_carousel.addSlide(slides[i]);
                }
                
                
        }
        );
        this.setDirection('vertical');
        this.add(this.items_final);       
        
        
        
        this.callParent();
    },   
    
    onDrag: function(e) {
        if (!this.isDragging) {
            return;
        }

        var startOffset = this.dragStartOffset,
            direction = this.getDirection(),
            delta = direction === 'horizontal' ? e.deltaX : e.deltaY,
            lastOffset = this.offset,
            flickStartTime = this.flickStartTime,
            dragDirection = this.dragDirection,
            now = Ext.Date.now(),
            currentActiveIndex = this.getActiveIndex(),
            maxIndex = this.getMaxItemIndex(),
            lastDragDirection = dragDirection,
            offset;

        if ((currentActiveIndex === 0 && delta > 0) || (currentActiveIndex === maxIndex && delta < 0)) {
          //  delta *= 0.5; 
          delta *=0.05;
        }

        offset = startOffset + delta;

        if (offset > lastOffset) {
            dragDirection = 1;
        }
        else if (offset < lastOffset) {
            dragDirection = -1;
        }

        if (dragDirection !== lastDragDirection || (now - flickStartTime) > 300) {
            this.flickStartOffset = lastOffset;
            this.flickStartTime = now;
        }

        this.dragDirection = dragDirection;

        this.setOffset(offset);
    },


    
    addSlide: function(slides){   

       var item ={
                xtype: slides.get('etl_carousel_item_xtype'),
                navBtnId:slides.get('etl_btn_id'),
                slideId:slides.get('etl_btn_id'),
                slideAllowUp:slides.get('etl_allow_up'),
                slideAllowDown:slides.get('etl_allow_down'),
                slideButtonClass:slides.get('etl_btn_class'),
                slideLoadingClass:slides.get('etl_btn_loadingclass'),
                slideActiveClass:slides.get('etl_btn_activeclass'),
                slideLabel:slides.get('etl_label'),
               // slideType:slides.get('etl_slide_type'),
                slideTabsVisible:slides.get('etl_tabsvisible'), 
                slidePleaseSeeVisible:slides.get('etl_please_see_visible'),
                slideNoSitemap:slides.get('etl_nositemap'),
                slideSiteMapID:slides.get('etl_sitemap_id'),                
                slideCarouselIndex:this.carouselID,//slides.get('etl_carousel_idx'),
                //slides[0].get('etl_carousel_item_xtype'),
                slideCarouselVertIndex:slides.get('etl_carousel_vert_idx'),
                slideOverlayBGColor:slides.get('etl_overlay_bg_fill_color'),
                slideHomeLink:slides.get('etl_hide_home_link') 
              
           }          
            
          
      this.items_final.push(item);    
      
      item.init
    }, 
    
    getItemsFinal : function(){
        return this.items_final;
    }
}
);

