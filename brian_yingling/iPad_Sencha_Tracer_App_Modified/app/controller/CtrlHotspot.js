/*
*  CtrlHotspot.js
*  
*  This  controller
*  handles hotspot taps inside the html (overlays, home,splash)
*
*/
Ext.define('epiduo_ped.controller.CtrlHotspot', {
    extend: 'Ext.app.Controller',
    
    my_overlay: undefined,
    m_carousel:[],

    config: {
        
        refs: { 
         //   MainCarousel:'maincarousel',
            Overlay_bgFill: '#overlay_bgFill',
            Hotspot_home: '#hotspot_home',
            Sitemap: '#sitemap',
            siteMapBtn: 'absAbstractButton[action=siteMapBtn]'
        },
        control: {
             maincarousel: {
                     viewready : 'onCarouselReady'
                },
            hotspot: {
                hotspottap: 'onHotspotTap',
                EvtCloseOverlay: 'onCloseOverlay'    
            }, 
            siteMapBtn: {
                tap: 'onSiteMapButtonTap'
            }
        }
    },
    init: function() {
       // console.log("running init"); 
    },
    //------------------------------------------------
    onCarouselReady:function(carousel){
        this.m_carousel = carousel            
    },
    //----------------------------------------------------
    onHotspotTap: function(e) {
       
        //alert('test');
      
        //TODO: DO WE NEED THIS??
        /*  check to see if page is 1_2_2 which tab is showing
        if (id_tapped == 'hotspot_page1-2-2_a' && Ext.getCmp('img_1-2-2_tab').getCls( )== 'page1_2_2-tab2-BG'){
            id_tapped = 'hotspot_page1-2-2_c';
        }else if (id_tapped == 'hotspot_page1-2-2_b' && Ext.getCmp('img_1-2-2_tab').getCls( )== 'page1_2_2-tab2-BG'){
            id_tapped = 'hotspot_page1-2-2_d';
        }
        */     
       //SKIP INTRODUCTION ANIMATION 
       if(e.etl_skip_intro){
           epiduo_ped.app.getController('Controller').showPagebyID('1-1-1',1,0);
           epiduo_ped.app.getController('Controller').showMenu();
           epiduo_ped.app.getController('Controller').getCmpHighlightNavBtn('1-1-1'); 
       } 
       //GO TO THE HOME PAGE
       else if(e.etl_go_home){
          // console.log("GO HOME     MainCarousel >>> " + this.m_carousel);
           // ******* this relies on the home page always being at 0,0 on the site map
           this.m_carousel.setActiveItem(0);
           this.m_carousel.getInnerItems( )[0].setActiveItem(0);           
//             this.m_carousel.setActiveItem(sectionNum);
//             this.m_carousel.getInnerItems( )[sectionNum].setActiveItem(pageNum);      
        
           //reset animations
            //if(typeof(Ext.getCmp('page1VideoIntro') == "object")) {
            //Ext.getCmp('page1VideoIntro').resetAnimations();

            //remove all icon active states:
            //epiduo_ped.app.getController('Controller').navIconRemoveAllActive();

            //hide menu
            //epiduo_ped.app.getController('Controller').hideMenu();

            //go to the page!
            //epiduo_ped.app.getController('Controller').setCarouselActiveItem(0); 

            //} else {
            //        alert("not ready");
            //};
       }
       //TAP FROM SPLASH - GO TO FIRST PAGE
       else if(e.etl_tap_from_splash){
           
           //we know if we have a tap on splash , we want to go to the first page:
               
            //hightlight btn
            epiduo_ped.app.getController('Controller').getCmpHighlightNavBtn('1-1-1'); 
            //go to the page!

            epiduo_ped.app.getController('Controller').setCarouselActiveItem(1);
            // show Menu
            epiduo_ped.app.getController('Controller').showMenu(); 
            epiduo_ped.app.getController('Controller').setTabsVisibility('1-1');
            
       }
       //SKIP A SECTION
       else if(e.skipStartTalking){
           
       }
       //SKIP A SECTION
       else if(e.skipStartTalking2){
           
       }
       //SHORTCUT:
       else if(e.etl_shortcut){
           
         this.navigateToPage(e.etl_shortcut_target_section,e.etl_shortcut_target_page);  
           
       }
       //JUST AN OVERLAY
       else{ // this is just an overlay:
          // console.log(e.etl_overlay_id + "  "  + e.etl_setImgbyID);
         this.showOverlay(e.etl_overlay_id,e.etl_setImgbyID) 
       }
       
    },
     //----------------------------------------------------
    onSiteMapButtonTap: function(navbtn){
        if(navbtn.etl_tap_from_splash){
        this.showOverlay('overlay_sitemap',-1);
        }

    },
    //-----------------------------------------------
    showOverlay:function (xtype1,etl_setImgbyID){
        
        if(this.my_overlay == undefined){
	   this.my_overlay = Ext.create('epiduo_ped.view.Overlay',xtype1,etl_setImgbyID);
           this.my_overlay.addToContainer();
	}
        else
            this.my_overlay.setXtype(xtype1,etl_setImgbyID);
        
        this.getOverlay_bgFill().show();
        this.getHotspot_home().hide();
        this.my_overlay.popIt();
     },
     //-----------------------------------------------
     onCloseOverlay:function(){
       //show home screen 
       this.getHotspot_home().show();

       //hid the iverlay back fill
       this.getOverlay_bgFill().hide();

       //reset sitemap icon:
       var sitemap = this.getSitemap();
       if(sitemap != undefined)
           sitemap.setIconCls('etl_nav_btn etl_nav_btn_sitemap');
         
       //finally close the overlay 
        this.my_overlay.closeIt();
     },
     navigateToPage:function(section,page){        
        this.m_carousel.setActiveItem(Number(section));
 
        var innerItems = this.m_carousel.getInnerItems();
        if(innerItems[section] instanceof Ext.carousel.Carousel){
           innerItems[section].setActiveItem(Number(page));
        }
     }
     //------------------------------------------------
     /*Not used currently 
     getValuefromHTML: function(fullstring, sKey) {
        
       //parse out and get the carousel_item_xtype
       //example: <div id="page1-0_A" etl_carousel_item_xtype="page1-0" class="hotspot"></div>
       
       var idx1 = fullstring.indexOf(sKey+"=");
       var idx2 = idx1 + sKey.length +2; //+2 to include ' =" '
       var idx3 = fullstring.indexOf("\"",idx2);
       
       return (fullstring.substring(idx2, idx3));
        
     },*/
});


