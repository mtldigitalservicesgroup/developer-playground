/*
*  Controller.js
*  
*  This is the main controller which
*  handles carousel and nav button events
*
*
*
*/
Ext.define('epiduo_ped.controller.Controller', {
    extend: 'Ext.app.Controller',
    
    //Carousel vars:
    activePage: 0,
    maxPages: 0,
    m_carousel: '',
    m_sitemapbtn: '',

    bTappedNavbtn:false,
    
	
    config: {
            refs: { 
                navBtn: 'button[action=navBtn]'
            },

    control: {
          //  navBtn: {
          //      tap: 'onNavButtonTap'
          //  },                
            thecarousel : {
                viewready : 'onCarouselReady',
                dragend: 'onDragEnd',
                drag: 'onDrag',
                dragstart: 'onDragStart',
                animationended:'onActiveItemAnimationEnd',
                activeitemchange:'onactiveitemchange'
            },
            hotspot_sitemap:{
                 siteMapTap: 'onSiteMapTap'
            }
            
            
        }
    },
    onactiveitemchange:function(){
        
      alert('test car')  
    },
    //------------------------------------------------
    onSiteMapTap:function(e){

        var id_tapped = e.target.id;
        
        console.log("### SITE MAP ID " + id_tapped);
        
        var theparent = this;
        
        var items = m_carousel.getInnerItems();
        
        switch(id_tapped){
            case 'hotspot_sitemap_1_0':
                Ext.getCmp('page1VideoIntro').resetAnimations()
                break          
            case'hotspot_sitemap_1_1':
                theparent.setCarouselActiveItem(1);
                items[1].setActiveItem(0);
               break;
            case 'hotspot_sitemap_1_8_1Continue':
                theparent.setCarouselActiveItem(8);
                items[8].setActiveItem(1); 
                break;
            case 'hotspot_sitemap_1_8':
                Ext.getCmp('quizanimation').setHtml(""); 
                Ext.getCmp('quizanimation').setHtml("<iframe id=\"quiz\" src=\"quiz.html\" frameborder=\"0\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>");
                break;
            case 'hotspot_sitemap_SkipIntro':      
                theparent.setCarouselActiveItem(1);
                items[1].setActiveItem(1);
                break;
            }
         
     
        
        var lastMainBtn =  "";
        Ext.getStore('Slides').load(function(slides){
                Ext.each(slides, function(slide){
                    
                    var slideSitemapId = slide.get('etl_sitemap_id');
                    //var navId   = navbtn.id;
                    var bequal = slideSitemapId == id_tapped;
                    
                    
                    if(  (slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_MAIN))
                        lastMainBtn = slide.get('etl_btn_id');
                    
                    if(  (slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_MAIN) && bequal ){
                       
                       //ok go tothe main slide:  
                       theparent.setCarouselActiveItem(slide.get('etl_carousel_idx'));
                       //this is a main slide so always set the vertical carousel (if this slide has children slides) to 0:
                       var items = m_carousel.getInnerItems();
                       var vertCarousel = items[slide.get('etl_carousel_idx')];
                       if(vertCarousel != 'undefined')
                            vertCarousel.setActiveItem(0);
                       ///////////////////////////////////////////////////////////    
                       
                       theparent.getCmpHighlightNavBtn(slide.get('etl_btn_id'));
                       theparent.deactivateSiteMapIcon();
                       epiduo_ped.app.getController('CtrlHotspot').closeOverlay();
                    
                    }
                    else if(slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SPLASH && bequal ){
                        
                       epiduo_ped.app.getController('CtrlHotspot').closeOverlay(); 
                        
                        //ok go tothe main slide:  
                       theparent.setCarouselActiveItem(slide.get('etl_carousel_idx'));
                       
                       theparent.deactivateSiteMapIcon();
                      
                       
                       //this is splash, so hide the toolbar:
                       theparent.hideMenu();
                       
                        
                    }
                    else if((slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SUB) && bequal ){ //this is a sub slide:
                        
                        epiduo_ped.app.getController('CtrlHotspot').closeOverlay();
                       
                        var items = m_carousel.getInnerItems();
                        
                        var vertCarousel = items[slide.get('etl_carousel_idx')];
                        
                        //ok we have the carousel with in the horizontal carousel,
                        //now set the main horizontal carousel active index then
                        //set the vertical carousel's active index
                        
                        theparent.setCarouselActiveItem(slide.get('etl_carousel_idx'));
                        theparent.getCmpHighlightNavBtn(lastMainBtn);
                        theparent.deactivateSiteMapIcon();
                       
                        vertCarousel.setActiveItem(slide.get('etl_carousel_vert_idx'));
                        
                    }
                });
         });
                           
                            
        
        
        
    },


    //
    //---------------------------------------------------
    deactivateSiteMapIcon:function(){
       
       if(this.m_sitemapbtn !='' && this.m_sitemapbtn != undefined)
           this.m_sitemapbtn.setIconCls('etl_nav_btn etl_nav_btn_sitemap');
    },
    //----------------------------------------------------
    onNavButtonTap: function(navbtn) {
        
        
        //based on the navbtn object , set the correct 'active' class:
        navbtn.setIconCls(navbtn.etl_btn_loadingclass);
       
        //kill all the vids:
        var overlay = Ext.getCmp('the_overlay');
        if(overlay != undefined && !overlay.isHidden() )
            epiduo_ped.app.getController('CtrlHotspot').closeOverlay();
        
        //site map is open AND user taps sitemap button again
        if(overlay != undefined && !overlay.isHidden() &&navbtn.id == "sitemap" ){
          this.deactivateSiteMapIcon();
          return;
        }
        
        
        bTappedNavbtn = true;
        
        
        //if its sitemap, pop the overlay with the sitemap in it:
        if(navbtn.id == "sitemap"){
            this.m_sitemapbtn = navbtn;
            epiduo_ped.app.getController('CtrlHotspot').showOverlay('overlay_sitemap');
            return;
        } else {
            
            this.resetSlideStates();
        }
        
       //go to the right slide:
        this.setCarouselActiveItem(navbtn.etl_carousel_idx);
        
        this.deactiveOtherNavBtns(navbtn);
        
        switch(navbtn.id){
             case '1-1':
                //Ext.getCmp('introanimation').setHtml(""); 
                //Ext.getCmp('introanimation').setHtml("<iframe id=\"quiz\" src=\"introAnimation.html\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>");
             break;
           /* case '1-6':
                Ext.getCmp('quizanimation').setHtml("");
                break;            
            case '1-5':
                Ext.getCmp('quizanimation').setHtml("");
                break;*/
            case '1-7':
			
                Ext.getCmp('quizanimation').setHtml("");
                break; 
            case '1-8':
			 Ext.getCmp('quizanimation').setHtml("");
             Ext.getCmp('quizanimation').setHtml("<iframe id=\"quiz\" src=\"quiz.html\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>");
                break;                
        }
        
       
        //set button to active:
        navbtn.setIconCls(navbtn.etl_btn_activeclass);

    },
    //---------------------------------------------------
    setCarouselActiveItem: function (idx){

     m_carousel.setActiveItem(idx);

    },

    //----------------------------------------------------
    showItem: function(id) {
    //console.log('Showing item',id);
    },
    //----------------------------------------------------
    onCarouselReady : function(carousel) {
        //do store load
        //use add on the carousel argument in store load callback.
        m_carousel = carousel 
        //alert('Carousel Ready!');

        //In case you want to start off at a different page other than the first,
        this.setCarouselActiveItem(0);
        activePage = m_carousel.getActiveIndex();
        maxPages = m_carousel.getItems();
        bTappedNavbtn = false;

    },
    //-------------------------------------------------
    showMenu: function(){
         var toolbar = Ext.getCmp('thetoolbar');
                    Ext.Animator.run({
                        element: toolbar.element,
                        duration: 0,
                        easing: 'ease-in',
                        preserveEndState: true,
                        from:{top:872},
                        to:{top:707}

                     });
    },
    hideMenu: function(){
         var toolbar = Ext.getCmp('thetoolbar');
                        Ext.Animator.run({
                        element: toolbar.element,
                        duration: 0,
                        easing: 'ease-in',
                        preserveEndState: true,
                        from:{top:707},
                        to:{top:872}

                     });
    },
     //---------------------------------------------------
     onCarouselChange :function ( cmp,newCard,oldCard,eopts) {
         
         console.log('on carousel change!');
        
     },
     //------------------------------------------------
     //Override
 
     onDragStart : function(e){
         
         //prevent scrolling back to splash screen
         if(e.deltaX >= 0 && m_carousel.getActiveIndex() ==1 || ( m_carousel.getActiveIndex() ==0) )
         {
             //alert('not going back');
             return;
         }
         
         
         
         var direction = m_carousel.getDirection(),
            absDeltaX = e.absDeltaX,
            absDeltaY = e.absDeltaY,
            directionLock = m_carousel.getDirectionLock();

        // alert("direction " + direction + "absDeltaX " + absDeltaX + "absDeltaY " + absDeltaY + " directionLock " + directionLock );
        m_carousel.isDragging = true;

        if (directionLock) {
            if ((direction === 'horizontal' && absDeltaX > absDeltaY)
                || (direction === 'vertical' && absDeltaY > absDeltaX)) {
                e.stopPropagation();
            }
            else {
                m_carousel.isDragging = false;
                return;
            }
        }

        if (m_carousel.isAnimating) {
            m_carousel.getActiveCarouselItem().getTranslatable().stopAnimation();
        }

        m_carousel.dragStartOffset = m_carousel.offset;
        
        
        
        m_carousel.dragDirection = 0;
     },
     //---------------------------------------------
     //Ovverride
     onDrag:function(e){
         
         if (!m_carousel.isDragging) {
            return;
        }

        var startOffset = m_carousel.dragStartOffset,
            direction = m_carousel.getDirection(),
            delta = direction === 'horizontal' ? e.deltaX : e.deltaY,
            lastOffset = m_carousel.offset,
            flickStartTime = m_carousel.flickStartTime,
            dragDirection = m_carousel.dragDirection,
            now = Ext.Date.now(),
            currentActiveIndex = m_carousel.getActiveIndex(),
            maxIndex = m_carousel.getMaxItemIndex(),
            lastDragDirection = dragDirection,
            offset;
        
            
            

        if ((currentActiveIndex === 0 && delta > 0) || (currentActiveIndex === maxIndex && delta < 0)) {
            delta *= 0.5;
        }

        offset = startOffset + delta;

        if (offset > lastOffset) {
            dragDirection = 1;
        }
        else if (offset < lastOffset) {
            dragDirection = -1;
        }

        if (dragDirection !== lastDragDirection || (now - flickStartTime) > 300) {
            m_carousel.flickStartOffset = lastOffset;
            m_carousel.flickStartTime = now;
        }

        m_carousel.dragDirection = dragDirection;

        m_carousel.setOffset(offset);
        
        
        
        /*    
        // prevent going back to splash:
        var flickDistance = offset - m_carousel.flickStartOffset;
        var flickDuration = now - m_carousel.flickStartTime;
        var velocity;
        var animationDirection;
        var activeIndex = m_carousel.getActiveIndex()
        if (flickDuration > 0 && Math.abs(flickDistance) >= 10) {
            velocity = flickDistance / flickDuration;

            if (Math.abs(velocity) >= 1) {
                if (velocity < 0 && activeIndex < maxIndex) {
                    animationDirection = -1;
                }
                else if (velocity > 0 && activeIndex > 0) {
                    animationDirection = 1;
                }
            }
        }
        if(animationDirection == 1 && m_carousel.getActiveIndex() ==1)
        {
            e.stopPropagation();
            return;
        }
         */
        
     },
     //------------------------------------------------
     //Override
     onDragEnd: function(e) {
        
        
         
         if (!m_carousel.isDragging) {
            return;
        }



        m_carousel.onDrag(e);

        m_carousel.isDragging = false;

        var now = Ext.Date.now(),
            itemLength = m_carousel.itemLength,
            threshold = itemLength / 2,
            offset = m_carousel.offset,
            activeIndex = m_carousel.getActiveIndex(),
            maxIndex = m_carousel.getMaxItemIndex(),
            animationDirection = 0,
            flickDistance = offset - m_carousel.flickStartOffset,
            flickDuration = now - m_carousel.flickStartTime,
            indicator = m_carousel.getIndicator(),
            velocity;

        if (flickDuration > 0 && Math.abs(flickDistance) >= 10) {
            velocity = flickDistance / flickDuration;

            if (Math.abs(velocity) >= 1) {
                if (velocity < 0 && activeIndex < maxIndex) {
                    animationDirection = -1;
                }
                else if (velocity > 0 && activeIndex > 0) {
                    animationDirection = 1;
                }
            }
        }
        
        
        //second check to make sure user can't swipe back to splash page.
        /*
        if(animationDirection == 1 && m_carousel.getActiveIndex() ==1)
         { 
              //alert  ("BLAH" + animationDirection + " " + m_carousel.getActiveIndex() ) 
             // m_carousel.animationDirection = animationDirection;
              //m_carousel.setOffsetAnimated(0);
               //e.stopPropagation();
              //m_carousel.setActiveItem(1);
              //this.resetSlideStates();
              //indicator.setActiveIndex(activeIndex - animationDirection);
              return;
         }
         */
        
        
        
        if (animationDirection === 0) {
            if (activeIndex < maxIndex && offset < -threshold) {
                animationDirection = -1;
            }
            else if (activeIndex > 0 && offset > threshold) {
                animationDirection = 1;
            }
        }
        
        

        if (indicator) {
            indicator.setActiveIndex(activeIndex - animationDirection);
        }

        m_carousel.animationDirection = animationDirection;
        m_carousel.setOffsetAnimated(animationDirection * itemLength);
        
        // if scrolling Horizontally no need to update the nav return here
        if(animationDirection * itemLength == 0){
            return;
        } else {
            this.resetSlideStates();
           
                  /*  items = m_carousel.getInnerItems();
                    for(var i = 0 ; i<items.length;++i ){            
                        if(items[i].xtype == "verticalCarousel"){
                                items[i].setActiveItem(0)
                        }
                    }*/
        }
        
        
        
        // PAGE SWIPE COMPLETE     
        
        //console.log(m_carousel.getActiveIndex())
        
        // Do stuff on page swipe
        var pageTarget = 0;
        if(m_carousel.getActiveIndex() == pageTarget) {
        	
        	
        	
        }
        
        

        //ok now update the nav toolbar with the correct active icon:
        var active_car_item = m_carousel.getActiveCarouselItem();
        var curnavBtnId = active_car_item._component.navBtnId;
         
        if(curnavBtnId == null || curnavBtnId == "undefined") 
            curnavBtnId = active_car_item._component._activeItem.navBtnId;
                
        //console.log( 
        //    Ext.getCmp('mypanel') .getActiveItem() ._activeItem     
        //);
       // console.log(active_car_item._component._activeItem.getEtl_tabsvisible)
        
       // this.getSlidebyID(curnavBtnId,'etl_tabsvisible');
        
        var newNavBtnId = "";
        var from_splash = false;
         //Ext.getCmp('quizanimation').setHtml("");
        switch(curnavBtnId)
        {
            case "1-0":
                newNavBtnId = "1-1";
                from_splash = true;
            break;
            
            
            case "1-1": case "1-1-1":
                if(animationDirection == -1)
                   newNavBtnId = "1-2";
                else
                   newNavBtnId = "1-1"
            break;
            
            
            case "1-2": case "1-2-1": case "1-2-2" :
                if(animationDirection == -1) //right
                   newNavBtnId = "1-3";
                else //left
                   newNavBtnId = "1-1-1"
            break;
            
            
            case "1-3": case "1-3-1": case "1-3-2" :
                if(animationDirection == -1) //right
                   newNavBtnId = "1-4";
                else //left
                   newNavBtnId = "1-2"
            break;
            
            
            case "1-4": case"1-4-1":
                if(animationDirection == -1) //right
                   newNavBtnId = "1-5";
                else //left
                   newNavBtnId = "1-3"
            break;
            
            
            case "1-5":
                if(animationDirection == -1) //right
                   newNavBtnId = "1-6";
                else //left
                   newNavBtnId = "1-4"
            break;
            
            
            case"1-6":  case"1-6-1": case"1-6-2": case"1-6-3":
                if(animationDirection == -1){//right
                   newNavBtnId = "1-7";                  
                } else {//left
                   newNavBtnId = "1-5" }
            break;
            
            
            case "1-7":
                if(animationDirection == -1){ //right\                  
                   Ext.getCmp('quizanimation').setHtml("<iframe id=\"quiz\" src=\"quiz.html\" frameborder=\"0\" scrolling=\"no\" style=\"width:1026px; height:768px; overflow:hidden;\"></iframe>"); 
                   newNavBtnId = "1-8";
                } else {//left
                   newNavBtnId = "1-6"
                }
            break;
            
            
            case "1-8": case"1-8-1": case"1-8-2":
			
				if(animationDirection == -1){ //right\                  
                   
                } else {//left
                   Ext.getCmp('quizanimation').setHtml("");
                }
			
			
			
                //if(animationDirection == -1) //right
                //   newNavBtnId = "1-8";
                //else //left
                newNavBtnId = "1-7"
            break;
        }
        
        //alert(flickDistance);
        var test1 = false;
        var test2 = false;
        
        if (this.testRange(flickDistance,-2048,150))
            test1 = true;
        
        if (this.testRange(flickDistance,150,2048))
            test2 = true;
        
        
        //alert(test1 + " " + test2);
        
        if(test1 || test2 && flickDistance != 0/*flickDistance < -1000 || flickDistance > 1000 flickDistance != 0 */)
        {
            
            this.getCmpHighlightNavBtn(newNavBtnId);
        }
        ////////////////////////////////////////////////////////
        
        alert('dragend')
        
     },
     //Override:
     //-----------------------------------------------
     onActiveItemAnimationEnd: function(translatable) {
         
         //ok now update the nav toolbar with the correct active icon:
        /*var active_car_item = m_carousel.getActiveCarouselItem();
        var curnavBtnId = active_car_item._component.navBtnId;
         
        if(curnavBtnId == null || curnavBtnId == "undefined") 
            curnavBtnId = active_car_item._component._activeItem.navBtnId;*/
        
      
        //alert('got it!');
        
        
        var currentActiveIndex = m_carousel.getActiveIndex(),
            animationDirection = m_carousel.animationDirection,
            axis = m_carousel.currentAxis,
            currentOffset = translatable[axis],
            itemLength = m_carousel.itemLength,
            offset;

        m_carousel.isAnimating = false;

        translatable.un(m_carousel.animationListeners);

        if (animationDirection === -1) {
            offset = itemLength + currentOffset;
        }
        else if (animationDirection === 1) {
            offset = currentOffset - itemLength;
        }
        else {
            offset = currentOffset;
        }

        offset -= m_carousel.itemOffset;
        m_carousel.offset = offset;
        m_carousel.setActiveItem(currentActiveIndex - animationDirection);
        
       
       
        ///////// SWAP FOR HIGH RES////////////////////////////////////////
        var increment = 0;
        var idx = (currentActiveIndex- animationDirection);
        //idx = idx +1; 
        item = m_carousel.getAt(idx);
        //this.swapImage(item);
        //////////////////////////////////////////////////////////////////////////////////
        
       
        
     },
     getSlidebyID:function(slideID,dataHandle){
          Ext.getStore('Slides').load(function(slides){
                var length = slides.length;
                for(var i=0 ;i< length; ++i){
                    
                   if(slides[i].get('etl_btn_id')== slideID ){

                       var mystring = slides[i].get('etl_tabsvisible');
                       return mystring;
                       
                   } 
                }

          });
         
     },
     //------------------------------------------------------------------------
     swapImage:function(item){
       
       //alert(item.navBtnId);
       
        
        var page_xtype = "";
        Ext.getStore('Slides').load(function(slides){
                var items = [];
                var bFoundIt = false;
                
                var length = slides.length;
                for(var i=0 ;i< length; ++i){
                    
                    if( (slides[i].get('etl_slide_type') != epiduo_ped.app.SLIDE_TYPE_SUB) /*&&
                         slide.get('etl_has_btn') == true*/ ) { //Only add Button for Main type of slide
                         
                        var theid = slide[i].get('etl_btn_id');
                        
                        bFoundIt = false;
                        
                        //swiping event
                        if(item != 'undefined' && item != undefined && item.navBtnId != undefined && item.navBtnId != "undefined"){
                                 if(theid == item.navBtnId){
                                   page_xtype =  slide[i].get('etl_carousel_item_xtype');
                                   bFoundIt = true;
                            }
                        }
                        //nav button event:
                        else if(theid != 'undefined' && item != 'undefined' && item != undefined){
                            
                            if(theid == item.id){
                                page_xtype =  slide[i].get('etl_carousel_item_xtype');
                                bFoundIt = true;
                            }
                        }
                        
                        //if we did not find it, then lets search the vertical carousel of this slide (if any)
                        if(!bFoundIt){
                            
                           var bMoreSlides = false;
                           do
                           {
                               slides[i].get('etl_carousel_item_xtype'),  
                               slides[i].get('etl_btn_id') 
                              
                               if( i +1 < slides.length && slides[i+1].get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SUB){
                                  ++i;
                                  bMoreSlides = true;   
                               }
                               else
                                  bMoreSlides = false;  

                           }
                           while (bMoreSlides);
                        }
                        
                     }
                 }
        });
        
        var slide = null;
        items = m_carousel.getInnerItems();
        var hi_res_item = null;
        for(var i = 0 ; i<items.length;++i ){
            
            if(items[i].xtype == page_xtype){
                
                //alert("got it")
                slide = items[i];
                
                var slide_items = slide.getItems();
                var slide_items1 = slide_items.items;
                hi_res_item = slide_items1[0];
                
                if(hi_res_item != undefined && hi_res_item != 'undefined')
                    hi_res_item.show();
                
                break;
            }
            
        }
        
        
        this.setLastHiResItem(hi_res_item);
     },
     //---------------------------------------------
     setLastHiResItem:function(hi_res_item)
     {
         
         if(this.m_lastHighResItem != '' &&  this.m_lastHighResItem != 'undefined' &&  this.m_lastHighResItem != undefined)
             this.m_lastHighResItem.hide();
        
         
         if(hi_res_item != null && hi_res_item != 'undefined' && hi_res_item != undefined)
           this.m_lastHighResItem = hi_res_item;
         else
           this.m_lastHighResItem = '';
         
     },
     //------------------------------------------------
     testRange:function (numberToCheck, bottom,top)
     {
        return (numberToCheck > bottom && numberToCheck < top);
     },
     //------------------------------------------------
     setRefImg:function(theid){
       //For Reference:
       //slide_panel_ref
       //src:     'app/view/1-1/images/ref.png',
       //id:      'id_ref_content'
       
       //alert("set ref id:" + theid)
       
       var sPath = 'app/view/' + theid + '/images/ref.png';
       var ref_content = Ext.getCmp('id_ref_content');
       ref_content.setSrc(sPath);
     },
     //-------------------------------------------------
     getCmpHighlightNavBtn: function(newNavBtnId){         
           var navBtnToActivate = Ext.getCmp(newNavBtnId);
           navBtnToActivate.setIconCls(navBtnToActivate.etl_btn_activeclass);
           this.deactiveOtherNavBtns(navBtnToActivate);
         
     },
          //-------------------------------------------------
     resetSlideStates: function(){
         
            items = m_carousel.getInnerItems();
            for(var i = 0 ; i<items.length;++i ){            
                if(items[i].xtype == "verticalCarousel"){
                    items[i].setActiveItem(0)
                }
            }
          //  this. resetSlideTabs();
           
      
     },
         //-------------------------------------------------
     resetSlideTabs: function(){
         

            this.updateImage('img_1-2-1_tab','page1_2_1-tab1-BG');
            this.updateImage('img_1-2-2_tab','page1_2_2-tab1-BG');
           
         
     },
     //-------------------------------------------------
     deactiveOtherNavBtns :function(navbtn){
         
         //de-activate other buttons:
         Ext.getStore('Slides').load(function(slides){
                Ext.each(slides, function(slide){
                    
                    var slideId = slide.get('etl_btn_id');
                    var navId   = navbtn.id;
                    var bequal = slideId == navId;
                    
                    if(  (slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_MAIN ||
                         slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SITEMAP) &&
                         !bequal ){
                         
                         //var theid  = slide.get('etl_btn_id');
                         var btn = Ext.getCmp(slideId);
                         btn.setIconCls(slide.get('etl_btn_class'));
                    }
                });
         });
         
         
     },
     //------------------------------------------------
     navIconRemoveAllActive :function(){
         
          Ext.getStore('Slides').load(function(slides){
                Ext.each(slides, function(slide){
                    
                    var slideId = slide.get('etl_btn_id');
                    
                    
                    if(  (slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_MAIN ||
                         slide.get('etl_slide_type') == epiduo_ped.app.SLIDE_TYPE_SITEMAP)  ){
                         
                         //var theid  = slide.get('etl_btn_id');
                         var btn = Ext.getCmp(slideId);
                         btn.setIconCls(slide.get('etl_btn_class'));
                    }
                });
         });
         
     },    
     //------------------------------------------------
     updateImage: function (img,newCls){
            var myImage = null;
            myImage = Ext.getCmp(img)            
             myImage.setCls(newCls);
     }
     
      

});

