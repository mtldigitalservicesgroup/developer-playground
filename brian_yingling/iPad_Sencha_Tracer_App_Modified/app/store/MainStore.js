Ext.define('epiduo_ped.store.MainStore', {
    extend: 'Ext.data.Store',        
    data: [
              /*  { 
                  section:                      '0',
                  sectiontype:                  'splashPage',
                  dataStore:                    'SlideSectionSplash'
                },
        */
                {                   
                  sectiontype:                  'slide',
                  dataStore:                    'SlidesSection1',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_1',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_1_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_1_active',
                  etl_btn_width:                95
                },
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection2',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_2',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_2_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_2_active',
                  etl_btn_width:                95
                },
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection3',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_3',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_3_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_3_active',
                  etl_btn_width:                95
                },
                /*
                { 
                  sectiontype:                  'carousel' ,
                  dataStore:                    'SlidesSection4',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_4',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_4_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_4_active',
                  etl_btn_width:                95
                },
                
                { 
                  sectiontype:                  'slide',
                  dataStore:                    'SlidesSection5',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_5',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_5_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_5_active',
                  etl_btn_width:                95
                },
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection6',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_6',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_6_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_6_active',
                  etl_btn_width:                95
                },
                { 
                  sectiontype:                  'slide',
                  dataStore:                    'SlidesSection7',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_7',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_7_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_7_active'  ,
                  etl_btn_width:                95                
                },
                
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection8',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_8',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_8_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_8_active',
                  etl_btn_width:                95
                },
          */      
                {                  
                  sectiontype:                  'siteMap',
                  dataStore:                    'SlidesSiteMap',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_sitemap',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_sitemap_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_sitemap_active',
                  etl_btn_width:                95
                }
                
          ]
    }
);




