/*
Ext.define('epiduo_ped.store.SlidesSection8', {
    extend: 'Ext.data.Store',    
    requires: 'epiduo_ped.model.Slide',

    config: {
        model: 'epiduo_ped.model.Slide',
        
    data: [
                 
                { etl_btn_id:                       '1-8',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_8',
                  etl_btn_loadingclass:             'etl_nav_btn etl_nav_btn_1_8_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_8_active',
                  etl_label:                        'Start Talking - Animation',
                  etl_tabsvisible:                  false,
                  etl_please_see_visible:           false,
                  etl_nositemap:                    false,
                  
                  etl_carousel_item_xtype:          'page1-8',
                  etl_sitemap_id:                   'hotspot_sitemap_1_8',
                  
                  etl_overlay_bg_fill_color:        '#ffffff',
                  etl_allow_up:                     true,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               true
                },
                 {etl_btn_id:                       '1-8-1',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_8',
                  etl_btn_loadingclass:             'etl_nav_btn etl_nav_btn_1_8_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_8_active',
                  etl_label:                        'Start Talking - Program Introduction',
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_item_xtype:          'page1-8-1',
                  etl_sitemap_id:                   'hotspot_sitemap_1_8_1',
                  
                  
                  etl_overlay_bg_fill_color:         '#000000',
                  etl_allow_up:                     false,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               false
                },
                { etl_btn_id:                       '1-8-2',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_8',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_1_8_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_8_active',
                  etl_label:                        'Start Talking - Components',
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_item_xtype:          'page1-8-2',
                  etl_sitemap_id:                   'hotspot_sitemap_1_8_2',
                  
                  
                  etl_overlay_bg_fill_color:         '#000000',
                  etl_allow_up:                     true,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               false
                }
                
          ]
    }
});

*/


