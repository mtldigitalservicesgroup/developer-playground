Ext.define('epiduo_ped.store.SlidesSectionSplash', {
    extend: 'Ext.data.Store',
    
    requires: 'epiduo_ped.model.Slide',

    config: {
        model: 'epiduo_ped.model.Slide',
        
    data: [
                { 
                  etl_btn_id:                      '1-0',
                  etl_btn_class:                    '',
                  etl_btn_activeclass:               '',
                  etl_label:                        'Splash',
                  etl_tabsvisible:                  false,
                  etl_please_see_visible:           false,
                  etl_nositemap:                    false,
                  etl_carousel_idx:                 0,
                  etl_carousel_item_xtype:          'page1-0',
                  etl_sitemap_id:                    'hotspot_sitemap_1_0',
                  etl_carousel_vert_idx:            -1,
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                     true,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               true                                 
                }
          ]
    }
});




