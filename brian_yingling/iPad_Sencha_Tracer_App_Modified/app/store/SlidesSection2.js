Ext.define('epiduo_ped.store.SlidesSection2', {
    extend: 'Ext.data.Store',
    requires: 'epiduo_ped.model.Slide',
    config: {
        model: 'epiduo_ped.model.Slide',
        
    data: [
                /*
                { etl_btn_id:                       '1-2',  
                    
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_2',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_1_2_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_2_active',
                  
                  etl_label:                        'Effective',
                  
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  
                  etl_carousel_item_xtype:          'page1-2',
                  etl_sitemap_id:                   'hotspot_sitemap_1_2',
                  
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                     false,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               false
                },
                */
                
                { etl_btn_id:                       '1-2-1',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_2',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_1_2_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_2_active',
                  etl_label:                        'Gollnick',
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_item_xtype:          'page1-2-1',
                  etl_sitemap_id:                   'hotspot_sitemap_1_2_1',
                  
                  
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                      true,
                  etl_allow_down:                    true,
                  etl_hide_home_link:                false
                }, 
                
                { etl_btn_id:                       '1-2-2',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_2',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_1_2_loading',  
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_2_active',
                  etl_label:                        'Patient Videos',
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_item_xtype:          'page1-2-2',
                  etl_sitemap_id:                   'hotspot_sitemap_1_2_2',
                  
                  
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                     true,
                  etl_allow_down:                   false,
                  etl_hide_home_link:               false
                }
          ]
    }
});




