Ext.define('epiduo_ped.model.Slide', {
    extend: 'Ext.data.Model',

    config: {
        fields: [
            {name: 'etl_btn_id',                    type: 'string'},
            {name: 'etl_btn_class',                 type: 'string'},
            {name: 'etl_btn_activeclass',           type: 'string'},
            {name: 'etl_btn_loadingclass',           type: 'string'},
            {name: 'etl_label',                     type: 'string'},            
            {name: 'etl_tabsvisible',               type: 'boolean'},
            {name: 'etl_please_see_visible',        type: 'boolean'},
            {name: 'etl_nositemap',                 type: 'boolean'},
            {name: 'etl_carousel_item_xtype',       type: 'string'},
            {name: 'etl_sitemap_id',                type: 'string'},
            {name: 'etl_overlay_bg_fill_color',     type: 'string'},
            {name: 'etl_allow_up',                  type: 'boolean'},
            {name: 'etl_allow_down',                type: 'boolean'},
            {name: 'etl_hide_home_link',            type: 'boolean'},
        ]
        
    }
});

