3.1.20 (Brainy Betty)
27d941520f8f563c766c96598b364f75903e8273
o:Sass::Tree::RootNode
:@has_childrenT:
@linei:@template"�@import '../global';

/**
 * @class Ext.Toolbar
 */

/**
 * @var {color} $toolbar-base-color
 * The primary color variable from which toolbars derive their light and dark UIs.
 */
$toolbar-base-color: $base-color !default;

/**
 * @var {measurement} $toolbar-spacing
 * Space between items in a toolbar (like buttons and fields)
 */
$toolbar-spacing: .2em !default;

/**
 * @var {string} $toolbar-gradient
 * Background gradient style for toolbars.
 */
$toolbar-gradient: $base-gradient !default;

/**
 * @var {boolean} $include-toolbar-uis
 * Optionally disable separate toolbar UIs (light and dark).
 */
$include-toolbar-uis: $include-default-uis !default;

/**
 * Includes default toolbar styles.
 */
@mixin sencha-toolbar {

  .x-toolbar {
    padding: 0 $toolbar-spacing;
    overflow: hidden;
    position: relative;
    height: $global-row-height;

    & > * {
      z-index: 1;
    }

    &.x-docked-top {
      border-bottom: .1em solid;
    }

    &.x-docked-bottom {
      border-top: .1em solid;
    }

    &.x-docked-left {
      width: 7em;
      height: auto;
      padding: $toolbar-spacing;
      border-right: .1em solid;
    }

    &.x-docked-right {
      width: 7em;
      height: auto;
      padding: $toolbar-spacing;
      border-left: .1em solid;
    }
  }

  .x-title {
    line-height: $global-row-height - .5em;
    font-size: 1.2em;
    text-align: center;
    font-weight: bold;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    margin: 0 0.3em;
    max-width: 100%;

    .x-innerhtml {
      white-space: nowrap;
      overflow: hidden;
      text-overflow: ellipsis;
      padding: 0 .3em;
    }
  }

  @if $include-toolbar-uis {
    @include sencha-toolbar-ui('dark', darken($toolbar-base-color, 10%));
    @include sencha-toolbar-ui('light', $toolbar-base-color);
    @include sencha-toolbar-ui('neutral', $neutral-color);

    .x-toolbar.x-toolbar-neutral .x-toolbar-inner .x-button.x-button-pressing {
      $mask-radial-glow: lighten($base-color, 25);
      @include background-image(radial-gradient(fade-out($mask-radial-glow, .3), fade-out($mask-radial-glow, 1) 24px));
      .x-button-icon.x-button-mask {
        @include background-gradient(#fff, 'recessed');
      }
    }
  }

  .x-navigation-bar {
    .x-container {
      overflow: visible;
    }
  }
}

/**
 * Creates a theme UI for toolbars.
 *
 *     // SCSS
 *     @include sencha-toolbar-ui('sub', #58710D, 'glossy');
 *
 *     // JS
 *     var myTb = new Ext.Toolbar({title: 'My Green Glossy Toolbar', ui: 'sub'})
 *
 * @param {string} $ui-label The name of the UI being created.
 *   Can not included spaces or special punctuation (used in class names)
 * @param {color} $color Base color for the UI.
 * @param {string} $gradient: $toolbar-gradien Background gradient style for the UI.
 */
@mixin sencha-toolbar-ui($ui-label, $color, $gradient: $toolbar-gradient) {

  $toolbar-border-color: darken($color, 50%);
  $toolbar-button-color: darken($color, 5%);

  .x-toolbar-#{$ui-label} {
    @include background-gradient($color, $gradient);
    border-color: $toolbar-border-color;

    .x-title {
      @include color-by-background($color);
      @include bevel-by-background($color);
    }

    &.x-docked-top {
      border-bottom-color: $toolbar-border-color;
    }

    &.x-docked-bottom {
      border-top-color: $toolbar-border-color;
    }

    &.x-docked-left {
      border-right-color: $toolbar-border-color;
    }

    &.x-docked-right {
      border-left-color: $toolbar-border-color;
    }

    .x-button,
    .x-field-select .x-component-outer,
    .x-field-select .x-component-outer:before {
      @include toolbar-button($toolbar-button-color, $gradient);
    }

    .x-form-label {
      @include color-by-background($color);
      @include bevel-by-background($color);
    }
  }
}
:@children[o:Sass::Tree::ImportNode
;i;0;	[ :@imported_filename"../global:@options{ o:Sass::Tree::CommentNode:@silent0:@value[""/**
 * @class Ext.Toolbar
 */;i;	[ ;@:
@loud0o;;0;["~/**
 * @var {color} $toolbar-base-color
 * The primary color variable from which toolbars derive their light and dark UIs.
 */;i;	[ ;@;0o:Sass::Tree::VariableNode;i:
@name"toolbar-base-color:@guarded"!default:
@expro:Sass::Script::Variable	:@underscored_name"base_color;i;"base-color;@;	[ ;@o;;0;["q/**
 * @var {measurement} $toolbar-spacing
 * Space between items in a toolbar (like buttons and fields)
 */;i;	[ ;@;0o;;i;"toolbar-spacing;"!default;o:Sass::Script::Number:@original"
0.2em;f0.20000000000000001 ��;i:@denominator_units[ :@numerator_units["em;@;	[ ;@o;;0;["Z/**
 * @var {string} $toolbar-gradient
 * Background gradient style for toolbars.
 */;i;	[ ;@;0o;;i;"toolbar-gradient;"!default;o;	;"base_gradient;i;"base-gradient;@;	[ ;@o;;0;["p/**
 * @var {boolean} $include-toolbar-uis
 * Optionally disable separate toolbar UIs (light and dark).
 */;i;	[ ;@;0o;;i";"include-toolbar-uis;"!default;o;	;"include_default_uis;i";"include-default-uis;@;	[ ;@o;;0;["0/**
 * Includes default toolbar styles.
 */;i$;	[ ;@;0o:Sass::Tree::MixinDefNode;T:
@args[ ;i';"sencha-toolbar;	[	o:Sass::Tree::RuleNode;T:
@tabsi ;i):
@rule[".x-toolbar:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;i):@members[o:Sass::Selector::Sequence;#[o:#Sass::Selector::SimpleSequence;"@K;i);#[o:Sass::Selector::Class;"@K;i);["x-toolbar;	[o:Sass::Tree::PropNode;i ;o:Sass::Script::List	;[o;;"0;i ;i*;[ ;[ ;@o;	;"toolbar_spacing;i*;"toolbar-spacing;@;i*:@separator:
space;@;i*;["padding:@prop_syntax:new;	[ ;@o;';i ;o:Sass::Script::String;"hidden:
@type:identifier;@;i+;["overflow;+;,;	[ ;@o;';i ;o;-;"relative;.;/;@;i,;["position;+;,;	[ ;@o;';i ;o;	;"global_row_height;i-;"global-row-height;@;i-;["height;+;,;	[ ;@o;;T;i ;i/;["
& > *; o;!;"" ;i/;#[o;$;#[o;%;"@y;i/;#[o:Sass::Selector::Parent;"@y;i/">o;%;"@y;i/;#[o:Sass::Selector::Universal;"@y;i/:@namespace0;	[o;';i ;o;-;"1;.;/;@;i0;["z-index;+;,;	[ ;@;@o;;T;i ;i3;["&.x-docked-top; o;!;"" ;i3;#[o;$;#[o;%;"@�;i3;#[o;0;"@�;i3o;&;"@�;i3;["x-docked-top;	[o;';i ;o;-;".1em solid;.;/;@;i4;["border-bottom;+;,;	[ ;@;@o;;T;i ;i7;["&.x-docked-bottom; o;!;"" ;i7;#[o;$;#[o;%;"@�;i7;#[o;0;"@�;i7o;&;"@�;i7;["x-docked-bottom;	[o;';i ;o;-;".1em solid;.;/;@;i8;["border-top;+;,;	[ ;@;@o;;T;i ;i;;["&.x-docked-left; o;!;"" ;i;;#[o;$;#[o;%;"@�;i;;#[o;0;"@�;i;o;&;"@�;i;;["x-docked-left;	[	o;';i ;o;-;"7em;.;/;@;i<;["
width;+;,;	[ ;@o;';i ;o;-;"	auto;.;/;@;i=;["height;+;,;	[ ;@o;';i ;o;	;"toolbar_spacing;i>;"toolbar-spacing;@;i>;["padding;+;,;	[ ;@o;';i ;o;-;".1em solid;.;/;@;i?;["border-right;+;,;	[ ;@;@o;;T;i ;iB;["&.x-docked-right; o;!;"" ;iB;#[o;$;#[o;%;"@�;iB;#[o;0;"@�;iBo;&;"@�;iB;["x-docked-right;	[	o;';i ;o;-;"7em;.;/;@;iC;["
width;+;,;	[ ;@o;';i ;o;-;"	auto;.;/;@;iD;["height;+;,;	[ ;@o;';i ;o;	;"toolbar_spacing;iE;"toolbar-spacing;@;iE;["padding;+;,;	[ ;@o;';i ;o;-;".1em solid;.;/;@;iF;["border-left;+;,;	[ ;@;@;@o;;T;i ;iJ;[".x-title; o;!;"" ;iJ;#[o;$;#[o;%;"@;iJ;#[o;&;"@;iJ;["x-title;	[o;';i ;o:Sass::Script::Operation
:@operand1o;	;"global_row_height;iK;"global-row-height;@;iK:@operand2o;;"
0.5em;f0.5;iK;[ ;["em;@;@:@operator:
minus;iK;["line-height;+;,;	[ ;@o;';i ;o;-;"
1.2em;.;/;@;iL;["font-size;+;,;	[ ;@o;';i ;o;-;"center;.;/;@;iM;["text-align;+;,;	[ ;@o;';i ;o;-;"	bold;.;/;@;iN;["font-weight;+;,;	[ ;@o;';i ;o;-;"nowrap;.;/;@;iO;["white-space;+;,;	[ ;@o;';i ;o;-;"hidden;.;/;@;iP;["overflow;+;,;	[ ;@o;';i ;o;-;"ellipsis;.;/;@;iQ;["text-overflow;+;,;	[ ;@o;';i ;o;-;"0 0.3em;.;/;@;iR;["margin;+;,;	[ ;@o;';i ;o;-;"	100%;.;/;@;iS;["max-width;+;,;	[ ;@o;;T;i ;iU;[".x-innerhtml; o;!;"" ;iU;#[o;$;#[o;%;"@P;iU;#[o;&;"@P;iU;["x-innerhtml;	[	o;';i ;o;-;"nowrap;.;/;@;iV;["white-space;+;,;	[ ;@o;';i ;o;-;"hidden;.;/;@;iW;["overflow;+;,;	[ ;@o;';i ;o;-;"ellipsis;.;/;@;iX;["text-overflow;+;,;	[ ;@o;';i ;o;-;"0 .3em;.;/;@;iY;["padding;+;,;	[ ;@;@;@u:Sass::Tree::IfNodee[o:Sass::Script::Variable	:@underscored_name"include_toolbar_uis:
@linei]:
@name"include-toolbar-uis:@options{ 0[	o:Sass::Tree::MixinNode:
@args[o:Sass::Script::String	:@value"	dark;i^:
@type:string;	@	o:Sass::Script::Funcall
;[o; 	;"toolbar_base_color;i^;"toolbar-base-color;	@	o:Sass::Script::Number:@original"10%;i;i^:@denominator_units[ :@numerator_units["%;	@	;i^;"darken:@keywords{ ;	@	;i^;"sencha-toolbar-ui:@children[ ;{ ;	@	o;
;[o;	;"
light;i_;;;	@	o; 	;"toolbar_base_color;i_;"toolbar-base-color;	@	;i_;"sencha-toolbar-ui;[ ;{ ;	@	o;
;[o;	;"neutral;i`;;;	@	o; 	;"neutral_color;i`;"neutral-color;	@	;i`;"sencha-toolbar-ui;[ ;{ ;	@	o:Sass::Tree::RuleNode:@has_childrenT:
@tabsi ;ib:
@rule["N.x-toolbar.x-toolbar-neutral .x-toolbar-inner .x-button.x-button-pressing:@parsed_ruleso:"Sass::Selector::CommaSequence:@filename" ;ib:@members[o:Sass::Selector::Sequence;[o:#Sass::Selector::SimpleSequence;@6;ib;[o:Sass::Selector::Class;@6;ib;["x-toolbaro;!;@6;ib;["x-toolbar-neutralo; ;@6;ib;[o;!;@6;ib;["x-toolbar-innero; ;@6;ib;[o;!;@6;ib;["x-buttono;!;@6;ib;["x-button-pressing;[o:Sass::Tree::VariableNode;ic;"mask-radial-glow:@guarded0:
@expro;
;[o; 	;"base_color;ic;"base-color;	@	o;;"25;i;ic;[ ;[ ;	@	;ic;"lighten;{ ;	@	;[ ;	@	o;
;[o;
;[o;
;[o; 	;"mask_radial_glow;id;"mask-radial-glow;	@	o;;"0.3;f0.29999999999999999 33;id;@Y;[ ;	@	;id;"fade-out;{ ;	@	o:Sass::Script::List	;[o;
;[o; 	;"mask_radial_glow;id;"mask-radial-glow;	@	o;;"1;i;id;@Y;[ ;	@	;id;"fade-out;{ ;	@	o;;"	24px;i;id;[ ;["px;	@	;id:@separator:
space;	@	;id;"radial-gradient;{ ;	@	;id;"background-image;[ ;{ ;	@	o;;T;i ;ie;["!.x-button-icon.x-button-mask;o;;" ;ie;[o;;[o; ;@�;ie;[o;!;@�;ie;["x-button-icono;!;@�;ie;["x-button-mask;[o;
;[o:Sass::Script::Color	:@attrs{	:
alphai:
greeni�:	bluei�:redi�;0;if;	@	o;	;"recessed;if;;;	@	;if;"background-gradient;[ ;{ ;	@	;	@	;	@	o;;T;i ;ik;[".x-navigation-bar; o;!;"" ;ik;#[o;$;#[o;%;"@w;ik;#[o;&;"@w;ik;["x-navigation-bar;	[o;;T;i ;il;[".x-container; o;!;"" ;il;#[o;$;#[o;%;"@�;il;#[o;&;"@�;il;["x-container;	[o;';i ;o;-;"visible;.;/;@;im;["overflow;+;,;	[ ;@;@;@;@o;;0;["�/**
 * Creates a theme UI for toolbars.
 *
 *     // SCSS
 *     @include sencha-toolbar-ui('sub', #58710D, 'glossy');
 *
 *     // JS
 *     var myTb = new Ext.Toolbar({title: 'My Green Glossy Toolbar', ui: 'sub'})
 *
 * @param {string} $ui-label The name of the UI being created.
 *   Can not included spaces or special punctuation (used in class names)
 * @param {color} $color Base color for the UI.
 * @param {string} $gradient: $toolbar-gradien Background gradient style for the UI.
 */;ir;	[ ;@;0o;;T;[[o;;"ui_label;"ui-label;@0[o;;"
color;"
color;@0[o;;"gradient;"gradient;@o;	;"toolbar_gradient;i{;"toolbar-gradient;@;i{;"sencha-toolbar-ui;	[o;;i};"toolbar-border-color;0;o:Sass::Script::Funcall
;[o;	;"
color;i};"
color;@o;;"50%;i7;i};[ ;["%;@;i};"darken:@keywords{ ;@;	[ ;@o;;i~;"toolbar-button-color;0;o;9
;[o;	;"
color;i~;"
color;@o;;"5%;i
;i~;[ ;["%;@;i~;"darken;:{ ;@;	[ ;@o;;T;i ;i�;[".x-toolbar-o;	;"ui_label;i�;"ui-label;@;	[o:Sass::Tree::MixinNode;[o;	;"
color;i�;"
color;@o;	;"gradient;i�;"gradient;@;i�;"background-gradient;	[ ;:{ ;@o;';i ;o;	;"toolbar_border_color;i�;"toolbar-border-color;@;i�;["border-color;+;,;	[ ;@o;;T;i ;i�;[".x-title; o;!;"" ;i�;#[o;$;#[o;%;"@�;i�;#[o;&;"@�;i�;["x-title;	[o;;;[o;	;"
color;i�;"
color;@;i�;"color-by-background;	[ ;:{ ;@o;;;[o;	;"
color;i�;"
color;@;i�;"bevel-by-background;	[ ;:{ ;@;@o;;T;i ;i�;["&.x-docked-top; o;!;"" ;i�;#[o;$;#[o;%;"@;i�;#[o;0;"@;i�o;&;"@;i�;["x-docked-top;	[o;';i ;o;	;"toolbar_border_color;i�;"toolbar-border-color;@;i�;["border-bottom-color;+;,;	[ ;@;@o;;T;i ;i�;["&.x-docked-bottom; o;!;"" ;i�;#[o;$;#[o;%;"@;i�;#[o;0;"@;i�o;&;"@;i�;["x-docked-bottom;	[o;';i ;o;	;"toolbar_border_color;i�;"toolbar-border-color;@;i�;["border-top-color;+;,;	[ ;@;@o;;T;i ;i�;["&.x-docked-left; o;!;"" ;i�;#[o;$;#[o;%;"@1;i�;#[o;0;"@1;i�o;&;"@1;i�;["x-docked-left;	[o;';i ;o;	;"toolbar_border_color;i�;"toolbar-border-color;@;i�;["border-right-color;+;,;	[ ;@;@o;;T;i ;i�;["&.x-docked-right; o;!;"" ;i�;#[o;$;#[o;%;"@G;i�;#[o;0;"@G;i�o;&;"@G;i�;["x-docked-right;	[o;';i ;o;	;"toolbar_border_color;i�;"toolbar-border-color;@;i�;["border-left-color;+;,;	[ ;@;@o;;T;i ;i�;["e.x-button,
    .x-field-select .x-component-outer,
    .x-field-select .x-component-outer:before; o;!;"" ;i�;#[o;$;#[o;%;"@];i�;#[o;&;"@];i�;["x-buttono;$;#["
o;%;"@];i�;#[o;&;"@];i�;["x-field-selecto;%;"@];i�;#[o;&;"@];i�;["x-component-outero;$;#["
o;%;"@];i�;#[o;&;"@];i�;["x-field-selecto;%;"@];i�;#[o;&;"@];i�;["x-component-outero:Sass::Selector::Pseudo
;"@]:	@arg0;i�;["before;.:
class;	[o;;;[o;	;"toolbar_button_color;i�;"toolbar-button-color;@o;	;"gradient;i�;"gradient;@;i�;"toolbar-button;	[ ;:{ ;@;@o;;T;i ;i�;[".x-form-label; o;!;"" ;i�;#[o;$;#[o;%;"@�;i�;#[o;&;"@�;i�;["x-form-label;	[o;;;[o;	;"
color;i�;"
color;@;i�;"color-by-background;	[ ;:{ ;@o;;;[o;	;"
color;i�;"
color;@;i�;"bevel-by-background;	[ ;:{ ;@;@;@;@;@