<!DOCTYPE html>
<html dir="ltr" lang="en-US" xmlns:og="http://opengraphprotocol.org/schema/">
<head>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="screen" href="http://www.cheesetoast.co.uk/cheesepress/wp-content/themes/yoko/style.css" />
<link rel="pingback" href="http://www.cheesetoast.co.uk/cheesepress/xmlrpc.php" />
<!--[if lt IE 9]>
<script src="http://www.cheesetoast.co.uk/cheesepress/wp-content/themes/yoko/js/html5.js" type="text/javascript"></script>
<![endif]-->


<!-- This site is optimized with the Yoast WordPress SEO plugin v1.2.8.7 - http://yoast.com/wordpress/seo/ -->
<title>Page Not Found - Cheesetoast</title>
<meta property='og:locale' content='en_US'/>
<meta property='og:title' content='Page Not Found - Cheesetoast'/>
<meta property='og:url' content=''/>
<meta property='og:site_name' content='Cheesetoast'/>
<meta property='og:type' content='website'/>
<!-- / Yoast WordPress SEO plugin. -->

<link rel="alternate" type="application/rss+xml" title="Cheesetoast &raquo; Feed" href="http://www.cheesetoast.co.uk/feed/" />
<link rel="alternate" type="application/rss+xml" title="Cheesetoast &raquo; Comments Feed" href="http://www.cheesetoast.co.uk/comments/feed/" />
<link rel='stylesheet' id='wordpress-popular-posts-css'  href='http://www.cheesetoast.co.uk/cheesepress/wp-content/plugins/wordpress-popular-posts/style/wpp.css?ver=3.4.2' type='text/css' media='all' />
<script type='text/javascript' src='http://www.cheesetoast.co.uk/cheesepress/wp-includes/js/jquery/jquery.js?ver=1.7.2'></script>
<script type='text/javascript' src='http://www.cheesetoast.co.uk/cheesepress/wp-content/themes/yoko/js/smoothscroll.js?ver=1.0'></script>
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://www.cheesetoast.co.uk/cheesepress/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://www.cheesetoast.co.uk/cheesepress/wp-includes/wlwmanifest.xml" /> 
<meta name="generator" content="WordPress 3.4.2" />

<style type="text/css">
a {color: #009BC2!important;}
#content .single-entry-header h1.entry-title {color: #009BC2!important;}
input#submit:hover {background-color: #009BC2!important;}
#content .page-entry-header h1.entry-title {color: #009BC2!important;}
.searchsubmit:hover {background-color: #009BC2!important;}
</style>
</head>
<body class="error404">
<div id="page" class="clearfix">
	<header id="branding">
		<nav id="mainnav" class="clearfix">
			<div class="menu-main-menu-container"><ul id="menu-main-menu" class="menu"><li id="menu-item-311" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-311"><a href="/">HOME</a></li>
<li id="menu-item-313" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-313"><a title="About Graham Clark" rel="author" href="http://www.cheesetoast.co.uk/about-me/">ABOUT ME</a></li>
<li id="menu-item-312" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-312"><a title="Contact Graham" href="http://www.cheesetoast.co.uk/contact/">CONTACT</a></li>
</ul></div>		</nav><!-- end mainnav -->

				
		<hgroup id="site-title">
					<h1><a href="http://www.cheesetoast.co.uk/" title="Cheesetoast">Cheesetoast</a></h1>
				<h2 id="site-description">Brain Food for Web Developers</h2>
				</hgroup><!-- end site-title -->
        
        						<img src="http://www.cheesetoast.co.uk/cheesepress/wp-content/uploads/2011/07/desk.png" class="headerimage" width="1102" height="350" alt="" /><!-- end headerimage -->
										<div class="clear"></div>
					
		<nav id="subnav">
					</nav><!-- end subnav -->	
</header><!-- end header -->
<div id="wrap">
<div id="main">

	<div id="content">
		<article id="page">
			<header class="page-entry-header">
				<h1 class="entry-title">Not Found</h1>
			</header><!-- end page-entry-header -->
		
			<div class="single-entry-content">
				<p>Apologies, but the page you requested could not be found. Perhaps searching will help.</p>
				<form role="search" method="get" class="searchform" action="http://www.cheesetoast.co.uk" >
    <div><label class="screen-reader-text" for="s"></label>
    <input type="text" class="search-input" value="" name="s" id="s" />
    <input type="submit" class="searchsubmit" value="Search" />
    </div>
    </form>			</div>
		
			<script type="text/javascript">
				// focus on search field after it has loaded
				document.getElementById('s') && document.getElementById('s').focus();
			</script>
		</article>
	</div><!-- end content -->


<div id="secondary" class="widget-area" role="complementary">
			<aside id="social_links-3" class="widget widget_social_links"><h3 class="widget-title"> </h3><ul><li class="widget_sociallinks"><a href="http://www.cheesetoast.co.uk/rss/" class="rss" target="_blank">RSS</a></li><li class="widget_sociallinks"><a href="https://twitter.com/#!/CheesetoastDev" class="twitter" target="_blank">Twitter</a></li><li class="widget_sociallinks"><a href="http://www.facebook.com/pages/Cheesetoast/262095313879361" class="facebook" target="_blank">Facebook</a></li><li class="widget_sociallinks"><a href="http://www.linkedin.com/in/gclark2 " class="linkedin" target="_blank">LinkedIn</a></li><li class="widget_sociallinks"><a href="http://d.me/mr_chesneys_ghost" class="delicious" target="_blank">Delicious</a></li></ul></aside><aside id="categories-3" class="widget widget_categories"><h3 class="widget-title">Categories</h3>		<ul>
	<li class="cat-item cat-item-8"><a href="http://www.cheesetoast.co.uk/category/css/" title="This section covers tips and tricks on CSS menus, image galleries, layout and more.">CSS</a>
</li>
	<li class="cat-item cat-item-5"><a href="http://www.cheesetoast.co.uk/category/general/" title="This section covers general aspects of web development such as content management systems, freelance web design and web technologies.">General</a>
</li>
	<li class="cat-item cat-item-3"><a href="http://www.cheesetoast.co.uk/category/html/" title="Articles and tutorials about all things (X)HTML.">HTML</a>
</li>
	<li class="cat-item cat-item-6"><a href="http://www.cheesetoast.co.uk/category/jquery/" title="JQuery for beginners. This topic contains tutorials on how to code some useful scripts, as well as discussions about plugins out there on the web.">JQuery</a>
</li>
	<li class="cat-item cat-item-4"><a href="http://www.cheesetoast.co.uk/category/php/" title="Information about PHP tips, useful functions and security issues.">PHP</a>
</li>
	<li class="cat-item cat-item-111"><a href="http://www.cheesetoast.co.uk/category/snippets/" title="Code snippets that come in handy.">Snippets</a>
</li>
	<li class="cat-item cat-item-49"><a href="http://www.cheesetoast.co.uk/category/web-usability/" title="A series of articles about website usability. They discuss Neilsen&#039;s five principles of usability (learnability, efficiency, memorability, errors and satisfaction), the importance of adopting usability testing early in the design process and the contributing factors for good website usability.">Usability</a>
</li>
	<li class="cat-item cat-item-7"><a href="http://www.cheesetoast.co.uk/category/wordpress/" title="This topic is all about the Wordpress content management system. This may cover anything from &#039;back-end&#039; administration to CSS/PHP editing and theme design.">Wordpress</a>
</li>
		</ul>
</aside>		</div><!-- #secondary .widget-area -->
</div><!-- end main -->

		<div id="tertiary" class="widget-area" role="complementary">
			<aside id="search-3" class="widget widget_search"><form role="search" method="get" class="searchform" action="http://www.cheesetoast.co.uk" >
    <div><label class="screen-reader-text" for="s"></label>
    <input type="text" class="search-input" value="" name="s" id="s" />
    <input type="submit" class="searchsubmit" value="Search" />
    </div>
    </form></aside><!-- Wordpress Popular Posts Plugin v2.3.2 [W] [all] [regular] -->
<aside id="wpp-3" class="widget popular-posts">
<h3 class="widget-title">Popular Posts</h3><!-- Wordpress Popular Posts Plugin v2.3.2 [Widget] [all] [regular] -->
<ul>
<li><a href="http://www.cheesetoast.co.uk/responsive-web-design-3-column-layout/" title="Responsive Web Design &#8211; A 3 Column Layout" class="wpp-post-title">Responsive Web Design &#8211; A 3 Column Layout</a> <span class="post-stats"></span></li>
<li><a href="http://www.cheesetoast.co.uk/css-call-action-button/" title="Pure CSS Call To Action Button" class="wpp-post-title">Pure CSS Call To Action Button</a> <span class="post-stats"></span></li>
<li><a href="http://www.cheesetoast.co.uk/css-horizontal-add-icons-menu-bar/" title="CSS Horizontal Menu Bar with Icons" class="wpp-post-title">CSS Horizontal Menu Bar with Icons</a> <span class="post-stats"></span></li>
<li><a href="http://www.cheesetoast.co.uk/comments-bubble-for-wordpress-titles/" title="Comments Bubble on WordPress Titles" class="wpp-post-title">Comments Bubble on WordPress Titles</a> <span class="post-stats"></span></li>
<li><a href="http://www.cheesetoast.co.uk/limit-number-word-length-wordpress-excerpt/" title="Limit the Number of Words in the WordPress Excerpt" class="wpp-post-title">Limit the Number of Words in the WordPress Excerpt</a> <span class="post-stats"></span></li>

</ul>
</aside>
<!-- End Wordpress Popular Posts Plugin v2.3.2 -->
<aside id="tag_cloud-5" class="widget widget_tag_cloud"><h3 class="widget-title">Tags</h3><div class="tagcloud"><a href='http://www.cheesetoast.co.uk/tag/accessibility/' class='tag-link-37' title='1 topic' style='font-size: 8pt;'>accessibility</a>
<a href='http://www.cheesetoast.co.uk/tag/aesthetics/' class='tag-link-38' title='1 topic' style='font-size: 8pt;'>aesthetics</a>
<a href='http://www.cheesetoast.co.uk/tag/automatic/' class='tag-link-23' title='1 topic' style='font-size: 8pt;'>automatic</a>
<a href='http://www.cheesetoast.co.uk/tag/background/' class='tag-link-11' title='1 topic' style='font-size: 8pt;'>background</a>
<a href='http://www.cheesetoast.co.uk/tag/categories/' class='tag-link-106' title='2 topics' style='font-size: 10.964705882353pt;'>categories</a>
<a href='http://www.cheesetoast.co.uk/tag/contact/' class='tag-link-30' title='1 topic' style='font-size: 8pt;'>contact</a>
<a href='http://www.cheesetoast.co.uk/tag/contents/' class='tag-link-22' title='1 topic' style='font-size: 8pt;'>contents</a>
<a href='http://www.cheesetoast.co.uk/tag/css-2/' class='tag-link-10' title='6 topics' style='font-size: 17.058823529412pt;'>css</a>
<a href='http://www.cheesetoast.co.uk/tag/css3/' class='tag-link-12' title='3 topics' style='font-size: 12.941176470588pt;'>css3</a>
<a href='http://www.cheesetoast.co.uk/tag/custom/' class='tag-link-88' title='3 topics' style='font-size: 12.941176470588pt;'>custom</a>
<a href='http://www.cheesetoast.co.uk/tag/exclude/' class='tag-link-105' title='2 topics' style='font-size: 10.964705882353pt;'>exclude</a>
<a href='http://www.cheesetoast.co.uk/tag/external/' class='tag-link-19' title='1 topic' style='font-size: 8pt;'>external</a>
<a href='http://www.cheesetoast.co.uk/tag/form/' class='tag-link-31' title='1 topic' style='font-size: 8pt;'>form</a>
<a href='http://www.cheesetoast.co.uk/tag/framework/' class='tag-link-42' title='1 topic' style='font-size: 8pt;'>framework</a>
<a href='http://www.cheesetoast.co.uk/tag/functions-php/' class='tag-link-125' title='4 topics' style='font-size: 14.588235294118pt;'>functions.php</a>
<a href='http://www.cheesetoast.co.uk/tag/header/' class='tag-link-63' title='2 topics' style='font-size: 10.964705882353pt;'>header</a>
<a href='http://www.cheesetoast.co.uk/tag/html-2/' class='tag-link-28' title='1 topic' style='font-size: 8pt;'>html</a>
<a href='http://www.cheesetoast.co.uk/tag/html5/' class='tag-link-55' title='2 topics' style='font-size: 10.964705882353pt;'>html5</a>
<a href='http://www.cheesetoast.co.uk/tag/icon/' class='tag-link-18' title='1 topic' style='font-size: 8pt;'>icon</a>
<a href='http://www.cheesetoast.co.uk/tag/icons/' class='tag-link-25' title='2 topics' style='font-size: 10.964705882353pt;'>icons</a>
<a href='http://www.cheesetoast.co.uk/tag/ipad/' class='tag-link-13' title='1 topic' style='font-size: 8pt;'>ipad</a>
<a href='http://www.cheesetoast.co.uk/tag/jquery-2/' class='tag-link-17' title='5 topics' style='font-size: 15.905882352941pt;'>jquery</a>
<a href='http://www.cheesetoast.co.uk/tag/layout/' class='tag-link-27' title='3 topics' style='font-size: 12.941176470588pt;'>layout</a>
<a href='http://www.cheesetoast.co.uk/tag/link/' class='tag-link-20' title='1 topic' style='font-size: 8pt;'>link</a>
<a href='http://www.cheesetoast.co.uk/tag/list/' class='tag-link-26' title='1 topic' style='font-size: 8pt;'>list</a>
<a href='http://www.cheesetoast.co.uk/tag/media/' class='tag-link-36' title='1 topic' style='font-size: 8pt;'>media</a>
<a href='http://www.cheesetoast.co.uk/tag/menu/' class='tag-link-109' title='2 topics' style='font-size: 10.964705882353pt;'>menu</a>
<a href='http://www.cheesetoast.co.uk/tag/mobile/' class='tag-link-15' title='2 topics' style='font-size: 10.964705882353pt;'>mobile</a>
<a href='http://www.cheesetoast.co.uk/tag/nielsen/' class='tag-link-50' title='2 topics' style='font-size: 10.964705882353pt;'>nielsen</a>
<a href='http://www.cheesetoast.co.uk/tag/php-2/' class='tag-link-29' title='1 topic' style='font-size: 8pt;'>php</a>
<a href='http://www.cheesetoast.co.uk/tag/plugins/' class='tag-link-53' title='3 topics' style='font-size: 12.941176470588pt;'>plugins</a>
<a href='http://www.cheesetoast.co.uk/tag/printing/' class='tag-link-35' title='1 topic' style='font-size: 8pt;'>printing</a>
<a href='http://www.cheesetoast.co.uk/tag/responsive/' class='tag-link-77' title='3 topics' style='font-size: 12.941176470588pt;'>responsive</a>
<a href='http://www.cheesetoast.co.uk/tag/rss/' class='tag-link-72' title='2 topics' style='font-size: 10.964705882353pt;'>rss</a>
<a href='http://www.cheesetoast.co.uk/tag/security/' class='tag-link-33' title='1 topic' style='font-size: 8pt;'>security</a>
<a href='http://www.cheesetoast.co.uk/tag/seo/' class='tag-link-54' title='3 topics' style='font-size: 12.941176470588pt;'>SEO</a>
<a href='http://www.cheesetoast.co.uk/tag/slideshow/' class='tag-link-79' title='2 topics' style='font-size: 10.964705882353pt;'>slideshow</a>
<a href='http://www.cheesetoast.co.uk/tag/tables/' class='tag-link-21' title='4 topics' style='font-size: 14.588235294118pt;'>tables</a>
<a href='http://www.cheesetoast.co.uk/tag/template/' class='tag-link-45' title='2 topics' style='font-size: 10.964705882353pt;'>template</a>
<a href='http://www.cheesetoast.co.uk/tag/theme/' class='tag-link-44' title='2 topics' style='font-size: 10.964705882353pt;'>theme</a>
<a href='http://www.cheesetoast.co.uk/tag/usability/' class='tag-link-39' title='6 topics' style='font-size: 17.058823529412pt;'>usability</a>
<a href='http://www.cheesetoast.co.uk/tag/validation/' class='tag-link-32' title='1 topic' style='font-size: 8pt;'>validation</a>
<a href='http://www.cheesetoast.co.uk/tag/visual/' class='tag-link-40' title='1 topic' style='font-size: 8pt;'>visual</a>
<a href='http://www.cheesetoast.co.uk/tag/wikipedia/' class='tag-link-24' title='1 topic' style='font-size: 8pt;'>wikipedia</a>
<a href='http://www.cheesetoast.co.uk/tag/wordpress-2/' class='tag-link-41' title='13 topics' style='font-size: 22pt;'>wordpress</a></div>
</aside>		</div><!-- end tertiary .widget-area -->
</div><!-- end wrap -->

	<footer id="colophon" class="clearfix">
		<a href="#page" class="top">Top</a>
	</footer><!-- end colophon -->
	
</div><!-- end page -->
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
var pageTracker = _gat._getTracker("UA-5033712-5");
pageTracker._trackPageview();
} catch(err) {}</script>

</body>
</html>
<!-- Performance optimized by W3 Total Cache. Learn more: http://www.w3-edge.com/wordpress-plugins/

Page Caching using disk: enhanced

Served from: www.cheesetoast.co.uk @ 2012-10-18 19:33:03 -->