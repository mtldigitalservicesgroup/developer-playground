<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<link rel="stylesheet" type="text/css" href="css/main.css">
<link rel="stylesheet" href="css/handheld.css" type="text/css" media="handheld" /> 
<link rel="stylesheet" href="css/lightbox.css" type="text/css" media="screen" />
<script src="js/lightbox.js" type="text/javascript"></script>
<script type="text/javascript" src="js/prototype.js"></script>
<script type="text/javascript" src="js/scriptaculous.js?load=effects,builder"></script>
<script type="text/javascript" src="js/lightbox.js"></script>
<script src="js/modernizr.min.js"></script>
<!--[if lt IE 9]
<script src="js/html5.js" type="text/javascript"></script>
<![endif]-->

<title>the wheel</title>
</head>

<body id="home">
<div class="container">

    <header>
    	<a href="index.html"><h1>brand logo</h1></a>
        <nav>
            <h3><a href="link1.html" class="link1">link1</a></h3>
            <h3><a href="link2.html" class="link2">link2</a></h3>
            <h3><a href="link3.html" class="link3">link3</a></h3>
            <h3><a href="link4.html" class="link4">link4</a></h3>
        </nav>
        
        
        
        
    </header>

    <div class="main"><?php include "contact.php"; ?></div>
    
    <aside>aside</aside>
    <footer>footer</footer>
    
</div><!-- end .container -->
</body>
</html>

