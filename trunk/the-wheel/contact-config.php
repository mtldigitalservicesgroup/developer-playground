<?php

/*******************************************************************************
 *	IMPORTANT
*******************************************************************************/

/* Email address where the messages should be delivered */

$to = 'theresacambria@gmail.com';

/* This will be appended to the subject of contact form message */

$subject_prefix = 'website feedback';

/* Name of the file where you are including the contact form */

$where_included = 'contact-us.php';

/*******************************************************************************
 *	OPTIONAL
*******************************************************************************/

/* Security question and answer array */

$question_answers = array (
'Moon is red or white' => 'white',
'Sun is blue or yellow' => 'yellow',
'Fire is hot or cold' => 'hot',
'Icecream is hot or cold' => 'cold'
);

/* From email address, in case your server prohibits sending emails from 
 * addresses other than those of your own domain (e.g. email@yourdomain.com). */

$from = '';

/* Whether to use header/footer files? If yes, then set to TRUE */

$use_header_footer = FALSE;

/* Form header file */

$header_file = 'contact-header.php';

/* Form footer file */

$footer_file = 'contact-footer.php';

/* Thank you message to be displayed after the form is submitted. Can include 
 * HTML tags. Write your message between <!-- Start message --> and <!-- End message --> */

$thank_you_message = <<<EOD
<!-- Start message -->
<p class="thankYou">Thank you for contacting us. We will respond as soon as possible.</p>
<!-- End message -->
EOD;

/* URL to be redirected to after the form is submitted. If this is specified, 
 * then the above message will not be shown and user will be redirected to this 
 * page after the form is submitted. */

$thank_you_url = '';

/* Default character encoding of emails */

$charset = 'charset=UTF-8';

/*******************************************************************************
 *	COSMETICS
*******************************************************************************/

/* Form width in px or % value */

$form_width = '70%';

/* Form height in px */

$form_height = '500px';

/* Form background color or image. Value can contain just a color value or 
 * complete background shorthand property (with background image). */
 
$form_background = 'url(images/contact_bg.png) no-repeat';

/* Form border color */

$form_border_color = '';

/* Form border width */

$form_border_width = '0px';

/* Form border style. Examples - dotted, dashed, solid, double */

$form_border_style = '';

/* Form cell padding */

$cell_padding = '5px';

/* Form left column width */

$left_col_width = '25%';


/*******************************************************************************
 *	Do not change anything below, unless of course you know very well 
 *	what you are doing :)
*******************************************************************************/

$name = array('Name','name',NULL,NULL);
$email = array('Email','email',NULL,NULL,NULL);
$subject = array('Subject','subject',NULL,NULL);
$message = array('Message','message',NULL,NULL);
$security = array('Security question','security',NULL,NULL,NULL);

?>