package com.model;

 

import java.util.HashMap;
 
import com.google.gwt.event.shared.EventBus;
import com.google.gwt.event.shared.GwtEvent;
import com.google.gwt.http.client.Request;
import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.RequestCallback;
import com.google.gwt.http.client.RequestException;
import com.google.gwt.http.client.Response;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.FlexTable;
import com.google.gwt.user.client.ui.FlowPanel;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.HTMLTable;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TabPanel;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element; 
import com.google.gwt.xml.client.NodeList;
import com.google.gwt.xml.client.XMLParser;
import com.control.AppController;
import com.control.EventObject;
import com.model.AppData;

public class DataLoader extends GwtEvent<DataLoadHandler> {

	public static Type<DataLoadHandler> TYPE = new Type<DataLoadHandler>();
	
	private static DataLoader instance; 
	private Document xmldocDom;
	private	Element xmldocElement;
	
	
	public static DataLoader getInstance(){
		 if (instance == null){
			  instance = new DataLoader();  
		 }  
		 return instance;
	}
	
	@Override
	protected void dispatch(DataLoadHandler handler) {
		handler.onEvent(this);
	}
	
	@Override
	public Type<DataLoadHandler> getAssociatedType() {
		throw new UnsupportedOperationException("TODO");
	}
	
	
	public void loadData(){
		Window.alert("DataLoader -loadData: DATA LOADED");
		RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, AppData.DATA_MODEL_XML);
		
		try {
			requestBuilder.sendRequest(null, new RequestCallback() {

				public void onError(Request request, Throwable exception) {
					 requestFailed(exception);
					 
				}

				public void onResponseReceived(Request request,
						Response response) {
					 
					parseData(response.getText());
					
					// renderXML(response.getText());
				}
			});

		}catch(RequestException ex) {
			 requestFailed(ex);

		}
	}
 
	private void requestFailed(Throwable exception) {
		Window.alert("DataLoader -requestFailed: Failed to send the message: " + exception.getMessage());
	}
	
	private void parseData(String xmlText){
		xmldocDom = XMLParser.parse(xmlText);
		xmldocElement = xmldocDom.getDocumentElement();
		XMLParser.removeWhitespace(xmldocElement);
		String viewTxt = getElementTextValue(xmldocElement, "view");
		
		NodeList view = xmldocElement.getElementsByTagName("view");
	 
		HashMap<Integer, HashMap> pages = new HashMap<Integer, HashMap>();
		
		for (int i = 0; i < view.getLength(); i++) { 
			Element currView = (Element) view.item(i); 
			HashMap<String, String> pageObject = new HashMap<String, String>();
			//TODO consider encapsulating data into a specific type of object IE page object
			//TODO look at better code practice versus try catch for potential nulls
			try{
				Element zoom = (Element) currView.getElementsByTagName("zoom");
				String zoomVal = getElementTextValue(currView, "zoom");
				pageObject.put("zoom", zoomVal);
			}catch(Exception ex) {
				
			}
			/*
			try{
				Element ancillary = (Element) currView.getElementsByTagName("ancillary").getNodeValue();
				pageObject.put("ancillary", page);
			}catch(Exception ex) {
				
			}
			
			try{
				Element references = (Element) currView.getElementsByTagName("references").getNodeValue();
				pageObject.put("references", page);
			}catch(Exception ex) {
				
			}
			*/
			
			
			String page = currView.getAttribute("id").toString();
		
			pageObject.put("page", page);
			pages.put(i, pageObject);
			
			System.out.println(pageObject);
			 
		}    
		 
		//DATA LOAD COMPLETE
		EventObject dataloaded = new EventObject();
		dataloaded.EVENT_TYPE = AppData.DATA_LOADED;
		AppController.getInstance().eventBus().fireEvent(dataloaded);
	}
	
	private String getElementTextValue(Element parent, String elementTag) {
		return parent.getElementsByTagName(elementTag).item(0).getFirstChild().getNodeValue();
	}
	
	/*
	private void renderXML(String xmlText) {
		final TabPanel tab = new TabPanel();
		final FlowPanel xmlSource = new FlowPanel();
		final FlowPanel xmlParsed = new FlowPanel();
		tab.add(xmlParsed, "Pages");
		tab.add(xmlSource, "XML");
		tab.selectTab(0);
		RootPanel.get().add(tab);
		xmlPane(xmlText, xmlSource);
		xmldocPane(xmlText, xmlParsed);
	}

	

	private void xmlPane(String xmlText, final FlowPanel xmlSource) {
		xmlText = xmlText.replaceAll("<", "&#60;");
		xmlText = xmlText.replaceAll(">", "&#62;");
		Label xml = new HTML("<pre>" + xmlText + "</pre>", false);
		// xml.setStyleName(XML_LABEL_STYLE);
		xmlSource.add(xml);
	}

	private void xmldocPane(String xmlText, FlowPanel xmlParsed) {
		Document xmldocDom = XMLParser.parse(xmlText);
		Element xmldocElement = xmldocDom.getDocumentElement();
		XMLParser.removeWhitespace(xmldocElement);

		String view = getElementTextValue(xmldocElement, "leadin");
		String title = "<h1>" + view + "</h1>";
		HTML titleHTML = new HTML(title);
		xmlParsed.add(titleHTML);

		String notesValue = getElementTextValue(xmldocElement, "zoom");
		Label notesText = new Label();
		// notesText.setStyleName(NOTES_STYLE);
		notesText.setText(notesValue);
		xmlParsed.add(notesText);

		FlexTable pendingTable = createViewsTable(xmlParsed, "Page Details");
		NodeList ancillarys = xmldocElement.getElementsByTagName("ancillary");
		int pendingRowPos = 0;
		int completedRowPos = 0;
		
		System.out.println(ancillarys.getLength());
		for (int i = 0; i < ancillarys.getLength(); i++) {
			Element ancillary = (Element) ancillarys.item(i);
			HTMLTable table;
			int rowPos;
			if (ancillary.getAttribute("status").equals("pending")) {
				table = pendingTable;
				rowPos = ++pendingRowPos;
			} else {
				table = pendingTable;
				rowPos = ++pendingRowPos;
			}
			int columnPos = 0;
			
			displayResults(xmldocElement, ancillary, table, rowPos, columnPos);
		}
	}

	private FlexTable createViewsTable(FlowPanel xmlParsed, String label) {
		HTML viewTableLabel = new HTML("<h2>" + label);
		xmlParsed.add(viewTableLabel);
		FlexTable viewTable = new FlexTable();
		// viewTable.setStyleName(USER_TABLE_STYLE);
		viewTable.setBorderWidth(1);
		// viewTable.getRowFormatter().setStyleName(0, USER_TABLE_LABEL_STYLE);
		viewTable.setText(0, 0, "View");
		viewTable.setText(0, 1, "Ancillary Buttons");
		viewTable.setText(0, 2, "Zooms");
		viewTable.setText(0, 3, "References"); 
		xmlParsed.add(viewTable);
		return viewTable;
	}

	private void displayResults(Element xmldocElement, Element view, HTMLTable table, int rowPos, int columnPos) {

		String viewId = view.getAttribute("id");
		table.setText(rowPos, columnPos++, viewId);

		Element leadin = (Element) view.getElementsByTagName("leadin").item(0);
		String leadinLabel = leadin.getNodeValue();
		//table.setWidget(rowPos, columnPos++, leadinLabel);

		//String viewedOnValue = getElementTextValue(xmldocElement, "title");
		//table.setText(rowPos, columnPos++, viewedOnValue);

		Element title = (Element) view.getElementsByTagName("title").item(0);
		XMLParser.removeWhitespace(title);
		NodeList lst = title.getChildNodes();
		for (int j = 0; j < lst.getLength(); j++) {
			Element next = (Element) lst.item(j);
			String titlePartText = next.getFirstChild().getNodeValue();
			table.setText(rowPos, columnPos++, titlePartText);
		}

	} */
}
