package com.model;

import com.google.gwt.event.shared.EventHandler;

public interface DataLoadHandler extends EventHandler {
  void onEvent(DataLoader event);
}
