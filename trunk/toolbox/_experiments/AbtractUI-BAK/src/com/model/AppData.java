package com.model;

import java.util.HashMap;
 

public class AppData {

	
	public static final String VIEWS_DIRECTORY  	= "views";
	public static final String DATA_MODEL_XML  		= "data.xml";
	
	/*
	 * 	APPLICATION GLOBAL EVENTS
	 */
	
	public static final String DATA_LOADED  		= "dataloaded";
	public static final String DISPLAY_CHANGE  		= "displaychange";
	
	private static AppData instance; 
	private static HashMap _pages;
	
	
	public static AppData getInstance(){
		 if (instance == null){
			  instance = new AppData();  
		 } 
		 
		 return instance;
	}
	
	public static HashMap<Integer, String> pages(){
		return _pages;
	}
	
	public void pages(HashMap<Integer, String> val){
		 _pages = val;
	}
}
