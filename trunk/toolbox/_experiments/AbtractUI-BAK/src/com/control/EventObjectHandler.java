package com.control;

import com.google.gwt.event.shared.EventHandler;


public interface EventObjectHandler extends EventHandler {
	void handleEvent(EventObject event, String EVENT_TYPE);
}
