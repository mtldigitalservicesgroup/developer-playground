package com.control;

import java.util.HashMap;

import com.google.gwt.event.logical.shared.ValueChangeEvent;
import com.google.gwt.event.logical.shared.ValueChangeHandler;
import com.google.gwt.event.shared.HandlerManager;
import com.google.gwt.user.client.History;
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.ui.HasWidgets;
import com.google.gwt.xml.client.Document;
import com.google.gwt.xml.client.Element;
import com.model.AppData;
import com.model.DataLoader;
import com.view.Display;
import com.control.*;

public class AppController implements Presenter, ValueChangeHandler<String> {
	
	private static AppController instance; 
	private HandlerManager _eventBus;
	private RemoteService _rpcService;
	private HasWidgets container;
	private Display view = Display.getInstance();
   
	
	public static AppController getInstance(){
		 if (instance == null){
			  instance = new AppController();  
		 }  
		 return instance;
	}
	
	
	public HandlerManager eventBus() {
		return _eventBus;
	}

	public void eventBus(HandlerManager val) {
		_eventBus = val;
	}

	public void start(RemoteService rpcService, HandlerManager eventBus) {
		_eventBus = eventBus;
		_rpcService = rpcService;
		bind();
	}

	private void bind() {
		History.addValueChangeHandler(this);

		_eventBus.addHandler(EventObject.TYPE, new EventObjectHandler() {
			public void handleEvent(EventObject event, String currentEvent) {
				// doAddNewContact();

				if (currentEvent == AppData.DATA_MODEL_XML) {
					view.buildDisplay();
				}

			}

		});
		/*
		 * eventBus.addHandler(EditContactEvent.TYPE, new
		 * EditContactEventHandler() { public void
		 * onEditContact(EditContactEvent event) { doEditContact(event.getId());
		 * } });
		 * 
		 * eventBus.addHandler(EditContactCancelledEvent.TYPE, new
		 * EditContactCancelledEventHandler() { public void
		 * onEditContactCancelled(EditContactCancelledEvent event) {
		 * doEditContactCancelled(); } });
		 * 
		 * eventBus.addHandler(ContactUpdatedEvent.TYPE, new
		 * ContactUpdatedEventHandler() { public void
		 * onContactUpdated(ContactUpdatedEvent event) { doContactUpdated(); }
		 * });
		 */
	}

	/*
	 * private void doAddNewContact() { History.newItem("add"); }
	 * 
	 * private void doEditContact(String id) { History.newItem("edit", false);
	 * Presenter presenter = new EditContactPresenter(rpcService, eventBus, new
	 * EditContactView(), id); presenter.go(container); }
	 * 
	 * private void doEditContactCancelled() { History.newItem("list"); }
	 * 
	 * private void doContactUpdated() { History.newItem("list"); }
	 */

	public void go(final HasWidgets container) {
		this.container = container;

		if ("".equals(History.getToken())) {
			History.newItem("list");
		} else {
			History.fireCurrentHistoryState();
		}
	}

	public void onValueChange(ValueChangeEvent<String> event) {
		String token = event.getValue();

		if (token != null) {
			Presenter presenter = null;

			if (token.equals("list")) {
				// presenter = new ContactsPresenter(rpcService, eventBus, new
				// ContactsView());
			} else if (token.equals("add")) {
				// presenter = new EditContactPresenter(rpcService, eventBus,
				// new EditContactView());
			} else if (token.equals("edit")) {
				// presenter = new EditContactPresenter(rpcService, eventBus,
				// new EditContactView());
			}

			if (presenter != null) {
				presenter.go(container);
			}
		}
	}
}
