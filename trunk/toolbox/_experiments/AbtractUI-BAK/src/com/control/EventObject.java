package com.control;

import com.google.gwt.event.shared.EventHandler;
import com.google.gwt.event.shared.GwtEvent; 
import com.google.gwt.event.shared.GwtEvent.Type;  

/*
 * Utilize this class by first setting a string EVENT_TYPE prior to firing event.
 * Goal here is to minimize code not having to create one billion separate
 * Event Classes per each Event
 * EVENT_TYPE declarations are defined in com.model.AppData Singleton
 */

public class EventObject extends GwtEvent<EventObjectHandler>{
	
	public String EVENT_TYPE;
	public static Type<EventObjectHandler> TYPE = new Type<EventObjectHandler>();
	
	@Override
	 public Type<EventObjectHandler> getAssociatedType() {
	    return TYPE;
	 }
	
	@SuppressWarnings("unchecked")
	@Override
	protected void dispatch(EventObjectHandler handler) {
		handler.handleEvent(this, EVENT_TYPE);
	}
 
}