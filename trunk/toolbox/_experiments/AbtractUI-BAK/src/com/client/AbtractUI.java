package com.client;
 
import com.control.AppController;
import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.client.GWT; 
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler; 
import com.google.gwt.event.shared.HandlerManager;  
import com.google.gwt.user.client.Window; 
import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.ui.Button;  
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel; 
import com.model.DataLoader;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class AbtractUI implements EntryPoint {
	 
	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network "
			+ "connection and try again.";
 
	private final GreetingServiceAsync greetingService = GWT
			.create(GreetingService.class);

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() { 
		
		//Initialize Modules
		RemoteService rpcService = null;	
		HandlerManager eventBus = new HandlerManager(null);
	  
		AppController.getInstance().start(rpcService, eventBus);
		
		Label label = new Label("App Started");
		Button button = new Button("Click To View XML");
		button.addClickHandler(new ClickHandler() {
			@Override
			public void onClick(ClickEvent event) {
				//DataLoader.getInstance().loadData(); 
				//Window.alert("XML LOADED"); 
			}
		});

		RootPanel.get().add(label);
		RootPanel.get().add(button); 
	  
	}
 
}
