
var numofTests = new Array();
var testsPassed = new Array();
var i = 0;

/*--------------------------------------------------------------

                   Put Tests in this Function

--------------------------------------------------------------*/

// unitTest('Tested Function Name', 'Value to Enter Function', 'Test Result')

/* Test Results: 'ifequal'   = Check if entered value and value outputted by function are EQUAL
 *               'ifint'     = Check if entered value and value outputted by function are INTERGER
 *               'ifstring'  = Check if entered value and value outputted by function are STRING
 *               'ifboolean' = Check if entered value and value outputted by function are BOOLEAN
 */

function runTest() {
    etl.unitTest(retInt, 0, 'ifboolean');
    etl.unitTest(retInt, false, 'ifboolean');
    etl.unitTest(retInt, 'hi', 'ifboolean');
    etl.unitTest(retStr, 1, 'ifboolean');
    etl.unitTest(retStr, false, 'ifboolean');
    etl.unitTest(retStr, 'hi', 'ifboolean');
    etl.unitTest(retBoo, 1, 'ifboolean');
    etl.unitTest(retBoo, false, 'ifboolean');
    etl.unitTest(retBoo, 'hi', 'ifboolean');
    
}

/*--------------------------------------------------------------

                        Unit Test Class

--------------------------------------------------------------*/

var Test = function() {
    var testOutput = document.getElementById('test');

    //Testing method
    this.unitTest = function(testingFunction, expectedValue, testType) {
        makeHeader(expectedValue);
        var outputValue = testingFunction();
        outputResult(testingFunction, outputValue, expectedValue, testType);
        testSummary();
    }
    
    var makeHolder = function(cls, content) {
        var result = document.createElement("div");
        result.className = cls;
        testOutput.appendChild(result);
        result.innerHTML = content;
    }
    
    var makeHeader = function(expectedValue) {    
        var name = "If '<span>" + expectedValue + "</span>' Entered: ";
        i++;
        var title = i + ') ' + name;
        makeHolder('test-title', title);
        numofTests.push(i);
    }
    
    var testSummary = function() {
        var testTitle = document.getElementById('title');
        var cls;
        if(testsPassed.length == numofTests.length) {
            cls = 'equal';
        }
        else {
            cls = 'notequal';
        }
        testTitle.innerHTML = '<span class="' + cls + '">' + testsPassed.length +
                              '</span> of <span class="' + cls + '">' + numofTests.length + '</span> tests passed.';
    }
    
    // Switches test parameters depending on desired output value
    var outputResult = function(testingFunction, outputValue, expectedValue, testType) {
        var holderId = 'result';
        makeHolder('test-name', '(<span>' + testingFunction.name.toUpperCase() + '</span>, <span>' + testType.toUpperCase() + '</span>)');
        
        switch(testType) {
            
            case "ifequal":
                
                if(outputValue == expectedValue){
                    if((outputValue === false && expectedValue === 0) || (outputValue === 0 && expectedValue === false) ||
                       (outputValue === true && expectedValue === 1) || (outputValue === 1 && expectedValue === true)) {
                        ifFailed(holderId, expectedValue, outputValue);
                    }
                    else {
                        makeHolder(holderId + '1', '<br /><p>PASSED: Values are equal.</p>');
                        testsPassed.push(i);
                    }
                }
                else ifFailed(holderId, expectedValue, outputValue);
                break;
                
            case "ifint":
                
                if(!isNaN(outputValue) && !(outputValue === true || outputValue === false)) {
                    if((outputValue + expectedValue) % 1 == 0 && !(expectedValue === true || expectedValue === false)) {
                        makeHolder(holderId + '1', '<br /><p>PASSED: Values are type int.</p>');
                        testsPassed.push(i);
                    }
                    else ifFailed(holderId, expectedValue, outputValue);
                }
                else {
                    makeHolder(holderId + '2', '<br /><p>TEST ERROR: Function "' + testingFunction.name + '" does not output an interger value.</p>');
                }
                break;
                
            case "ifstring":
                
                if(isNaN(outputValue)) {
                    if(isNaN(expectedValue)){
                        makeHolder(holderId + '1', '<br /><p>PASSED: Values are type string.</p>');
                        testsPassed.push(i);
                    }
                    else ifFailed(holderId, expectedValue, outputValue);
                }
                else {
                    makeHolder(holderId + '2', '<br /><p>TEST ERROR: Function "' + testingFunction.name + '" does not output a string value.</p>');
                }
                break;
                
            case "ifboolean":
                
                if(outputValue === true || outputValue === false) {
                    if(expectedValue === true || expectedValue === false){
                        makeHolder(holderId + '1', '<br /><p>PASSED: Values are type boolean.</p>');
                        testsPassed.push(i);
                    }
                    else ifFailed(holderId, expectedValue, outputValue);
                }
                else {
                    makeHolder(holderId + '2', '<br /><p>TEST ERROR: Function "' + testingFunction.name + '" does not output a boolean value.</p>');
                }
        }
    }
    
    var ifFailed = function(holderId, expectedValue, outputValue) {
        makeHolder(holderId + '2', '<br /><p>FAILED</p>');
        makeHolder('values', '<p>Expected Output: <span>' + expectedValue + '</span></p>\n\
                              <p>Actual Output: <span>' + outputValue + '</span></p>'); 
    }
}

var etl = new Test();