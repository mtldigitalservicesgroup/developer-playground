Ext.require([
    'Ext.carousel.Carousel'
]);

Ext.application({
    
    
    
    name: 'epiduo_ped',
    
    requires: ['epiduo_ped.view.Hotspot',
               'epiduo_ped.view.AbsPage',
               'epiduo_ped.view.Pi',
               //'epiduo_ped.view.Isi',
               'epiduo_ped.view.Ref',
               'epiduo_ped.view.HotspotSlidingPanel',
               'epiduo_ped.view.HotspotVideo',
               'epiduo_ped.view.HotspotSitemap',
               'epiduo_ped.view.HotspotPagination',
               'epiduo_ped.view.CarouselVertical',
               'epiduo_ped.view.Overlay',
               'epiduo_ped.view.Carousel',
               'epiduo_ped.view.PleaseSee',               
               'epiduo_ped.view.VideoPlayer',
               'epiduo_ped.view.MainCarousel',
               'epiduo_ped.view.VertCarousel',
               'epiduo_ped.view.AbsNavButton','epiduo_ped.view.AbsSlidingContainer',
               'epiduo_ped.view.SliderExtended'],

    //CONSTS:
    SLIDE_TYPE_MAIN:     0,
    SLIDE_TYPE_SUB:      1,
    SLIDE_TYPE_SPLASH:   2,
    SLIDE_TYPE_SITEMAP:  3,
    
    
    views: ['1-2.Page_1-2',
            '1-2.Overlay_1-2-a',
            '1-2-2.Page_1-2-2',
            '1-2-2.Overlay_1-2-2a',
            '1-2-2.Overlay_1-2-2b',
            '1-2-2.Overlay_1-2-2c',
            '1-3.Page_1-3',
            '1-3.Overlay_1-3-a',
            '1-3-1.Page_1-3-1',
            '1-3-1.Overlay_1-3-1a',
            '1-3-2.Page_1-3-2',
            '1-3-2.Overlay_1-3-2a',
            '1-3-2.Overlay_1-3-2b',
            '1-6-3.Page_1-6-3',
            '1-6-3.Overlay_1-6-3-a',
            '1-6-3.Overlay_1-6-3-b',
            'sitemap.Overlay_sitemap',
            'VideoPlayer',
            'MainCarousel',
            'VertCarousel',
            'AbsPage',
            'AbsOverlayContent',
            'AbsContent',
            'NavBar',
        ],

   
    //'Controller' controls the nav and carousel as they go hand in hand
    //other controlers should be created and added here ie a controller for lightboxes?
    controllers: [
        //'Controller',
        'CtrlMainCarousel',
        'CtrlHotspot',
        'CtrlSlidingPanel',
        'CtrlHotspotVideo',
        'CtrlPage',
        'CtrlContent',
        'CtrlMainNavToolbar',
        'CtrlSiteMapHotspot'
       ],
    
    
    models: ['Slide'],
    stores: ['Slides','MainStore','SlidesSection2','SlidesSection3','SlidesSection6','SlidesSiteMap'],
    
    
    launch: function(){

         //Ext.Loader.setConfig({disableCaching: true});
        
        Ext.fly('appLoadingIndicator').destroy();
        // Ext.Viewport.add(Ext.create('GS.view.Main'));
      Ext.create('epiduo_ped.view.Main');
       
    }
});