Ext.define('epiduo_ped.store.MainStore', {
    extend: 'Ext.data.Store',        
    data: [
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection2',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_2',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_2_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_2_active',
                  etl_btn_width:                91
                },{ 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection3',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_3',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_3_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_3_active',
                  etl_btn_width:                91
                },
                { 
                  sectiontype:                  'carousel',
                  dataStore:                    'SlidesSection6',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_1_6',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_1_6_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_1_6_active',
                  etl_btn_width:                91
                },
                {                  
                  sectiontype:                  'siteMap',
                  dataStore:                    'SlidesSiteMap',
                  etl_btn_class:                'etl_nav_btn etl_nav_btn_sitemap',
                  etl_btn_loadingclass:         'etl_nav_btn etl_nav_btn_sitemap_loading',
                  etl_btn_activeclass:          'etl_nav_btn etl_nav_btn_sitemap_active',
                  etl_btn_width:                91
                }
                
          ]
    }
);




