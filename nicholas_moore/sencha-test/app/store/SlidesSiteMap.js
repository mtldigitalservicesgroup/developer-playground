Ext.define('epiduo_ped.store.SlidesSiteMap', {
    extend: 'Ext.data.Store',
    requires: 'epiduo_ped.model.Slide',
    config: {
    model: 'epiduo_ped.model.Slide',
        
    data: [
                //////////////////////////////////////// PAGE //////////////////////////////////////////          
                { etl_btn_id:                       'sitemap',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_sitemap',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_sitemap_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_sitemap_active',
                  etl_label:                        'Sitemap',
                  etl_tabsvisible:                  true,
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_idx:                 4,
                  etl_carousel_item_xtype:          'sitemap',
                  etl_sitemap_id:                   'hotspot_sitemap_1_8_3',
                  etl_carousel_vert_idx:            -1,
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                     true,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               false
                  
                }
                
          ]
    }
});




