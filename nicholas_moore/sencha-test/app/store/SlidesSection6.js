Ext.define('epiduo_ped.store.SlidesSection6', {
    extend: 'Ext.data.Store',
    
    requires: 'epiduo_ped.model.Slide',

    config: {
        model: 'epiduo_ped.model.Slide',
        
    data: [
                { etl_btn_id:                       '1-6-3',
                  etl_btn_class:                    'etl_nav_btn etl_nav_btn_1_6',
                  etl_btn_loadingclass:              'etl_nav_btn etl_nav_btn_1_6_loading',
                  etl_btn_activeclass:              'etl_nav_btn etl_nav_btn_1_6_active',
                  etl_label:                        'Adapelene vs. Tretinion/BPO',
                  etl_please_see_visible:           true,
                  etl_nositemap:                    false,
                  etl_carousel_item_xtype:          'page1-6-3',
                  etl_sitemap_id:                   'hotspot_sitemap_1_6_3',
                  
                  
                  etl_overlay_bg_fill_color:         '#ffffff',
                  etl_allow_up:                     true,
                  etl_allow_down:                   true,
                  etl_hide_home_link:               false
                }
                
          ]
    }
});




