Ext.define('epiduo_ped.view.HotspotSitemap',
{
    extend:  'Ext.Img',   
    xtype:   'hotspot_sitemap',
    me_hotspot_overlay:  '',
   
    
    config:
    {    
        action   : 'siteMapButtonTap',
        border : '2px',
        style: 'color: red; border: solid',
        listeners: {
            tap: {
                fn: function(e) {         
                    
                    Ext.Viewport.setMasked({xtype:'loadmask',message:'loading...'});
                    setTimeout(function(){
                        Ext.Viewport.setMasked(false); },100);           
                            setTimeout(function(){
                            //LEAVE THIS!
                            
                            me_hotspot_sitemap.fireEvent('siteMapTap', e);},90);
                    
                },
                element: 'element'
            }
        }
    },
    initialize: function(){        
       me_hotspot_sitemap = this; 
    }
    
});


