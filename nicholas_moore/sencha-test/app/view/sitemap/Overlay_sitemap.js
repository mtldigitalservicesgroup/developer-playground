var me = null;
Ext.define('epiduo_ped.view.sitemap.Overlay_sitemap.js',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_sitemap',
    
    
    /*this is where hostspots start in the top left corner
    //all other one will build off these coordinates.
    hs_top:    60,
    hs_left:   20,
    hs_width:  100,
    hs_height: 105,*/
    
    config:
    {
       
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',

        items:[
        {  
           xtype:'img',
           src: 'app/view/sitemap/images/sitemap@2xb.png',
           cls:"sitemap",
           height: '490px',
           width:  '906px',
           top:0,		   
           right:'10px',
           id:'id_img_overlay_sitemap'
        }, 
        /////////////////////////////////////////////////
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            42,
           id: '1-2_0_0'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             164,
           left:            42,
           id: '1-2-2_0_1'
        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            150,
           id: '1-3_1_0'

        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             164,
           left:            150,
           id: '1-3-1_1_1'

        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             268,
           left:            150,
           id: '1-3-2_1_2'

        },
        {  
           xtype:'hotspot_sitemap',
           width:           100,
           height:          105,
           top:             60,
           left:            258,
           id: '1-6-3_2_0'

        }
    
    ]
    
    },
    initialize: function(){
    
       var this_overlay = this;
       
       
      
    },
    //---------------------------------
    swapImg:function(direction){
        
        
       
    },        
    
    closeIt:function(){
        this.parent.hide();
       // this.stopVids();
    }
    

});


