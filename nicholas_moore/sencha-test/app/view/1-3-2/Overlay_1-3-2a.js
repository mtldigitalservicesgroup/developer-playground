var me = null;
Ext.define('epiduo_ped.view.1-3-2.Overlay_1-3-2a',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_1-3-2a',
    id:     'overlay_1-3-2a',
    
    config:
    {
        //src: 'app/view/1-2/images/overlay.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
          //style:   'background-color: #ffffff;',
        
        items: [

            {   
                cls:'overlay_1-3-2HeaderFemale',
                height: '19px',
                width :'585px',
                left: '50px',
                top: '-40px',
                id:'1_3_2aOverLayHeader'

            },
            { 
                 xtype     : 'video',
                 left      : '50px',
                 top       :'0px',
                 height    : '371px',
                 width     :'654px',
                 url       : "app/view/1-3-2/video/female_Tolerability.mp4",
                 posterUrl : 'resources/images/video/genericPoster@2x.png',
                 style     : 'border-style:Solid; border-width:1px;',
                 id:'1_3_2avideoPlayer'
             },
              {   
                cls:'overlay_1-3-2Text',
                height: '204px',
                width :'936px',
                left:'5px',
                top: '395px'
            }
            ,
              {   
                cls:'overlay_1-3-2ButtonFemaleActive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_3_2afemaleButton'
            }
            ,
              {   
                cls:'overlay_1-3-2ButtonMaleInactive',
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_3_2amaleButton'
            },
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '0px',
                id:'1_3_2afemaleVidButton'
            }
            ,
              {   
                xtype:'hotspotVideo',  
                height: '142px',
                width :'150px',
                left:'730px',
                top: '150px',
                id:'1_3_2amaleVidButton'
            }
            
            
            
        ]
    },

    initialize: function(){        
      //1_1_1_femaleVidButton.me_video = ''; 
    //  myobject = Ext.get('1_1_1_femaleVidButton');
    // alert(myobject);
    //    myobject.setVideo('app/view/1-1-1/video/male_Baseline.mp4');
    }
    
   
});
