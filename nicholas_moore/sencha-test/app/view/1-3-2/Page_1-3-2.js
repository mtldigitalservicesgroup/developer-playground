Ext.define('epiduo_ped.view.1-3-2.Page_1-3-2',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-3-2',
    fullscreen: true,
    
    config:
    {
     cls:"page1_3_2BG",
     
     items:[ 
      
      
            {  
                xtype:'hotspot',
                width:320,
                height:402,
                top:190,
                left:170,
                //style: 'color: red; border: solid',
                id: 'hotspot_page1-3-2_a',
                etl_overlay_id:'overlay_1-3-2a'
             },
             {  
                xtype:'hotspot',
                width:320,
                height:402,
                top:190,
                left:532,
                //style: 'color: red; border: solid',
                id: 'hotspot_page1-3-2_b',
                etl_overlay_id:'overlay_1-3-2a'
             },
             {
                 xtype:'pleasesee'

             }
         ]
        
      },
       
    }
);
