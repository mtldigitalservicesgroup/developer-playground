Ext.define('epiduo_ped.view.HotspotSlidingPanel',
{
    extend:  'epiduo_ped.view.Hotspot',  
    
    
    
    
    xtype:   'hotspotSlidingPanel',
    me_hotspotSlidingPanel:  '',
    slide_back:false,
    
    
    config:
    {
        listeners: {
            tap: {
                fn: function(e) {
                   
                       if(!this.isCorrectClass(e))
                          return;
                   
                       this.me_hotspotSlidingPanel.fireEvent('hotspot_sliding_panel_tap',this);
                },
                element: 'element'
            }
        }
    },
    initialize: function(){
       this.me_hotspotSlidingPanel = this; 
       
       this.callParent();
      
    }
    
});


