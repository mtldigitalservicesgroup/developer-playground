Ext.define('epiduo_ped.view.1-2.Page_1-2',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-2',
    fullscreen: true,
    
    config:
    {
       cls:"page1_2BG",
       items:[ 
      
        {  
           xtype:'hotspot',
           width:640,
           height:313,
           top:200,
           left:200,
           id: 'hotspot_page1-2_a',
           etl_overlay_id:'overlay_1-2-a'
        },
        {
            xtype:'pleasesee'
            
        }
       ]
      
    }
   
});
