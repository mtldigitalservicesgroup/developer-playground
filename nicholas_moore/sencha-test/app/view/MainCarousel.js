Ext.define('epiduo_ped.view.MainCarousel', {
    
    extend: 'Ext.carousel.Carousel',
    xtype:     'maincarousel',
    direction :'vertical',
    directionLock : true,
   // indicator : false,
    zindex :0,
    
    the_carousel:'',
    items_final :[],
    subCarousels:[],
    carousels_loaded :'',    
    useAnimation:{duration: 300, easing: {type: 'ease-out'}   },
    _data:[],
    
    config: {
        defaults: {
            styleHtmlContent: true
        }
    },
    
    initialize: function(){    
    the_carousel = this;         
    the_carousel.toggleSwipe(true);
    the_carousel.setAnimation(this.useAnimation)
    the_carousel.on('painted', function() {the_carousel.fireEvent('viewready', the_carousel);}, null, { single : true });
   
    // prevent native nidicator from appearing on page
    the_carousel.setIndicator(false);
    
    //add all slides from our app/store/Slides.js        
    var count = 0;    
        Ext.getStore('MainStore').load(function(slides){
            for( var i=0;i<slides.length;i++){
                var slideType = slides[i].get('sectiontype');
                
                switch(slideType){
                    case 'carousel':
                    if( the_carousel.carousels_loaded == slides[i].get('dataStore') ){
                            break;
                    } else {
                            the_carousel.carousels_loaded = slides[i].get('dataStore');
                    }                        
                            the_carousel.addVerticalCarousel(slides[i].get('dataStore'),count);
                            count++
                            break;
                        
                    case 'siteMap':
                        the_carousel.addSiteMap('SiteMap');
                        //
                            break;                    
                }
            }
            
       for(i=0;i<the_carousel.getItemsFinal().length; i++){
             the_carousel.add(the_carousel.getItemsFinal()[i]);
         }
               
            }); //end  Ext.getStore('Slides').load(function(slides)
            the_carousel.callParent();
            
    }, 
    
    add : function() {        
        this.callParent(arguments);
        var item = this.getInnerItems()[0];
        if (item instanceof Ext.carousel.Carousel){     
            //if a vertical carousel then we need to get the first item and
            //fire an event on that guy            
            item.getInnerItems()[0].fireShowEvent();
        } else {  
            item.fireShowEvent();
        } 
       
    },
    
    onActiveItemAnimationEnd : function() {
        var me             = this,
            prevActiveItem = me.getActiveItem(),
            currentActiveItem;

        me.callParent(arguments);

        currentActiveItem = me.getActiveItem();
        
        if (currentActiveItem instanceof Ext.carousel.Carousel){
            
            //if a vertical carousel then we need to get the first item and
            //fire an event on that guy:
            var items = currentActiveItem.getInnerItems();
            currentActiveItem = items[0];
            
        }     
        (prevActiveItem != currentActiveItem) && currentActiveItem.fireShowEvent(currentActiveItem);
    },
    
    setOffsetAnimated: function(offset) {
        var me            = this,
            animDirection = me.animationDirection,
            prevItem      = me.getActiveItem();

        this.callParent(arguments);

        if (animDirection != 0) {
            
            if (prevItem instanceof Ext.carousel.Carousel){
                //if a vertical carousel then we need to get the first item and
                //fire an event on that guy:
                var items = prevItem.getInnerItems();
                prevItem  = items[0];
            }
            
            prevItem.fireHideEvent(prevItem);
        }
    },

   toggleSwipe : function(allow) {
        this.element[allow ? 'on' : 'un']({
            dragstart : 'onDragStart',
            drag      : 'onDrag',
            dragend   : 'onDragEnd',
            scope     : this
        });
    },
    
   onDrag: function(e) {
        if (!this.isDragging) {
            return;
        }

        var startOffset = this.dragStartOffset,
            direction = this.getDirection(),
            delta = direction === 'horizontal' ? e.deltaX : e.deltaY,
            lastOffset = this.offset,
            flickStartTime = this.flickStartTime,
            dragDirection = this.dragDirection,
            now = Ext.Date.now(),
            currentActiveIndex = this.getActiveIndex(),
            maxIndex = this.getMaxItemIndex(),
            lastDragDirection = dragDirection,
            offset;

        if ((currentActiveIndex === 0 && delta > 0) || (currentActiveIndex === maxIndex && delta < 0)) {
          //  delta *= 0.5; 
          delta *=0.05;
        }

        offset = startOffset + delta;

        if (offset > lastOffset) {
            dragDirection = 1;
        }
        else if (offset < lastOffset) {
            dragDirection = -1;
        }

        if (dragDirection !== lastDragDirection || (now - flickStartTime) > 300) {
            this.flickStartOffset = lastOffset;
            this.flickStartTime = now;
        }

        this.dragDirection = dragDirection;

        this.setOffset(offset);
    },

    onDragEnd: function(e) {
        if (!this.isDragging) {
            return;
        }

        this.onDrag(e);
        this.isDragging = false;

        var now = Ext.Date.now(),
            itemLength = this.itemLength,
            threshold = itemLength / 2,
            offset = this.offset,
            activeIndex = this.getActiveIndex(),
            maxIndex = this.getMaxItemIndex(),
            animationDirection = 0,
            flickDistance = offset - this.flickStartOffset,
            flickDuration = now - this.flickStartTime,
            indicator = this.getIndicator(),
            velocity;

        if (flickDuration > 0 && Math.abs(flickDistance) >= 10) {
            velocity = flickDistance / flickDuration;

            if (Math.abs(velocity) >= 1) {
                if (velocity < 0 && activeIndex < maxIndex) {
                    animationDirection = -1;
                }
                else if (velocity > 0 && activeIndex > 0) {
                    animationDirection = 1;
                }
            }
        }

        if (animationDirection === 0) {
            if (activeIndex < maxIndex && offset < -threshold) {
                animationDirection = -1;
            }
            else if (activeIndex > 0 && offset > threshold) {
                animationDirection = 1;
            }
        }

        if (indicator) {
            indicator.setActiveIndex(activeIndex - animationDirection);
        }
        this.fireEvent('mainsectionChange', activeIndex, animationDirection);
        
        this.animationDirection = animationDirection;
        this.setOffsetAnimated(animationDirection * itemLength);
    }, 
  
    /* These 3 events added to let us know when carousel item has changed (or in other words, 'is in view) */

    addSlide: function(myStore,count){        
        var item = '';
            Ext.getStore(myStore).load(function(slide){
            // console.log(slide[0].get(['etl_carousel_item_xtype']) );
            item ={                
              xtype:                            slide[0].get('etl_carousel_item_xtype'),  
              navBtnId:                         slide[0].get('etl_btn_id'),
              slideId:                          slide[0].get('etl_btn_id'),                              
              slideAllowUp:                     slide[0].get('etl_allow_up'), 
              slideAllowDown:                   slide[0].get('etl_hide_home_link'), 
              slideButtonClass:                 slide[0].get('etl_btn_class'), 
              slideLoadingClass:                slide[0].get('etl_btn_loadingclass'), 
              slideActiveClass:                 slide[0].get('etl_btn_activeclass'), 
              slideLabel:                       slide[0].get('etl_label'), 
              slideTabsVisible:                 slide[0].get('etl_tabsvisible'), 
              slidePleaseSeeVisible:            slide[0].get('etl_please_see_visible'), 
              slideNoSitemap:                   slide[0].get('etl_nositemap'), 
              //etl_carousel_item_xtype:          slide[0].get('etl_carousel_item_xtype'), 
              slideSiteMapID:                   slide[0].get('etl_sitemap_id'), 
              slideCarouselIndex:               count, //slide[0].get('etl_carousel_idx'), 
              slideCarouselVertIndex:           slide[0].get('etl_carousel_vert_idx'), 
              slideOverlayBGColor:              slide[0].get('etl_overlay_bg_fill_color'), 
              slideHomeLink:                    slide[0].get('etl_hide_home_link')              
           }           
          // console.log("slideTabsVisible" + slide[0].get('etl_please_see_visible'));
        });
        this.getItemsFinal().push(item);
        //console.log("items_final >>> " + this.getItemsFinal());
       // items_final.push(item);
    }, 
    
    addVerticalCarousel: function(myStore,count){
        //console.log(myStore);                
        var myCarousel = new Ext.create('epiduo_ped.view.VertCarousel', 
        {dataFile:myStore,carouselID:count,useAnimation:this.useAnimation})
        
        this.getItemsFinal().push(myCarousel);  
        this.getSubCarousels().push(myCarousel);
        
    }, 
    
    addSiteMap: function(myStore){
        //console.log(myStore);          
    }, 
    addSplashPage: function(myStore){
        //console.log(myStore);
    }, 
        
    setUpListeners: function(){
	        //this code block so Controller will become aware when
                //this carousel is painted on screen
                //then we will have reference in controller for this carousel
                    the_carousel.on('painted', function() {
                        the_carousel.fireEvent('viewready', the_carousel);
                    }, null, { single : true });
                
                //Override on drag end so we can prevent user from dragging back to
                //splash and sync up carousel with nav button active states
                //Controller will handle
                    the_carousel.onDragStart = function(e) {
                        the_carousel.fireEvent('dragstart', e);
                    },
                    the_carousel.onDrag = function(e) {
                        the_carousel.fireEvent('drag', e);
                    },
                    the_carousel.onDragEnd = function(e) {
                        the_carousel.fireEvent('dragend', e);
                    },
                    the_carousel.animationListeners.animationend = function(e) {
                       the_carousel.fireEvent('animationended', e);
                    }            
    },
    
    getSubCarousels : function(){
        return this.subCarousels;
    },
    
    getItemsFinal : function(){
        return this.items_final;
    }

});