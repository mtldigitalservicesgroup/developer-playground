Ext.define('epiduo_ped.view.HotspotPagination',
{
    extend:  'epiduo_ped.view.Hotspot',  
    xtype:   'hotspot_pagination',
   
    config:
    {
       
        listeners: {
            tap: {
                fn: function(e) {
                    
                    if(!this.isCorrectClass(e))
                            return;
                   
                    this.parent.firePaginate(this.etl_paginate_direction);
                    
                    
                },
                element: 'element'
                
            }
        }
    }
    
 });




