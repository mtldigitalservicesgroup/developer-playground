Ext.define('epiduo_ped.view.Ref', {
	extend: 'epiduo_ped.view.AbsSlidingContainer',
	
        me_ref:'',
        id:'slide_panel_ref',
        left: '988px',
        
        slide_panel_type: 1,
        
        openFromleft:   988,
        openToLeft:     493,
        closeFromleft:  493,
        closeToLeft:    988,
        
       
         config: {
		
		items: [
                            {
                            xtype:'panel',
                            layout: 'hbox',
                            items:[
                            
                             {
                                xtype:   'image',
                                //src:     'resources/images/tabs/tab-ref.png',
                                height:  '35px',
                                width:   '38px',
                                top:     '182px',
                                id:      'tab_ref'
                             },
                             {
                                 xtype:   'container',
                                 style:   'background-color: #7d7d7d;',
                                 height:  '768px',
                                 left:    '35px',
                                 width:   '534px'
                             },
                       
                             {  
                                xtype:'image',
                                 src:     'resources/images/tabs/panelCloseBox.png',
                                 height:  '35px',
                                 width:   '35px',
                                 top:15,
                                 left:475,
                                 id: 'tabCloseButton'
                             } ,
							 
							 
                             {
                                xtype:   'image',
                                src:     'app/view/1-2/images/ref.png',
                                height:  '600px',
                                width:   '700px',
                                top:     '60px',
                                left:    '78px',
                                id:      'id_ref_content'  
                             },
			     {  
                                 xtype:'hotspotSlidingPanel',
                                 height:  '90px',
                                 width:   '90px',
                                 padding: '15px',
                                 top:0,
                                 left:444,
                                 slide_back:false,
                                 id: 'hotspot_ref_closeX'
                             },
                             {  
                                xtype:'hotspotSlidingPanel',
                                width:60,
                                height:55,
                                top:164,
                                right:-37,
                                slide_back:false,
                                id: 'hotspot_ref'
                            }
                        ]
                     }
                    
                        
                         
                       ]
	}
        
});


