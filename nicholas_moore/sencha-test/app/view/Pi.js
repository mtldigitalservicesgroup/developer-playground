Ext.define('epiduo_ped.view.Pi', {
    extend: 'epiduo_ped.view.AbsSlidingContainer',
    
    
  //  me_pi:this,
    m_hotspot_pi:null,
    id:   'slide_panel_pi',
    xtype:     'piPanel',
    left: '988px',
    
    slide_panel_type: 0,
        
    openFromleft:   988,
    openToLeft:      28,
    closeFromleft:   28,
    closeToLeft:    988,
    
    m_pi_hotspot:null,
       
      
    config: {
        
        
            
        items: [
                         
        {
            xtype:'panel',
            id:'id_pipanel_container',
            layout: 'hbox',
            items:[
            /*                                     {
                                         xtype:   'container',
                                         style:   'background-color: #ffffff;',
                                         height:  '707px',
                                         left:    '-45px',
                                         cls:      'tabPanelBGFill',
                                         width:   '80px',
                                         id:      'pi_bgFill',
                                         hidden : true
                                     },
*/                                     
            {
                xtype:   'image',
                //src:     'resources/images/tabs/tab-pi.png',
                height:  '215px',
                width:   '38px',
                top:     '223px',
                id:      'tab_pi'
            },
            {
                xtype:   'container',
                style:   'background-color: #7c7c7c;',
                height:  '768px',
                left:    '35px',
                width:   '988px',
            },

            {  
                xtype:'hotspotSlidingPanel',
                src:     '',
                height:  '35px',
                width:   '35px',
                top:15,
                left:935,
                slide_back:true,
                id: 'hotspot_pi_closeX'
            }
            ,
            {  
                xtype:'hotspotSlidingPanel',
                width:60,
                height:215,
                top:223,
                right:-36,
                id: 'hotspot_pi',
                slide_back:false //this is implied by parent class
            }
            ,
            {
                xtype: 'container',
                top:     '50px',
                left:    '50px',
                height:  '708px',
                width:   '900px',
                             
                layout: 'vbox',  
                                 
               // scrollable: true,
               scrollable: {
                    direction: 'vertical',
                    directionLock: true
                },
                scroll: 'vertical',
                id: 'PI_content_container',
                             
                        items:[
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_01.png',
                            height:  '297px',
                            width:   '850px'
                        },
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_02.png',
                            height:  '261px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_03.png',
                            height:  '430px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_04.png',
                            height:  '383px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_05.png',
                            height:  '400px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_06.png',
                            height:  '439px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_07.png',
                            height:  '460px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_08.png',
                            height:  '448px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_09.png',
                            height:  '398px',
                            width:   '850px'
                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_10.png',
                            height:  '439px',
                            width:   '850px'

                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_11.png',
                            height:  '561px',
                            width:   '850px'

                        }
                        ,
                        {
                            xtype:   'image',
                            src:     'resources/images/tabs/PI_12.png',
                            height:  '469px',
                            width:   '850px'

                        }

                        ]
                          
            }
            ]
        }
        ]
    },
    /*any class implementing THIS class should call this function (i.e: PleaseSee.js)*/
    doPiSlide:function(){
        throw "unimplemented method: Pi.doPiSlide() ";
    }
});


