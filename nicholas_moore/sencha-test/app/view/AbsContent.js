/* 
 * 
 * Base Class for AbsOverlayContent & AbsPage
 * 
 */
Ext.define('epiduo_ped.view.AbsContent',
{
    extend:  'Ext.Container',  
    xtype:   'absContent',
    fullscreen: true,
    me_absContent:null,
    
    /* For child pages that have tabbed views*/
    theTab:             null,  
    tabClsNameBegin:    null,
    tabClsNameEnd:      null,
    tabNumber:          null,
    
    /* For pages that need to preload*/
    //imageArray:null,
    
    /* For pages that need to paginate*/
    paginateImageArray:null,
    paginateTarget:null,
    paginateCurIdx:0,
    
    // Use timer to automatically paginate images
    paginateStartDelay:0,
    paginateTimeBetween:0,
    
    //For pages that want to use a slider for pagination
    paginateSlider:false,
    //array of imcrements, where the slider will show the next image:
    paginateSliderIncrements:null,
    
    
    
    config:
    {
        cls: ''
        
    },
    initialize:function(){
       
       this.callParent();      
    },
    fireChangeTabEvent:function(num){
        
        this.tabNumber = num;
        this.fireEvent('EvtChangeTab', this);
    },
    firePreloadEvent:function(imageArray){
        
        this.fireEvent('EvtPreload',imageArray);
    },
    paginateTimer:function(startDelay,timeBetween){
        
        this.paginateStartDelay = startDelay;
        this.paginateTimeBetween = timeBetween;
        this.fireEvent('EvtPaginateTimer',this);
        
    },
    firePaginate:function(direction){
        
       this.fireEvent('EvtPaginate',this,direction );
    },
    fireSetImageById:function(num){
       
      this.fireEvent('EvtSetImageById',this,num );
    },
    fireSliderEvent:function(slider,position){
        
      this.fireEvent('EvtSlider',this,slider,position);
    }

});


