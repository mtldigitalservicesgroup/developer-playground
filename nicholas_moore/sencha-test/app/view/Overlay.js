Ext.define('epiduo_ped.view.Overlay', {
	
        me_overlay: '',
        item_xtype: '',
        overlay: '',
        zindex :20,
        iImgId:-1,
        xtype:'xtype_etl_overlay',
        
        constructor: function(xtype1,imgId) {           
            // passes in the content for this overlay:
            this.item_xtype = xtype1;            
            this.iImgId = imgId;            
            this.createOverlay();
        },
        createOverlay: function(){
             //alert('creating overlay! id= ' + this.item_xtype)
             this.overlay = Ext.create('Ext.Panel', {
                 
                        modal: false,
                        hideOnMaskTap: true,
                        showAnimation: {
                            type: 'popIn',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        hideAnimation: {
                            type: 'popOut',
                            duration: 250,
                            easing: 'ease-out'
                        },
                        //scroll: 'vertical',
                        //centered: true,
                        top:28,
                        left:25,
                        width:  937,
                        height: 663,
                        cls: 'etl_overlay',
                        id:'the_overlay',
                       
                        

                        //styleHtmlContent: true,
                        //html:'Overlay',
                        items: [
                            /* if we ever need an overlay with a toolbar
                            {
                                docked: 'top',
                                xtype: 'toolbar',
                                text: 'Overlay Title'
                            },*/
                            {
                               xtype: this.item_xtype,
                               etl_setImgbyID : this.iImgId
                            },
                            {  
                               xtype:'hotspot',
                               width:90,
                               height:75,
                               top:0,
                               right:0,
                              
                               listeners: {
                               tap: {
                                        fn: function(e) {
                                            
                                            if(!this.isCorrectClass(e))
                                                return;
                                            
                                            
                                            this.fireEvent('EvtCloseOverlay','');
                                        },
                                        element: 'element'
                                    }
                                }
                            }
                        ],
                        scrollable: false,
                        initialize: function( ){
                            //alert("overlay initialized");
                            
                            //ok at this point our item (content) for this overlay should be initialized
                            //so we can now call functions on it for custom actions
                            //(i.e: fireSetImageById())
                            var p_instance = this;
                            this.on('painted', function(){
                               
                               var overlayObj = p_instance.getItems().items[0];
                               if(overlayObj.etl_setImgbyID >=0 )
                                    overlayObj.fireSetImageById(overlayObj.etl_setImgbyID);

                                
                            },p_instance )
                            
                        }
                     });
        },
        //--------------------------------------------
        closeIt:function(){

            this.overlay.hide();
            
            this.stopVids();
        },
        //--------------------------------------------
        stopVids:function(){
          
            try{
                var vidAr = Ext.ComponentQuery.query('video');

                for(var i  = 0 ; i < vidAr.length; ++i){

                    if(vidAr[i] != undefined && vidAr[i] != null && vidAr[i] != 'undefined' && vidAr[i].isPlaying() )
                        vidAr[i].stop();
                }
            }
            catch(e)
            {
                
            }
        },
        //----------------------------------------------
        popIt:function (){

            this.overlay.show();
        
        },
        //----------------------------------------------
        setXtype:function(theXtype,imgId){
            
             this.iImgId = imgId;
            
             this.overlay.removeAt(0);
             
             var items = [];
             var item =  {
                            xtype:theXtype,
                            etl_setImgbyID : imgId
                         }
             items.push(item);
             
            
             this.overlay.insert(0,items);
        },
        //-----------------------------------------------
        addToContainer:function(){
            
           Ext.Viewport.add(this.overlay);  
            
        }
});