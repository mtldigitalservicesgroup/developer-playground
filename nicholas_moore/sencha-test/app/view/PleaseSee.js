/* 
 * 
 * Please See to be included in all aslides
 * 
 */
Ext.define('epiduo_ped.view.PleaseSee',
{
    extend:  'Ext.Container',
    
    
    //"Implements"
    //Sencha's version of interface
 //   mixins: {
 //       slidePi: 'epiduo_ped.view.Pi'       
 //   },
        
    xtype:   'pleasesee',
      
    config:
    {   
    
        
       
       //also add the please see image on top of all the slides:
       cls: 'pleasee',
       width:362,
       height:17,
       bottom:82,
       left:88,
       items:[
           {
               xtype:'hotspot',
               width:195,
               height:40,
               bottom:-10,
               left:70,
               //id: 'hotspot_please_see',
               listeners: {
                   tap: {
                        fn: function(e) {
                             
                             if(!this.isCorrectClass(e))
                                 return;
                             
                             this.parent.doPiSlide();
                        },
                        element: 'element'
                        //scope:this
                    }
               }        
           }
       ]
    },
    /*This essenatially ovverides Pi's version of this function
     *this way, 'this' below IS the Pi class instance
     *  */ 
    doPiSlide:function(){
        this.fireEvent('onShowPITab');
        //this.fireEvent('hotspot_pleasee_tap',this,true);
    }
});


