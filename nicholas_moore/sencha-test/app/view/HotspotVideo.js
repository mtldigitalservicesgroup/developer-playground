/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

Ext.define('epiduo_ped.view.HotspotVideo',
{
    extend:  'epiduo_ped.view.Hotspot',  
    xtype:   'hotspotVideo',
    me_hotspotVideo:  '',
    me_video:'',
    me_player:'',
    
    config:
    {
        src: 'resources/images/hotspot/hotspot.png',
      //  style: 'color: red; border: solid; border-width:1px;',
        listeners: {
            tap: {
                fn: function(e) {
                    //alert('video')
                    me_hotspotVideo.fireEvent('hotspot_video_overlay_tap', e);
                },
                element: 'element'
            }
        }
    },

    initialize: function(){        
       me_hotspotVideo = this;     

    },
      getVideo: function(){
              return me_video
          },
          
          setVideo: function(s){
              me_video = s;
              //alert(me_video);
           },
          
          getPlayer: function(){
              return me_player;
              //alert(me_video);
           },
          
          setPlayer: function(s){me_player = s}
    
});






