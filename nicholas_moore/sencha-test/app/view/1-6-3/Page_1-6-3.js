Ext.define('epiduo_ped.view.1-6-3.Page_1-6-3',
{
    extend:  'epiduo_ped.view.AbsPage',  
    xtype:   'page1-6-3',
    fullscreen: true,
    
    config:
    {
        cls:"page1_6_3BG",
       items:[ 
       
        {  
           xtype:'hotspot',
           width:332,
           height:233,
		   top:230,
		   left:150,
           //style: 'color: red; border: solid',
           id: 'hotspot_page1-6-3-a',
           etl_overlay_id:'overlay_1-6-3-a'
        },
        {  
           xtype:'hotspot',
           width:332,
           height:233,
		   top:230,
		   left:483,
           //style: 'color: red; border: solid',
           id: 'hotspot_page1-6-3-b',
           etl_overlay_id:'overlay_1-6-3-b'
        }
       ]
    }
    
   
});
