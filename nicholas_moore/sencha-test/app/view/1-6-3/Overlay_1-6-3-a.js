var me = null;
Ext.define('epiduo_ped.view.1-6-3.Overlay_1-6-3-a.js',
{
    extend:  'Ext.Container',  
    xtype:   'overlay_1-6-3-a',
    
    config:
    {
        //src: 'app/view/1-6-3/images/overlay-a.png',
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
        
    
        items:[
        {  
           xtype:'img',
           src: 'app/view/1-6-3/images/overlay-a@2x.png',
	   	   cls:'page1_6_3-a-BG',
           height: '603px',
           width:  '936px',		   
           top:0,
           right:0,
           id:'id_img_overlay1-6-3'
        },
    
        ]
    }

});


