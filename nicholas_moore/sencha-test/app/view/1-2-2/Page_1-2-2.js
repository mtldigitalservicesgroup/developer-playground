Ext.define('epiduo_ped.view.1-2-2.Page_1-2-2',
{
    extend:  'epiduo_ped.view.AbsPage',
    xtype:   'page1-2-2',
    me_1_2_2: '',
    theTab: '',  
   
    config:
    {
       cls:"page1_2_2BG",
       items:[ 
           
            {
               xtype:'image',
               cls:'page1_2_2-tab1-BG',
               width:766,
               height:405,
               top:171,
               left:106,
               id: 'img_1-2-2_tab'
       
           },
           
           // Tab1
           {  
               xtype:'hotspot',
               width:150,
               height:40,
               top:165,
               left:105,
               id: 'hotspot1-2-2a',
               listeners: {
                   tap: {
                        fn: function(e) {
                             this.parent.fireChangeTabEvent("1");
                        },
                        element: 'element'
                    }
               }
            },
            
            // Tab2
            {  
               xtype:'hotspot',
               width:150,
               height:40,
               top:165,
               left:265,
               id: 'hotspot1-2-2b',
               listeners: {
                   tap: {
                        fn: function(e) {
                            this.parent.fireChangeTabEvent("2");
                        },
                        element: 'element'
                    }
               }
            },
            
            // Tab3
            {  
               xtype:'hotspot',
               width:150,
               height:40,
               top:165,
               left:423,
               id: 'hotspot1-2-2c',
               listeners: {
                   tap: {
                        fn: function(e) {
                            this.parent.fireChangeTabEvent("3");
                        },
                        element: 'element'
                    }
               }
            },
            
            // Tab Hotboxes
            {
               xtype:'hotspot',
               width:232,
               height:266,
               top:253,
               left:124,
               cls: 'tab_hotspot',
               //style: 'color: red; border: solid',
               etl_overlay_id:'overlay_1-2-2a'
            },
            {
               xtype:'hotspot',
               width:232,
               height:266,
               top:253,
               left:372,
               cls: 'tab_hotspot',
               //style: 'color: red; border: solid',
               etl_overlay_id:'overlay_1-2-2a'
            },
            {
               xtype:'hotspot',
               width:232,
               height:266,
               top:253,
               left:623,
               cls: 'tab_hotspot',
               //style: 'color: red; border: solid',
               etl_overlay_id:'overlay_1-2-2a'
            },
            
            
            {
                xtype:'pleasesee'
            
            }
       ]
      
    },
    
    initialize: function(){
    
       //set up the tab
       this.theTab = this.items.get('img_1-2-2_tab');
       
       this.tabClsNameBegin     ="page1_2_2-tab";
       this.tabClsNameEnd       = "-BG";
       this.overlayName         ="overlay_1-2-2";
       
       //preload images:
       var ar_bkgrnd_img = new Array();
       ar_bkgrnd_img[0]='app/view/1-2-2/images/tab-1.png';
       ar_bkgrnd_img[1]='app/view/1-2-2/images/tab-2.png';
       ar_bkgrnd_img[2]='app/view/1-2-2/images/tab-3.png';
       this.firePreloadEvent(ar_bkgrnd_img);
       
    }
    
});
