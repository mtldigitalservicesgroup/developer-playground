var me = null;
Ext.define('epiduo_ped.view.1-2-2.Overlay_1-2-2a.js',
{
    extend: 'Ext.Container',
    xtype:  'overlay_1-2-2a',
    id:     'overlay_1-2-2a',
    
    config: 
    {
        height: '603px',
        width:  '936px',
        top:    '75px',
        left:   '0px',
        
        items: [
           {
               xtype:'image',
               src: 'app/view/1-2-2/images/female_wk12-inactive.png',
               width: 279,
               height: 263,
               top:171,
               left:106
           }  
        ]
    }

});

