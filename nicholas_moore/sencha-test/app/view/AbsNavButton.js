/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


Ext.define('epiduo_ped.view.AbsNavButton',
{
    extend:  'Ext.Button',  
    xtype:   'absAbstractButton',
    fullscreen: true,
    config:
    {
     
    },
    
    initialize:function(){
       this.callParent();      
    },
    
    highlight : function(){ 
       this.setIconCls( this.etl_btn_activeclass )
    },
    
    unHighlight : function(){       
        this.setIconCls( this.etl_iconClass );
    }
   
    
});


