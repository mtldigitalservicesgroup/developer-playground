Ext.define('epiduo_ped.view.NavBar',
{
    extend:  'epiduo_ped.view.AbsSlidingContainer',  
    xtype:   'navBar',
    id: 'thetoolbar',
    
    require:[epiduo_ped.view.AbsNavButton],
    
    slide_panel_type: 2,
        
    openFromleft:   0,
    openToLeft:     -100,
    closeFromleft:  -100,
    closeToLeft:    0,

    config: {
        style: 'width:100px; background-color: #7a5b97;',
        
        items: [
            {
                xtype:'panel',
                layout: 'hbox',

                items:[
                    
                    {
                        xtype:'image',
                        width:'38px',
                        height:'143px',
                        top:'0px',
                        left:'100px',
                        id:'tab_nav',
                    },
                    {
                        xtype:'hotspotSlidingPanel',
                        width:'60px',
                        height:'143px',
                        top:'0px',
                        left:'100px'
                    }
                ]
            }
        ]
    },

    initialize: function(){  //add all the buttons dynamically - based on store config           
        var toolbar = this;
        var count = 0;
        toolbar.on('painted', function() {toolbar.fireEvent('viewready', toolbar);}, null, { single : true });
    
        Ext.getStore('MainStore').load(function(slides){                
            var items = [];
            Ext.each(slides, function(slide){                    
                if(slide.get('sectiontype') == 'slide' || slide.get('sectiontype') == 'carousel') {
                   var button = new Ext.create('epiduo_ped.view.AbsNavButton', {
                        sectionIndex: count,                         
                        iconCls: slide.get('etl_btn_class') ,
                        etl_iconClass: slide.get('etl_btn_class') , 
                        etl_btn_activeclass: slide.get('etl_btn_activeclass'),
                        etl_btn_loadingclass:slide.get('etl_btn_loadingclass'),
                        pressedCls:'pressedBlank',
                        action   : 'navBtn',
                        width:slide.get('etl_btn_width'),
                        ui:'plain'
                     });
                     items.push(button);
                } else if(slide.get('sectiontype') == 'siteMap'  ){                    
                       var button = new Ext.create('epiduo_ped.view.AbsNavButton', {
                        sectionIndex: count,                         
                        iconCls: slide.get('etl_btn_class') ,
                        etl_iconClass: slide.get('etl_btn_class') , 
                        etl_btn_activeclass: slide.get('etl_btn_activeclass'),
                        etl_btn_loadingclass:slide.get('etl_btn_loadingclass'),
                        // adding this object for case switch on CtrlHotspot 
                        etl_tap_from_splash:'splashPage',
                        pressedCls:'pressedBlank',
                        action   : 'siteMapBtn',
                        width:slide.get('etl_btn_width'),
                        ui:'plain'
                     });
                     items.push(button);
                }
                count ++
            });
            
            if(items.length > 0){
                     toolbar.setItems(items);
           }
        });
        this.callParent();
    }

});