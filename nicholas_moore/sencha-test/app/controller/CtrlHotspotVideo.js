
Ext.define('epiduo_ped.controller.CtrlHotspotVideo', {
    extend: 'Ext.app.Controller',
    
    overlay:null,
    
    config: {


        control: {
            
            hotspotVideo: {
               hotspot_video_overlay_tap: 'hotspottap'
            }
        }
    },
    //----------------------------------------------------
        

    //----------------------------------------------------
    hotspottap: function(e) {    
        var playerCmp = null;
        var button1 = null;
        var button2 = null;
        var Cmp = null;
        
        var id_tapped = e.target.id;
       
      // alert(id_tapped);

       switch(id_tapped)
       {
           
           
           case '1_1_1_femaleVidButton':
                this.updateVideo('1_1_1videoPlayer','app/view/1-1-1/video/female_Baseline.mp4')
                this.updateImage('1_1_1OverLayHeader','overlay_1-1-1HeaderFemale');
                this.updateImage('1_1_1femaleButton','overlay_1-1-1ButtonFemaleActive');               
                this.updateImage('1_1_1maleButton','overlay_1-1-1ButtonMaleInactive');
                break;
                
           case '1_1_1_maleVidButton':
                this.updateVideo('1_1_1videoPlayer','app/view/1-1-1/video/male_Baseline.mp4')
                this.updateImage('1_1_1OverLayHeader','overlay_1-1-1HeaderMale');
                this.updateImage('1_1_1femaleButton','overlay_1-1-1ButtonFemaleInactive');               
                this.updateImage('1_1_1maleButton','overlay_1-1-1ButtonMaleActive');
                break;
                
          case '1_1_1_bfemaleVidButton':
                this.updateVideo('1_1_1bvideoPlayer','app/view/1-1-1/video/female_Baseline.mp4')                 
                this.updateImage('1_1_1OverLayHeader','overlay_1-1-1HeaderFemale');
                this.updateImage('1_1_1bfemaleButton','overlay_1-1-1ButtonFemaleActive');               
                this.updateImage('1_1_1bmaleButton','overlay_1-1-1ButtonMaleInactive');
                break;
                
           case '1_1_1_bmaleVidButton':
                this.updateVideo('1_1_1bvideoPlayer','app/view/1-1-1/video/male_Baseline.mp4');                
                this.updateImage('1_1_1OverLayHeader','overlay_1-1-1HeaderMale');
                this.updateImage('1_1_1bfemaleButton','overlay_1-1-1ButtonFemaleInactive');
                this.updateImage('1_1_1bmaleButton','overlay_1-1-1ButtonMaleActive');
                break;    
                
                
                
           case '1_3_2afemaleVidButton':
                this.updateVideo('1_3_2avideoPlayer','app/view/1-3-2/video/female_Tolerability.mp4')
                this.updateImage('1_3_2aOverLayHeader','overlay_1-3-2HeaderFemale');
                this.updateImage('1_3_2afemaleButton','overlay_1-3-2ButtonFemaleActive');               
                this.updateImage('1_3_2amaleButton','overlay_1-3-2ButtonMaleInactive');
                break;
                
           case '1_3_2amaleVidButton':
                this.updateVideo('1_3_2avideoPlayer','app/view/1-3-2/video/male_Tolerability.mp4')
                this.updateImage('1_3_2aOverLayHeader','overlay_1-3-2HeaderMale');
                this.updateImage('1_3_2afemaleButton','overlay_1-3-2ButtonFemaleInactive');               
                this.updateImage('1_3_2amaleButton','overlay_1-3-2ButtonMaleActive');
                break;
                
          case '1_3_2bfemaleVidButton':
                this.updateVideo('1_3_2bvideoPlayer','app/view/1-3-2/video/female_Tolerability.mp4')                 
                this.updateImage('1_3_2bOverLayHeader','overlay_1-3-2HeaderFemale');
                this.updateImage('1_3_2bfemaleButton','overlay_1-3-2ButtonFemaleActive');
                this.updateImage('1_3_2bmaleButton','overlay_1-3-2ButtonMaleInactive');
                break;
                
           case '1_3_2bmaleVidButton':
                this.updateVideo('1_3_2bvideoPlayer','app/view/1-3-2/video/male_Tolerability.mp4');                
                this.updateImage('1_3_2bOverLayHeader','overlay_1-3-2HeaderMale');
                this.updateImage('1_3_2bfemaleButton','overlay_1-3-2ButtonFemaleInactive');
                this.updateImage('1_3_2bmaleButton','overlay_1-3-2ButtonMaleActive');
                break;                    
                


       
           case '1_2_2_afemaleVidButton':
                this.updateVideo('1_2_2avideoPlayer','app/view/1-2-2/video/female_Week4.mp4')
                this.updateImage('1_2_2afemaleButton','overlay_1-2-2ButtonFemaleActive');               
                this.updateImage('1_2_2a12femaleButton','overlay_1-2-2ButtonFemale12WKInactive');
                break;
                
           case '1_2_2_afemale12wVidButton':
                this.updateVideo('1_2_2avideoPlayer','app/view/1-2-2/video/female_Week12.mp4')
                this.updateImage('1_2_2afemaleButton','overlay_1-2-2ButtonFemaleInctive');               
                this.updateImage('1_2_2a12femaleButton','overlay_1-2-2ButtonFemale12WKActive');
                break;
                
                
           case '1_2_2_bfemaleVidButton':
                this.updateVideo('1_2_2bvideoPlayer','app/view/1-2-2/video/female_Week4.mp4')
                this.updateImage('1_2_2bfemaleButton','overlay_1-2-2ButtonFemaleActive');               
                this.updateImage('1_2_2b12femaleButton','overlay_1-2-2ButtonFemale12WKInactive');
                break;
                
           case '1_2_2_bfemale12wVidButton':
                this.updateVideo('1_2_2bvideoPlayer','app/view/1-2-2/video/female_Week12.mp4')
                this.updateImage('1_2_2bfemaleButton','overlay_1-2-2ButtonFemaleInctive');               
                this.updateImage('1_2_2b12femaleButton','overlay_1-2-2ButtonFemale12WKActive');
                break;                

           case '1_2_2_cplay4Wk':               
                this.updateVideo('1_2_2cvideoPlayer','app/view/1-2-2/video/male_Week4.mp4')
                this.updateImage('1_2_2_cmaleVidButton','overlay_1-2-2ButtonMaleActive');               
                this.updateImage('1_2_2_cmale12wVidButton','overlay_1-2-2ButtonMale12WKInactive');
                break;
                
           case '1_2_2_cplay12Wk':               
                this.updateVideo('1_2_2cvideoPlayer','app/view/1-2-2/video/male_Week12.mp4')
                this.updateImage('1_2_2_cmaleVidButton','overlay_1-2-2ButtonMaleInactive');               
                this.updateImage('1_2_2_cmale12wVidButton','overlay_1-2-2ButtonMale12WKActive');
                break;
              
              
              
                
           case '1_2_2_dplay4Wk':
                this.updateVideo('1_2_2dvideoPlayer','app/view/1-2-2/video/male_Week4.mp4')
                this.updateImage('1_2_2_dmaleVidButton','overlay_1-2-2ButtonMaleActive');               
                this.updateImage('1_2_2_dmale12wVidButton','overlay_1-2-2ButtonMale12WKInactive');
                break;
                
           case '1_2_2_dplay12Wk':
                this.updateVideo('1_2_2dvideoPlayer','app/view/1-2-2/video/male_Week12.mp4')
                this.updateImage('1_2_2_dmaleVidButton','overlay_1-2-2ButtonMaleInactive');               
                this.updateImage('1_2_2_dmale12wVidButton','overlay_1-2-2ButtonMale12WKActive');
                break;                
                            
       }
        
       
       
        
    },
    
    updateVideo: function (videoPlayer,videoSrc){
            var playerCmp = null;
            playerCmp = Ext.getCmp(videoPlayer)
            playerCmp.pause();
            playerCmp.setUrl(videoSrc);
            //playerCmp.play();
            return playerCmp;
            
    },
    
    updateImage: function (img,newCls){
            var myImage = null;
            myImage = Ext.getCmp(img)            
            myImage.setCls(newCls);
    }
    
});


