/*
*  CtrlHotspot.js
*  
*  This  controller
*  handles hotspot taps on the sliding panels (isi,pi,ref)
*
*/
Ext.define('epiduo_ped.controller.CtrlSlidingPanel', {
    extend: 'Ext.app.Controller',
    
    config: {
        
        refs: {
            //Hotspot_navblocker:     '#hotspot_navblocker',
            Hotspot_home:           '#hotspot_home',
            Overlay_bgFill:         '#overlay_bgFill',
            The_overlay:            '#the_overlay',
            piPanel:                'piPanel'
        },

        control: {
            
            hotspotSlidingPanel: {
                hotspot_sliding_panel_tap: 'onHotspotTap'
            },          
            pleasesee :{
                onShowPITab: 'onShowPITab'
            }
            
        },

        routes: {
            /* not used currently
            'show/:id' : 'showItem'*/
        }
    },
    //----------------------------------------------------
    onHotspotTap: function(hotspot,piInstance) {
       if(this.getThe_overlay() != undefined && !this.getThe_overlay().isHidden())
          return;
       
       
       
       var bOpenIt = true;
       
       //first, lets see if this is a close request or an open request:
       if(hotspot.slide_back)
            bOpenIt = false;
        
       var theContainer= null; 
       if(piInstance != undefined && piInstance == true) 
           //this means we got here from pleaseSee implementation (or any class that implements a Sliding based class)
           theContainer = hotspot;
       else
            //go up 2 levels to get the AbsSlidingContainer class
            theContainer = hotspot.up('container').up('container');
      
      
       var fromleft =  0;
       var toLeft   =  0;
       
       //if its already opened then close it:
       if(bOpenIt && theContainer.bOpened == true)
           bOpenIt = false;    
       
       if(bOpenIt){
           //if(hotspot.parent.parent.slide_panel_type != 2)
           
           //fire this event so all OTHER sliding panels will hide themselves!
           this.getApplication().fireEvent('slidingPanelOpened',theContainer.slide_panel_type);    
           //this.getHotspot_navblocker().setHidden(false);
           this.getHotspot_home().setHidden(true);
           
           if(hotspot.parent.parent.slide_panel_type == 2){
               this.getOverlay_bgFill().setHidden(true);
           }
           else {
               this.getOverlay_bgFill().setHidden(false);
           }
           
           fromleft =  theContainer.openFromleft;
           toLeft   =  theContainer.openToLeft; 
           
           this.slideIt(true,theContainer,fromleft,toLeft)
       }
       else{ //close it
           
           //this.getHotspot_navblocker().setHidden(true);
           this.getHotspot_home().setHidden(false);
           this.getOverlay_bgFill().setHidden(true);
           
           fromleft =  theContainer.closeFromleft;
           toLeft   =  theContainer.closeToLeft; 
           
           this.slideIt(false,theContainer,fromleft,toLeft)
       }
       
    },
     //----------------------------------------------------
    onShowPITab: function(){
       this.onHotspotTap(this.getPiPanel(),true);
    },
    //------------------------------------------------------
    slideIt:function(bOpen,cmp,fromleft,toLeft,bcurPanelSet){
       
            
        var parentObj = this;
        Ext.Animator.run({
            element: cmp.element,
            duration: 500,
            easing: 'ease-in-out',
            preserveEndState: true,
            from:{left:fromleft},
            to:{left:toLeft},
            onEnd:function(){
              
              
              if(!bOpen){
                  //fire event so all OTHER sliding panels will show themselves again:
                  parentObj.getApplication().fireEvent('slidingPanelClosed',cmp.slide_panel_type);
              }
            }
         });
     }
   
});


