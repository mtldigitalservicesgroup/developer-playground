/*
*  CtrlPage.js
*  
*  Page Controller
*  Handles the way the page looks  (tabs or no tabs, background fill color etc...)
*
*
*
*/
Ext.define('epiduo_ped.controller.CtrlPage', {
    extend: 'Ext.app.Controller',
    xtype:  'ctrlpage',
    //me: null,
    
    config: {
        
        refs: {
            //Hotspot_navblocker:     '#hotspot_navblocker',
            Overlay_bgFill:         '#overlay_bgFill',
            Hotspot_home:           '#hotspot_home',
            Slide_panel_pi:         '#slide_panel_pi',
            //Slide_panel_isi:        '#slide_panel_isi',
            Slide_panel_ref:        '#slide_panel_ref',
            Hotspot_isi:            '#hotspot_isi',
            Hotspot_pi:             '#hotspot_pi',
            Hotspot_ref:            '#hotspot_ref',
            Ref_content:            '#id_ref_content'
        },
            
        control: {
               
               absPage:{
                    
                    carouselItemActivate:  'onCarouselItemActivate',       
                    carouselItemDeActivate:'onCarouselItemDeActivate'
                    
                }
        }
    },
    onCarouselItemActivate : function(item) {
        if(item != undefined){
            //console.log("on page Activate, id:" + item.navBtnId);
        
            this.setTabsVisibility(item.slideTabsVisible);
            this.setHomeLinkVisibility(item.slideHomeLink);
            this.changeBackGrndFill(item.slideOverlayBGColor);
            this.setRefImg(item.navBtnId);
        }
        
    },
    onCarouselItemDeActivate : function(item) {
         if(item != undefined){
            //console.log("on page De Activate, id:" + item.navBtnId);
            //nothing right now
         }
     },
    initialize: function(){
        
        //this.me = this;
        this.callParent();
    },
    //---------------------------------------------------------------
    setTabsVisibility:function(visible){
        
         //make sure panels are available first!
         var panels_hotspots_available = this.getSlide_panel_pi() != undefined &&
             //this.getSlide_panel_isi() != undefined &&
             this.getSlide_panel_ref() != undefined &&
             //this.getHotspot_isi() != undefined &&
             this.getHotspot_pi() != undefined &&
             this.getHotspot_ref() != undefined;
         
       
         if(panels_hotspots_available && visible){
             //alert('etl_tabsvisible');
            this.getSlide_panel_pi().show();
            //this.getSlide_panel_isi().show();
            this.getSlide_panel_ref().show();
            //this.getHotspot_isi().show();
            this.getHotspot_pi().show();
            this.getHotspot_ref().show();
         }
         else if (panels_hotspots_available && !visible ){
             //alert('not etl_tabsvisible');
            this.getSlide_panel_pi().hide();
            //this.getSlide_panel_isi().hide();
            this.getSlide_panel_ref().hide();
            //this.getHotspot_isi().hide();
            this.getHotspot_pi().hide();
            this.getHotspot_ref().hide();
         }
                    
    },
    //--------------------------------------------------------------
    setHomeLinkVisibility:function(hide){

        var hostpot_home_available = this.getHotspot_home() != undefined;
       
        
        if(hostpot_home_available && hide) 
             this.getHotspot_home().hide();
        else if (hostpot_home_available && !hide)
             this.getHotspot_home().show();
    },
    //--------------------------------------------------------------
    changeBackGrndFill:function(color){
        
        //alert(color);
        
        if( this.getOverlay_bgFill() != undefined)
            this.getOverlay_bgFill().setStyle("background-color:"+ color + ";" );
    },
    //--------------------------------------------------------------
    setRefImg:function(theid){
       
       if(this.getRef_content() != undefined){
           var sPath = 'app/view/' + theid + '/images/ref.png';
           this.getRef_content().setSrc(sPath);
       }
     }
    
});

