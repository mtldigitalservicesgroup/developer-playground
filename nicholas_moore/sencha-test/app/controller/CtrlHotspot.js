/*
*  CtrlHotspot.js
*  
*  This  controller
*  handles hotspot taps inside the html (overlays, home,splash)
*
*/
Ext.define('epiduo_ped.controller.CtrlHotspot', {
    extend: 'Ext.app.Controller',
    
    my_overlay: undefined,
    m_carousel:[],

    config: {
        
        refs: { 
         //   MainCarousel:'maincarousel',
            Overlay_bgFill: '#overlay_bgFill',
            Hotspot_home: '#hotspot_home',
            Sitemap: '#sitemap',
            siteMapBtn: 'absAbstractButton[action=siteMapBtn]',
            tabHotspot: 'hotspot[cls=tab_hotspot]'
        },
        control: {
             maincarousel: {
                     viewready : 'onCarouselReady'
                },
            hotspot: {
                hotspottap: 'onHotspotTap',
                EvtCloseOverlay: 'onCloseOverlay'    
            }, 
            siteMapBtn: {
                tap: 'onSiteMapButtonTap'
            },
            tabHotspot: {
                tap: 'onTabHotspotTap'
            }
        }
    },
    init: function() {
       // console.log("running init"); 
    },
    //------------------------------------------------
    onCarouselReady:function(carousel){
        this.m_carousel = carousel            
    },
    //----------------------------------------------------
    onHotspotTap: function(e) {
         this.showOverlay(e.etl_overlay_id,e.etl_setImgbyID);     
    },
    onTabHotspotTap: function(e) {
        var hotspotTab = e.parent.theTab.getCls();
        
        var tabBegin = e.parent.tabClsNameBegin;
        var tabEnd = e.parent.tabClsNameEnd;
        var overlay = e.parent.overlayName;
        
        if(hotspotTab == tabBegin + '1' + tabEnd){
            e.etl_overlay_id = overlay + 'a';
        }
        else if(hotspotTab == tabBegin + '2' + tabEnd){
            e.etl_overlay_id = overlay + 'b';
        }
        else if(hotspotTab == tabBegin + '3' + tabEnd){
            e.etl_overlay_id = overlay + 'c';
        }
    },
     //----------------------------------------------------
    onSiteMapButtonTap: function(navbtn){
        if(navbtn.etl_tap_from_splash){
        this.showOverlay('overlay_sitemap',-1);
        }

    },
    //-----------------------------------------------
    showOverlay:function (xtype1,etl_setImgbyID){
        
        if(this.my_overlay == undefined){
	   this.my_overlay = Ext.create('epiduo_ped.view.Overlay',xtype1,etl_setImgbyID);
           this.my_overlay.addToContainer();
	}
        else
            this.my_overlay.setXtype(xtype1,etl_setImgbyID);
        
        this.getOverlay_bgFill().show();
        this.getHotspot_home().hide();
        this.my_overlay.popIt();
     },
     //-----------------------------------------------
     onCloseOverlay:function(){
       //show home screen 
       this.getHotspot_home().show();

       //hid the iverlay back fill
       this.getOverlay_bgFill().hide();

       //reset sitemap icon:
       var sitemap = this.getSitemap();
       if(sitemap != undefined)
           sitemap.setIconCls('etl_nav_btn etl_nav_btn_sitemap');
         
       //finally close the overlay 
        this.my_overlay.closeIt();
     },
     navigateToPage:function(section,page){        
        this.m_carousel.setActiveItem(Number(section));
 
        var innerItems = this.m_carousel.getInnerItems();
        if(innerItems[section] instanceof Ext.carousel.Carousel){
           innerItems[section].setActiveItem(Number(page));
        }
     }
     //------------------------------------------------
     /*Not used currently 
     getValuefromHTML: function(fullstring, sKey) {
        
       //parse out and get the carousel_item_xtype
       //example: <div id="page1-0_A" etl_carousel_item_xtype="page1-0" class="hotspot"></div>
       
       var idx1 = fullstring.indexOf(sKey+"=");
       var idx2 = idx1 + sKey.length +2; //+2 to include ' =" '
       var idx3 = fullstring.indexOf("\"",idx2);
       
       return (fullstring.substring(idx2, idx3));
        
     },*/
});


