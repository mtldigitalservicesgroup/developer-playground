/*
*  CtrlContent.js
*  
*  Page Controller
*  Handles universal page AND overlay tasks such as:
*  preloading,changing tabs & iterating through views etc...
*
*
*
*/
Ext.define('epiduo_ped.controller.CtrlContent', {
    extend: 'Ext.app.Controller',
    xtype:  'ctrlcontent',
    timeOutObj:null,
    
    config: {
        
        refs: {
           
        },
            
        control: {
               
                absContent:{
                   EvtChangeTab:'onChangeTab',
                   EvtPreload:'onPreload',
                   EvtPaginate:'onPaginate',
                   EvtSetImageById:'onSetImageById',
                   EvtPaginateTimer:'onPaginateTimer',
                   EvtSlider:'onSlider'
                }
        }
    },
    initialize: function(){
        
        //this.me = this;
        this.callParent();
    },
    onChangeTab:function(e){
       var theCls = e.tabClsNameBegin + e.tabNumber + e.tabClsNameEnd;
       e.theTab.setCls(theCls);
    },
    onPreload:function(imgArray){
       
       
       this.imageArray = imgArray; 
       this.me_absContent = this;
       
       /*pre load all the background images. so there is no flicker*/
       for(var idx=0 ; idx < imgArray.length ; ++ idx){
           this.me_absContent.image = new Image();
           this.me_absContent.image.src = imgArray[idx];
           //alert('image loaded! '+  ar_bkgrnd_img[idx])
           this.me_absContent.image.onload = function() {	
           //alert('loaded!')
            //nothing right now...
          }
        }
     },
     onPaginate:function(e,direction){
         
        var the_src = "";
        
        if(direction == "fwd"){
            
            if(e.paginateCurIdx == e.paginateImageArray.length -1){
               the_src =  e.paginateImageArray[0]
               e.paginateCurIdx = 0
            }
            else{
               ++e.paginateCurIdx;
               the_src =  e.paginateImageArray[e.paginateCurIdx]; 
            }
              
        }
        else{
            
            if(e.paginateCurIdx == 0){
               the_src =  e.paginateImageArray[e.paginateImageArray.length-1];
               e.paginateCurIdx = e.paginateImageArray.length-1;
            }
            else{
               --e.paginateCurIdx;
               the_src =  e.paginateImageArray[e.paginateCurIdx]; 
            }
            
        }
        
        e.paginateTarget.setCls(the_src);
   
     },
     onSetImageById:function(e,num){
         
        //alert('onSetImageById ' + num);
        
        var the_src = "";
        
        e.paginateCurIdx = num;
        the_src =  e.paginateImageArray[num]; 

        e.paginateTarget.setCls(the_src);

        //e.paginateTarget.show();
     },
     onPaginateTimer:function(e){
         
           /*
             paginateStartDelay:0,
             paginateTimeBetween:0,
           */
         
          var this_instance = this;
          var timeout = 0;
          
          if(e.paginateCurIdx == e.paginateImageArray.length-1)
              clearTimeout(this.timeOutObj)
          else{
         
              if(e.paginateCurIdx ==0)
                  timeout = e.paginateStartDelay;
              else
                  timeout = e.paginateTimeBetween;
           
              
              this.timeOutObj = setTimeout(function(){
                
                //on timeout, first set the next image
                this_instance.onPaginate(e,'fwd')
                
                //then call this function again to set the timeout again:
                this_instance.onPaginateTimer(e);

             },timeout,this_instance,e);
          }
         
     },
     onSlider:function(e,slider,pos){
       
        var curIncr = 0
        var pointHalfway = 0;
        var imgIndex = 0;
        var diff = 0;
        
        //console.log('Slider position: ' + pos);
        
        
        //indexOf() was not working so this is workaround...hmmm
        var idx = 0;
        for(var i = 0 ; i < e.paginateSliderIncrements.length ; ++i)
            if(e.paginateSliderIncrements[i] == pos  )
                idx = i;
        
        //console.log('Slider index for ImageArray: ' + idx);
        //console.log('Slider paginateSliderIncrements length  ' + e.paginateSliderIncrements.length);
        
        var the_src =  e.paginateImageArray[idx];
        
        e.paginateTarget.setCls(the_src);
        
        
        /* This is not needed as Sencha's slider
          *has an increment attribute that snaps it to the closest
          *increment as the user slides it 
          *keep this in case we want a continuous (smoother) slider...
        for(var i= 0 ; i < e.paginateSliderIncrements.length;++i ){
            
            curIncr = e.paginateSliderIncrements[i];
            
            //find half way point between this increment and the next increment &
            //is the slider's position within the curIncr?
            if(  curIncr == 0 && e.paginateSliderIncrements[i+1] != undefined &&
                 pos > 0 && pos < e.paginateSliderIncrements[i+1]
              ) { //first 
                
                pointHalfway = e.paginateSliderIncrements[i+1] / 2;
                
                if(pos > pointHalfway )
                    slider.
                    
            }
            else if(  i == e.paginateSliderIncrements.length -1 && e.paginateSliderIncrements[i-1] != undefined &&
                      pos > e.paginateSliderIncrements[i-1] && pos < e.paginateSliderIncrements[i]
                   ){ //last
                
                diff = e.paginateSliderIncrements[i] -  e.paginateSliderIncrements[i -1];
                pointHalfway = diff / 2;
            
            }// all others in between
            else if ( e.paginateSliderIncrements[i+1] != undefined &&
                      pos < e.paginateSliderIncrements[i] > pos < e.paginateSliderIncrements[i-1]
                    ) {
                
                diff = e.paginateSliderIncrements[i+1] - e.paginateSliderIncrements[i];
                pointHalfway = diff / 2; 
            }
            
        } 
      */  
     }
    
    
});

