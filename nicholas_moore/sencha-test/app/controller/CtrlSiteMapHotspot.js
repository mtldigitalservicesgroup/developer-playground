/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
Ext.define('epiduo_ped.controller.CtrlSiteMapHotspot', {
    extend: 'Ext.app.Controller',
    sitemap:[],
    m_carousel:[],
    config: {
        
        refs: { 
            MainCarousel:'maincarousel',
            Overlay_bgFill: '#overlay_bgFill',
            Hotspot_home: '#hotspot_home',
            Sitemap: 'overlay_sitemap',
            siteMapBtn: 'absAbstractButton[action=siteMapBtn]',
            Ref_content: '#id_ref_content'
        },
        control: {
             maincarousel: {
                     viewready : 'onCarouselReady'
                }, 
            hotspot_sitemap: {
                siteMapTap: 'siteMapButtonTap'
            }
        }
    },
    // -------------------------------------------
    
    siteMapButtonTap: function(e){
        var navCall = e.target.id.split("_");
        var sectionNum = Number(navCall[1]);
        var pageNum = Number(navCall[2]);
        var myItem = this.m_carousel.getInnerItems( )[sectionNum];
         
        this.onCloseOverlay();
        this.m_carousel.setActiveItem(sectionNum);
        this.m_carousel.getInnerItems( )[sectionNum].setActiveItem(pageNum);
        
        //highlight nav button
        this.m_carousel.fireEvent('mainsectionChange', sectionNum, 0);
        //set ref image
        this.setRefImg(navCall[0]);          
    },

    //------------------------------------------------
    onCarouselReady:function(carousel){
        this.m_carousel = carousel            
    },
    
     //-----------------------------------------------
     onCloseOverlay:function(){
         
       //show home screen 
       this.getHotspot_home().show();

       //hid the iverlay back fill
       this.getOverlay_bgFill().hide();
       
       sitemap = this.getSitemap();
       if(sitemap != undefined){           
           sitemap.closeIt();          
        }
     },
     
     setRefImg:function(theid){
       
       if(this.getRef_content() != undefined){
           var sPath = 'app/view/' + theid + '/images/ref.png';
           this.getRef_content().setSrc(sPath);
       }
     }
    
    
});