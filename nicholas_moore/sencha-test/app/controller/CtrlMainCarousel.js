Ext.define('epiduo_ped.controller.CtrlMainCarousel', {
    extend: 'Ext.app.Controller',
    m_carousel: '',
    subCarousels:[],
    config: {
            
            control: {
                         
                maincarousel: {
                     viewready : 'onCarouselReady',
                     activeitemchange:'onactiveitemchange'
                }            
            }
    },  
    
    //------------------------------------------------
    
    onCarouselReady:function(carousel){
            this.m_carousel = carousel;
            this.subCarousels = carousel.getSubCarousels();
    },
    
    onactiveitemchange: function(par, currentActiveItem, oldActiveItem, eOpts){    
        if (currentActiveItem instanceof Ext.carousel.Carousel){            
            //if a vertical carousel then we need to get the first item and
            //fire an event on that guy:
            var items = currentActiveItem.getInnerItems(currentActiveItem);
            currentActiveItem = items[0];
            
        }     
        
        this.resetVerticalCarousels();
        currentActiveItem.fireShowEvent(currentActiveItem);
    },
    
    
    resetVerticalCarousels: function(){
        var myItems = this.subCarousels;
        for(var i=0;i< this.subCarousels.length;i++){ 
           myItems[i].setActiveItem(0);
        }
        
    }
    
    

});
