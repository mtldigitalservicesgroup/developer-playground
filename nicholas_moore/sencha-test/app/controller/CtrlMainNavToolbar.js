/*
*  Controller.js
*
*
*/
Ext.define('epiduo_ped.controller.CtrlMainNavToolbar', {
    extend: 'Ext.app.Controller',
    m_carousel: '',
    m_toolbar:'',
    previousButton:[],
    currentButton:[],        
    my_overlay: undefined,
    
    config: {
            refs: {
                Overlay_bgFill: '#overlay_bgFill',
                Hotspot_home: '#hotspot_home',
                navBtn: 'absAbstractButton[action=navBtn]'
              
            },
            control: {
                navBtn: {
                    tap: 'onNavButtonTap'
                }, 
                maincarousel: {
                     viewready : 'onCarouselReady',
                     mainsectionChange :'onMainsectionChange'
                },   
                navBar:{
                     viewready : 'onToolBarReady'
                },
                absPage:{
                   carouselItemActivate:  'onCarouselItemActivated'       
                }
            }
    },  
    
    //-----------------------------------------------
    onMainsectionChange: function(activeIndex, animationDirection ){
        this.findButtonBySection(this.m_carousel.getActiveIndex() - animationDirection);
        //alert(activeIndex + ', ' + animationDirection);
    },
    
    //------------------------------------------------
    onToolBarReady : function(toolbar){
        this.m_toolbar = toolbar;
        this.setHighlight(this.m_toolbar.getInnerItems( )[1]);
    },
    
    //------------------------------------------------
    onCarouselReady:function(carousel){
        this.m_carousel = carousel            
    },
    
    //------------------------------------------------
    onNavButtonTap:function(navbtn){        
        this.m_carousel.setActiveItem(Number(navbtn.sectionIndex));
         this.setHighlight(navbtn)
    },
    
    //------------------------------------------------    
    onCarouselItemActivated: function(item){
        if(item != undefined){
            this.findButtonBySection(item.slideCarouselIndex);
        }
    },
     
    //------------------------------------------------
    findButtonBySection: function(activeID){
          for(var i=0;i < this.m_toolbar.getInnerItems( ).length; i++){
            if(activeID == this.m_toolbar.getInnerItems( )[i].sectionIndex){
                this.setHighlight(this.m_toolbar.getInnerItems( )[i]);
                break;
            }                            
          }              
    },
   
    //--
    //------------------------------------------------
    setHighlight: function (navbtn){      
        this.currentButton = navbtn;    
        this.currentButton.highlight();          
        
        if(this.previousButton instanceof epiduo_ped.view.AbsNavButton && this.previousButton != this.currentButton){
            this.previousButton.unHighlight();            
        }
        this.previousButton = this.currentButton; 
    }
    
});